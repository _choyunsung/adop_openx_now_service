<?php
// banner table columns
$this->oDbh->exec("ALTER TABLE `".$aConfig['table']['prefix']."banners` ADD COLUMN `com_no` INT(11) NULL COMMENT '매체사 코드' " );
$this->oDbh->exec("ALTER TABLE `".$aConfig['table']['prefix']."banners` ADD COLUMN `net_com_no` INT(11) NULL COMMENT '네트워크 코드' " );
$this->oDbh->exec("ALTER TABLE `".$aConfig['table']['prefix']."banners` ADD COLUMN `ad_unit_nm` VARCHAR(100) NULL COMMENT '광고 코드' " );
$this->oDbh->exec("ALTER TABLE `".$aConfig['table']['prefix']."banners` ADD COLUMN `mobile_android` VARCHAR(100) NULL COMMENT 'iOS 광고코드' " );
$this->oDbh->exec("ALTER TABLE `".$aConfig['table']['prefix']."banners` ADD COLUMN `mobile_ios` VARCHAR(100) NULL COMMENT 'Android 광고코드' " );
$this->oDbh->exec("ALTER TABLE `".$aConfig['table']['prefix']."banners` ADD COLUMN `dimension` VARCHAR(100) NULL COMMENT '배너타입' " );
$this->oDbh->exec("ALTER TABLE `".$aConfig['table']['prefix']."users` ADD COLUMN `publishers` INT(11) NULL COMMENT '네트워크 코드' " );
$this->oDbh->exec("ALTER TABLE `".$aConfig['table']['prefix']."agency` ADD COLUMN `publishers` INT(11) NULL COMMENT '네트워크 코드' " );