<?php

/*
+---------------------------------------------------------------------------+
| Revive Adserver                                                           |
| http://www.revive-adserver.com                                            |
|                                                                           |
| Copyright: See the COPYRIGHT.txt file.                                    |
| License: GPLv2 or later, see the LICENSE.txt file.                        |
+---------------------------------------------------------------------------+
*/

// Set text direction and characterset
$GLOBALS['phpAds_TextDirection']  		= "ltr";
$GLOBALS['phpAds_TextAlignRight'] 		= "right";
$GLOBALS['phpAds_TextAlignLeft']  		= "left";
//$GLOBALS['phpAds_CharSet'] 				= "EUC-KR";

$GLOBALS['phpAds_DecimalPoint']			= ',';
$GLOBALS['phpAds_ThousandsSeperator']	= '.';


// Date & time configuration
$GLOBALS['date_format']				= "%Y-%m-%d";
$GLOBALS['time_format']				= "%H:%M:%S";
$GLOBALS['minute_format']			= "%H:%M";
$GLOBALS['month_format']			= "%Y-%m";
$GLOBALS['day_format']				= "%m-%d";
$GLOBALS['week_format']				= "%Y-%W";
$GLOBALS['weekiso_format']			= "%G-%V";



/*********************************************************/
/* Translations                                          */
/*********************************************************/

$GLOBALS['strHome'] 				= "홈";
$GLOBALS['strHelp']					= "도움말";
$GLOBALS['strNavigation'] 			= "Navigation";
$GLOBALS['strShortcuts'] 			= "바로가기";
$GLOBALS['strAdminstration'] 		= "광고관리";
$GLOBALS['strMaintenance']			= "유지보수";
$GLOBALS['strActions']                  = "실행";
$GLOBALS['strProbability']			= "노출확률";
$GLOBALS['strInvocationcode']			= "광고코드";
$GLOBALS['strBasicInformation'] 		= "기본 정보";
$GLOBALS['strContractInformation'] 		= "계약 정보";
$GLOBALS['strLoginInformation'] 		= "로그인 정보";
$GLOBALS['strOverview']				= "목록보기";
$GLOBALS['strSearch']				= "검색";
$GLOBALS['strHistory']				= "기록";
$GLOBALS['strPreferences'] 			= "설정";
$GLOBALS['strDetails']				= "자세히";
$GLOBALS['strCompact']				= "간단히";
$GLOBALS['strVerbose']				= "세부내용";
$GLOBALS['strUser']				= "사용법";
$GLOBALS['strEdit']				= "편집";
$GLOBALS['strCreate']				= "작성";
$GLOBALS['strDuplicate']			= "복제";
$GLOBALS['strMoveTo']				= "이동하기";
$GLOBALS['strDelete'] 				= "삭제";
$GLOBALS['strActivate']				= "활성화";
$GLOBALS['strDeActivate'] 			= "배너중지";
$GLOBALS['strConvert']				= "변환";
$GLOBALS['strRefresh']				= "새로고침";
$GLOBALS['strSaveChanges']		 	= "변경사항 저장";
$GLOBALS['strUp'] 				= "위로";
$GLOBALS['strDown'] 				= "아래로";
$GLOBALS['strSave'] 				= "저장";
$GLOBALS['strCancel']				= "취소";
$GLOBALS['strPrevious'] 			= "이전";
$GLOBALS['strPrevious_Key'] 			= "이전(<u>p</u>)";
$GLOBALS['strNext'] 				= "다음";
$GLOBALS['strNext_Key'] 				= "다음(<u>n</u>)";
$GLOBALS['strYes']				= "예";
$GLOBALS['strNo']				= "아니오";
$GLOBALS['strNone'] 				= "없음";
$GLOBALS['strCustom']				= "사용자 지정";
$GLOBALS['strDefault'] 				= "기본설정";
$GLOBALS['strOther']				= "기타";
$GLOBALS['strUnknown']				= "알려지지 않음";
$GLOBALS['strUnlimited'] 			= "제한없음";
$GLOBALS['strUntitled']				= "제목없음";
$GLOBALS['strAll'] 				= "전체";
$GLOBALS['strAvg'] 				= "평균";
$GLOBALS['strAverage']				= "평균";
$GLOBALS['strOverall'] 				= "전체";
$GLOBALS['strTotal'] 				= "합계";
$GLOBALS['strActive'] 				= "사용 가능";
$GLOBALS['strFrom']				= "From";
$GLOBALS['strTo']				= "to";
$GLOBALS['strLinkedTo'] 			= "linked to";
$GLOBALS['strDaysLeft'] 			= "남은 기간";
$GLOBALS['strCheckAllNone']			= "모두 선택/ 해제";
$GLOBALS['strKiloByte']				= "KB";
$GLOBALS['strExpandAll']			= "전체 보기(<u>E</u>)";
$GLOBALS['strCollapseAll']			= "전체 숨기기(<u>C</u>)";
$GLOBALS['strShowAll']				= "전체 보기";
$GLOBALS['strNoAdminInteface']			= "서비스를 이용할 수 없습니다.";
$GLOBALS['strFilterBySource']			= "소스 필터링";
$GLOBALS['strFieldContainsErrors']		= "다음 필드에 오류가 있습니다.:";
$GLOBALS['strFieldFixBeforeContinue1']		= "오류를 수정한 후에";
$GLOBALS['strFieldFixBeforeContinue2']		= "다시 시작해야 합니다..";
$GLOBALS['strDelimiter']			= "구분기호";
$GLOBALS['strMiscellaneous']		= "기타";
$GLOBALS['strComments']                 = "설명";


// Properties
$GLOBALS['strName']				= "이름";
$GLOBALS['strSize']				= "크기";
$GLOBALS['strWidth'] 				= "너비";
$GLOBALS['strHeight'] 				= "높이";
$GLOBALS['strURL2']				= "URL";
$GLOBALS['strTarget']				= "대상 프레임";
$GLOBALS['strLanguage'] 			= "언어";
$GLOBALS['strDescription'] 			= "설명";
$GLOBALS['strID']				= "ID";


// Login & Permissions
$GLOBALS['strAuthentification'] 		= "인증";
$GLOBALS['strWelcomeTo']			= "환영합니다. ";
$GLOBALS['strEnterUsername']			= "로그인하기 위해 사용자ID과 비밀번호를 입력하세요.";
$GLOBALS['strEnterBoth']			= "사용자ID와 비밀번호를 전체 입력하세요.";
$GLOBALS['strEnableCookies']			= MAX_PRODUCT_NAME."을 사용하려면 쿠키를 활성화해야합니다.";
$GLOBALS['strLogin'] 				= "로그인ID";
$GLOBALS['strLogout'] 				= "로그아웃";
$GLOBALS['strUsername'] 			= "사용자ID";
$GLOBALS['strPassword']				= "비밀번호";
$GLOBALS['strAccessDenied']			= "액세스할 수 없습니다.";
$GLOBALS['strPasswordWrong']			= "올바른 비밀번호가 아닙니다.";
$GLOBALS['strNotAdmin']				= "권한이 없습니다.";
$GLOBALS['strDuplicateClientName']		= "입력한 ID가 미 있습니다. 다른 ID를 입력하세요.";


// General advertising
$GLOBALS['strViews'] 				= "AdViews";
$GLOBALS['strClicks']				= "AdClicks";

$GLOBALS['strRequests']                 = "요청수";
$GLOBALS['strImpressions']              = "노출수";
$GLOBALS['strClicks']                   = "클릭수";
$GLOBALS['strConversions']              = "전환";
$GLOBALS['strCTRShort']                 = "CTR";
$GLOBALS['strCTRShortHigh']             = "최고 CTR";
$GLOBALS['strCTRShortLow']              = "최저 CTR";
$GLOBALS['strCNVRShort']                = "SR";
$GLOBALS['strCTR']                      = "클릭율";
$GLOBALS['strCNVR']                     = "판매율";
$GLOBALS['strCPC']                      = "클릭당 비용";
$GLOBALS['strCPCo']                     = "전환율";
$GLOBALS['strCPCoShort']                = "CPCo";
$GLOBALS['strCPCShort']                 = "CPC";
$GLOBALS['strTotalViews']               = "전체 노출수";
$GLOBALS['strTotalClicks']              = "전체 클릭수";
$GLOBALS['strTotalConversions']         = "전체 전환수";
$GLOBALS['strViewCredits']              = "노출 보장";
$GLOBALS['strClickCredits']             = "클릭 보장";
$GLOBALS['strConversionCredits']        = "전환 보장";
$GLOBALS['strImportStats']              = "Import 통계";
$GLOBALS['strDateTime']                 = "시간";
$GLOBALS['strTrackerID']                = "추적기 ID";
$GLOBALS['strTrackerName']              = "추적기명";
$GLOBALS['strTrackerImageTag']          = "Image Tag";
$GLOBALS['strTrackerJsTag']             = "Javascript Tag";
$GLOBALS['strTrackerAlwaysAppend']      = "전환이  추적기에 기록되지 않더라도 항상 추가 코드를 표시하시겠습니까?";
$GLOBALS['strBanners']                  = "배너";
$GLOBALS['strCampaigns']                = "캠페인";
$GLOBALS['strCampaignID']               = "캠페인 ID";
$GLOBALS['strCampaignName']             = "캠페인 이름";
$GLOBALS['strCountry']                  = "국가";
$GLOBALS['strStatsAction']              = "실행 ";
$GLOBALS['strWindowDelay']              = "Window delay";
$GLOBALS['strStatsVariables']           = "변수";


// Time and date related
$GLOBALS['strDate'] 				= "날짜";
$GLOBALS['strToday'] 				= "오늘";
$GLOBALS['strDay']				= "일";
$GLOBALS['strDays']				= "일";
$GLOBALS['strLast7Days']			= "최근 7일";
//ted추가
$GLOBALS['strLast30Days']			= "최근 30일";

$GLOBALS['strWeek'] 				= "주";
$GLOBALS['strWeeks']				= "주";
$GLOBALS['strMonths']				= "월";
$GLOBALS['strThisMonth'] 			= "월";
$GLOBALS['strMonth'][0] = "1월";
$GLOBALS['strMonth'][1] = "2월";
$GLOBALS['strMonth'][2] = "3월";
$GLOBALS['strMonth'][3] = "4월";
$GLOBALS['strMonth'][4] = "5월";
$GLOBALS['strMonth'][5] = "6월";
$GLOBALS['strMonth'][6] = "7월";
$GLOBALS['strMonth'][7] = "8월";
$GLOBALS['strMonth'][8] = "9월";
$GLOBALS['strMonth'][9] = "10월";
$GLOBALS['strMonth'][10] = "11월";
$GLOBALS['strMonth'][11] = "12월";

$GLOBALS['strDayShortCuts'][0] = "일";
$GLOBALS['strDayShortCuts'][1] = "월";
$GLOBALS['strDayShortCuts'][2] = "화";
$GLOBALS['strDayShortCuts'][3] = "수";
$GLOBALS['strDayShortCuts'][4] = "목";
$GLOBALS['strDayShortCuts'][5] = "금";
$GLOBALS['strDayShortCuts'][6] = "토";

$GLOBALS['strHour']					= "시";
$GLOBALS['strSeconds']				= "초";
$GLOBALS['strMinutes']				= "분";
$GLOBALS['strHours']				= "시";
$GLOBALS['strTimes']				= "회";

$GLOBALS['strCollectedToday']           = "오늘";
$GLOBALS['strCollectedYesterday']       = "어제";
$GLOBALS['strCollectedThisWeek']        = "이번";
$GLOBALS['strCollectedLastWeek']        = "지난주";
$GLOBALS['strCollectedThisMonth']       = "이번달";
$GLOBALS['strCollectedLastMonth']       = "지난달";
$GLOBALS['strCollectedLast7Days']       = "지난 7일";
$GLOBALS['strCollectedSpecificDates']   = "날짜선택";

// Advertiser
$GLOBALS['strClient']				= "광고주";
$GLOBALS['strClients'] 				= "광고주";
$GLOBALS['strClientsAndCampaigns']		= "광고주 & 캠페인";
$GLOBALS['strAddClient'] 			= "새 광고주 추가";
$GLOBALS['strAddClient_Key'] 		= "새 광고주 추가(<u>n</u>)";
$GLOBALS['strTotalClients'] 			= "총 광고주 수";
$GLOBALS['strClientProperties']			= "광고주 등록정보";
$GLOBALS['strClientHistory']			= "광고주 기록";
$GLOBALS['strNoClients']			= "현재 등록된 광고주가 없습니다.";
$GLOBALS['strConfirmDeleteClient'] 		= "해당 광고주를 삭제합니까?";
$GLOBALS['strConfirmResetClientStats']		= "해당 광고주에 대한 모든 통계를 삭제합니까?";
$GLOBALS['strHideInactiveAdvertisers']		= "사용하지 않는 광고주 숨기기";
$GLOBALS['strInactiveAdvertisersHidden']	= "광고주가 숨겨져 있습니다.";

$GLOBALS['strAllAdvertisers']            = "모든 광고주";


// Advertisers properties
$GLOBALS['strContact'] 				= "광고주 명";
$GLOBALS['strContactName']			= "담당자 이름";
$GLOBALS['strEMail'] 				= "이메일";
$GLOBALS['strChars']				= "chars";
$GLOBALS['strSendAdvertisingReport']		= "광고 보고서를 이메일로 발송합니다.";
$GLOBALS['strNoDaysBetweenReports']		= "보고서 발송 간격(날짜)";
$GLOBALS['strSendDeactivationWarning']  	= "캠페인이 활성화/중지 되면 이메일로 발송합니다.";
$GLOBALS['strAllowClientModifyInfo'] 		= "사용자가 설정을 수정하는 것을 허용합니다.";
$GLOBALS['strAllowClientModifyBanner'] 		= "사용자가 배너를 수정하는 것을 허용합니다.";
$GLOBALS['strAllowClientAddBanner'] 		= "사용자가 배너를 추가할 수 있게 합니다.";
$GLOBALS['strAllowClientDisableBanner'] 	= "사용자가 자신의 배너를 중지할 수 있게 합니다.";
$GLOBALS['strAllowClientActivateBanner'] 	= "사용자가 자신의 배너를 운영할 수 있게 합니다.";
$GLOBALS['strAllowClientViewTargetingStats']	= "사용자가 타케팅 보고서를 볼 수 있게 합니다.";
$GLOBALS['strAllowCreateAccounts']				= "사용자가 새로운 계정을 생성할 수 있게 합니다.";
$GLOBALS['strCsvImportConversions']				= "사용자가 오프라인 전환을 가져올 수 있게 합니다.";
$GLOBALS['strAdvertiserLimitation']				= "해당 광고주의 웹페이지에서 하나의 배너만 표시합니다.";
$GLOBALS['strAllowAuditTrailAccess']			= "사용자가 로그를 추적하는 것을 허용합니다.";


// Campaign
$GLOBALS['strCampaign']				= "캠페인";
$GLOBALS['strCampaigns']			= "캠페인";
$GLOBALS['strOverallCampaigns']		= "캠페인";
$GLOBALS['strTotalCampaigns'] 			= "전체 캠페인";
$GLOBALS['strActiveCampaigns'] 			= "운영중인 캠페인";
$GLOBALS['strAddCampaign'] 			= "새 캠페인 추가";
$GLOBALS['strAddCampaign_Key'] 		= "새 캠페인 추가(<u>n</u>)";
$GLOBALS['strCampaignForAdvertiser']        = "광고주를 위한";
$GLOBALS['strCreateNewCampaign']		= "새 캠페인 생성";
$GLOBALS['strModifyCampaign']			= "캠페인 수정";
$GLOBALS['strMoveToNewCampaign']		= "새로운 캠페인으로 이동";
$GLOBALS['strBannersWithoutCampaign']		= "캠페인이 없는 배너";
$GLOBALS['strDeleteAllCampaigns']		= "모든 캠페인 삭제";
$GLOBALS['strLinkedCampaigns']              = "연결된 캠페인";
$GLOBALS['strCampaignStats']			= "캠페인 보고서";
$GLOBALS['strCampaignProperties']		= "캠페인 등록정보";
$GLOBALS['strCampaignOverview']			= "캠페인 개요";
$GLOBALS['strCampaignHistory']			= "캠페인 보고서";
$GLOBALS['strNoCampaigns']			= "현재 정의된 캠페인이 없습니다.";
$GLOBALS['strNoCampaignsAddAdvertiser']     = "현재 정의된 캠페인이 없습니다. 캠페인 등록을 위해 <a href='advertiser-edit.php'>새 광고주 추가</a>를 먼저 진행하세요.";
$GLOBALS['strNoCampaignsForBanners']        = "현재 광고주에 등록된 캠페인이 없습니다. 배너를 등록하시려면 <a href='campaign-edit.php?clientid=%s'>새 캠페인 추가</a>를 먼저 진행하세요.";
$GLOBALS['strConfirmDeleteAllCampaigns']	= "해당 광고주의 모든 캠페인을 삭제합니까?";
$GLOBALS['strConfirmDeleteCampaign']		= "이 캠페인을 정말로 삭제합니까?";
$GLOBALS['strConfirmDeleteCampaigns']		= "선택된 캠페인들을 정말로 삭제합니까?";
$GLOBALS['strConfirmResetCampaignStats']		= "이 캠페인에 존재하는 모든 통계를 정말로 삭제합니까?";
$GLOBALS['strShowParentAdvertisers']		= "광고주 단위 보기";
$GLOBALS['strHideParentAdvertisers']		= "캠페인 단위 보기";
$GLOBALS['strHideInactiveCampaigns']		= "운영하지 않는 캠페인 숨기기";
$GLOBALS['strInactiveCampaignsHidden']		= "운영하지 않는 캠페인이 숨겨져 있습니다.";
$GLOBALS['strContractDetails']		= "계약 상세내용";
$GLOBALS['strInventoryDetails']		= "인벤토리 상세내용";
$GLOBALS['strPriorityInformation']		= "다른 캠페인 간의 관계에서 우선순위";
$GLOBALS['strImpressionGoal']		= "목표 노출";
$GLOBALS['strECPMInformation']		= "미디에이션(eCPM 우선순위)";
$GLOBALS['strRemnantEcpmDescription']		= "eCPM이 이 캠페인의 성과에 기반하여 자동으로 계산됩니다.<br />표준 계약 캠페인들의 관계에 대한 우선순위를 매기는 데 사용될 것입니다.";
$GLOBALS['strContractEcpmDescription']		= "eCPM이 이 캠페인의 성과에 기반하여 자동으로 계산됩니다.<br />%s에서 %s의 우선순위 레벨의 캠페인들의 우선순위를 매기는 데 사용될 것입니다.";
$GLOBALS['strEcpmMinImpsDescription']		= "이 캠페인의 eCPM을 계산하는 최소한의 기준을 설정하세요.";
$GLOBALS['strPriorityExclusive']		= "다른 비독점 캠페인보다 우선시";
$GLOBALS['strPriorityHigh']		= "- 유료 캠페인";
$GLOBALS['strPriorityLow']		= "- 하우스 / 무료 캠페인";
$GLOBALS['strPriorityHighShort']		= "높음";
$GLOBALS['strPriorityLowShort']		= "낮음";
$GLOBALS['strHiddenCampaign']		= "캠페인";
$GLOBALS['strHiddenAd']		= "광고";
$GLOBALS['strHiddenAdvertiser'] 		= "광고주";
$GLOBALS['strHiddenTracker']		= "추적기";
$GLOBALS['strHiddenWebsite'] 		= "웹사이트";
$GLOBALS['strHiddenZone'] 		= "영역";
$GLOBALS['strUnderdeliveringCampaigns']		= "미달 캠페인";
$GLOBALS['strCampaignDelivery']		= "캠페인 전달";
$GLOBALS['strBookedMetric']		= "예약된 Metric";
$GLOBALS['strValueBooked']		= "예약된 값";
$GLOBALS['strRemaining']		= "남은";
$GLOBALS['strCompanionPositioning']		= "동반 포지셔닝";
$GLOBALS['strSelectUnselectAll']		= "전체 선택 / 해제";
$GLOBALS['strConfirmOverwrite']		= "변동사항을 저장하면 개인적인 배너-영역 링크들을 덮어씁니다. 정말 저장하시겠습니까?";
$GLOBALS['strCampaignsOfAdvertiser']		= "of";
$GLOBALS['strShowCappedNoCookie']		= "쿠키를 사용할 수 없으면 상한된 광고가 보여짐";

// Campaign-zone linking page
$GLOBALS['strCalculatedForAllCampaigns']	= "모든 캠페인에 대한 계산";
$GLOBALS['strCalculatedForThisCampaign']	= "이 캠페인 계산";
$GLOBALS['strLinkingZonesProblem']			= "영역에 연결 시 문제 발생";
$GLOBALS['strUnlinkingZonesProblem']		= "영역 연결 해제시 문제 발생";
$GLOBALS['strZonesLinked']					= "연결된 영역";
$GLOBALS['strZonesUnlinked']				= "연결되지 않은 영역";
$GLOBALS['strZonesSearch']					= "검색";
$GLOBALS['strZonesSearchTitle']				= "이름으로 영역과 웹사이트 검색";
$GLOBALS['strNoWebsitesAndZones']			= "웹사이트와 영역 없음";
$GLOBALS['strNoWebsitesAndZonesCategory']	= "in category";
$GLOBALS['strNoWebsitesAndZonesText']		= "with \"%s\" in name";
$GLOBALS['strToLink']						= "연결";
$GLOBALS['strToUnlink']						= "연결 해제";
$GLOBALS['strLinked']						= "연결됨";
$GLOBALS['strAvailable']					= "이용가능";
$GLOBALS['strShowing']						= "보기";
$GLOBALS['strAllCategories']				= "모든 카테고리";
$GLOBALS['strUncategorized']				= "분류되지 않음";
$GLOBALS['strEditZone']						= "영역 수정";
$GLOBALS['strEditWebsite']					= "웹사이트 수정";

// Campaign properties
$GLOBALS['strDontExpire']			= "이 캠페인을 만료하지 않습니다.";
$GLOBALS['strActivateNow'] 			= "이 캠페인을 지금 활성화합니다.";
$GLOBALS['strSetSpecificDate']           = "날짜 지정";
$GLOBALS['strActivationDateComment']    = "해당 날짜에 캠페인이 시작됩니다.";
$GLOBALS['strLow']				= "낮은";
$GLOBALS['strHigh']				= "높이";

//$GLOBALS['strExpirationDate']			= "만료날짜";
//$GLOBALS['strActivationDate']			= "활성날짜";
//ted변경
$GLOBALS['strExpirationDate']			= "종료일";
$GLOBALS['strActivationDate']			= "시작일";

$GLOBALS['strViewsPurchased'] 			= "AdView 남은 기간";
$GLOBALS['strClicksPurchased'] 			= "AdClick 남은 기간";
$GLOBALS['strCampaignWeight']			= "캠페인 가중치";
$GLOBALS['strHighPriority']			= "이 캠페인을 배너를 높은 우선 순위로 설정합니다.<br>이 옵션을 선택하면 phpAdsNew는 하루동안 지정한 횟수만큼 배너를 노출시키게 됩니다. 이와 같은 시작을 하려면 이미 축적된 통계가 필요합니다.";
$GLOBALS['strLowPriority']			= "이 캠페인을 배너를 낮은 우선 순위로 설정합니다.<br>이 옵션을 높은 우선 순위로 배너가 노출되지 않음 때 배너를 노출하기 위해 사용합니다. 노출 횟수를 지정할 수 없으며, 가중치를 통해서 낮은 우선 순위 배너들간에 비율만 설정할 수 있습니다.";
$GLOBALS['strTargetLimitAdviews']		= "대상 최대 AdView를 ";
$GLOBALS['strTargetPerDay']			= "회로 제한합니다.";
$GLOBALS['strPriorityAutoTargeting']		= "남은 기간 동안 배너를 균등하게 노출시킵니다. 따라서 매일 매일 AdView의 노출 횟수가 설정됩니다.";

$GLOBALS['strRevenueInfo']              = "수익 정보";
$GLOBALS['strTotalRevenue']             = "전체 수익";
$GLOBALS['strImpressionsRemaining']     = "남은 노출수";
$GLOBALS['strClicksRemaining']             = "남은 클릭수";
$GLOBALS['strConversionsRemaining']     = "남은 전환수";
$GLOBALS['strImpressionsBooked']         = "예약된 노출수";
$GLOBALS['strClicksBooked']             = "예약된 클릭수";
$GLOBALS['strConversionsBooked']         = "예약된 전환수";
$GLOBALS['strCampaignWeight']			= "캠페인 가중치 설정";
$GLOBALS['strTargetLimitAdImpressions'] = "광고노출 제한";
$GLOBALS['strOptimise']                    = "캠페인의 광고 전달 최적화.";
$GLOBALS['strAnonymous']                = "이 캠페인의 광고주와 웹사이트 숨기기.";

$GLOBALS['strHighPriority']			= "이 캠페인에서 배너를 높은 우선 순위로 설정합니다.<br>이 옵션을 선택하면 ".MAX_PRODUCT_NAME."는 하루동안 지정한 횟수만큼 배너를 노출시키게 됩니다. 이와 같은 시작을 하려면 이미 축적된 통계가 필요합니다.";
$GLOBALS['strLowPriority']			= "이 캠페인에서 배너를 낮은 우선 순위로 설정합니다.<br>이 옵션을 선택하면 높은 우선 순위의 배너가 노출되지 않을 때 배너를 노출하기 위해 사용합니다. 노출 횟수를 지정할 수 없으며, 가중치를 통해서 낮은 우선 순위 배너들간에 비율만 설정할 수 있습니다.";
//$GLOBALS['strTargetLimitAdviews']		= "대상 최대 AdView를 ";
$GLOBALS['strTargetPerDay']			= "회로 제한합니다.";
$GLOBALS['strTargetLimitImpressionsTo']		= "노출수 제한";
$GLOBALS['strPriorityAutoTargeting']		= "남은 기간 동안 배너를 균등하게 노출시킵니다. 따라서 매일 매일 AdView의 노출 횟수가 설정됩니다.";
$GLOBALS['strCampaignWarningRemnantNoWeight']		= "이 캠페인 형식은 '표준 계약'로 설정되었지만, \n가중치는 0으로 설정되거나 설정되지 않습니다.\n 이것은 캠페인의 비활성화를 초래하고 \n가중치가 유효한 숫자로 설정될때까지 \n캠페인의 배너는 전달되지 않을 것입니다. \n\n계속하시겠습니까?";
$GLOBALS['strCampaignWarningEcpmNoRevenue']		= "이 캠페인은 eCPM 최적화를 사용하지만 \n'매출'은 0으로 설정되거나 설정되지 않습니다. \n이것은 캠페인의 비활성화를 초래하고 \n매출이 유효한 숫자로 설정될때까지 \n캠페인의 배너는 전달되지 않을 것입니다. \n\n계속하시겠습니까?";
$GLOBALS['strCampaignWarningExclusiveNoWeight']		= "이 캠페인 형식은 '독점'으로 설정되었지만, \n가중치는 0으로 설정되거나 설정되지 않습니다. \n이것은 캠페인의 비활성화를 초래하고 \n가중치가 유효한 숫자로 설정될때까지 \n캠페인의 배너는 전달되지 않을 것입니다. \n\n계속하시겠습니까?";
$GLOBALS['strCampaignWarningNoTarget']		= "이 캠페인 형식은 '계약'으로 설정되었지만, \n일일 제한은 설정되지 않습니다. \n이것은 캠페인의 비활성화를 초래하고 \n 유효한 일일 제한이 설정될때까지 \n캠페인의 배너는 전달되지 않을 것입니다. \n\n계속하시겠습니까?";
$GLOBALS['strCampaignStatusPending']		= "보류";
$GLOBALS['strCampaignStatusInactive'] 		= "비활성";
$GLOBALS['strCampaignStatusRunning']		= "운영중";
$GLOBALS['strCampaignStatusPaused']		= "일시정지됨";
$GLOBALS['strCampaignStatusAwaiting']		= "대기";
$GLOBALS['strCampaignStatusExpired']		= "완료됨";
$GLOBALS['strCampaignStatusApproval']		= "대기 승인 »";
$GLOBALS['strCampaignStatusRejected']		= "거부됨";
$GLOBALS['strCampaignStatusAdded']		= "추가됨";
$GLOBALS['strCampaignStatusStarted']		= "시작됨";
$GLOBALS['strCampaignStatusRestarted']		= "재시작됨";
$GLOBALS['strCampaignStatusDeleted'] 		= "삭제됨";
$GLOBALS['strCampaignApprove']		= "승인";
$GLOBALS['strCampaignApproveDescription']		= "이 캠페인을 수용";
$GLOBALS['strCampaignReject']		= "거부";
$GLOBALS['strCampaignRejectDescription']		= "이 캠페인을 거부";
$GLOBALS['strCampaignPause']		= "일시정지";
$GLOBALS['strCampaignPauseDescription']		= "이 캠페인을 일시적으로 정지";
$GLOBALS['strCampaignRestart']		= "재개";
$GLOBALS['strCampaignRestartDescription']		= "이 캠페인을 재개";
$GLOBALS['strCampaignStatus']		= "캠페인 상태";
$GLOBALS['strReasonForRejection']		= "거부 이유";
$GLOBALS['strReasonSiteNotLive']		= "죽은 사이트";
$GLOBALS['strReasonBadCreative']		= "부적절한 생성";
$GLOBALS['strReasonBadUrl']		= "부적절한 목표 url";
$GLOBALS['strReasonBreakTerms']		= "조건에 대한 웹사이트";
$GLOBALS['strChangeStatus']		= "상태 변경";
$GLOBALS['strCampaignType'] 		= "캠페인 형식";
$GLOBALS['strType']		= "형식";
$GLOBALS['strContract'] 		= "계약";
$GLOBALS['strRemnant']		= "표준 캠페인";
$GLOBALS['strStandardContract'] 		= "제한 캠페인";
$GLOBALS['strExclusiveContract']		= "독점 캠페인";
//$GLOBALS['strRemnant']		= "표준 캠페인";
$GLOBALS['strStandardContractInfo']		= "일일 노출 한도 설정이 가능하여 종료일이 될 때까지 일일 노출 한도에 따라 광고가 균등하게 노출 됩니다.";
$GLOBALS['strExclusiveContractInfo']		= "이 캠페인은 광고 게재 우선순위가 가장 높으며 다른 캠페인보다 우선 노출됩니다.";
$GLOBALS['strRemnantInfo']		= "이 캠페인은 종료일이나 특정 한도를 지정할 수 있는 기본 캠페인 입니다.";
$GLOBALS['strECPMInfo']		= "이 캠페인은 종료일 및 노출 한도를 지정할 수 있는 기본 캠페인 입니다. 설정에 따라 eCPM을 사용하여 우선순위가 정해질 수 있습니다.";
$GLOBALS['strContractCampaign']		= "표준 캠페인";
$GLOBALS['strRemnantCampaign']		= "제한 캠페인";
$GLOBALS['strPricing']		= "가격";
$GLOBALS['strPricingModel']		= "가격 모델";
$GLOBALS['strSelectPricingModel']		= "-- 선택 --";
$GLOBALS['strRatePrice']		= "비율 / 가격";
$GLOBALS['strMinimumImpressions']		= "최소 일일 노출";
$GLOBALS['strLimit']		= "한도 지정";
$GLOBALS['strLowExclusiveDisabled']		= "노출/클릭/전환 제한 중 하나와 종료일, 두 가지 모두 설정되었기 때문에, 이 캠페인을 표준캠페인 혹은 독점 캠페인으로 변경할 수 없습니다. <br>캠페인 형식을 바꾸기 위해선, 종료일을 없애거나 한도를 삭제해야만 합니다.";
$GLOBALS['strCannotSetBothDateAndLimit']		= "표준 캠페인 혹은 독점 캠페인은 종료일과 한도를 둘 다 설정할 수 없습니다.<br>종료일과 노출/클릭/전환에 대한 한도 모두 설정하려면 제한 캠페인을 사용하세요.";
$GLOBALS['strWhyDisabled']		= "왜 사용 불가인가요?";
$GLOBALS['strBackToCampaigns']		= "캠페인으로 돌아가기";
$GLOBALS['strCampaignBanners']		= "캠페인의 배너";
$GLOBALS['strCookies']		= "쿠키";
$GLOBALS['strDeliveryCappingReset']       = "설정된 시간 이후 뷰 카운터를 리셋:";
$GLOBALS['strDeliveryCappingTotal']       = "전체";
$GLOBALS['strDeliveryCappingSession']     = "세션 당";
$GLOBALS['strDeliveryCapping']            = "방문자별 광고전달 상한(Delivery capping)";

$GLOBALS['strCappingZone'] = array();
$GLOBALS['strCappingZone']['title'] = $GLOBALS['strDeliveryCapping'];
$GLOBALS['strCappingZone']['limit'] = "영역 뷰 한도:";

$GLOBALS['strCappingBanner'] = array();
$GLOBALS['strCappingBanner']['title'] = "방문자별 광고전달 상한(Delivery capping)";
$GLOBALS['strCappingBanner']['limit'] = "배너 뷰 한도:";

// Tracker
$GLOBALS['strTracker']		= "추적기";
$GLOBALS['strTrackers']		= "추적기";
$GLOBALS['strTrackerOverview'] 		= "추적기 개요";
$GLOBALS['strTrackerPreferences']		= "추적기 환경설정";
$GLOBALS['strAddTracker']		= "새 추적기 추가";
$GLOBALS['strAddTracker_Key']		= "새 추적기 추가<u>n</u>";
$GLOBALS['strTrackerForAdvertiser']		= "광고주를 위한";
$GLOBALS['strNoTrackers']		= "현재는 이 광고주에 정의된 추적기가 없습니다.";
$GLOBALS['strConfirmDeleteAllTrackers'] 		= "이 광고주 소유의 모든 추적기를 삭제합니까?";
$GLOBALS['strConfirmDeleteTrackers'] 		= "선택된 모든 추적기를 삭제합니까?";
$GLOBALS['strConfirmDeleteTracker'] 		= "이 추적기를 삭제합니까?";
$GLOBALS['strDeleteAllTrackers'] 		= "모든 추적기 삭제";
$GLOBALS['strTrackerProperties'] 		= "추적기 속성";
//$GLOBALS['strTrackerOverview'] 		= "추적기 개요";
$GLOBALS['strModifyTracker']		= "추적기 수정";
$GLOBALS['strLog']		= "로그";
$GLOBALS['strDefaultStatus']		= "기본 상태";
$GLOBALS['strStatus']		= "상태";
$GLOBALS['strLinkedTrackers']		= "연결된 추적기";
$GLOBALS['strTrackerInformation']		= "추적기 정보";
$GLOBALS['strConversionWindow']		= "전환 윈도우";
$GLOBALS['strUniqueWindow']		= "유일한 윈도우";
$GLOBALS['strClick']		= "Click";
$GLOBALS['strView']		= "보기";
$GLOBALS['strArrival']		= "도착";
$GLOBALS['strManual']		= "수동";
$GLOBALS['strImpression']		= "노출";
$GLOBALS['strConversionClickWindow']		= "초 내에 발생한 Click 전환을 카운트";
$GLOBALS['strConversionViewWindow']		= "초 내에 발생한 View 전환을 카운트";
$GLOBALS['strTotalTrackerImpressions']		= "총 노출";
$GLOBALS['strTotalTrackerConnections']		= "총 연결";
$GLOBALS['strTotalTrackerConversions']		= "총 전환";
$GLOBALS['strTrackerImpressions']		= "노출";
$GLOBALS['strTrackerImprConnections']		= "노출 연결";
$GLOBALS['strTrackerClickConnections']		= "클릭 연결";
$GLOBALS['strTrackerImprConversions']		= "노출 전환";
$GLOBALS['strTrackerClickConversions']		= "클릭 전환";
$GLOBALS['strConversionType']		= "전환 형식";
$GLOBALS['strLinkCampaignsByDefault']		= "기본값으로 새롭게 생성된 캠페인 연결";
$GLOBALS['strNoLinkedTrackersDropdown']		= "-- 연결된 추적기 없음 --";
$GLOBALS['strPerSingleImpression']		= "매 노출 마다";
$GLOBALS['strBackToTrackers']		= "추적기로 돌아가기";


// Banners (General)
$GLOBALS['strBanner'] 				= "배너";
$GLOBALS['strBanners'] 				= "배너";
$GLOBALS['strBannersReport']    	= "배너 보고서";
$GLOBALS['strBannerFilter']			= "배너 필터";
$GLOBALS['strAddBanner'] 			= "새 배너 추가";
$GLOBALS['strAddBanner_Key'] 		= "새 배너 추가(<u>n</u>)";
$GLOBALS['strBannerToCampaign']		= "캠페인으로";
$GLOBALS['strModifyBanner'] 			= "배너 수정";
$GLOBALS['strActiveBanners'] 			= "운영중인 배너";
$GLOBALS['strTotalBanners'] 			= "총 배너 수";
$GLOBALS['strShowBanner']			= "배너 보기";
$GLOBALS['strShowAllBanners']	 		= "모든 배너 보기";
//$GLOBALS['strShowBannersNoAdClicks']		= "AdClick 없는 배너 보기";
//$GLOBALS['strShowBannersNoAdViews']		= "AdView 없는 배너 보기";
$GLOBALS['strShowBannersNoAdViews']			= "노출수 없는 배너 보기";
$GLOBALS['strShowBannersNoAdClicks']		= "클릭수 없는 배너 보기";
$GLOBALS['strShowBannersNoAdConversions']	= "판매 없는 배너 보기";
$GLOBALS['strDeleteAllBanners']	 		= "모든 배너 삭제";
$GLOBALS['strActivateAllBanners']		= "모든 배너 활성화";
$GLOBALS['strDeactivateAllBanners']		= "모든 배너중지";
$GLOBALS['strBannerOverview']			= "배너 목록";
$GLOBALS['strBannerProperties']			= "배너 등록 정보";
$GLOBALS['strBannerHistory']			= "배너 보고서";
$GLOBALS['strBannerNoStats'] 			= "해당 배너에 대한 통계가 없습니다.";
$GLOBALS['strNoBanners']			= "현재 등록된 배너가 없습니다.";
$GLOBALS['strNoBannersAddCampaign']		= "캠페인이 없기 때문에 현재는 정의된 배너가 없습니다. 배너를 생성하려면, 우선 <a href='campaign-edit.php?clientid=%s'>새로운 캠페인을 생성</a>하세요.";
$GLOBALS['strNoBannersAddAdvertiser']		= "광고주가 없기 때문에 현재는 정의된 배너가 없습니다. 배너를 생성하려면, 우선 <a href='advertiser-edit.php'>새로운 광고주를 생성</a>하세요.";
$GLOBALS['strConfirmDeleteBanner']		= "배너를 삭제하면 배너에 대한 보고서도 함께 삭제됩니다. \\n정말로 해당 배너를 삭제합니까?";
$GLOBALS['strConfirmDeleteBanners']		= "배너들을 삭제하면 배너에 대한 보고서들도 함께 삭제됩니다. \\n정말로 선택된 배너들을 삭제합니까?";
$GLOBALS['strConfirmDeleteAllBanners']		= "배너를 삭제하면 배너에 대한 보고서도 함께 삭제됩니다. \\n정말로 이 캠페인에 연결된 모든 배너를 삭제합니까?";
$GLOBALS['strConfirmResetBannerStats']		= "해당 배너에 대한 모든 보고서를 삭제합니까?";
$GLOBALS['strShowParentCampaigns']		= "캠페인 단위로 보기";
$GLOBALS['strHideParentCampaigns']		= "배너 단위로 보기";
$GLOBALS['strHideInactiveBanners']		= "운영하지 않는 배너 숨김";
$GLOBALS['strInactiveBannersHidden']	= "운영하지 않는 배너가 숨겨져 있습니다.";
$GLOBALS['strAppendOthers']				= "다른 배너 추가";
$GLOBALS['strAppendTextAdNotPossible']	= "다른 배너를 텍스트 광고로 추가할 수 없습니다.";
$GLOBALS['strHiddenBanner']				= "숨겨진 배너";
$GLOBALS['strWarningTag1']				= "경고,";
$GLOBALS['strWarningTag2']				= " 태그가 정상적으로 열리거나 닫혀있지 않습니다.";
$GLOBALS['strWarningMissing']			= "경고, ";
$GLOBALS['strWarningMissingClosing']	= " 닫힘 태그 '>' 나";
$GLOBALS['strWarningMissingOpening']	= " 열림 태그 '<' 가 없습니다.";
$GLOBALS['strSubmitAnyway']				= "무조건 전송";
$GLOBALS['strOverallBanners']			= "배너";
$GLOBALS['strBannersOfCampaign']		= "in";

// Banner Preferences
$GLOBALS['strBannerPreferences'] 			= "배너 환경설정";
$GLOBALS['strCampaignPreferences'] 			= "미디에이션 설정";
$GLOBALS['strDefaultBanners']   			= "기본 배너";
$GLOBALS['strDefaultBannerUrl']				= "기본 이미지 URL";
$GLOBALS['strDefaultBannerDestination'] 	= "기본 도착 URL";
$GLOBALS['strAllowedBannerTypes'] 			= "허용 배너 유형";
$GLOBALS['strTypeSqlAllow'] 				= "SQL 로컬 배너를 허용";
$GLOBALS['strTypeWebAllow'] 				= "웹 서버 로컬 배너를 허용";
$GLOBALS['strTypeUrlAllow'] 				= "외부 배너를 허용";
$GLOBALS['strTypeHtmlAllow']				= "HTML 배너 허용";
$GLOBALS['strTypeTxtAllow'] 				= "텍스트 광고 허용";
$GLOBALS['strTypeHtmlSettings'] 			= "HTML 배너 옵션";
$GLOBALS['strTypeHtmlAuto']					= "강제 클릭 추적을 위한 HTML 배너 자동 변경";
$GLOBALS['strTypeHtmlPhp']					= "PHP 표현식을 HTML 배너 내에서 실행할 수 있도록 허용";


// Banner (Properties)
$GLOBALS['strChooseBanner'] 			= "배너 형식을 선택하세요.";
$GLOBALS['strMySQLBanner'] 			= "로컬 배너(SQL - DB 저장방식)";
$GLOBALS['strWebBanner'] 			= "로컬 배너(웹서버 - 웹 저장 방식)";
$GLOBALS['strMobileBanner'] 			= "Mobile 배너";
$GLOBALS['strURLBanner'] 			= "외부 배너";
$GLOBALS['strHTMLBanner'] 			= "HTML 배너";
$GLOBALS['strTextBanner'] 			= "텍스트 광고";
$GLOBALS['strAutoChangeHTML']			= "AdClick을 추가하기 위해 HTML을 수정합니다.";
$GLOBALS['strUploadOrKeep']			= "현재 이미지를 사용하거나<br> 다른 이미지를 업로드할<br> 수 있습니다.";
$GLOBALS['strNewBannerFile'] 			= "배너의 사용할 이미지를 선택하세요.";
$GLOBALS['strNewBannerURL'] 			= "이미지 URL(incl. http://)";
$GLOBALS['strURL'] 				= "대상 URL(incl. http://)";
$GLOBALS['strHTML'] 				= "HTML";
$GLOBALS['strTextBelow'] 			= "이미지 설명";
$GLOBALS['strKeyword'] 				= "키워드";
$GLOBALS['strWeight'] 				= "가중치";
$GLOBALS['strAlt'] 				= "Alt 텍스트";
$GLOBALS['strStatusText']			= "상태표시줄 문구";
$GLOBALS['strBannerWeight']			= "배너 가중치";
$GLOBALS['strBannerType']				= "광고 형식";
$GLOBALS['strAdserverTypeGeneric']		= "일반적인 HTML 배너";
$GLOBALS['strDoNotAlterHtml']			= "HTML을 대신하지 않음";
$GLOBALS['strAdserverTypeMax']			= "리치 미디어 - OpenX";
$GLOBALS['strAdserverTypeAtlas']		= "리치 미디어 - Atlas";
$GLOBALS['strAdserverTypeBluestreak']	= "리치 미디어 - Bluestreak";
$GLOBALS['strAdserverTypeDoubleclick']	= "리치 미디어 - DoubleClick";
$GLOBALS['strAdserverTypeEyeblaster']	= "리치 미디어 - Eyeblaster";
$GLOBALS['strAdserverTypeFalk']			= "리치 미디어 - Falk";
$GLOBALS['strAdserverTypeMediaplex']	= "리치 미디어 - Mediaplex";
$GLOBALS['strAdserverTypeTangozebra']	= "리치 미디어 - Tango Zebra";
$GLOBALS['strGenericOutputAdServer']	= "일반";
$GLOBALS['strSwfTransparency']			= "투명 배경을 허용";
$GLOBALS['strBackToBanners']			= "배너로 돌아가기";
$GLOBALS['strSelectHtmlBanners']		= "외부 스크립트 배너";
$GLOBALS['strSelectTextBanners']		= "외부 텍스트 배너";
$GLOBALS['strBannersLink']				= "배너 링크";
$GLOBALS['strBannersText']				= "배너 문구";
$GLOBALS['strBannersAdditional']		= "추가 내용";
$GLOBALS['strBannersContent']			= "내용";
$GLOBALS['strAlterHTML']                = "클릭을 추적 할 수 있도록 HTML을 변경합니다:<br>";
$GLOBALS['strBannersAds']			    = "매체사 & 네트워크 광고사 정보";
$GLOBALS['strBannersMobile']			= "모바일 광고코드";
$GLOBALS['strBannersMobile_android']	= "Android";
$GLOBALS['strBannersMobile_ios']		= "iOS";
$GLOBALS['strBannersMobile_script']		= "Script";
$GLOBALS['strBannersMobile_dimension']	= "배너타입";
$GLOBALS['strBannersMobile_dimension_title']	= "320x50,300x250,768x1024 (전면배너)";
$GLOBALS['strBannersMobile_dimension_value']	= "320x50,300x250,768x1024";
$GLOBALS['strBannersAdsM​edium']		    = "매체사";
$GLOBALS['strBannersAdsContent']	    = "네트워크 광고사";
$GLOBALS['strBannersAdsTextCode']	    = "광고단위 코드";
$GLOBALS['strBannersAdsMediumEmpty']	= "매체사 없음";
$GLOBALS['strBannersAdsNetworkEmpty']	= "네트워크 광고사 없음";

// Banner (advanced)
$GLOBALS['strBannerPrependHTML']		= "이 배너의 앞에 다음의 HTML 코드를 항상 붙임";
$GLOBALS['strBannerAppendHTML']			= "이 배너의 다음에 다음의 HTML 코드를 항상 붙임";


// Banner (swf)
$GLOBALS['strCheckSWF']				= "플래시 파일에 입력된 링크를 확인합니다.";
$GLOBALS['strConvertSWFLinks']			= "플래시 링크 변환합니다.";
$GLOBALS['strHardcodedLinks']			= "내장형 링크";
$GLOBALS['strConvertSWF']			= "<br />업로드한 플래시 파일에 URL를 포함시킬 수 있습니다. 업로드한 플래시 파일에 URL이 포함되어 있습니다. ". MAX_PRODUCT_NAME ."는 플래시 파일에 포함된 URL를 변환하지 않으면 배너에 대한 AdClick 수를 추적할 수 없습니다. 다음의 플래시 파일에 포함된 URL 목록입니다. 이 URL를 변환하려면 <b>변환</b>을 클릭하고, 아니면 <b>취소</b>를 클릭하세요.</b>.<br /><br />주의: <b>변환</b>을 클릭하면 업로드한 플래시 파일에서 실제로 변경합니다.<br />원본 파일은 백업하십시오. 배너를 만드는걸 사용한 플래시 파일에는 버전은 상관없음 결과 파일에는 플래시 4 파일로 생성합니다.<br /><br />";
$GLOBALS['strCompressSWF']			= "보다 빠른 파일 전송을 위해 SWF 파일 압축";
$GLOBALS['strOverwriteSource']		= "소스 파이미터 덮어쓰기";
$GLOBALS['strLinkToShort']			= "경고: 하드코딩된 URL이 감지됨 - 하지만 URL이 자동으로 수정되기엔 너무 짧음";


// Banner (network)
$GLOBALS['strBannerNetwork']			= "HTML 템플릿";
$GLOBALS['strChooseNetwork']			= "사용할 템플릿 선택하세요.";
$GLOBALS['strMoreInformation']			= "자세한 정보...";
$GLOBALS['strRichMedia']			= "리치미디어";
$GLOBALS['strTrackAdClicks']			= "AdClicks 추가";


// Banner (AdSense)
$GLOBALS['strAdSenseAccounts']			= "AdSense 계정";
$GLOBALS['strLinkAdSenseAccount']		= "AdSense 계정 연결";
$GLOBALS['strCreateAdSenseAccount']		= "AdSense 계정 생성";
$GLOBALS['strEditAdSenseAccount']		= "AdSense 계정 수정";


// Display limitations
$GLOBALS['strModifyBannerAcl'] 			= "전달유지 옵션";
$GLOBALS['strACL'] 				= "전달유지";
$GLOBALS['strACLAdd'] 				= "새 제한 추가";
$GLOBALS['strACLAdd_Key'] 				= "새 제한 추가(<u>n</u>)";
$GLOBALS['strNoLimitations']			= "제한 없음";
$GLOBALS['strApplyLimitationsTo']		= "제한 적용하기";
$GLOBALS['strRemoveAllLimitations']		= "모든 제한 제거";
$GLOBALS['strEqualTo']				= "같은 경우";
$GLOBALS['strDifferentFrom']			= "다른 경우";
$GLOBALS['strAND']				= "그리고";  						// logical operator
$GLOBALS['strOR']				= "또는"; 						// logical operator
$GLOBALS['strOnlyDisplayWhen']			= "다음 조건에서만 배너를 표시합니다.:";
$GLOBALS['strWeekDay'] 				= "주중(월-금)";
$GLOBALS['strTime'] 				= "시간";
$GLOBALS['strUserAgent'] 			= "사용자 에이전트";
$GLOBALS['strDomain'] 				= "이메일";
$GLOBALS['strClientIP'] 			= "클라이언트 IP";
$GLOBALS['strSource'] 				= "소스";
$GLOBALS['strBrowser'] 				= "브라우저";
$GLOBALS['strOS'] 				= "OS";
$GLOBALS['strCountry'] 				= "국가";
$GLOBALS['strContinent'] 			= "지역";
$GLOBALS['strDeliveryLimitations']		= "전달유지 제한";
$GLOBALS['strTimeCapping']			= "사용자에게 배너를 보여주면 다음 기간 동안 배너를 다시 노출시키지 않습니다.:";
$GLOBALS['strImpressionCapping']		= "노출한 사용자에게 배너를 노출하는 횟수:";


// Publisher
$GLOBALS['strAffiliate']					= "웹사이트";
$GLOBALS['strAffiliates']					= "웹사이트";
$GLOBALS['strAffiliatesAndZones']			= "웹사이트 & 영역";
$GLOBALS['strAddNewAffiliate']				= "새 웹사이트 추가";
$GLOBALS['strAddNewAffiliate_Key']			= "새 웹사이트 추가(<u>n</u>)";
$GLOBALS['strAddAffiliate']					= "웹사이트 생성";
$GLOBALS['strAffiliateProperties']			= "웹사이트 등록정보";
$GLOBALS['strAffiliateOverview']			= "웹사이트 개요";
$GLOBALS['strAffiliateHistory']				= "웹사이트 보고서";
$GLOBALS['strZonesWithoutAffiliate']		= "웹사이트 미등록 영역";
$GLOBALS['strMoveToNewAffiliate']			= "새 웹사이트로 이동";
$GLOBALS['strNoAffiliates']					= "현재 정의된 웹사이트가 없습니다.";
$GLOBALS['strConfirmDeleteAffiliate']		= "해당 웹사이트를 삭제합니까?";
$GLOBALS['strConfirmDeleteAffiliates']		= "선택된 웹사이트들을 삭제합니까?";
$GLOBALS['strMakePublisherPublic']			= "공개적으로 사용가능하도록 해당 웹사이트에 소유된 영역을 만듭니다.";
$GLOBALS['strAffiliateInvocation'] 			= "광고코드";
$GLOBALS['strAdvertiserSetup']				= "광고주 가입";
$GLOBALS['strTotalAffiliates']				= "전체 웹사이트";
$GLOBALS['strInactiveAffiliatesHidden'] 	= "운영하지 않는 웹사이트가 숨겨져 있습니다.";
$GLOBALS['strShowParentAffiliates']			= "웹사이트별 보기";
$GLOBALS['strHideParentAffiliates']			= "광고 영역별 보기";

// Publisher (properties)
$GLOBALS['strWebsite']						= "웹사이트";
$GLOBALS['strWebsiteURL']					= "웹사이트 URL";
$GLOBALS['strMnemonic']						= "기억하는";
$GLOBALS['strAllowAffiliateModifyInfo'] 	= "사용자가 설정을 수정하는 것을 허용합니다.";
$GLOBALS['strAllowAffiliateModifyZones'] 	= "사용자가 영역을 수정하는 것을 허용합니다.";
$GLOBALS['strAllowAffiliateLinkBanners'] 	= "사용자가 자신의 영역에 배너를 연결할 수 있게 합니다.";
$GLOBALS['strAllowAffiliateAddZone'] 		= "사용자가 새 영역을 정의하는 것을 허용합니다.";
$GLOBALS['strAllowAffiliateDeleteZone'] 	= "사용자가 기존 영역을 삭제하는 것을 허용합니다.";
$GLOBALS['strAllowAffiliateGenerateCode']	= "사용자가 광고코드를 생성하는 것을 허용합니다";
$GLOBALS['strAllowAffiliateZoneStats']		= "사용자가 영역 통계를 보는 것을 허용합니다";
$GLOBALS['strAllowAffiliateApprPendConv']	= "사용자가 오직 승인되거나 대기중인 전환만을 보는 것을 허용합니다";
$GLOBALS['strWebsiteName']					= "웹사이트 명";


// Zone
$GLOBALS['strChooseZone']			= "영역 선택";
$GLOBALS['strZone']					= "영역";
$GLOBALS['strZones']				= "영역";
$GLOBALS['strAddNewZone']			= "새 영역 추가";
$GLOBALS['strAddNewZone_Key']		= "새 영역 추가(<u>n</u>)";
$GLOBALS['strAddZone']				= "영역 생성";
$GLOBALS['strModifyZone']			= "영역 수정";
$GLOBALS['strZoneToWebsite']		= "웹사이트로";
$GLOBALS['strLinkedZones']			= "연결된 영역";
$GLOBALS['strAvailableZones']		= "사용가능한 영역";
$GLOBALS['strLinkingNotSuccess']	= "연결이 완료되지 않았습니다. 다시 시도하세요.";
$GLOBALS['strZoneOverview']			= "영역 목록";
$GLOBALS['strZoneProperties']		= "영역 등록정보";
$GLOBALS['strZoneHistory']			= "영역 보고서";
$GLOBALS['strNoZones']				= "현재 등록된 영역이 없습니다.";
$GLOBALS['strNoZonesAddWebsite']		= "현재는 웹사이트가 없기 때문에, 정의된 영역이 없습니다. 영역을 생성하려면, 우선 <a href='affiliate-edit.php'>새로운 웹사이트를 생성</a>하세요.";
$GLOBALS['strConfirmDeleteZone']	= "이 영역을 삭제합니까?";
$GLOBALS['strConfirmDeleteZones'] 		= "선택 영역을 삭제합니까?";
$GLOBALS['strConfirmDeleteZoneLinkActive']		= "이 영역에 아직 연결되어 있는 캠페인이 있습니다. 만약 이것을 삭제한다면, 이것들은 작동하지 않을 것이고 그로부터 오는 수입도 없을 것입니다.";

$GLOBALS['strZoneType']				= "영역 종류";
$GLOBALS['strBannerButtonRectangle']		= "버튼 또는 사각형 배너";
$GLOBALS['strInterstitial']			= "격자 또는 플로팅 DHTML";
$GLOBALS['strPopup']				= "팝업";
$GLOBALS['strTextAdZone']			= "텍스트 광고";
$GLOBALS['strEmailAdZone']			= "이메일/뉴스레터 영역";
$GLOBALS['strZoneClick']			= "영역 클릭 추적";
$GLOBALS['strZoneVideoInstream']	= "인라인 비디오 광고";
$GLOBALS['strZoneVideoOverlay']		= "오버레이 비디오 광고";
$GLOBALS['strShowMatchingBanners']		= "매치하는 배너 보기";
$GLOBALS['strHideMatchingBanners']		= "매치하는 배너 숨기기";

$GLOBALS['strBannerLinkedAds']		= "영역에 연결된 배너";
$GLOBALS['strCampaignLinkedAds']		= "영역에 연결된 캠페인";
$GLOBALS['strTotalZones']		= "전체 영역";
$GLOBALS['strInactiveZonesHidden'] 		= "활성화되지 않은 영역이 숨겨짐";
$GLOBALS['strWarnChangeZoneType']		= "영역 형식을 텍스트나 이메일로 변경하면 이 영역 형식의 제한때문에 모든 배너/캠페인의 연결이 해제될 것입니다.<ul>
  <li>텍스트 영역은 오직 텍스트 광고만 연결될 수 있습니다</li>
  <li>이메일 영역 캠페인은 오직 한 번에 하나의 활성 배너를 가질 수 있습니다</li>
</ul>";
$GLOBALS['strWarnChangeZoneSize']		= "영역 크기를 변경하면 새로운 크기가 아닌 배너들은 연결이 해제될 것입니다. 또한 새로운 크기의 연결된 캠페인의 배너들은 추가될 것입니다.";
$GLOBALS['strWarnChangeBannerSize']		= "배너 크기를 변경하면 새로운 크기가 아닌 영역의 이 배너는 연결이 해제될 것이고, 만약 이 배너의 <strong>캠페인</strong>이 새로운 크기의 영역에 연결되어 있다면, 이 배너는 자동으로 연결될 것입니다";
$GLOBALS['strWarnBannerReadonly']		= "이 배너는 확장이 불가능하기 때문에, 읽기전용입니다. 더 자세한 정보는 당신의 Administrator를 참조하세요.";
$GLOBALS['strInventoryForecasting']		= "예상 인벤토리";
$GLOBALS['strZonesOfWebsite']		= "in";
$GLOBALS['strBackToZones']		= "영역으로 돌아가기";
$GLOBALS['strIab']['IAB_FullBanner(468x60)']		= "IAB Full Banner (468 x 60)";
$GLOBALS['strIab']['IAB_Skyscraper(120x600)']		= "IAB Skyscraper (120 x 600)";
$GLOBALS['strIab']['IAB_Leaderboard(728x90)']		= "IAB Leaderboard (728 x 90)";
$GLOBALS['strIab']['IAB_Button1(120x90)']		= "IAB Button 1 (120 x 90)";
$GLOBALS['strIab']['IAB_Button2(120x60)']		= "IAB Button 2 (120 x 60)";
$GLOBALS['strIab']['IAB_HalfBanner(234x60)']		= "IAB Half Banner (234 x 60)";
$GLOBALS['strIab']['IAB_MicroBar(88x31)']		= "IAB Micro Bar (88 x 31)";
$GLOBALS['strIab']['IAB_SquareButton(125x125)']		= "IAB Square Button (125 x 125)";
$GLOBALS['strIab']['IAB_Rectangle(180x150)*']		= "IAB Rectangle (180 x 150)";
$GLOBALS['strIab']['IAB_SquarePop-up(250x250)']		= "IAB Square Pop-up (250 x 250)";
$GLOBALS['strIab']['IAB_VerticalBanner(120x240)']		= "IAB Vertical Banner (120 x 240)";
$GLOBALS['strIab']['IAB_MediumRectangle(300x250)*']		= "IAB Medium Rectangle (300 x 250)";
$GLOBALS['strIab']['IAB_LargeRectangle(336x280)']		= "IAB Large Rectangle (336 x 280)";
$GLOBALS['strIab']['IAB_VerticalRectangle(240x400)']		= "IAB Vertical Rectangle (240 x 400)";
$GLOBALS['strIab']['IAB_WideSkyscraper(160x600)*']		= "IAB Wide Skyscraper (160 x 600)";
$GLOBALS['strIab']['IAB_Pop-Under(720x300)']		= "IAB Pop-Under (720 x 300)";
$GLOBALS['strIab']['IAB_3:1Rectangle(300x100)']		= "IAB 3:1 Rectangle (300 x 100)";
$GLOBALS['strIab']['IAB_Mobile(320x50)']             = "IAB Mobile (320 x 50)";
$GLOBALS['strIab']['IAB_Mobile(768x1024)']           = "IAB Mobile (768 x 1024)";

// Advanced zone settings
$GLOBALS['strAdvanced']				= "고급 설정";
$GLOBALS['strChains']				= "연결";
$GLOBALS['strChainSettings']			= "연결 설정";
$GLOBALS['strZoneNoDelivery']			= "이 영역에서 어떤 배너는 전달할 수 없습니다...";
$GLOBALS['strZoneStopDelivery']			= "연결유지를 중지하고 배너를 표시하지 않습니다.";
$GLOBALS['strZoneOtherZone']			= "선택이 영역을 대신 표시합니다.";
$GLOBALS['strZoneUseKeywords']			= "아래의 입력한 키워드를 사용해서 배너를 선택하세요.";
$GLOBALS['strZoneAppend']			= "이 영역에 연결된 배너의 팝업이나 격자 배너 호출 코드를 항상 추가합니다.";
$GLOBALS['strAppendSettings']			= "배너 첨부 설정";
$GLOBALS['strZonePrependHTML']			= "이 영역에 표시는 텍스트 광고 앞에 HTML 코드를 추가합니다.";
$GLOBALS['strZoneAppendHTML']			= "이 영역에 표시는 텍스트 광고 뒤에 HTML 코드를 추가합니다.";
$GLOBALS['strZoneAppendNoBanner']	= "전달된 배너가 없어도 (앞에)첨부";
$GLOBALS['strZoneAppendType']		= "형식 첨부";
$GLOBALS['strZoneAppendHTMLCode']		= "HTML 코드";
$GLOBALS['strZoneAppendZoneSelection']		= "팝업 혹은 interstitial";
$GLOBALS['strZoneAppendSelectZone'] 		= "이 영역에 연결된 배너를 팝업이나 격자 배너 호출 코드를 항상 추가합니다.";


// Zone probability
$GLOBALS['strZoneProbListChain']		= "선택한 영역에 연결된 배너는 모두 널(null) 우선순위입니다. 영역 연결된 다음과 같습니다.:";
$GLOBALS['strZoneProbNullPri']			= "이 영역에 연결된 배너는 모두 널(null) 우선순위입니다.";
$GLOBALS['strZoneProbListChainLoop']	= "영역 체인을 따라하는 것은 순환 루프를 발생합니다. 이 영역에 대한 전달이 중지됩니다.";


// Linked banners/campaigns
$GLOBALS['strSelectZoneType']			= "연결할 배너의 종류를 선택하세요.";
//$GLOBALS['strBannerSelection']			= "배너 선택";
$GLOBALS['strLinkedBanners']			= "개별 배너에 연결";
//$GLOBALS['strCampaignSelection']		= "캠페인 선택";
$GLOBALS['strCampaignDefaults']			= "캠페인 배너링크";
$GLOBALS['strLinkedCategories']			= "카테고리 배너 링크";
$GLOBALS['strWithXBanners']				= "%d 배너";
$GLOBALS['strInteractive']			= "Interactive";
$GLOBALS['strRawQueryString']			= "키워드";
$GLOBALS['strIncludedBanners']			= "연결된 배너";
$GLOBALS['strLinkedBannersOverview']		= "연결된 배너 개요";
$GLOBALS['strLinkedBannerHistory']		= "연결된 배너 히스토리";
$GLOBALS['strNoZonesToLink']			= "배너와 연결할 수 있는 영역이 없습니다.";
$GLOBALS['strNoBannersToLink']			= "현재 이 영역에 연결할 배너가 없습니다.";
$GLOBALS['strNoLinkedBanners']			= "현재 이 영역에 연결된 배너가 없습니다.";
$GLOBALS['strMatchingBanners']			= "{count} 개의 배너 일치";
$GLOBALS['strNoCampaignsToLink']		= "현재 이 영역에 연결할 캠페인이 없습니다.";
$GLOBALS['strNoTrackersToLink']			= "이 캠페인에 연결할 수 있습니다. 가능한 추적기는 현재 없습니다";
$GLOBALS['strNoZonesToLinkToCampaign']  	= "현재 이 영역에 연결이 캠페인을 없습니다.";
$GLOBALS['strSelectBannerToLink']		= "이 영역에 연결할 배너를 선택하세요:";
//$GLOBALS['strNoTrackersToLink']		= "이 캠페인에 연결할 수 있습니다. 가능한 추적기는 현재 없습니다";
//$GLOBALS['strNoZonesToLinkToCampaign']  		= "현재 이 영역에 연결이 캠페인을 없습니다.";
//$GLOBALS['strSelectBannerToLink']		= "";
$GLOBALS['strSelectBannerOrMarketCampaignToLink']		= "이 영역에 연결할 배너를 선택하거나, OpenX 마켓 광고주를 위해, 연결하고자 하는 캠페인을 선택하세요:";
$GLOBALS['strSelectCampaignToLink']		= "이 영역에 연결할 캠페인 선택합니다:";
$GLOBALS['strSelectAdvertiser']				= "광고주 선택";
$GLOBALS['strSelectPlacement']				= "캠페인 선택";
$GLOBALS['strSelectAd']						= "배너 선택";
$GLOBALS['strSelectPublisher']				= "웹사이트 선택";
$GLOBALS['strSelectZone']					= "영역 선택";
//$GLOBALS['strTrackerCode']					= "각 자바스크립트 추적기 노출에 다음의 코드 첨부";
$GLOBALS['strTrackerCodeSubject']			= "추적기 코드 첨부";
$GLOBALS['strAppendTrackerNotPossible']		= "추적기를 첨부할 수 없습니다.";
$GLOBALS['strStatusPending']				= "보류";
$GLOBALS['strStatusApproved']				= "승인";
$GLOBALS['strStatusDisapproved']			= "미승인";
$GLOBALS['strStatusDuplicate'] 				= "복제";
$GLOBALS['strStatusOnHold']					= "보류";
$GLOBALS['strStatusIgnore']					= "무시";
$GLOBALS['strConnectionType']				= "형식";
$GLOBALS['strConnTypeSale'] 				= "판매";
$GLOBALS['strConnTypeLead']					= "Lead";
$GLOBALS['strConnTypeSignUp']				= "가입";
$GLOBALS['strShortcutEditStatuses']			= "상태 수정";
$GLOBALS['strShortcutShowStatuses']			= "상태 보기";


// Statistics
$GLOBALS['strStats'] 					= "보고서";
$GLOBALS['strNoStats']					= "현재 사용할 수 있는 보고서가 없습니다.";
$GLOBALS['strNoTargetingStats'] 		= "현재 사용할 수 있는 타겟팅 보고서가 없습니다.";
$GLOBALS['strNoStatsForPeriod']			= "%s ~ %s 동안 이용가능한 통계가 없습니다.";
$GLOBALS['strNoTargetingStatsForPeriod']		= "%s ~ %s 동안 이용가능한 타겟팅 통계가 없습니다.";
$GLOBALS['strConfirmResetStats']		= "모든 보고서를 삭제하시겠습니까?";
$GLOBALS['strGlobalHistory']			= "전체 보고서";
$GLOBALS['strDailyHistory']				= "일일 히스토리";
$GLOBALS['strDailyStats'] 				= "일일 보고서";
$GLOBALS['strWeeklyHistory']			= "주간 보고서";
$GLOBALS['strMonthlyHistory']			= "월별 보고서";
$GLOBALS['strCreditStats'] 				= "보장 보고서";
$GLOBALS['strDetailStats'] 				= "상세 보고서";
$GLOBALS['strTotalThisPeriod']			= "기간 합계";
$GLOBALS['strAverageThisPeriod']		= "기간 평균";
$GLOBALS['strPublisherDistribution']		= "웹사이트 보고서";
$GLOBALS['strCampaignDistribution']		= "캠페인 보고서";
$GLOBALS['strDistributionBy']			= "Distribution by";
//$GLOBALS['strDistribution']				= "분포";
$GLOBALS['strResetStats'] 				= "보고서 초기화";
$GLOBALS['strSourceStats']				= "소스 보고서";
$GLOBALS['strSources'] 					= "소스";
$GLOBALS['strAvailableSources'] 		= "사용가능한 소스";
$GLOBALS['strSelectSource']		= "확인할 소스를 선택하세요:";
$GLOBALS['strSizeDistribution']		= "크기별 분포";
$GLOBALS['strCountryDistribution']		= "국가별 분포";
$GLOBALS['strEffectivity']		= "유효성";
$GLOBALS['strTargetStats']		= "타겟팅 통계";
$GLOBALS['strCampaignTarget']		= "타겟";
$GLOBALS['strTargetRatio']		= "타겟 비율";
$GLOBALS['strTargetModifiedDay']		= "포함된 하루 내 타겟이 수정되었습니다. 타겟팅이 정확하지 않을 수 있습니다.";
$GLOBALS['strTargetModifiedWeek']		= "포함된 주중 타겟이 수정되었습니다. 타겟팅이 정확하지 않을 수 있습니다.";
$GLOBALS['strTargetModifiedMonth']		= "포함된 한달 내 타겟이 수정되었습니다. 타겟팅이 정확하지 않을 수 있습니다.";
$GLOBALS['strNoTargetStats']		= "현재는 이용가능한 타겟팅에 대한 통계가 없습니다";
//$GLOBALS['strOVerall']					= "전체";
$GLOBALS['strByZone']					= "By Zone";
$GLOBALS['strImpressionsRequestsRatio']		= "뷰 요청률 (%)";
$GLOBALS['strViewBreakdown']			= "View by";
$GLOBALS['strBreakdownByDay'] 			= "일";
$GLOBALS['strBreakdownByWeek'] 			= "주";
$GLOBALS['strBreakdownByMonth'] 		= "월";
$GLOBALS['strBreakdownByDow']			= "요일";
$GLOBALS['strBreakdownByHour'] 			= "시";
$GLOBALS['strItemsPerPage']				= "페이지 당 아이템";
$GLOBALS['strDistributionHistory']		= "분산 보고서";
$GLOBALS['strDistributionHistoryCampaign']		= "분산 보고서 (캠페인)";
$GLOBALS['strDistributionHistoryBanner']		= "분산 보고서 (배너)";
$GLOBALS['strDistributionHistoryWebsite']		= "분산 보고서 (웹사이트)";
$GLOBALS['strDistributionHistoryZone']		= "분산 보고서 (영역)";
$GLOBALS['strShowGraphOfStatistics']		= "통계 그래프 보기<u>G</u>";
$GLOBALS['strExportStatisticsToExcel']		= "통계 엑셀로 추출<u>E</u>";
$GLOBALS['strGDnotEnabled']		= "그래프를 보기 위해서 반드시 PHP에서 GD를 활성화시켜야 합니다. <br/>서버에 GD를 설치하는 방법을 포함하여 더 자세한 정보는 <a href='http://www.php.net/gd' target='_blank'>http://www.php.net/gd</a>를 확인하세요.";
$GLOBALS['strTTFnotEnabled']		= "PHP에서 GD를 활성화시켜야 하지만 FreeType 지원에 문제가 있습니다. <br/>그래프를 보려면 FreeType이 필요합니다. <br/>서버 환경설정을 확인하세요.";
$GLOBALS['strStatsArea']		= "Area";
$GLOBALS['strImpressions_short']                = "임프레션";
$GLOBALS['strClicks_short']                     = "클릭";
$GLOBALS['strRevenue_short']                    = "수익";

// Hosts
$GLOBALS['strHosts']				= "호스트";
$GLOBALS['strTopHosts'] 			= "요청 호스트 순위";
$GLOBALS['strTopCountries'] 		= "요청 국가 순위";
$GLOBALS['strRecentHosts'] 			= "최근 요청 호스트";


// Expiration
$GLOBALS['strExpired']				= "만료날짜";
$GLOBALS['strExpiration'] 			= "만료날짜";
$GLOBALS['strNoExpiration'] 			= "만료날짜 설정없음";
$GLOBALS['strEstimated'] 			= "예상 만료날짜";
$GLOBALS['strNoExpirationEstimation']		= "만료날짜를 예측할 수 없습니다.";
$GLOBALS['strDaysAgo']					= "며칠 전";
$GLOBALS['strCampaignStop']				= "캠페인 중지";



// Reports
$GLOBALS['strReports']				= "보고서";
$GLOBALS['strAdvancedReports']		= "고급 보고서";
$GLOBALS['strAdminReports']			= "어드민 보고서";
$GLOBALS['strAdvertiserReports']		= "광고주 보고서";
$GLOBALS['strAgencyReports']		= "에이전시 보고서";
$GLOBALS['strPublisherReports']		= "웹사이트 보고서";
$GLOBALS['strSelectReport']			= "생성할 보고서를 선택하세요.";
$GLOBALS['strStartDate']			= "시작일";
$GLOBALS['strEndDate']				= "종료일";
$GLOBALS['strNoData']				= "이 기간동안 사용할 수 있는 데이터가 없습니다.";
$GLOBALS['strPeriod']				= "기간";
$GLOBALS['strLimitations']			= "제한";
$GLOBALS['strWorksheets']			= "워크시트";
$GLOBALS['strAdminReportsText']			= "관리자 계정 명세";
$GLOBALS['strAdminReportsDetail']		= "목록 Adviews / AdClicks / AdSales는 주어진 달 동안 합계.";


// Admin_UI_Fields
$GLOBALS['strAllAdvertisers']            = "모든 광고주";
$GLOBALS['strAnonAdvertisers']           = "익명 광고주";
$GLOBALS['strAllPublishers']             = "모든 웹사이트";
$GLOBALS['strAnonPublishers']            = "익명 웹사이트";
$GLOBALS['strAllAvailZones']             = "모든 사용가능한 영역";


// Userlog
$GLOBALS['strUserLog']					= "사용자 로그";
$GLOBALS['strUserLogDetails']			= "사용자 로그 항목";
$GLOBALS['strDeleteLog']				= "로그 삭제";
$GLOBALS['strAction']					= "활동 기록";
$GLOBALS['strNoActionsLogged']			= "기록할 내용이 없습니다.";


// Code generation
$GLOBALS['strGenerateBannercode']		= "광고코드 생성";
$GLOBALS['strChooseInvocationType']		= "생성할 코드 형식을 선택하세요";
$GLOBALS['strGenerate']				= "생성하기";
$GLOBALS['strParameters']			= "Tag 세팅";
$GLOBALS['strFrameSize']			= "프레임 크기";
$GLOBALS['strBannercode']			= "광고코드";
$GLOBALS['strTrackercode']			= "추적코드";
$GLOBALS['strOptional']				= "옵션";
$GLOBALS['strBackToTheList']		= "리포트 리스트로 돌아가기";
$GLOBALS['strGoToReportBuilder']	= "리포트 선택으로 돌아가기";
$GLOBALS['strCharset']				= "언어선택";
$GLOBALS['strAutoDetect']			= "자동선택";
$GLOBALS['strCacheBusterComment']		= "  * 생성된 임의의 숫자 (또는 타임스템프)와
  * {random}의 인스턴트를 모두 변경합니다.
  *";
$GLOBALS['strSSLBackupComment']		= "* 이 태그의 백업이미지 섹션은 non-SSL 페이지에서 사용하기 위해 생성되었습니다.
  * 만약 이 태그를 SSP페이지에 배치할 경우 다음과 같이 변경합니다.
  * 'http://%s/...' 
  * 에서 
  * 'https://%s/...'
  *";
$GLOBALS['strSSLDeliveryComment']		= "  * 이 태그는 non-SSL 페이지에서 사용하기 위해 생성되었습니다.
  * 만약 이 태그를 SSP페이지에 배치할 경우 다음과 같이 변경합니다.
  * 'http://%s/...'
  * 에서
  * 'https://%s/...'
  *";
$GLOBALS['strThirdPartyComment']		= "* 이 광고를 제3자 광고서버를 통해 전달해야 하는 경우, 
클릭 추적 URL에 '{clickurl}'텍스트를 대체하는 것을 잊지 마십시오.";


// Errors
$GLOBALS['strMySQLError'] 			= "SQL 오류:";
$GLOBALS['strErrorDatabaseConnetion']		= "DB 연결 오류.";
$GLOBALS['strErrorCantConnectToDatabase']		= "치명적인 오류가 발생하여 데이터베이스에 연결할 수 없습니다. 
관리자 인터페이스를 사용할 수 없으므로 배너 노출도 영향을 받을 수 있습니다.
이 문제의 원인은 다음과 같습니다.
<ul>
<Li>데이터베이스 서버가 지금 작동하지 않는 경우</li>
<li>데이터베이스 서버의 위치가 변경된 경우</li>
<li>데이터베이스 서버에 연결하는 사용자이름과 비밀번호가 일치하지 않는 경우</li>
<li>PHP에서 MySQL 확장프로그램이 로딩되지 않는 경우</li>
</ul>";
$GLOBALS['strLogErrorClients'] 			= "[phpAds] 데이터베이스에서 광고주를 가져오는 동안 오류가 발생했습니다..";
$GLOBALS['strLogErrorBanners'] 			= "[phpAds] 데이터베이스에서 배너를 가져오는 동안 오류가 발생했습니다..";
$GLOBALS['strLogErrorViews'] 			= "[phpAds] 데이터베이스에서 AdView를 가져오는 동안 오류가 발생했습니다..";
$GLOBALS['strLogErrorClicks'] 			= "[phpAds] 데이터베이스에서 AdClick를 가져오는 동안 오류가 발생했습니다.";
$GLOBALS['strErrorViews'] 			= "뷰 횟수를 입력하거나 제한하지 않음 상자를 체크해야합니다!";
$GLOBALS['strLogErrorConversions']		= "[phpAds] 데이터베이스에서 전환을 가져오는 동안 오류가 발생했습니다.";
$GLOBALS['strErrorViews'] 		= "노출수를 입력하거나 제한하지 않음 상자를 체크해야합니다!";
$GLOBALS['strErrorNegViews'] 		= "부정적인 노출은 허용되지 않습니다";
$GLOBALS['strErrorClicks'] 		= "클릭수를 입력하거나 제한하지 않음 상자를 체크해야합니다!";
$GLOBALS['strErrorNegClicks'] 		= "부정한 클릭(negative click)을 허용하지 않습니다.";
$GLOBALS['strErrorConversions']		= "전환 수를 입력하거나 무제한 박스를 선택해야합니다!";
$GLOBALS['strErrorNegConversions']		= "전환 수에 음수는 허용되지 않습니다.";
$GLOBALS['strNoMatchesFound']		= "검색 결과가 없습니다.";
$GLOBALS['strErrorOccurred']		= "오류가 발생했습니다.";
$GLOBALS['strErrorUploadSecurity']		= "보안 문제가 발견되었습니다. 업로드를 중지합니다 !";
$GLOBALS['strErrorUploadBasedir']		= "업로드한 파일에 액세스할 수 없습니다. 안전 모드 또는 open_basedir 제한 때문일 수 있습니다.";
$GLOBALS['strErrorUploadUnknown']		= "알 수 없는 경유로 업로드한 파일에 액세스할 수 없습니다. PHP 설정을 확인하십시오.";
$GLOBALS['strErrorStoreLocal']		= "로컬 디렉터리로 배너를 저장하는 동안 오류가 발생했습니다. 로컬 디렉터리 경로 설정을 잘못되었을 수 있습니다.";
$GLOBALS['strErrorStoreFTP']		= "FTP 서버로 배너를 업로드하는 동안 오류가 발생했습니다. 서버를 사용할 수 없거나 FTP 서버 설정을 잘못되었을 수 있습니다.";
$GLOBALS['strErrorDBPlain']		= "데이터베이스를 액세스 하는 동안 에러가 발생했습니다.";
$GLOBALS['strErrorDBSerious']		= "데이터베이스에 심각한 문제가 발견되었습니다.";
$GLOBALS['strErrorDBNoDataPlain']		= "데이터베이스 문제로 인해 ".MAX_PRODUCT_NAME." 데이터를 검색하거나 저장할 수 없습니다.";
$GLOBALS['strErrorDBNoDataSerious']		= "데이터베이스 심각한 문제로 인해 ".MAX_PRODUCT_NAME." 데이터를 검색 할 수 없습니다.";
$GLOBALS['strErrorDBCorrupt']		= "데이터베이스 테이블이 손상되어 복구해야합니다. 복구 손상된 테이블에 대한 자세한 내용은 장을 참조하십시오 <i> 문제 해결 </i> <i> 관리자 안내서 </i>.";
$GLOBALS['strErrorDBContact']		= "서버 관리자에게 문의하여 문제를 알려주시기 바랍니다.";
$GLOBALS['strErrorDBSubmitBug']		= "이 문제가 반복된다면, ".MAX_PRODUCT_NAME."버그로 인해 발생 될 수 있습니다.".MAX_PRODUCT_NAME."제작자에게 다음의 정보를 알려주십시오.";
$GLOBALS['strMaintenanceNotActive']		= "유지보수 스크립트가 지난 24시간동안 실행되지 않았습니다.".MAX_PRODUCT_NAME."가 제대로 작동하려면 항상 실행되어야 합니다. \n\n유지보수 스크립트를 구성하는 방법에 대한 자세한 내용은 관리자 안내서를 참조하시기 바랍니다.";
$GLOBALS['strErrorBadUserType']		= "시스템에서 귀하의 사용자 계정 유형을 확인 할 수 없습니다.";
$GLOBALS['strErrorLinkingBanner']		= "다음 이유로 해당 영역에 배너를 연결할 수 없습니다 :";
$GLOBALS['strUnableToLinkBanner']		= "배너 연결 오류 : ";
$GLOBALS['strErrorEditingCampaign']		= "캠페인 업데이트 오류 :";
$GLOBALS['strUnableToChangeCampaign']		= "다음 이유로 캠페인을 변경할 수 없습니다 :";
$GLOBALS['strErrorEditingCampaignRevenue'] 		= "수익 항목에 잘못된 숫자 형식이 있습니다.";
$GLOBALS['strErrorEditingCampaignECPM']		= "ECPM 항목에 잘못된 숫자 형식이 있습니다.";
$GLOBALS['strErrorEditingZone']			= "영역 업데이트 오류 :";
$GLOBALS['strUnableToChangeZone']		= "다음 이유로 영역을 변경할 수 없습니다 : ";
$GLOBALS['strDatesConflict']			= "연결하려는 캠페인의 날짜와 이미 연결된 캠페인의 날짜가 중복됩니다.";
$GLOBALS['strEmailNoDates']			= "이메일 영역에 연결된 캠페인은 반드시 시작날짜와 종료일이 설정되어 있어야 합니다.";
$GLOBALS['strWarningInaccurateStats']		= "이 통계중 일부가 UTC 시간대가 아닌 상태로 기록되어 올바른 시간대로 표시되지 않을 수 있습니다.";
$GLOBALS['strWarningInaccurateReadMore']	= "좀 더 알아보기";
$GLOBALS['strWarningInaccurateReport']		= "이 보고서의 통계중 일부가 UTC 시간대가 아닌 상태로 기록되어 올바른 시간대로 표시되지 않을 수 있습니다.";


// Validation
$GLOBALS['strRequiredFieldLegend']		= "필수 항목 표시";
$GLOBALS['strFormContainsErrors']		= "오류가 발생했습니다. 아래 표시된 항목을 수정하십시오.";
$GLOBALS['strRequiredField']			= "필수 항목";
$GLOBALS['strXRequiredField']			= "%s는(은) 필수입니다.";
$GLOBALS['strMaxLengthField']			= "최대 문자를 입력하세요.";
$GLOBALS['strEmailField']				= "유효한 이메일주소를 입력하세요.";
$GLOBALS['strNumericField']				= "숫자만 입력이 가능합니다.";
$GLOBALS['strGreaterThanZeroField']		= "0보다 커야 합니다.";
$GLOBALS['strXGreaterThanZeroField']				= "%s는(은) 0보다 커야 합니다.";
$GLOBALS['strXPositiveWholeNumberField']			= "%s는(은) 양의 정수 이어야 합니다.";
$GLOBALS['strXUniqueField']							= "%s와 %s는(은) 이미 존재합니다.";
$GLOBALS['strXDecimalFieldWithDecimalPlaces']		= "최대 자리수에 맞게 소수점을 넣어주세요.";
$GLOBALS['strInvalidWebsiteURL']					= "잘못된 웹사이트 URL입니다.";


// E-mail
$GLOBALS['strSirMadam']				= "님";
$GLOBALS['strMailSubject'] 			= "광고주 보고서";
$GLOBALS['strAdReportSent']			= "광고주 보고서를 보냈습니다";
//$GLOBALS['strMailSubjectDeleted'] 		= "배너 운영중지";
$GLOBALS['strMailHeader'] 			= "{contact}님,\n";
$GLOBALS['strMailBannerStats'] 			= "{clientname}의 배너 보고서는 다음과 같습니다.";
$GLOBALS['strMailBannerActivatedSubject']		= "캠페인 활성화";
$GLOBALS['strMailBannerDeactivatedSubject']		= "캠페인 비활성화";
$GLOBALS['strMailBannerActivated']		= "캠페인 활성화 날짜가 시작되어 캠페인이 활성화 되었습니다.";
$GLOBALS['strMailBannerDeactivated']		= "캠페인이 다음 이유로 비활성화 되었습니다.";
$GLOBALS['strMailFooter'] 			= "감사합니다.\n   {adminfullname}";
$GLOBALS['strMailClientDeactivated'] 		= "다음 배너는 다음 경유로 사용할 수 없습니다.";
$GLOBALS['strMailNothingLeft'] 			= "웹 사이트를 계속 하고자한다면 담당자에게 문의하십시오.";
$GLOBALS['strClientDeactivated']		= "이 캠페인을 현재 다음과 같은 경유로 운영하지 않습니다.";
$GLOBALS['strBeforeActivate']			= "아직 시작날짜가 아닙니다.";
$GLOBALS['strAfterExpire']			= "유효 기간에 도달했습니다";
$GLOBALS['strNoMoreImpressions']		= "남아있는 노출이 없습니다";
$GLOBALS['strNoMoreClicks']			= "남아있는 AdClicks이 없습니다.";
//$GLOBALS['strNoMoreViews']			= "남아있는 AdViews이 없습니다.";
$GLOBALS['strNoMoreConversions']		= "남아있는 Sales가 없습니다.";
$GLOBALS['strWeightIsNull']			= "가중치는 0으로 설정되어 있습니다.";
$GLOBALS['strRevenueIsNull']			= "수익은 0으로 설정되어 있습니다.";
$GLOBALS['strTargetIsNull']			= "일일 한계가 0으로 설정되어 있습니다. 특정 종료날짜와 한계를 설정하거나 일일 한계 설정을 입력해야 합니다.";
$GLOBALS['strWarnClientTxt']			= "배너의 남아있는 AdClciks 수는 AdViews에 {limit}입니다. \n남아있는 AdCliks나 AdViews가 없을 때 배너를 운영 중지합니다. ";
$GLOBALS['strImpressionsClicksConversionsLow']		= "노출수/클릭수/전환이 낮습니다.";
//$GLOBALS['strViewsClicksLow']						= "AdViews/AdClicks이 낮습니다.";
$GLOBALS['strNoViewLoggedInInterval']   			= "이 기간 동안에 보고서를 기록한 AdViews가 없습니다.";
$GLOBALS['strNoClickLoggedInInterval']  			= "이 기간 동안에 보고서를 기록한 AdClicks가 없습니다.";
$GLOBALS['strNoConversionLoggedInInterval']			= "이 기간 동안에 보고서를 기록한 전환이 없습니다.";
$GLOBALS['strMailReportPeriod']						= "이 보고서를는 {startdate}에서 {enddate}까지의 통계를 포함하고 있습니다.";
$GLOBALS['strMailReportPeriodAll']					= "이 보고서를는 {enddate}까지의 통계를 포함하고 있습니다.";
$GLOBALS['strNoStatsForCampaign'] 					= "이 캠페인에서 사용할 수 있는 보고서가 없습니다.";
$GLOBALS['strImpendingCampaignExpiry']				= "캠페인 만료 임박";
$GLOBALS['strYourCampaign']							= "캠페인";
$GLOBALS['strTheCampiaignBelongingTo']				= "캠페인이 다음에 속해있습니다.";
$GLOBALS['strImpendingCampaignExpiryDateBody']		= "{clientname}는(은) {date}에 종료될 예정입니다.";
$GLOBALS['strImpendingCampaignExpiryImpsBody']		= "{clientname}는(은) {limit}회의 남아있는 노출수보다 적습니다.";
$GLOBALS['strImpendingCampaignExpiryBody']			= "캠페인은 곧 자동으로 비활성화 되며, 캠페인의 배너도 함께 중지 됩니다.";

// Priority
$GLOBALS['strPriority']				= "우선순위";
$GLOBALS['strSourceEdit']			= "소스 수정";


// Preferences
$GLOBALS['strMyAccount']			= "내 계정";
$GLOBALS['strConfiguration']		= "구성";
$GLOBALS['strMainPreferences']		= "기본 환경설정";
$GLOBALS['strAccountPreferences']		= "계정 설정";
$GLOBALS['strCampaignEmailReportsPreferences']		= "캠페인 이메일 보고서 설정";
$GLOBALS['strTimezonePreferences']		= "시간대 설정";
$GLOBALS['strAdminEmailWarnings']		= "관리자 이메일 알림";
$GLOBALS['strAgencyEmailWarnings']		= "에이전시 이메일 알림";
$GLOBALS['strAdveEmailWarnings']		= "광고주 이메일 알림";
$GLOBALS['strFullName']					= "전체이름";
$GLOBALS['strEmailAddress']				= "이메일 주소";
$GLOBALS['strUserDetails']				= "사용자 정보";
$GLOBALS['strLanguageTimezone']			= "언어 및 시간대";
$GLOBALS['strLanguageTimezonePreferences']		= "언어 및 시간대 설정";
$GLOBALS['strUserInterfacePreferences']		= "사용자 인터페이스 설정";
$GLOBALS['strPluginPreferences']			= "플러그인 환경설정";
$GLOBALS['strInvocationPreferences']		= "광고 코드생성 설정";
$GLOBALS['strColumnName']				= "열 이름";
$GLOBALS['strShowColumn']				= "열 표시";
$GLOBALS['strCustomColumnName']			= "사용자 지정 열 이름";
$GLOBALS['strColumnRank']				= "열 순위";
$GLOBALS['strUserPreferences']			= "사용자 기본 설정";
$GLOBALS['strChangePassword']			= "비밀번호 변경";
$GLOBALS['strChangeEmail']				= "이메일 변경";
$GLOBALS['strCurrentPassword']			= "현재 비밀번호";
$GLOBALS['strChooseNewPassword']		= "새 비밀번호 선택";
$GLOBALS['strReenterNewPassword']		= "새 비밀번호 재 입력";
$GLOBALS['strNameLanguage']				= "이름 및 언어";

// Statistics columns
// Long names
$GLOBALS['strRevenue']			= "수익 ";
$GLOBALS['strNumberOfItems']		= "항목 수";
$GLOBALS['strRevenueCPC']		= "수익 CPC";
$GLOBALS['strERPM']				= "ERPM";
$GLOBALS['strERPC']				= "ERPC";
$GLOBALS['strERPS']				= "ERPS";
$GLOBALS['strEIPM']				= "EIPM";
$GLOBALS['strEIPC']				= "EIPC";
$GLOBALS['strEIPS']				= "EIPS";
$GLOBALS['strECPM']				= "eCPM";
$GLOBALS['strECPC']				= "ECPC";
$GLOBALS['strECPS']				= "ECPS";
$GLOBALS['strEPPM']				= "EPPM";
$GLOBALS['strEPPC']				= "EPPC";
$GLOBALS['strEPPS']				= "EPPS";
$GLOBALS['strPendingConversions']		= "보류중인 변환";
$GLOBALS['strImpressionSR']		= "노출수 SR";
$GLOBALS['strClickSR']			= "클릭수 SR";
$GLOBALS['strRequiredImpressions']		= "필요한 노출수";
$GLOBALS['strRequestedImpressions']		= "요청 노출수";
$GLOBALS['strActualImpressions']		= "노출수";
$GLOBALS['strZoneForecast']			= "영역 예측";
$GLOBALS['strZonesForecast']		= "영역 합계 예측";
$GLOBALS['strZoneImpressions']		= "영역 노출수";
$GLOBALS['strZonesImpressions']		= "영역 노출수 합계";


// Settings
$GLOBALS['strSettings'] 			= "설정";
$GLOBALS['strGeneralSettings']			= "일반 설정";
$GLOBALS['strMainSettings']			= "설정";
$GLOBALS['strAdminSettings']			= "관리 설정";


// Product Updates
$GLOBALS['strProductUpdates']			= "제품 업데이트";
$GLOBALS['strViewPastUpdates']		= "지난 업데이트 및 백업 관리";
$GLOBALS['strFromVersion']		= "시작버전";
$GLOBALS['strToVersion']		= "버전";
$GLOBALS['strToggleDataBackupDetails']		= "데이터 백업 세부정보";
$GLOBALS['strClickViewBackupDetails']		= "백업 세부정보를 확인하시려면 클릭하세요";
$GLOBALS['strClickHideBackupDetails']		= "백업 세부정보를 숨기시려면 클릭하세요";
$GLOBALS['strShowBackupDetails']		= "데이터 백업 세부정보 표시";
$GLOBALS['strHideBackupDetails']		= "데이터 백업 세부정보 숨김";
$GLOBALS['strInstallation']		= "설치";
$GLOBALS['strBackupDeleteConfirm']		= "정말로 생성된 모든 백업을 삭제하시겠습니까?";
$GLOBALS['strDeleteArtifacts']		= "Artifacts 삭제";
$GLOBALS['strArtifacts']		= "Artifacts";
$GLOBALS['strBackupDbTables']		= "데이터베이스 테이블 백업";
$GLOBALS['strLogFiles']		= "로그파일";
$GLOBALS['strConfigBackups']		= "컨퍼런스 백업";
$GLOBALS['strUpdatedDbVersionStamp']		= "업데이트 된 데이터베이스 버전 확인";
$GLOBALS['aProductStatus']['UPGRADE_COMPLETE']		= "업그레이드 완료";
$GLOBALS['aProductStatus']['UPGRADE_FAILED']		= "업그레이드 실패";


// Agency
$GLOBALS['strAgencyManagement']		= "계정관리";
$GLOBALS['strAgency']		= "계정";
$GLOBALS['strAgencies']		= "계정";
$GLOBALS['strAddAgency']		= "새 계정 추가";
$GLOBALS['strAddAgency_Key']		= "새 계정 추가<u>n</u>";
$GLOBALS['strTotalAgencies']		= "총 계정";
$GLOBALS['strAgencyProperties']		= "계정 등록정보";
$GLOBALS['strNoAgencies']		= "현재 정의된 계정이 없습니다.";
$GLOBALS['strConfirmDeleteAgency']		= "정말 이 계정을 삭제하시겠습니까?";
$GLOBALS['strHideInactiveAgencies']		= "비활성 계정 숨기기";
$GLOBALS['strInactiveAgenciesHidden']		= "비활성 계정이 숨겨져 있습니다.";
$GLOBALS['strAllowAgencyEditConversions']		= "사용자가 전환을 수정하는 것을 허용합니다.";
$GLOBALS['strAllowMoreReports']		= "추가 리포트 버튼을 허용합니다.";
$GLOBALS['strSwitchAccount']		= "계정 전환";




/*********************************************************/
/* Keyboard shortcut assignments                         */
/*********************************************************/


// Reserved keys
// Do not change these unless absolutely needed
$GLOBALS['keyHome']			= 'h';
$GLOBALS['keyUp']			= 'u';
$GLOBALS['keyNextItem']		= '.';
$GLOBALS['keyPreviousItem']	= ',';
$GLOBALS['keyList']			= 'l';


// Other keys
// Please make sure you underline the key you
// used in the string in default.lang.php
$GLOBALS['keySearch']		= 's';
$GLOBALS['keyCollapseAll']	= 'c';
$GLOBALS['keyExpandAll']	= 'e';
$GLOBALS['keyAddNew']		= 'n';
$GLOBALS['keyNext']			= 'n';
$GLOBALS['keyPrevious']		= 'p';



// Note: New translations not found in original lang files but found in CSV
$GLOBALS['strStatusDuplicate'] = "복제";
$GLOBALS['strPriorityOptimisation'] = "기타";
$GLOBALS['strCollectedAllStats'] = "모든 통계";
$GLOBALS['strVariableDescription'] = "설명";
$GLOBALS['strDuplicateAgencyName'] = "입력한 ID가 이미 있습니다. 다른 ID를 입력하세요.";
$GLOBALS['strBreakdownByDay'] = "일";
$GLOBALS['strBreakdownByWeek'] = "주";
$GLOBALS['strSingleMonth'] = "월";
$GLOBALS['strBreakdownByMonth'] = "월";
$GLOBALS['strBreakdownByHour'] = "시";
$GLOBALS['strHiddenAdvertiser'] = "광고주";
$GLOBALS['strHiddenZone'] = "영역";
$GLOBALS['strTrackerOverview'] = "배너 목록";
$GLOBALS['strAddTracker_Key'] = "새 배너 추가(<u>n</u>)";
$GLOBALS['strConfirmDeleteAllTrackers'] = "해당 광고주의 모든 캠페인을 삭제합니까?";
$GLOBALS['strConfirmDeleteTracker'] = "해당 배너를 삭제합니까??";
$GLOBALS['strDeleteAllTrackers'] = "모든 배너 삭제";
$GLOBALS['strTrackerProperties'] = "배너 등록 정보";
$GLOBALS['strUploadOrKeepAlt'] = "현재 이미지를 허용하거나<br /> 다른 이미지를 업로드할<br /> 수 있습니다.";
$GLOBALS['strChannelLimitations'] = "전달유지 옵션";
$GLOBALS['strWeekDays'] = "주중(월-금)";
$GLOBALS['strAffiliateInvocation'] = "광고코드";
$GLOBALS['strInactiveAffiliatesHidden'] = "배너가 숨겨져 있습니다.";
$GLOBALS['strInactiveZonesHidden'] = "배너가 숨겨져 있습니다.";
$GLOBALS['strZoneAppendSelectZone'] = "이 영역에 연결된 배너를 팝업이나 격자 배너 호출 코드를 항상 추가합니다.";
$GLOBALS['strNoTrackersToLink'] = "현재 이 영역에 연결할 캠페인이 없습니다.";
$GLOBALS['strConnTypeSale'] = "저장";
$GLOBALS['strNoTargetingStats'] = "현재 사용할 수 있는 통계가 없습니다.";
$GLOBALS['strAllAdvertisers'] = "모든 광고주 수";
$GLOBALS['strLogErrorConversions'] = "[phpAds] 데이터베이스에서 AdView를 가져오는 동안 오류가 발생했습니다..";
$GLOBALS['strMailBannerActivatedSubject'] = "캠페인 {id} 활성화";
$GLOBALS['strMailBannerDeactivatedSubject'] = "캠페인 {id} 활성화";
$GLOBALS['strNoMoreConversions'] = "남아있는 AdClicks이 없습니다.";
$GLOBALS['strNoConversionLoggedInInterval'] = "이 기간 동안에 보고서를 기록한 AdViews가 없습니다.";
$GLOBALS['strAddAgency_Key'] = "새 영역 추가(<u>n</u>)";
$GLOBALS['strNoAgencies'] = "현재 등록된 영역이 없습니다.";
$GLOBALS['strConfirmDeleteAgency'] = "이 영역을 삭제합니까?";
$GLOBALS['strInactiveAgenciesHidden'] = "배너가 숨겨져 있습니다.";
$GLOBALS['strNoChannels'] = "현재 등록된 배너가 없습니다.";
$GLOBALS['strConfirmDeleteChannel'] = "해당 배너를 삭제합니까??";
$GLOBALS['strUserProperties'] = "배너 등록 정보";
$GLOBALS['strNoAdminInterface'] = "서비스를 사용할 수 없습니다.";
$GLOBALS['strOverallAdvertisers'] = "광고주";
$GLOBALS['strLinkUserHelpUser'] = "사용자ID";
$GLOBALS['strPasswordRepeat'] = "비밀번호 확인";
$GLOBALS['strCampaignStatusDeleted'] = "삭제";
$GLOBALS['strCampaignStop'] = "캠페인 기록";
$GLOBALS['strCheckForUpdates'] = "업데이트 검색";
$GLOBALS['strGlobalSettings'] = "일반 설정";
$GLOBALS['strFinanceCTR'] = "클릭율";
$GLOBALS['strAdvertiserCampaigns'] = "광고주 & 캠페인";
$GLOBALS['strCampaignStatusInactive'] = "사용 가능";
$GLOBALS['strCampaignType'] = "캠페인 형식";
$GLOBALS['strContract'] = "연락처";
$GLOBALS['strStandardContract'] = "제한 캠페인";
$GLOBALS['strWebsiteZones'] = "광고게시판 & 영역";
$GLOBALS['strConfirmDeleteClients'] = "해당 광고주를 삭제합니까?";
$GLOBALS['strConfirmDeleteCampaigns'] = "이 캠페인을 정말로 삭제합니까?";
$GLOBALS['strConfirmDeleteTrackers'] = "해당 배너를 삭제합니까??";
$GLOBALS['strConfirmDeleteBanners'] = "해당 배너를 삭제합니까??";
$GLOBALS['strConfirmDeleteAffiliates'] = "해당 광고게시판를 삭제합니까?";
$GLOBALS['strConfirmDeleteZones'] = "이 영역을 삭제합니까?";
$GLOBALS['strID_short'] = "ID";
$GLOBALS['strCTR_short'] = "클릭율";
$GLOBALS['strConfirmDeleteChannels'] = "해당 배너를 삭제합니까??";
$GLOBALS['strSite'] = "크기";
$GLOBALS['strHiddenWebsite'] = "광고게시판";
$GLOBALS['strYouHaveNoCampaigns'] = "광고주 & 캠페인";
$GLOBALS['strHideInactiveOverview'] = "사용하지 않는 항목록은 모든 목록 페이지에서 숨깁니다.";
$GLOBALS['strHiddenPublisher'] = "광고게시판";
$GLOBALS['strClick-ThroughRatio'] = "클릭율";
$GLOBALS['strPreference'] = "설정";
$GLOBALS['strDeliveryLimitation'] = "전달유지 제한";
$GLOBALS['str_ID'] = "ID";
$GLOBALS['str_CTR'] = "클릭율";

// Global Settings
$GLOBALS['strPlugins']		= "플러그인";
$GLOBALS['strChooseSection']		= "섹션 선택";

// Finance
$GLOBALS['strFinanceCPM']		= "CPM";
$GLOBALS['strFinanceCPC']		= "CPC";
$GLOBALS['strFinanceCPA']		= "CPA";
$GLOBALS['strFinanceMT']		= "Tenancy";
$GLOBALS['strFinanceCTR'] 		= "클릭율";
$GLOBALS['strFinanceCR']		= "CR";
$GLOBALS['strPercentRevenueSplit']		= "% 나눠진 매출";
$GLOBALS['strPercentBasketValue']		= "% 바스켓 값";
$GLOBALS['strAmountPerItem']		= "아이템당 양";
$GLOBALS['strPercentCustomVariable']		= "% 맞춤 변수";
$GLOBALS['strPercentSumVariables']		= "% 변수의 합";

// Login & Permissions
$GLOBALS['strUserAccess']				= "사용자 관리";
$GLOBALS['strAdminAccess']				= "관리자 관리";
$GLOBALS['strUserProperties']			= "사용자 등록정보";
$GLOBALS['strLinkNewUser']			= "새로운 사용자 연결";
$GLOBALS['strPermissions']			= "권한 ";
$GLOBALS['strAuthentification'] 		= "사용자 계정";
$GLOBALS['strWelcomeTo']			= "환영합니다. ";
$GLOBALS['strEnterUsername']			= "로그인하기 위해 사용자ID과 비밀번호를 입력하세요.";
$GLOBALS['strEnterBoth']			= "사용자 ID와 비밀번호를 입력하세요.";
$GLOBALS['strEnableCookies']			= MAX_PRODUCT_NAME."을 사용하려면 쿠키를 활성화해야합니다.";
$GLOBALS['strSessionIDNotMatch']		= "세션 쿠키 에러, 다시 로그인하세요";
$GLOBALS['strLogin'] 				= "로그인";
$GLOBALS['strLogout'] 				= "로그아웃";
$GLOBALS['strUsername'] 			= "사용자 이름";
$GLOBALS['strPassword']				= "비밀번호";
$GLOBALS['strPasswordRepeat'] 		= "비밀번호 확인";
$GLOBALS['strAccessDenied']			= "액세스할 수 없습니다.";
$GLOBALS['strUsernameOrPasswordWrong']		= "사용자이름 혹은 비밀번호가 올바르지 않습니다. 다시 시도하세요.";
$GLOBALS['strPasswordWrong']		= "올바른 비밀번호가 아닙니다.";
$GLOBALS['strParametersWrong']		= "제공한 파라미터가 올바르지 않습니다";

$GLOBALS['strNotAdmin']				= "이 계정은 이 기능을 사용할 권한이 없습니다. 다른 계정으로 로그인하여 사용하세요.";
$GLOBALS['strDuplicateClientName']		= "입력한 사용자이름이 이미 있습니다. 다른 사용자이름을 입력하세요.";
$GLOBALS['strDuplicateAgencyName']	= "입력한 사용자이름이 이미 있습니다. 다른 사용자이름을 입력하세요.";
$GLOBALS['strInvalidPassword']		= "새로운 비밀번호가 유효하지 않으니, 다른 비밀번호를 사용해주세요.";
$GLOBALS['strInvalidEmail']		= "이메일이 올바른 형식이 아니니, 올바른 이메일 주소를 입력해주세요.";
$GLOBALS['strNotSamePasswords']		= "두 비밀번호가 일치하지 않습니다.";
$GLOBALS['strRepeatPassword']		= "비밀번호 확인";
$GLOBALS['strOldPassword']		= "이전 비밀번호";
$GLOBALS['strNewPassword']		= "새 비밀번호";
$GLOBALS['strNoBannerId']		= "배너 ID 없음";
$GLOBALS['strDeadLink']			= "링크가 유효하지 않습니다.";
$GLOBALS['strNoPlacement']		= "선택된 캠페인이 존재하지 않습니다. 대신 이 <a href='{link}'>링크</a>로 시도하세요";
$GLOBALS['strNoAdvertiser']		= "선택된 광고주가 존재하지 않습니다. 대신 이 <a href='{link}'>링크</a>로 시도하세요";

// Channels
$GLOBALS['strChannel']			= "타게팅 채널";
$GLOBALS['strChannels']			= "타게팅 채널";
$GLOBALS['strChannelOverview']		= "타게팅 채널 개요";
$GLOBALS['strChannelManagement']	= "타게팅 채널 관리";
$GLOBALS['strAddNewChannel']		= "새 타게팅 채널 추가";
$GLOBALS['strAddNewChannel_Key']	= "새 타게팅 채널 추가<u>n</u>";
$GLOBALS['strChannelToWebsite']		= "웹사이트로";
$GLOBALS['strNoChannels']		= "현재 정의된 채널이 없습니다.";
$GLOBALS['strNoChannelsAddWebsite']	= "웹사이트가 없기 때문에 현재 정의된 채널이 없습니다. 타케팅 채널을 만들기 위해서 먼저 <a href='affiliate-edit.php'>새 웹사이트 추가</a>를 진행합니다.";
$GLOBALS['strEditChannelLimitations']	= "타게팅 채널 제한 수정";
$GLOBALS['strChannelProperties']	= "타게팅 채널 등록정보";
$GLOBALS['strChannelLimitations'] 	= "광고전달 옵션";
$GLOBALS['strConfirmDeleteChannel']	= "정말로 타게팅 채널을 삭제하시겠습니까?";
$GLOBALS['strConfirmDeleteChannels']	= "해당 배너를 삭제하시겠습니까?";
$GLOBALS['strModifychannel']		= "타게팅 채널 수정";
$GLOBALS['strChannelsOfWebsite']	= '에서'; //페이지명과 웹사이트명 사이에 'in'이 추가됩니다. 예, 'Targeting channels in www.example.com'

// Tracker Variables
$GLOBALS['strVariableName']		= "변수명";
$GLOBALS['strVariableDescription']	= " 설명";
$GLOBALS['strVariableDataType']		= "데이터 형식";
$GLOBALS['strVariablePurpose']		= "목적";
$GLOBALS['strGeneric']			= "범용";
$GLOBALS['strBasketValue']		= "바구니 값";
$GLOBALS['strNumItems']			= "항목수";
$GLOBALS['strVariableIsUnique']		= "중복제거";
$GLOBALS['strJavascript']		= "전환 중복을 제거하시겠습니까?";
$GLOBALS['strRefererQuerystring']		= "Javascript";
$GLOBALS['strQuerystring']		= "Referer Querystring";
$GLOBALS['strInteger']			= "Querystring";
$GLOBALS['strNumber']			= "정수";
$GLOBALS['strString']			= "숫자";
$GLOBALS['strTrackFollowingVars']	= "문자열";
$GLOBALS['strAddVariable']		= "다음과 같은 변수를 추적";
$GLOBALS['strNoVarsToTrack']		= "변수 추가";
$GLOBALS['strVariableHidden']		= "추적을 위한 변수 없음";
$GLOBALS['strVariableRejectEmpty']	= "웹사이트에 변수를 숨기시겠습니까?";
$GLOBALS['strTrackingSettings']		= "빈 경우 거절하시겠습니까?";
$GLOBALS['strTrackerType']		= "추적 설정";
$GLOBALS['strTrackerTypeJS']		= "추적 유형";
$GLOBALS['strTrackerTypeDefault']	= "추적 JavaScript 변수";
$GLOBALS['strTrackerTypeDOM']		= "추적 JavaScript 변수(이전 버전과 호환)";
$GLOBALS['strTrackerTypeCustom']	= "사용자 정의 JS코드";
$GLOBALS['strVariableCode']		= "Javascript 추적코드";

// Upload conversions
$GLOBALS['strRecordLengthTooBig']	= "레코드 길이가 너무 깁니다";
$GLOBALS['strRecordNonInt']		= "값은 숫자여야 합니다";
$GLOBALS['strRecordWasNotInserted']	= "레코드가 추가되지 않았습니다.";
$GLOBALS['strWrongColumnPart1']		= "<br>CSV파일 Column 오류 <b>";
$GLOBALS['strWrongColumnPart2']		= "</b> 이 추적에 대해 허용되지 않습니다.";
$GLOBALS['strMissingColumnPart1']	= "<br>CSV파일 Column 오류 <b>";
$GLOBALS['strMissingColumnPart2']	= "</b> 누락되었습니다.";
$GLOBALS['strYouHaveNoTrackers']	= "광고주에 추적기가 없습니다";
$GLOBALS['strYouHaveNoCampaigns'] 	= "광고주에 캠페인이 없습니다.";
$GLOBALS['strYouHaveNoBanners']		= "캠페인에 배너가 없습니다.";
$GLOBALS['strYouHaveNoZones']		= "배너는 영역에 연결되어 있지 않습니다.";
$GLOBALS['strNoBannersDropdown']	= "--배너를 찾을 수 없습니다--";
$GLOBALS['strNoZonesDropdown']		= "--영역을 찾을 수 없습니다--";
$GLOBALS['strInsertErrorPart1']		= "<br><br><center><b> 오류발생";
$GLOBALS['strInsertErrorPart2']		= "레코드가 추가되지 않았습니다! </b></center>";
$GLOBALS['strDuplicatedValue']		= "중복된 값입니다!";
$GLOBALS['strInsertCorrect']		= "<br><br><center><b> 파일이 제대로 업로드 되었습니다. </b></center>";
$GLOBALS['strReuploadCsvFile']		= "CSV파일 재 등록";
$GLOBALS['strConfirmUpload']		= "업로드 확인";
$GLOBALS['strLoadedRecords']		= "레코드 기록";
$GLOBALS['strBrokenRecords']		= "모든 레코드에서 깨진 항목이 있습니다.";
$GLOBALS['strWrongDateFormat']		= "잘못된 날짜 형식";

// Password recovery
$GLOBALS['strForgotPassword']		= "비밀번호를 잊으셨습니까?";
$GLOBALS['strPasswordRecovery']		= "비밀번호 복구";
$GLOBALS['strEmailRequired']		= "이메일은 필수 항목입니다.";
$GLOBALS['strPwdRecEmailSent']		= "복구 이메일 전송";
$GLOBALS['strPwdRecEmailNotFound']	= "이메일 주소를 찾을 수 없습니다.";
$GLOBALS['strPwdRecPasswordSaved']	= "새 암호가 저장되었습니다. 바로 <a href='index.php'>로그인</a> 하기";
$GLOBALS['strPwdRecWrongId']		= "잘못된 ID 입니다";
$GLOBALS['strPwdRecEnterEmail']		= "아래에 이메일 주소를 입력하세요.";
$GLOBALS['strPwdRecEnterPassword']	= "아래에 비밀번호를 입력하세요.";
$GLOBALS['strPwdRecReset']		= "비밀번호 재 설정";
$GLOBALS['strPwdRecResetLink']		= "비밀번호 재 설정 링크";
$GLOBALS['strPwdRecResetPwdThisUser']	= "이 사용자의 비밀번호를 재 설정";
$GLOBALS['strPwdRecEmailPwdRecovery']	= "비밀번호 복구";
$GLOBALS['strProceed']			= "진행 >";
$GLOBALS['strNotifyPageMessage']	= "암호를 다시 설정하고 로그인 할 수 있는 링크를 포함한 이메일이 발송되었습니다.<br /> 메일이 도착하는데 몇 분 정도 소요될 수 있습니다. <br />이메일을 수신하지 못하셨을 경우 스팸메일 폴더를 확인해보시기 바랍니다.<br /> <a href=\"index.php\">로그인 페이지로 돌아가기</a> ";


// Audit
$GLOBALS['strAdditionalItems']		= "추가 항목";
$GLOBALS['strFor']			= "for";
$GLOBALS['strHas']			= "has";
$GLOBALS['strAdZoneAsscociation']	= "광고 영역 연합";
$GLOBALS['strBinaryData']		= "이진 데이터";
$GLOBALS['strAuditTrailDisabled']	= "감사 추적은 관리자에 의해 중지되었습니다. 더 이상 이벤트가 기록되지 않으며 감사추적 목록에서 보이지 않습니다.";
$GLOBALS['strAccount']			= "계정";
$GLOBALS['strAccountUserAssociation']		= "계정 사용자 연합";
$GLOBALS['strEvent']			= "이벤트";
$GLOBALS['strImage']			= "이미지";
$GLOBALS['strCampaignZoneAssociation']		= "캠페인 영역 연합";
$GLOBALS['strAccountPreferenceAssociation']	= "계정 환설설정 연합";

// Widget - Audit
$GLOBALS['strAuditNoData']		= "선택한 기간동안 사용자의 활동 내역이 기록되지 않았습니다.";
$GLOBALS['strAuditTrail']		= "사용자 로그 추적";
$GLOBALS['strAuditTrailSetup']		= "사용자 로그 추적 설정";
$GLOBALS['strAuditTrailGoTo']		= "사용자 로그 추적 페이지로 이동";
$GLOBALS['strAuditTrailNotEnabled']	= "<li>사용자 로그 추적은 누가 어디에서 무엇을 했는지를 볼수 있도록 합니다.</li>";

// Widget - Campaign
$GLOBALS['strCampaignGoTo']		= "캠페인 페이지로 이동";
$GLOBALS['strCampaignSetUp']		= "켐페인 설정";
$GLOBALS['strCampaignNoRecords']	= "<li>캠페인은 일반적인 광고에서 사용중인 여러가지 배너 형태 및 광고크기를 그룹화 할 수 있습니다.</li>
<li>캠페인에서 배너를 그룹화하여 각각 광고 개별적인 광고 전송 설정을 정의하지 않으므로 시간을 줄일 수 있습니다.</li>
<li><a class='site-link' target='help' href='".OX_PRODUCT_DOCSURL."/inventory/advertisersAndCampaigns/campaigns'>캠페인 문서</a> 확인!</li>";

$GLOBALS['strCampaignNoRecordsAdmin']		= "<li>표시 할 캠페인 활동이 없습니다.</li>";
$GLOBALS['strCampaignNoDataTimeSpan']		= "<li>선택된 기간동안 시작하거나 종료된 캠페인이 없습니다.</li>";
$GLOBALS['strCampaignAuditNotActivated']	= "<li>선택된 기간중에 시작하거나 종료된 캠페인을 보기위해 사용자 로그 추적을 활성화 해야 합니다.</li><li>사용자 로그 추적을 활성화 하지 않았기 때문에 이 메시지가 보여집니다.</li>";
$GLOBALS['strCampaignAuditTrailSetup']		= "캠페인 조회를 위해서 사용자 로그 추적을 활성화 하세요.";
$GLOBALS['strUnsavedChanges']		= "이 페이지의 변경사항을 저장하지 았았습니다. 변경사항을 &quot;저장&quot;하시면 완료됩니다.";
$GLOBALS['strDeliveryLimitationsDisagree']		= "경고 : 광고전달 엔진의 제한사항은 다음의 제한에 <strong>동의하지 않습니다</strong>.<br />광고전달 엔징의 규칙을 업데이트하여 변경사항을 저장하세요.";
$GLOBALS['strDeliveryLimitationsInputErrors']		= "광고 전달 제한사항의 일부는 잘못된 값을 보고합니다. ";


//confirmation messages
$GLOBALS['strYouAreNowWorkingAsX']		= "<b>%s</b>에서 작업중입니다.";
$GLOBALS['strYouDontHaveAccess']		= "해당 페이지에 액세스 할 수 없습니다. Re-direction 되었습니다.";
$GLOBALS['strAdvertiserHasBeenAdded']		= "광고주 <a href='%s'>%s</a>가(이) 추가되었습니다. <a href='%s'>캠페인 추가</a> 하세요.";
$GLOBALS['strAdvertiserHasBeenUpdated']		= "광고주 <a href='%s'>%s</a>가(이) 업데이트 되었습니다.";
$GLOBALS['strAdvertiserHasBeenDeleted']		= "광고주 <a href='%s'>%s</a>가(이) 삭제 되었습니다.";
$GLOBALS['strAdvertisersHaveBeenDeleted']		= "선택된 모든 광고주가 삭제되었습니다.";
$GLOBALS['strTrackerHasBeenAdded']		= "추적기 <a href='%s'>%s</a>가(이) 추가되었습니다.";
$GLOBALS['strTrackerHasBeenUpdated']		= "추적기 <a href='%s'>%s</a>가(이) 업데이트 되었습니다.";
$GLOBALS['strTrackerVarsHaveBeenUpdated']		= "추적기 변수 <a href='%s'>%s</a>가(이) 업데이트 되었습니다.";
$GLOBALS['strTrackerCampaignsHaveBeenUpdated']		= "추적기 <a href='%s'>%s</a>의 연결된 캠페인이 업데이트 되었습니다.";
$GLOBALS['strTrackerAppendHasBeenUpdated']		= "추적기 <a href='%s'>%s</a>에 첨부된 추적코드가 업데이트 되었습니다.";
$GLOBALS['strTrackerHasBeenDeleted']		= "추적기 <a href='%s'>%s</a>가(이) 삭제되었습니다.";
$GLOBALS['strTrackersHaveBeenDeleted']		= "선택된 모든 추적기가 삭제되었습니다.";
$GLOBALS['strTrackerHasBeenDuplicated']		= "추적기 <a href='%s'>%s</a>가(이) <a href='%s'>%s</a>로 복사되었습니다.";
$GLOBALS['strTrackerHasBeenMoved']		= "추적기 <b>%s</b>는(은) 광고주 <b>%s</b>로 이동되었습니다.";
$GLOBALS['strCampaignHasBeenAdded']		= "캠페인 <a href='%s'>%s</a>가(이) 추가되었습니다. <a href='%s'>배너 추가</a> 하세요.";
$GLOBALS['strCampaignHasBeenNoBanner']		= "캠페인 <a href='%s'>%s</a>가(이) 추가되었습니다.";
$GLOBALS['strCampaignHasBeenUpdated']		= "캠페인 <a href='%s'>%s</a>가(이) 업데이트 되었습니다.";
$GLOBALS['strCampaignTrackersHaveBeenUpdated']		= "캠페인 <a href='%s'>%s</a>의 연결된 추적기가 업데이트 되었습니다.";
$GLOBALS['strCampaignHasBeenDeleted']		= "캠페인 <a href='%s'>%s</a>가(이) 삭제 되었습니다.";
$GLOBALS['strCampaignsHaveBeenDeleted']		= "선택된 모든 캠페인이 삭제 되었습니다.";
$GLOBALS['strCampaignHasBeenDuplicated']		= "캠페인 <a href='%s'>%s</a>가(이) <a href='%s'>%s</a>로 복사 되었습니다.";
$GLOBALS['strCampaignHasBeenMoved']		= "캠페인 <b>%s</b>는(은) 광고주 <b>%s</b>로 이동 되었습니다.";
$GLOBALS['strBannerHasBeenAdded']		= "배너 <a href='%s'>%s</a>가(이) 추가 되었습니다.";
$GLOBALS['strBannerHasBeenUpdated']		= "배너 <a href='%s'>%s</a>가(이) 업데이트 되었습니다.";
$GLOBALS['strBannerAdvancedHasBeenUpdated']		= "배너 <a href='%s'>%s</a>의 고급설정이 업데이트 되었습니다.";
$GLOBALS['strBannerAclHasBeenUpdated']		= "배너 <a href='%s'>%s</a>의 광고 전달 옵션이 업데이트 되었습니다.";
$GLOBALS['strBannerAclHasBeenAppliedTo']		= "배너 <a href='%s'>%s</a>의 광고 전달 옵션이 %d 배너로 적용 되었습니다.";
$GLOBALS['strBannerHasBeenDeleted']		= "배너 <b>%s</b>가(이) 삭제 되었습니다.";
$GLOBALS['strBannersHaveBeenDeleted']		= "선택된 모든 배너가 삭제 되었습니다.";
$GLOBALS['strBannerHasBeenDuplicated']		= "배너 <a href='%s'>%s</a>가(이) <a href='%s'>%s</a>로 복사 되었습니다.";
$GLOBALS['strBannerHasBeenMoved']		= "배너 <b>%s</b>가(이) 캠페인 <b>%s</b>로 이동 되었습니다.";
$GLOBALS['strBannerHasBeenActivated']		= "배너 <a href='%s'>%s</a>가(이) 활성화 되었습니다.";
$GLOBALS['strBannerHasBeenDeactivated']		= "배너 <a href='%s'>%s</a>가(이) 비활성화 되었습니다.";
$GLOBALS['strXZonesLinked']		= "<b>%s</b> 영역(s) 연결됨";
$GLOBALS['strXZonesUnlinked']		= "<b>%s</b> 영역(s) 연결 해제";
$GLOBALS['strWebsiteHasBeenAdded']		= "웹사이트 <a href='%s'>%s</a>가(이) 추가 되었습니다. <a href='%s'>a영역추가</a> 하세요.";
$GLOBALS['strWebsiteHasBeenUpdated']		= "웹사이트 <a href='%s'>%s</a>가(이) 업데이트 되었습니다.";
$GLOBALS['strWebsiteHasBeenDeleted']		= "웹사이트 <b>%s</b>가(이) 삭제 되었습니다.";
$GLOBALS['strWebsitesHaveBeenDeleted']		= "선택된 모든 웹사이트가 삭제 되었습니다.";
$GLOBALS['strZoneHasBeenAdded']		= "영역 <a href='%s'>%s</a>가(이) 추가 되었습니다.";
$GLOBALS['strZoneHasBeenUpdated']		= "영역 <a href='%s'>%s</a>가(이) 업데이트 되었습니다.";
$GLOBALS['strZoneAdvancedHasBeenUpdated']		= "영역 <a href='%s'>%s</a>의 고급 설정이 업데이트 되었습니다.";
$GLOBALS['strZoneHasBeenDeleted']		= "영역 <b>%s</b>이(가) 삭제 되었습니다.";
$GLOBALS['strZonesHaveBeenDeleted']		= "선택된 모든 영역이 삭제 되었습니다.";
$GLOBALS['strZoneHasBeenDuplicated']		= "<a href='%s'>%s</a>가(이) <a href='%s'>%s</a>(으)로 복제 되었습니다.";
$GLOBALS['strZoneHasBeenMoved']		= "<b>%s</b>가(이) 웹사이트 <b>%s</b>(으)로 이동하였습니다.";
$GLOBALS['strZoneLinkedBanner']		= "영역 <a href='%s'>%s</a>(으)로 배너가 연결 되었습니다.";
$GLOBALS['strZoneLinkedCampaign']		= "영역 <a href='%s'>%s</a>(으)로 캠페인이 연결 되었습니다.";
$GLOBALS['strZoneRemovedBanner']		= "영역 <a href='%s'>%s</a>으로부터 캠페인 연결이 해제 되었습니다.";
$GLOBALS['strZoneRemovedCampaign']		= "영역 <a href='%s'>%s</a>으로부터 캠페인 연결이 해제 되었습니다.";
$GLOBALS['strChannelHasBeenAdded']		= "타케팅 채널 <a href='%s'>%s</a>가(이) 추가되었습니다. <a href='%s'>광고 전달 옵션</a>을 변경하세요.";
$GLOBALS['strChannelHasBeenUpdated']		= "타케팅 채널 <a href='%s'>%s</a>가(이) 업데이트 되었습니다.";
$GLOBALS['strChannelAclHasBeenUpdated']		= "타케팅 채널 <a href='%s'>%s</a>의 광고전달 옵션이 업데이트 되었습니다.";
$GLOBALS['strChannelHasBeenDeleted']		= "타케팅 채널 <b>%s</b>가(이) 삭제 되었습니다.";
$GLOBALS['strChannelsHaveBeenDeleted']		= "선택된 모든 타케팅 채널이 삭제 되었습니다.";
$GLOBALS['strChannelHasBeenDuplicated']		= "타케팅 채널 <a href='%s'>%s</a>가(이) <a href='%s'>%s</a>로 복사 되었습니다.";
$GLOBALS['strUserPreferencesUpdated']		= "<b>%s</b>의 환경설정이 업데이트 되었습니다.";
$GLOBALS['strPreferencesHaveBeenUpdated']		= "환경설정이 업데이트 되었습니다.";
$GLOBALS['strEmailChanged']		= "E-mail이 변경 되었습니다.";
$GLOBALS['strPasswordChanged']		= "비밀번호가 변경 되었습니다.";
$GLOBALS['strXPreferencesHaveBeenUpdated']		= "<b>%s</b>가(이) 업데이트 되었습니다.";
$GLOBALS['strXSettingsHaveBeenUpdated']		= "<b>%s</b>가(이) 업데이트 되었습니다.";
$GLOBALS['strTZPreferencesWarning']		= "캠페인 활성화 및 만료일이 업데이트 되지 않으며, 시간 기반 배너 제한도 없습니다.<br />
새로운 시간대를 사용하려는 경우 수동으로 업데이트 해야 합니다.";

/*-------------------------------------------------------*/
/* Languages Names                                       */
/*-------------------------------------------------------*/

$GLOBALS['str_ar']                  = "아랍어";
$GLOBALS['str_bg']                  = "불가리어";
$GLOBALS['str_cs']                  = "체코어";
$GLOBALS['str_cy']                  = "웨일즈어";
$GLOBALS['str_da']                  = "덴마크어";
$GLOBALS['str_de']                  = "독일어";
$GLOBALS['str_el']                  = "그리스어";
$GLOBALS['str_en']                  = "영어";
$GLOBALS['str_es']                  = "스페인어";
$GLOBALS['str_fa']                  = "페르시아어";
$GLOBALS['str_fr']                  = "프랑스어";
$GLOBALS['str_he']                  = "히브리어";
$GLOBALS['str_hr']                  = "크로아티아어";
$GLOBALS['str_hu']                  = "헝가리어";
$GLOBALS['str_id']                  = "인도네시아어";
$GLOBALS['str_it']                  = "이탈리아어";
$GLOBALS['str_ja']                  = "일본어";
$GLOBALS['str_ko']                  = "한국어";
$GLOBALS['str_lt']                  = "리투아니아어";
$GLOBALS['str_ms']                  = "말레이시아어";
$GLOBALS['str_nb']                  = "노르웨이어";
$GLOBALS['str_nl']                  = "네덜란드어";
$GLOBALS['str_pl']                  = "폴란드어";
$GLOBALS['str_pt_BR']               = "포르투칼 브라질어";
$GLOBALS['str_pt_PT']               = "포르투칼어";
$GLOBALS['str_ro']                  = "루마니아어";
$GLOBALS['str_ru']                  = "러시아어";
$GLOBALS['str_sk']                  = "슬로바키아어";
$GLOBALS['str_sl']                  = "슬로베니아어";
$GLOBALS['str_sq']                  = "알바니아어";
$GLOBALS['str_sv']                  = "스웨덴어";
$GLOBALS['str_tr']                  = "터키어";
$GLOBALS['str_uk']                  = "우르가니아어";
$GLOBALS['str_zh_CN']               = "중국어(간체)";
$GLOBALS['str_zh_TW']               = "중국어(번체)";


//ADOP 추가
$GLOBALS['strUserId']   			= "사용자 ID";
$GLOBALS['strRemove']   			= "삭제";
$GLOBALS['strShow']     			= "보기";
$GLOBALS['strAllBanner'] 			= "모든 배너";
$GLOBALS['strActiveAdvertisers']	= "운영중인 광고주";
$GLOBALS['strAllCampaigns']	        = "모든 캠페인";
$GLOBALS['strCampaignsReport']	    = "캠페인 보고서";
$GLOBALS['strZonesReport']	        = "영역 보고서";

// Dashboard
$GLOBALS['strDashboardCommunity']		= "커뮤니티";
$GLOBALS['strDashboardDashboard']		= "대시보드";
$GLOBALS['strDashboardForum']			= "OpenX 포럼";
$GLOBALS['strDashboardDocs']			= "OpenX 문서";
$GLOBALS['strDashboardCantBeDisplayed']		= "대시보드를 볼 수 없습니다";
$GLOBALS['strNoCheckForUpdates']		= "업데이트 확인 설정이 이용가능할 때까지<br/> 대시보드를 볼 수 없습니다.";
$GLOBALS['strEnableCheckForUpdates']		= "<a href='account-settings-update.php' target='_top'>업데이트 설정</a> 페이지의 <a href='account-settings-update.php' target='_top'><br/>업데이트 확인 설정</a>을 허용하세요.";
$GLOBALS['strChoosenDisableHomePage']		= "홈페이지를 이용하지 않음으로 선택하셨습니다.";
$GLOBALS['strAccessHomePage']			= "홈페이지에 접근하려면 클릭하세요";
$GLOBALS['strEditSyncSettings']			= "그리고 동기화 설정을 수정하세요";

// Dashboard Errors
$GLOBALS['strDashboardErrorCode']		= "코드";
$GLOBALS['strDashboardGenericError']		= "일반적 에러";
$GLOBALS['strDashboardSystemMessage']		= "시스템 메시지";
$GLOBALS['strDashboardErrorHelp']		= "이 에러가 반복된다면, 문제를 상세히 기술하여 <a href='http://forum.openx.org/'>OpenX 포럼</a>에 올려주세요.";
$GLOBALS['strDashboardErrorMsg800']		= "XML-RPC 연결 에러";
$GLOBALS['strDashboardErrorMsg801']		= "인증되지 않음";
$GLOBALS['strDashboardErrorMsg802']		= "CAPTCHA 실패";
$GLOBALS['strDashboardErrorMsg803']		= "잘못된 파라미터";
$GLOBALS['strDashboardErrorMsg804']		= "사용자 이름이 플랫폼과 일치하지 않음";
$GLOBALS['strDashboardErrorMsg805']		= "플랫폼이 존재하지 않음";
$GLOBALS['strDashboardErrorMsg806']		= "서버 에러";
$GLOBALS['strDashboardErrorMsg807']		= "권한 없음";
$GLOBALS['strDashboardErrorMsg808']		= "지원하지 않는 XML-RPC 버전입니다.";
$GLOBALS['strDashboardErrorMsg900']		= "전송 에러 코드";
$GLOBALS['strDashboardErrorMsg821']		= "M2M 인증 에러 - 허용하지 않는 계정 형식";
$GLOBALS['strDashboardErrorMsg822']		= "M2M 인증 에러 - 존재하는 비밀번호";
$GLOBALS['strDashboardErrorMsg823']		= "M2M 인증 에러 - 유효하지 않은 비밀번호";
$GLOBALS['strDashboardErrorMsg824']		= "M2M 인증 에러 - 만료된 비밀번호";
$GLOBALS['strDashboardErrorMsg825']		= "M2M 인증 에러 - 연결할 수 없음";
$GLOBALS['strDashboardErrorMsg826']		= "M2M 인증 에러 - 다시 연결할 수 없음";
$GLOBALS['strDashboardErrorDsc800']		= "대시보드는 중앙 서버로부터의 정보를 몇몇 위젯으로 가져옵니다. 이에 영향을 주는 몇 가지 것들이 있습니다.<br/>당신의 서버는 이용가능한 Curl 확장을 가지지 않을 지도 모릅니다. 당신은 Curl 확장을 설치하거나 이용가능하도록 만들어야 할 필요가 있을 수 있으니, 더 많은 정보를 보기 위해서 <a href='http://php.net/curl'>여기</a>를 확인하세요.당신은 또한 방화벽이 외부로 향하는 연결을 막고있지 않은지 확인해야만 합니다.";
$GLOBALS['strDashboardErrorDsc803']		= "서버 요청 에러 - 잘못된 파라미터, 데이터를 다시 전송하세요.";
$GLOBALS['strDashboardErrorDsc805']		= "OpenX 설치 중에는 XML-RPC 연결이 허용되지 않습니다. 그리고 OpenX 중앙 서버가 당신의 OpenX 설치를 유효하다고 인식하지 않습니다.<br/>중앙 서버에 연결과 등록을 하기 위해 관리자 계정 내 계정 -> 제품 업데이트로 이동하세요.";


// Priority
$GLOBALS['strPriorityLevel']		= "우선순위 레벨";
$GLOBALS['strPriorityTargeting']		= "분포";
$GLOBALS['strPriorityOptimisation']		= "기타";
$GLOBALS['strExclusiveAds']		= "독점 계약 광고";
$GLOBALS['strHighAds']		= "계약 광고";
$GLOBALS['strECPMAds']		= "eCPM 광고";
$GLOBALS['strLowAds']		= "표준 계약 광고";
$GLOBALS['strCapping']		= "상한";
$GLOBALS['strCapped']		= "상한된";
$GLOBALS['strNoCapping']		= "상한 없음";

// User access
$GLOBALS['strWorkingAs']		= "Working as";
$GLOBALS['strWorkingAs_Key']		= "계정 전환 : ";
$GLOBALS['strWorkingAs']		= "Working as";
$GLOBALS['strSwitchTo']			= "전환하기";
$GLOBALS['strUseSearchBoxToFindMoreAccounts']	= "더 많은 계정을 찾으려면 전환기의 검색 박스를 사용하세요";
$GLOBALS['strWorkingFor']		= "%s for...";
$GLOBALS['strNoAccountWithXInNameFound']	= "\"%s\"이란 이름으로 찾아진 계정 없음";
$GLOBALS['strRecentlyUsed']		= "최근 사용된";
$GLOBALS['strLinkUser']			= "사용자 추가";
$GLOBALS['strLinkUser_Key']		= "사용자 추가(<u>u</u>)";
$GLOBALS['strUsernameToLink']		= "사용자 로그인 ID";
$GLOBALS['strEmailToLink']		= "추가할 사용자의 이메일";
$GLOBALS['strNewUserWillBeCreated']		= "새로운 사용자가 생성될 것입니다";
$GLOBALS['strToLinkProvideEmail']		= "사용자를 추가하려면, 사용자의 이메일을 제공하세요";
$GLOBALS['strToLinkProvideUsername']		= "사용자를 추가하려면, 사용자이름을 제공하세요";
$GLOBALS['strErrorWhileCreatingUser']		= "사용자: %s를 생성하던 중 에러";
$GLOBALS['strUserLinkedToAccount']		= "사용자가 계정에 추가되었습니다";
$GLOBALS['strUserAccountUpdated']		= "갱신된 사용자 계정";
$GLOBALS['strUserUnlinkedFromAccount']		= "계정으로부터 사용자가 제거되었습니다";
$GLOBALS['strUserWasDeleted']		= "사용자가 삭제되었습니다";
$GLOBALS['strUserNotLinkedWithAccount']		= "계정에 연결되어있는 어떠한 사용자도 없습니다";
$GLOBALS['strCantDeleteOneAdminUser']		= "사용자를 삭제할 수 없습니다. 관리자 계정에는 최소한 한 명의 사용자가 연결되어있어야 합니다.";
$GLOBALS['strLinkUserHelp']		= "<b>존재하는 사용자</b>를 추가하려면, %s를 입력하고 {$GLOBALS['strLinkUser']}를 클릭하세요 <br /><b>새로운 사용자</b>를 추가하려면, 원하는 %s를 입력하고 {$GLOBALS['strLinkUser']}를 클릭하세요";
$GLOBALS['strLinkUserHelpUser']		= " 사용자ID";
$GLOBALS['strLinkUserHelpEmail']		= "이메일 주소";
$GLOBALS['strLastLoggedIn']		= "마지막 로그인";
$GLOBALS['strDateLinked']		= "연결된 날짜";
$GLOBALS['strUnlink']			= "제거";
$GLOBALS['strUnlinkingFromLastEntity']		= "최근 엔티티로부터의 사용자 제거";
$GLOBALS['strUnlinkingFromLastEntityBody']		= "최근 엔티티로부터의 사용자 제거는 사용자를 삭제할 것입니다. 이 사용자를 제거하시겠습니까?";
$GLOBALS['strUnlinkAndDelete']		= "사용자 제거 &amp; 삭제";
$GLOBALS['strUnlinkUser']		= "사용자 제거";
$GLOBALS['strUnlinkUserConfirmBody']		= "이 사용자를 정말로 제거하시겠습니까?";


$GLOBALS['strAdultUrl'] 	= "성인 URL";
$GLOBALS['strAdultUrlSub'] 	= "매체별 성인 URL";
$GLOBALS['strAdultNo']		= "No.";
$GLOBALS['strAdultContCode']	= "관리번호";
$GLOBALS['strAdultSiteUrl']		= "사이트명";
$GLOBALS['strAdultUpDate'] 		= "갱신일자";
$GLOBALS['strAdultUrlCount'] 		= "성인URL 갯수";
$GLOBALS['strAdultUrlDelBtn'] 		= "삭제";
$GLOBALS['strAdultUrlAddBtn']		= "성인URL 추가";
$GLOBALS['strAdultUrlTxtCom']		= "매체사";
$GLOBALS['strAdultKeyWord']			= "키워드";
$GLOBALS['strAdultImage']			= "이미지";
$GLOBALS['strAdultUrlDelMsg']		= "삭제 하시겠습니까?";
$GLOBALS['strAdultUrlAddMsg']		= "입력하신 내용으로 등록 하시겠습니까?";

$GLOBALS['strManualLive'] 	= "수동 라이브";

// ted추가 //
$GLOBALS['strCampaignReport'] 	= "캠페인 보고서";
$GLOBALS['strBannerReport']		= "배너 보고서";
$GLOBALS['strZoneReport']		= "영역 보고서";
$GLOBALS['strDayReport']		= "날짜 보고서";
$GLOBALS['strWebsiteReport']	= "웹사이트 보고서";
$GLOBALS['strTimeReport']		= "시간 보고서";
$GLOBALS['strAdunitReport']		= "광고단위 보고서";
$GLOBALS['strAdunit']		= "광고단위";

$GLOBALS['strReportTitleMsg']		        = "표준 보고서";
$GLOBALS['strReportSubTitleAdsMsg']	        = "광고 분석 보고서";
$GLOBALS['strReportSubTitleAnalysisMsg']	= "캠페인 분석 보고서";
$GLOBALS['strReportSubTitleDeliveryMsg']	= "캠페인 전달 보고서";
$GLOBALS['strReportSubTitleTrackingMsg']	= "전환 추적 보고서";
$GLOBALS['strReportAdsMsg']	        = "날짜, 캠페인, 영역별로 특정 광고주나 웹사이트에 대한 상세 내역 리포트.";
$GLOBALS['strReportAnalysisMsg']	= "날짜, 배너, 영역별로 특정 캠페인에 대한 상세 내역 리포트.";
$GLOBALS['strReportDeliveryMsg']	= "선택된 기간동안 진행 되었던 모든 캠페인에 대한 통계 리포트(실적이 저조한 캠페인은 강조 표시 됨).";
$GLOBALS['strReportTrackingMsg']	= "광고주나 웹사이트 별 모든 전환에 대한 상세 내역 리포트.";
?>