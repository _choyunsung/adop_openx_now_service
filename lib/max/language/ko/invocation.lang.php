<?php

/*
+---------------------------------------------------------------------------+
| Revive Adserver                                                           |
| http://www.revive-adserver.com                                            |
|                                                                           |
| Copyright: See the COPYRIGHT.txt file.                                    |
| License: GPLv2 or later, see the LICENSE.txt file.                        |
+---------------------------------------------------------------------------+
*/

// Invocation Types
$GLOBALS['strInvocationRemote']			= "원격 호출";
$GLOBALS['strInvocationJS']			= "원격 호출(자바스크립트용)";
$GLOBALS['strInvocationIframes']		= "원격 호출(프레임용)";
$GLOBALS['strInvocationXmlRpc']			= "XML-RPC를 이용한 원격 호출";
$GLOBALS['strInvocationCombined']		= "원격 호출 조합";
$GLOBALS['strInvocationPopUp']			= "팝업";
$GLOBALS['strInvocationAdLayer']		= "격자 또는 플로팅 DHTML";
$GLOBALS['strInvocationLocal']			= "로컬 모드";


// Other
$GLOBALS['strCopyToClipboard']			= "클립보드에 복사";


// Measures
$GLOBALS['strAbbrPixels']			= "px";
$GLOBALS['strAbbrSeconds']			= "sec";


// Common Invocation Parameters
$GLOBALS['strInvocationWhat']			= "배너 선택";
$GLOBALS['strInvocationClientID']		= "광고주 또는 캠페인";
$GLOBALS['strInvocationTarget']			= "대상 프레임";
$GLOBALS['strInvocationSource']			= "소스";
$GLOBALS['strInvocationWithText']		= "배너 밑에 텍스트를 표시합니다.";
$GLOBALS['strInvocationDontShowAgain']		= "같은 페이지에 연속해서 배너를 표시하지 않습니다.";
$GLOBALS['strInvocationTemplate'] 		= "배너를 템플릿에서 사용할 수 있도록 변수에 저장합니다.";
$GLOBALS['strInvocationDontShowAgainCampaign']  = "같은 페이지에 동일 캠페인의 배너를 다시 표시하지 않습니다.";
$GLOBALS['strInvocationComments']               = "주석을 포함";
$GLOBALS['strInvocationTagsComents']            = "아래 설명에 따라 스크립트를 복사하여 광고 페이지에 정확히 추가하십시오.";
$GLOBALS['strInvocationTagsComentsTitle']       = "다음과 같은 조건으로 광고코드가 설정되어 있습니다";
$GLOBALS['strInvocationHeaderComents']          = "사이트의 모든 페이지 상단 </head>앞에 다음 스크립트를 추가하십시오. 이 코드는 위치하며 광고 스크립트 앞에 위치해야 합니다.";
$GLOBALS['strInvocationAdscriptComents']        = "다음 스크립트를 복사하여 광고가 노출되는 위치에 추가하십시오.";
$GLOBALS['strInvocationHeaderComents']          = "사이트의 모든 페이지 상단 </head>앞에 다음 스크립트를 추가하십시오. 이 코드는 위치하며 광고 스크립트 앞에 위치해야 합니다.";
$GLOBALS['strInvocationDoneComents']        = "배너는 이제 귀하의 웹 사이트에 나타납니다";


// Iframe
$GLOBALS['strIFrameRefreshAfter']		= "새로고침 간격";
$GLOBALS['strIframeResizeToBanner']		= "iframe을 배너에 맞게 크기 조정";
$GLOBALS['strIframeMakeTransparent']		= "iframe을 투명하게 설정";
$GLOBALS['strIframeIncludeNetscape4']		= "넷스케이프 4 ilayer 호환";


// PopUp
$GLOBALS['strPopUpStyle']			= "팝업 형식";
$GLOBALS['strPopUpStylePopUp']			= "팝업";
$GLOBALS['strPopUpStylePopUnder']		= "팝언더";
$GLOBALS['strPopUpCreateInstance']		= "팝업이 생성되는 경우";
$GLOBALS['strPopUpImmediately']			= "바로 띄우기";
$GLOBALS['strPopUpOnClose']			= "페이지를 닫을 때";
$GLOBALS['strPopUpAfterSec']			= "후에 ";
$GLOBALS['strAutoCloseAfter']			= "후에 자동으로 닫음";
$GLOBALS['strPopUpTop']				= "격자 배너 위치(top)";
$GLOBALS['strPopUpLeft']			= "격자 배너 위치(left)";


// XML-RPC
$GLOBALS['strXmlRpcLanguage']			= "사용 언어";


// AdLayer
$GLOBALS['strAdLayerStyle']			= "배너 형식";

$GLOBALS['strAlignment']			= "정렬";
$GLOBALS['strHAlignment']			= "가로 정렬";
$GLOBALS['strLeft']				= "Left";
$GLOBALS['strCenter']				= "Center";
$GLOBALS['strRight']				= "Right";

$GLOBALS['strVAlignment']			= "세로 정렬";
$GLOBALS['strTop']				= "Top";
$GLOBALS['strMiddle']				= "Middle";
$GLOBALS['strBottom']				= "Bottom";

$GLOBALS['strAutoCollapseAfter']		= "지정된 시간 이후에 자동으로 닫음";
$GLOBALS['strCloseText']			= "배너 닫기 문구(Close text)";
$GLOBALS['strClose']				= "[Close]";
$GLOBALS['strBannerPadding']			= "배너 간격(padding)";

$GLOBALS['strHShift']				= "수평 이동";
$GLOBALS['strVShift']				= "수직 이동";

$GLOBALS['strShowCloseButton']			= "닫기 버튼 표시";
$GLOBALS['strBackgroundColor']			= "배경색";
$GLOBALS['strBorderColor']			= "테두리 색";

$GLOBALS['strDirection']			= "방향";
$GLOBALS['strLeftToRight']			= "왼쪽에서 오른쪽으로";
$GLOBALS['strRightToLeft']			= "오른쪽에서 왼쪽으로";
$GLOBALS['strLooping']				= "반복하기";
$GLOBALS['strAlwaysActive']			= "항상 사용";
$GLOBALS['strSpeed']				= "Speed";
$GLOBALS['strPause']				= "Pause";
$GLOBALS['strLimited']				= "Limited";
$GLOBALS['strLeftMargin']			= "왼쪽 여백";
$GLOBALS['strRightMargin']			= "오른쪽 여백";
$GLOBALS['strTransparentBackground']		= "배경 투명색";

$GLOBALS['strSmoothMovement']		= "부드러운 움직임";
$GLOBALS['strHideNotMoving']		= "커서가 이동되지 않는 경우 배너 숨기기";
$GLOBALS['strHideDelay']			= "배너가 숨겨지기 전에 지연시킴";
$GLOBALS['strHideTransparancy']		= "숨겨진 배너의 투명도";



$GLOBALS['strAdLayerStyleName']['geocities'] = "Geocities";
$GLOBALS['strAdLayerStyleName']['simple'] = "단순";
$GLOBALS['strAdLayerStyleName']['cursor'] = "커서";
$GLOBALS['strAdLayerStyleName']['floater'] = "떠있는";




// Note: New translations not found in original lang files but found in CSV
$GLOBALS['strChooseTypeOfInvocation'] = "호출할 배너 종류를 선택하세요.";
$GLOBALS['strChooseTypeOfBannerInvocation'] = "호출할 배너 종류를 선택하세요.";

// Support for 3rd party server clicktracking

$GLOBALS['str3rdPartyTrack']		= "타사 서버 클릭 추적을 지원";

// Support for cachebusting code

$GLOBALS['strCacheBuster']			= "Cache-Busting 코드를 삽입";

// Non-Img creatives Warning for zone image-only invocation

$GLOBALS['strNonImgWarningZone']	= "경고 : 이미지가 아닌 배너가 이 영역에 연결되어 있습니다. 해당 배너는 이 태그로 전송되지 않습니다.";

$GLOBALS['strNonImgWarning']		= "경고 : 해당 배너는 이미지가 아니므로 이 태그는 작동하지 않습니다.";

// unkown HTML tag type Warning for zone invocation

$GLOBALS['strUnknHtmlWarning']		= "경고 :이 배너는 알 수 없는 HTML 광고 형식입니다.";

// sql/web banner-type warning for clickonly zone invocation

$GLOBALS['strWebBannerWarning']		= "경고 :이 배너는 다운로드 되어야 합니다. 배너의 올바른 URL을 알려주십시오.
<br /> 1) 배너 다운로드 : ";

$GLOBALS['strDwnldWebBanner']		 = "마우스 오른쪽 버튼을 클릭하고 다른 이름으로 대상 저장(Save Target As)을 선택합니다.";

$GLOBALS['strWebBannerWarning2']    = "<br /> 2) 웹 서버에 배너를 업로드하고 여기에 위치를 입력하세요. : ";

// IMG invocation selected for tracker with appended code

$GLOBALS['strWarning'] = "경고";

$GLOBALS['strImgWithAppendWarning'] = "추적기 코드가 추가되었습니다. 추가된 코드는 자바스크립트 태그와 함께 작동합니다.";

// Local Invocation

$GLOBALS['strWarningLocalInvocation'] = "<span class='tab-s'><strong>경고:</strong> 로컬 모드 광고코드 생성은 코드를 호출하는 사이트가 애드서버와 동일한 물리적 기계에 있을 때에만 동작합니다.</span><br />
MAX의 기본 디렉토리를 가리키는 MAX_PATH의 코드를 확인하세요.<br />
그리고 광고가 보여지는 사이트의 도메인에 대한 설정 파일이 있는지 확인하세요. (in MAX_PATH/var)";


$GLOBALS['strIABNoteLocalInvocation'] = "<b> 참고 : </ b> 로컬 모드 광고코드 태그를 통한 노출 데이터는 광고 노출 측정을위한 IAB 가이드 라인을 준수하지 않습니다.";

$GLOBALS['strIABNoteXMLRPCInvocation'] = "<b> 참고 : </ b> XML-RPC 광고코드 태그를 통한 노출 데이터는 광고 노출 측정을위한 IAB 가이드 라인을 준수하지 않습니다.";


?>