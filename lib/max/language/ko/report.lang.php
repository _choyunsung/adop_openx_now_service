<?php

/*
+---------------------------------------------------------------------------+
| Revive Adserver                                                           |
| http://www.revive-adserver.com                                            |
|                                                                           |
| Copyright: See the COPYRIGHT.txt file.                                    |
| License: GPLv2 or later, see the LICENSE.txt file.                        |
+---------------------------------------------------------------------------+
*/

$GLOBALS['strPluginAffiliate'] 		= "해당 광고게시자의 기록 목록을 생성합니다. 보고서는 스프레드 시트를 위해 CSV 형식을 사용합니다.";
$GLOBALS['strPluginCampaign'] 		= "해당 캠페인의 기록 목록을 생성합니다. 보고서는 스프레드 시트를 위해 CSV 형식을 사용합니다.";
$GLOBALS['strPluginClient'] 		= "해당 광고주의 기록 목록을 생성합니다. 보고서는 스프레드 시트를 위해 CSV 형식을 사용합니다.";
$GLOBALS['strPluginGlobal'] 		= "전체 기록 목록을 생성합니다. 보고서는 스프레드 시트를 위해 CSV 형식을 사용합니다.";
$GLOBALS['strPluginZone'] 		= "선택한 영역의 기록 목록을 생성합니다. 보고서는 스프레드 시트를 위해 CSV 형식을 사용합니다.";

$GLOBALS['strAdvertiserCampaignHistoryReport'] = '캠페인 히스토리 보고서';
$GLOBALS['strAdvertiserCampaignHistoryDescription'] = '특정 캠페인의 모든 활동에 대한 일일 내역';
$GLOBALS['strAdvertiserHistoryReport'] = '히스토리 보고서';
$GLOBALS['strAdvertiserHistoryDescription'] = "특정 광고주의 모든 활동에 대한 일일 내역";
$GLOBALS['strAdvertiserKeywordHistoryReport'] = '키워드 캠페인 히스토리 보고서';
$GLOBALS['strAdvertiserKeywordHistoryDescription'] = "전환 추적 데이터를 포함하는 키워드 캠페인의 상세 내역";
$GLOBALS['strAdvertiserTrackerHistoryReport'] = '추적 보고서';
$GLOBALS['strAdvertiserTrackerHistoryDescription'] = '전환 추적에 대한 상세 내역';
$GLOBALS['strAdvertiserZoneAnalysisReport'] = '영역 분석';
$GLOBALS['strAdvertiserZoneAnalysisDescription'] = '이 표는 웹사이트와 영역 별 전체 캠페인 활동 보고서 입니다.';
$GLOBALS['strAdvertiserDayOfWeekAnalysisReport'] = '요일별 보고서';
$GLOBALS['strAdvertiserDayOfWeekAnalysisDescription'] = '이 보고서는 요일별 평균 캠페인 활동 보고서 입니다.';
$GLOBALS['strAdvertiserHourOfDayAnalysisReport'] = '시간대별 보고서';
$GLOBALS['strAdvertiserHourOfDayAnalysisDescription'] = '이 표는 시간대별 총 캠페인 활동 보고서입니다.';
$GLOBALS['strAdvertiserCreativeSummaryReport'] = '건별 요약 보고서';
$GLOBALS['strAdvertiserCreativeSummaryDescription'] = '이 보고서는 건별로 캠페인을 요약한 보고서 입니다.';
$GLOBALS['strAdvertiserDailyViewsAnalysisReport'] = 'Daily Views 분석';
$GLOBALS['strAdvertiserDailyViewsAnalysisDescription'] = '이 표는 하루의 총 활동 보고서 입니다.';
$GLOBALS['strAdvertiserCampaignSummaryReport'] = '캠페인 요약 보고서';
$GLOBALS['strAdvertiserCampaignSummaryDescription'] = '캠페인 요약 보고서';
$GLOBALS['strAdvertiserCampaignAnalysisReport'] = '캠페인 분석';
$GLOBALS['strAdvertiserCampaignAnalysisDescription'] = '이 보고서는 게재위치로 캠페인을 요약한 보고서 입니다.';
$GLOBALS['strAdvertiserConversionTrackingAnalysisReport'] = '전환 추적 분석';
$GLOBALS['strAdvertiserConversionTrackingAnalysisDescription'] = '이 보고서는 모든 전환 추적 활동 보고서 입니다. ';

$GLOBALS['strPublisherAdvertisingSummaryReport'] = '광고주 요약 보고서';
$GLOBALS['strPublisherAdvertisingSummaryDescription'] = '이 보고서는 사이트에서 광고주 활동을 요약한 보고서 입니다.';
$GLOBALS['strPublisherBannerTypeAnalysisReport'] = '배너 유형 분석';
$GLOBALS['strPublisherBannerTypeAnalysisDescription'] = '이 보고서는 게재위치로 캠페인을 요약한 보고서 입니다.';
$GLOBALS['strPublisherBannerAnalysisReport'] = '배너 분석';
$GLOBALS['strPublisherBannerAnalysisDescription'] = '이 보고서는 게재위치로 캠페인을 요약한 보고서입니다.';
$GLOBALS['strPublisherCampaignAnalysisReport'] = '캠페인 분석';
$GLOBALS['strPublisherCampaignAnalysisDescription'] = '이 표는 사이트에서 캠페인을 요약한 보고서 입니다.';
$GLOBALS['strPublisherDayOfWeekAnalysisReport'] = '요일별 분석';
$GLOBALS['strPublisherDayOfWeekAnalysisDescription'] = '이 보고서는 요일별 평균 광고 활동 보고서입니다.';
$GLOBALS['strPublisherHourOfDayAnalysisReport'] = '시간대별 분석';
$GLOBALS['strPublisherHourOfDayAnalysisDescription'] = '이 보고서는 시간대별 평균 광고 활동 보고서입니다.';
$GLOBALS['strPublisherRequestViewsAnalysisReport'] = '요청 / 분석보고서 보기';
$GLOBALS['strPublisherRequestViewsAnalysisDescription'] = '이 표는 전체 광고 요청수와 실제 노출수의 비교 보고서 입니다.';
$GLOBALS['strPublisherZoneAnalysisReport'] = '영역 보고서';
$GLOBALS['strPublisherZoneAnalysisDescription'] = '이 보고서는 영역별로 세분화된 총 광고 활동 보고서 입니다.';
$GLOBALS['strPublisherHistoryReport'] = '히스토리 보고서';
$GLOBALS['strPublisherHistoryDescription'] = 'Report of website distribution for an advertiser.';
$GLOBALS['strPublisherZoneHistoryReport'] = '영역 히스토리 보고서';
$GLOBALS['strPublisherZoneHistoryDescription'] = 'Report of website and zone distribution for an advertiser.';
$GLOBALS['strPublisherConversionTrackingAnalysisReport'] = '전환 추적분석';
$GLOBALS['strPublisherConversionTrackingAnalysisDescription'] = '이 보고서는 특정 웹 사이트 (제휴)에 대한 모든 전환 추적 활동 보고서 입니다.';

$GLOBALS['strAgencyCampaignDeliveryReport'] = '캠페인 전달 보고서';
$GLOBALS['strAgencyCampaignDeliveryDescription'] = "지정된 날짜동안 광고전달 목표를 얼마나 잘 충족시키는지를 포함하여 모든 캠페인의 활동 내역 보고서 입니다.";
$GLOBALS['strAgencyCampaignUnderdeliveryReport'] = '캠페인 미달 보고서';
$GLOBALS['strAgencyCampaignUnderdeliveryDescription'] = '지정된 날짜동안 목표를 달성하지 않은 캠페인의 활동 내역 보고서 입니다.';
$GLOBALS['strAgencyHistoryReport'] = '전체 보고서';
$GLOBALS['strAgencyHistoryDescription']  = '세분화된 전체 광고 히스토리';

$GLOBALS['strCampaignPredictedFullDeliveryMessage']  = 'On track';

$GLOBALS['strStatsAnalysisReport']  = '통계 분석 보고서';

// Error messages
$GLOBALS['strReportErrorMissingSheets'] = "보고서에 선택된 워크시트가 없음";
$GLOBALS['strReportErrorUnknownCode']   = "알 수 없는 에러 코드 #";
?>