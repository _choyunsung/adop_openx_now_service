<?php

/*
+---------------------------------------------------------------------------+
| Revive Adserver                                                           |
| http://www.revive-adserver.com                                            |
|                                                                           |
| Copyright: See the COPYRIGHT.txt file.                                    |
| License: GPLv2 or later, see the LICENSE.txt file.                        |
+---------------------------------------------------------------------------+
*/

// Require the initialisation file
require_once '../../init.php';

// Required files
require_once MAX_PATH . '/lib/OA/Dal.php';
require_once MAX_PATH . '/lib/max/Admin/Languages.php';
require_once MAX_PATH . '/www/admin/config.php';
require_once MAX_PATH . '/www/admin/lib-statistics.inc.php';
require_once MAX_PATH . '/lib/OA/Session.php';
require_once MAX_PATH . '/lib/OA/Admin/Menu.php';
require_once MAX_PATH . '/lib/max/other/html.php';
require_once MAX_PATH . '/lib/OA/Auth.php';
require_once MAX_PATH . '/lib/OA/Admin/UI/UserAccess.php';

/*-------------------------------------------------------*/
/* HTML framework                                        */
/*-------------------------------------------------------*/
if ($agencyid != '') {
//     addPageTools($agencyid);
    if (OA_Permission::isAccount(OA_ACCOUNT_ADMIN)) {
    	OA_Admin_Menu::setAgencyPageContext($agencyid, 'agency-edit.php');
    	$doAgency = OA_Dal::staticGetDO('agency', $agencyid);
        MAX_displayInventoryBreadcrumbs(array(array("name" => $doAgency->name)), "agency");
    	phpAds_PageHeader("4.1.3");
    	phpAds_ShowSections(array("4.1.2", "4.1.3"));
    } else {
        phpAds_PageHeader(null);
        phpAds_ShowSections(array("4.1", "4.2", "4.3", "4.4"));
    }
} else {
	MAX_displayInventoryBreadcrumbs(array(array("name" => phpAds_getClientName($agencyid))), "agency");
	phpAds_PageHeader("4.1.1");
	phpAds_ShowSections(array("4.1.1"));
}
$tabindex = 1;

//phpAds_PageHeader(null, 3);
/*-------------------------------------------------------*/
/* Main code                                             */
/*-------------------------------------------------------*/

require_once MAX_PATH . '/lib/OA/Admin/Template.php';

$oTpl = new OA_Admin_Template('adulturl_sub.html');

// Ensure that any template variables for the authentication plugin are set
$oPlugin = OA_Auth::staticGetAuthPlugin();
$oPlugin->setTemplateVariables($oTpl);

$comIdx = $_GET['comIdx'];
$oTpl->assign('infomessage', OA_Session::getMessage());

$oTpl->assign('entityIdName', 'agencyid');
$oTpl->assign("comIdx", $comIdx);
$oTpl->assign('subPage', 'adulturl_sub.php?comIdx=' . $comIdx);

$comList = getComList();
$oTpl->assign('comList', $comList["result"]);


$pageNum = ($_GET['page']) ? $_GET['page'] : 1;
$networkData["listCount"] = 100;
$sNo = ( $networkData["listCount"] * $pageNum ) - $networkData["listCount"] + 1;
$oTpl->assign('no', $sNo);

$networkData["comIdx"] = $comIdx;
$networkData["startNo"] = ($sNo - 1);//($_GET['page']) ? $_GET['page'] : 1;



$adultList = adultSubList($networkData);

$totCount = $adultList["totalCount"];

$strPaging = "";

$list = $networkData["listCount"];
$b_pageNum_list = 10; //블럭에 나타낼 페이지 번호 갯수
$block = ceil($pageNum/$b_pageNum_list); //현재 리스트의 블럭 구하기
 
$b_start_page = ( ($block - 1) * $b_pageNum_list ) + 1; //현재 블럭에서 시작페이지 번호
$b_end_page = $b_start_page + $b_pageNum_list - 1; //현재 블럭에서 마지막 페이지 번호

$total_page =  ceil($totCount/$list); //총 페이지 수

	if ($b_end_page > $total_page)
		$b_end_page = $total_page;
 
    if ( $pageNum <= 1 ) {
        $strPaging .= "<font style='padding-left:5px' size=2  color=red>First</font>";
    } else {
        $strPaging .= "<font style='padding-left:5px' size=2><a href=\"?comIdx=" . $comIdx . "&page=" . $page . "\">First</a></font>";
    }
 
    if ($block <= 1 ) {
        $strPaging .= "<font style='padding-left:5px'> </font>";
    } else {
        $strPaging .= "<font style='padding-left:5px' size=2><a href=\"?comIdx=" . $comIdx . "&page=" . ($b_start_page-1) . "\">Prev</a></font>";
	}
 
    for($j = $b_start_page; $j <=$b_end_page; $j++)
    {
        if($pageNum == $j) {
            $strPaging .= "<font style='padding-left:5px' size=2 color=red>" . $j . "</font>";
    	} else {
            $strPaging .= "<font style='padding-left:5px' size=2><a href=\"?comIdx=" . $comIdx . "&page=" . $j . "\">" . $j . "</a></font>";
        }
    }
 
    $total_block = ceil($total_page/$b_pageNum_list);
 
    if($block >= $total_block) {
    	$strPaging .= "<font style='padding-left:5px'> </font>";
    } else {
        $strPaging .= "<font style='padding-left:5px' size=2><a href=\"?comIdx=" . $comIdx . "&page=" . ($b_end_page + 1) . "\">Next</a></font>";
    }

    if($pageNum >= $total_page) {
    	$strPaging .= "<font style='padding-left:5px' size=2 color=red>Last</font>";
	} else {
    	$strPaging .= "<font style='padding-left:5px' size=2><a href=\"?comIdx=" . $comIdx . "&page=" . $total_page . "\">Last</a></font>";
 	}




$oTpl->assign('strPaging', $strPaging);

$oTpl->assign('adult_list', $adultList["result"]);
$oTpl->display();

/*-------------------------------------------------------*/
/* HTML framework                                        */
/*-------------------------------------------------------*/

phpAds_PageFooter();

function adultSubList($networkData) {
	$conf = $GLOBALS ['_MAX'] ['CONF'];
	$url = "http://" . $conf['webpath']['api_url'] . ":8091/m/adult/list/detail/" . $conf['webpath']['api_key'];
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
	curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: application/json"));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($networkData));
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
	$string = curl_exec($ch);
	curl_close($ch);

	$string = json_decode($string, true);

	return $string;
}

function getComList() {
	$networkData["startNo"] = 0;
	$networkData["listCount"] = 1000;
	
	$conf = $GLOBALS ['_MAX'] ['CONF'];
	//$url = "http://" . $conf['webpath']['api_url'] . ":8091/m/adult/list/" . $conf['webpath']['api_key'];
	$url = "http://" . $conf['webpath']['api_url'] . ":8091/m/company/" . $conf['webpath']['api_key'];
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
	curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: application/json"));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($networkData));
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
	$string = curl_exec($ch);
	curl_close($ch);

	$string = json_decode($string, true);

	$publishers = $GLOBALS[session][user]->aUser[publishers];                 // 등록된 매체사 코드
	$account_id = $GLOBALS[session][user]->aUser[default_account_id];         // 관리자 등급

	if(!$publishers) {
	    $publishers = $_GET['comIdx'];
	}
	
	$company['status'] = "ok";
	
	if(count($string['result']) > 0) {
		foreach($string['result'] as $key) {
			if($publishers == $key['comNo']) {
				$company['result'][0]['siteNm'] = $key['siteNm'];
				$company['result'][0]['comNo'] = $key['comNo'];
			}
		}
	}
	$string = $company;
	
	return $string;
}

/**
		curl_setopt($ch, CURLOPT_URL, $url);
    	curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: application/json"));
    	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($networkData));
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
    	$resData = curl_exec($ch);
 */

$strScript = "<script type=\"text/javascript\">
				function adultUrlDelFunc() {
					//$(\"#adultSubForm\").serialize()

					var str = \"\";  
				    $(\"input:checkbox:checked\").each(function (index) {
				        str += \"{\'adtIdx\':\" + $(this).val() + \"},\";
				    });
					str = str.substring( 0, str.length - 1 );
                    var strData = {'apiKey':'" . $conf['webpath']['api_key'] . "','param': '[' + str + ']'};
					if( str && confirm(\"" . $GLOBALS['strAdultUrlDelMsg'] . "\") ) {
							
						$.ajax({
				            url: \"http://" . $GLOBALS['_MAX']['CONF']['webpath']['api_url']. ":8091/m/adult/list/delete\",
				            type: \"GET\",
				            dataType: \"jsonp\",
				           	jsonp: \"callback\",
				            data: strData,
				            contentType: \"application/json; charset=utf-8\",
				            success: function (data) {
				            	//alert(JSON.stringify(data));
				            	location.reload();
				            },
				            error: function(){
				              alert(\"Cannot get data\");
				            }
				        });
					} else {
				       alert(\"Checkbox Values Empty\");
				    }
				}
		
				function adultUrlAddFunc() {
					var maskHeight = $(document).height();  
					var maskWidth = $(window).width();  
		
					$('#mask').css({'width':maskWidth,'height':maskHeight});  
			
					$('#mask').fadeIn(1000);
					$('#mask').fadeTo(\"slow\",0.8);    

					$('#AdultUrlAddLayer').show();
				}
		
				function adultUrlSaveFunc() {
					var insertData = {
					         \"apiKey\":\"" . $conf['webpath']['api_key'] . "\", \"comIdx\": $(\"#addComIdx\").val(), \"url\": $(\"#addUrl\").val()
					     };

				    var paramUrl = $(\"#addComIdx\").val() + '/' + encodeURIComponent($(\"#addUrl\").val());
					if( confirm(\"" . $GLOBALS['strAdultUrlAddMsg'] . "\") ) {
	
					      $.ajax({
					           url: \"http://" . $GLOBALS['_MAX']['CONF']['webpath']['api_url']. ":8091/m/adult/list/insert/\",
					           type: \"GET\",
					           dataType: \"jsonp\",
					           jsonp: \"callback\",
					           contentType: \"application/json; charset=utf-8\",
					           processData: true,
					           data: insertData,
					           success: function (data) {
					             //alert(JSON.stringify(data));
					             location.reload();
					           },
					           error: function(){
					             alert(\"Cannot get data\");
					           }
					       });
					}
				}
		
				$(document).ready(function() {
					$('#mask, .close').click(function(e) {  
					    e.preventDefault();
					    $('#mask, #AdultUrlAddLayer').hide();
					});

					$('#mask').click(function() {  
					    $(this).hide();
					    $('#AdultUrlAddLayer').hide();
					});
				});
			</script>";

echo $strScript;

function addPageTools($agencyId)
{
    addPageLinkTool($GLOBALS["strLinkUser_Key"], "agency-user-start.php?agencyid={$agencyId}", "iconAdvertiserAdd", $GLOBALS["strAddNew"] );
}


?>
