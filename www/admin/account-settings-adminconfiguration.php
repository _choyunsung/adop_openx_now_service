<?php

/*
Revive Ad server 3.0.2 and This is custom file to configure admin details from Dreamajax Technologies
*/

// Require the initialisation file
require_once '../../init.php';

// Required files
require_once MAX_PATH . '/lib/OA/Admin/Option.php';
require_once MAX_PATH . '/lib/OA/Admin/Settings.php';
require_once MAX_PATH . '/lib/max/Plugin/Translation.php';
require_once MAX_PATH . '/www/admin/config.php';
require_once MAX_PATH . '/lib/OA/Creative/File.php';
require_once MAX_PATH . '/lib/max/Plugin/Translation.php';

// Required files
require_once MAX_PATH . '/lib/OA/Dal.php';
require_once MAX_PATH . '/lib/max/other/common.php';
require_once MAX_PATH . '/lib/max/other/html.php';
require_once MAX_PATH . '/lib/OA/Admin/UI/component/Form.php';
require_once MAX_PATH . '/lib/max/Admin_DA.php';
require_once MAX_PATH . '/lib/OA/Dal/Delivery/mysql.php';

// Security check
OA_Permission::enforceAccount(OA_ACCOUNT_ADMIN);

 $table_prefix = $GLOBALS['_MAX']['CONF']['table']['prefix'];
// Create a new option object for displaying the setting's page's HTML form


$oOptions = new OA_Admin_Option('settings');

$prefSection = "adminconfiguration";

$setPref = $oOptions->getSettingsPreferences($prefSection);

$title = $setPref[$prefSection]['name'];


// Display the settings page's header and sections

$oHeaderModel = new OA_Admin_UI_Model_PageHeaderModel($title);
phpAds_PageHeader('account-settings-index', $oHeaderModel);


if (isset($_POST['submitok']) && $_POST['submitok'] == 'true')
{

		
		
			$publisher =!empty($_POST['pubshare']) ? $_POST['pubshare'] : '';

				if($publisher<=100)
				{
					if(!empty($publisher))
					{


						$select2=OA_Dal_Delivery_query("select * from {$table_prefix}dj_admin_configuration");

						if(OA_Dal_Delivery_numRows($select2)>0)
						{


							OA_Dal_Delivery_query("update {$table_prefix}dj_admin_configuration set pubshare ='".$publisher."' ");

							 	 	
						}
						else
						{
							OA_Dal_Delivery_query("INSERT INTO {$table_prefix}dj_admin_configuration(pubshare) VALUES ('".$publisher."')");
			
							 	 	 
						}

					}
					else
					{

						print_r("<font color='red'> All Field Values are required.</font>");	 


					}	
				}
				else
				{
					print_r("<font color='red'> Percentage must be 1-100</font>");
	
				}

	
				if($GLOBALS['_MAX']['CONF']['plugins']['RTB']==1)
				{
					$rtb_time=!empty($_POST['rtbsec']) ? $_POST['rtbsec'] : '';

					if(!empty($rtb_time))
					{
	
						if($rtb_time>0 && $rtb_time<1000)
						{
		
								$select2=OA_Dal_Delivery_query("select * from {$table_prefix}dj_admin_configuration");

								if(OA_Dal_Delivery_numRows($select2)>0)
								{

									OA_Dal_Delivery_query("update {$table_prefix}dj_admin_configuration set rtb_maxrestime ='".$rtb_time."' ");

						
								}
								else
								{

									OA_Dal_Delivery_query("INSERT INTO {$table_prefix}dj_admin_configuration(rtb_maxrestime)VALUES('".$rtb_time."')");
					
											 
								}
						}
						else
						{
							print_r("<font color='red'> Provide valid time in seconds below 1000. </font>");
						}

					}
					else
					{
						print_r("<font color='red'> All Field Values are required. </font>");
					}

		
				}



		
				if(!empty($_POST['sdk_android']))
				{
					$select2=OA_Dal_Delivery_query("select * from {$table_prefix}dj_admin_configuration");

					if(OA_Dal_Delivery_numRows($select2)>0)
					{


						OA_Dal_Delivery_query("update {$table_prefix}dj_admin_configuration set SDK_Androidpath ='".$_POST['sdk_android']."' ");

						 	 	
					}
					else
					{
						OA_Dal_Delivery_query("INSERT INTO {$table_prefix}dj_admin_configuration(SDK_Androidpath)VALUES('".$_POST['sdk_android']."')");
			
						 	 	 
					}
				}
		
		
				if(!empty($_POST['sdk_ios']))
				{
					$select2=OA_Dal_Delivery_query("select * from {$table_prefix}dj_admin_configuration");

					if(OA_Dal_Delivery_numRows($select2)>0)
					{
						OA_Dal_Delivery_query("update {$table_prefix}dj_admin_configuration set SDK_iOSpath ='".$_POST['sdk_ios']."' ");

						 	 	
					}
					else
					{

						OA_Dal_Delivery_query("INSERT INTO {$table_prefix}dj_admin_configuration(SDK_iOSpath)VALUES('".$_POST['sdk_ios']."')");
						 	 	 
					}
				}
	

	
				if(!empty($_POST['tera_path']))
				{
					$select2=OA_Dal_Delivery_query("select * from {$table_prefix}dj_admin_configuration");

					if(OA_Dal_Delivery_numRows($select2)>0)
					{
						OA_Dal_Delivery_query("update {$table_prefix}dj_admin_configuration set tera_path ='".$_POST['tera_path']."' ");

						 	 	
					}
					else
					{

						OA_Dal_Delivery_query("INSERT INTO {$table_prefix}dj_admin_configuration(tera_path)VALUES('".$_POST['tera_path']."')");
						 	 	 
					}
				}
		
		

				$select=OA_Dal_Delivery_query("select * from {$table_prefix}dj_admin_configuration");



				if(OA_Dal_Delivery_numRows($select)>0)
				{
					if($_REQUEST['passback_ads']==true){$is_enabled=1;}else{$is_enabled=0;}
					if($_REQUEST['house_ads']==true){$is_house_enabled=1;}else{$is_house_enabled=0;}
					OA_Dal_Delivery_query("update {$table_prefix}dj_admin_configuration set passback_ads ='".$is_enabled."',house_ads='".$is_house_enabled."'");

						
				}
				else
				{
					if($_REQUEST['passback_ads']==true){$is_enabled=1;}else{$is_enabled=0;}
					if($_REQUEST['house_ads']==true){$is_house_enabled=1;}else{$is_house_enabled=0;}
					OA_Dal_Delivery_query("INSERT INTO {$table_prefix}dj_admin_configuration(passback_ads,house_ads)VALUES('".$is_enabled."','".$is_house_enabled."')");
					
							 
				}

}
// Set the correct section of the settings pages and display the drop-down menu


$select=OA_Dal_Delivery_query("select *from {$table_prefix}dj_admin_configuration");

$row=OA_Dal_Delivery_fetchAssoc($select);

$pubshare = $row['pubshare']; 

$rtbtime=$row['rtb_maxrestime'];

$houseads='';

$djax_mobileappandroid='';

$djax_mobileappios='';

$djax_mobile_terapath='';

$djax_rtb_resetime='';

if($GLOBALS['_MAX']['CONF']['plugins']['HouseAds']==1)
{

	$houseads=array (
		'text'    => 'Plugin Settings',
		'items'   => array (
		   			 array (
								'type'    => 'checkbox',
								'name'    => 'passback_ads',
								'text'    => 'PassBack Tags Enbled',
								'size'    => 100,
								'value'	  => $row['passback_ads']
		    			),
					    array (
				       			 'type'    => 'break'
				  		),
						 array (
								'type'    => 'checkbox',
								'name'    => 'house_ads',
								'text'    => 'House Ads Enbled',
								'size'    => 100,
								'value'	  => $row['house_ads']
		    				),
					    	array (
				       			 'type'    => 'break'
				  		)
			
	)
	);

}

if($GLOBALS['_MAX']['CONF']['plugins']['MobileAPPAndroid']==1)
{

	$djax_mobileappandroid=array ('text'    => 'Mobile SDK Android download link',
		'items'   => array (
		   			 array (
								'type'    => 'text',
								'name'    => 'sdk_android',
								'text'    => 'Mobile  Android  SDK download link',
								'size'    => 100,
								'value'	  => $row['SDK_Androidpath']
		    		),
					    array (
				       			 'type'    => 'break'
				  		)
			
	)
	);
}


if($GLOBALS['_MAX']['CONF']['plugins']['MobileAPPiOS']==1)
{

	$djax_mobileappios=array (
		'text'    => 'Mobile iOS  download link',
		'items'   => array (
		   			 array (
								'type'    => 'text',
								'name'    => 'sdk_ios',
								'text'    => 'Mobile iOS SDK download link',
								'size'    => 100,
								'value'	  => $row['SDK_iOSpath']
		    		),
					    array (
				       			 'type'    => 'break'
				  		)
			
	)
	);
}





$djax_mobile_terapath=array (
		'text'    => 'Tera WURFL Path',
		'items'   => array (
		   			 array (
								'type'    => 'text',
								'name'    => 'tera_path',
								'text'    => 'Tera WURFL Path Settings',
								'size'    => 100,
								'value'	  => $row['tera_path']
		    		),
					    array (
				       			 'type'    => 'break'
				  		)
			
	)
	);



if($GLOBALS['_MAX']['CONF']['plugins']['RTB']==1)
{

$djax_rtb_resetime=array('text'    => 'RTB Buyer Max Response Time',
			'items'   => array(
		   			 	array(
								'type'    => 'text',
								'name'    => 'rtbsec',
								'text'    => 'RTB Buyer Max Response Time in seconds:',
								'size'    => 30,
								'value'	  => $rtbtime	
		    				),
					    	array
						(
				       			 'type'    => 'break'
				  		)
			
					));


}

if($GLOBALS['_MAX']['CONF']['plugins']['RTB']==1 || $GLOBALS['_MAX']['CONF']['plugins']['Mobilesite'] || $GLOBALS['_MAX']['CONF']['plugins']['MobileAPPAndroid'] || $GLOBALS['_MAX']['CONF']['plugins']['MobileAPPiOS'])
{

$djax_pub_share=array (
		'text'    => 'Publisher Share Percentage',
		'items'   => array (
		   			 array (
		       			 	'type'    => 'text',
						'name'    => 'pubshare',
						'text'    => 'Publisher Share Percentage(%):',
						'size'    => 30,
						'value'	  => $pubshare	
		    				),
					    array (
				       			 'type'    => 'break'
				  		  )
			
	)
	);

}

$aSettings=array($houseads,$djax_mobileappandroid,$djax_mobileappios,$djax_mobile_terapath,$djax_pub_share,$djax_rtb_resetime);

$oOptions->show($aSettings, null);

phpAds_PageFooter();

?>
