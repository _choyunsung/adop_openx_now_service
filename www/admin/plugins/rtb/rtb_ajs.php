<?php
use Adrequest\Request;

/* BASE OPENX SETTING --START */

function parseDeliveryIniFile($configPath = null, $configFile = null, $sections = true) {

    if (!$configPath) {
        $configPath = MAX_PATH . '/var';
    }
    if ($configFile) {
        $configFile = '.' . $configFile;
    }
    $host = OX_getHostName();
    $configFileName = $configPath . '/' . $host . $configFile . '.conf.php';
    $conf = @parse_ini_file($configFileName, $sections);
    if (isset($conf['realConfig'])) {
        $realconf = @parse_ini_file(MAX_PATH . '/var/' . $conf['realConfig'] . '.conf.php', $sections);
        $conf = mergeConfigFiles($realconf, $conf);
    }
    if (!empty($conf)) {
        return $conf;
    } elseif ($configFile === '.plugin') {
        $pluginType = basename($configPath);
        $defaultConfig = MAX_PATH . '/plugins/' . $pluginType . '/default.plugin.conf.php';
        $conf = @parse_ini_file($defaultConfig, $sections);
        if ($conf !== false) {
            return $conf;
        }
        echo "Revive Adserver could not read the default configuration file for the {$pluginType} plugin";
        exit(1);
    }
    $configFileName = $configPath . '/default' . $configFile . '.conf.php';
    $conf = @parse_ini_file($configFileName, $sections);
    if (isset($conf['realConfig'])) {
        $conf = @parse_ini_file(MAX_PATH . '/var/' . $conf['realConfig'] . '.conf.php', $sections);
    }
    if (!empty($conf)) {
        return $conf;
    }
    if (file_exists(MAX_PATH . '/var/INSTALLED')) {
        echo "Revive Adserver has been installed, but no configuration file was found.\n";
        exit(1);
    }
    echo "Revive Adserver has not been installed yet -- please read the INSTALL.txt file.\n";
    exit(1);
}
if (!function_exists('mergeConfigFiles')) {

    function mergeConfigFiles($realConfig, $fakeConfig) {

        foreach ($fakeConfig as $key => $value) {
            if (is_array($value)) {
                if (!isset($realConfig[$key])) {
                    $realConfig[$key] = array ();
                }
                $realConfig[$key] = mergeConfigFiles($realConfig[$key], $value);
            } else {
                if (isset($realConfig[$key]) && is_array($realConfig[$key])) {
                    $realConfig[$key][0] = $value;
                } else {
                    if (isset($realConfig) && !is_array($realConfig)) {
                        $temp = $realConfig;
                        $realConfig = array ();
                        $realConfig[0] = $temp;
                    }
                    $realConfig[$key] = $value;
                }
            }
        }
        unset($realConfig['realConfig']);
        return $realConfig;
    }
}

function OX_getMinimumRequiredMemory($limit = null) {

    if ($limit == 'maintenance') {
        return 134217728;
    }
    return 134217728;
}

function OX_getMemoryLimitSizeInBytes() {

    $phpMemoryLimit = ini_get('memory_limit');
    if (empty($phpMemoryLimit) || $phpMemoryLimit == -1) {
        return -1;
    }
    $aSize = array (
        'G' => 1073741824,
        'M' => 1048576,
        'K' => 1024
    );
    $phpMemoryLimitInBytes = $phpMemoryLimit;
    foreach ($aSize as $type => $multiplier) {
        $pos = strpos($phpMemoryLimit, $type);
        if (!$pos) {
            $pos = strpos($phpMemoryLimit, strtolower($type));
        }
        if ($pos) {
            $phpMemoryLimitInBytes = substr($phpMemoryLimit, 0, $pos) * $multiplier;
        }
    }
    return $phpMemoryLimitInBytes;
}

function OX_checkMemoryCanBeSet() {

    $phpMemoryLimitInBytes = OX_getMemoryLimitSizeInBytes();
    if ($phpMemoryLimitInBytes == -1) {
        return true;
    }
    OX_increaseMemoryLimit($phpMemoryLimitInBytes + 1);
    $newPhpMemoryLimitInBytes = OX_getMemoryLimitSizeInBytes();
    $memoryCanBeSet = ($phpMemoryLimitInBytes != $newPhpMemoryLimitInBytes);
    @ini_set('memory_limit', $phpMemoryLimitInBytes);
    return $memoryCanBeSet;
}

function OX_increaseMemoryLimit($setMemory) {

    $phpMemoryLimitInBytes = OX_getMemoryLimitSizeInBytes();
    if ($phpMemoryLimitInBytes == -1) {
        return true;
    }
    if ($setMemory > $phpMemoryLimitInBytes) {
        if (@ini_set('memory_limit', $setMemory) === false) {
            return false;
        }
    }
    return true;
}

function setupConfigVariables() {

    $GLOBALS['_MAX']['MAX_DELIVERY_MULTIPLE_DELIMITER'] = '|';
    $GLOBALS['_MAX']['MAX_COOKIELESS_PREFIX'] = '__';
    $GLOBALS['_MAX']['thread_id'] = uniqid();
    $GLOBALS['_MAX']['SSL_REQUEST'] = false;
    if ((!empty($_SERVER['SERVER_PORT']) && !empty($GLOBALS['_MAX']['CONF']['openads']['sslPort']) && ($_SERVER['SERVER_PORT'] == $GLOBALS['_MAX']['CONF']['openads']['sslPort'])) || (!empty($_SERVER['HTTPS']) && ((strtolower($_SERVER['HTTPS']) == 'on') || ($_SERVER['HTTPS'] == 1))) || (!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && (strtolower($_SERVER['HTTP_X_FORWARDED_PROTO']) == 'https')) || (!empty($_SERVER['HTTP_X_FORWARDED_SSL']) && (strtolower($_SERVER['HTTP_X_FORWARDED_SSL']) == 'on')) || (!empty($_SERVER['HTTP_FRONT_END_HTTPS']) && (strtolower($_SERVER['HTTP_FRONT_END_HTTPS']) == 'on')) || (!empty($_SERVER['FRONT-END-HTTPS']) && (strtolower($_SERVER['FRONT-END-HTTPS']) == 'on'))) {
        $GLOBALS['_MAX']['SSL_REQUEST'] = true;
    }
    $GLOBALS['_MAX']['MAX_RAND'] = isset($GLOBALS['_MAX']['CONF']['priority']['randmax']) ? $GLOBALS['_MAX']['CONF']['priority']['randmax'] : 2147483647;
    list($micro_seconds, $seconds) = explode(" ", microtime());
    $GLOBALS['_MAX']['NOW_ms'] = round(1000 * ((float)$micro_seconds + (float)$seconds));
    if (substr($_SERVER['SCRIPT_NAME'], -11) != 'install.php') {
        $GLOBALS['serverTimezone'] = date_default_timezone_get();
        OA_setTimeZoneUTC();
    }
    /*DAC015*/
    $GLOBALS['_MAX']['djax_campaign_details'] = array ();
    $GLOBALS['_MAX']['highest_bids'] = array ();
}
/*DAC015*/

function djax_getCapabilityvalue($url) {

    $rfd = fopen($url, 'r');
    stream_set_blocking($rfd, true);
    stream_set_timeout($rfd, 20); // 20-second timeout
    $data = stream_get_contents($rfd);
    $status = stream_get_meta_data($rfd);
    fclose($rfd);

    if ($status['timed_out']) {
        $xml = simplexml_load_file($url);
    } else {
        $xml = simplexml_load_string($data);
    }

    return $xml;

}


/*DAC015*/
function setupServerVariables() {

    if (empty($_SERVER['REQUEST_URI'])) {
        $_SERVER['REQUEST_URI'] = $_SERVER['SCRIPT_NAME'];
        if (!empty($_SERVER['QUERY_STRING'])) {
            $_SERVER['REQUEST_URI'] .= '?' . $_SERVER['QUERY_STRING'];
        }
    }
}

function setupDeliveryConfigVariables() {

    if (!defined('MAX_PATH')) {
        define('MAX_PATH', dirname(__FILE__) . '/../../../../');
    }
    if (!defined('OX_PATH')) {
        define('OX_PATH', MAX_PATH);
    }
    if (!defined('LIB_PATH')) {
        define('LIB_PATH', MAX_PATH . DIRECTORY_SEPARATOR . 'lib' . DIRECTORY_SEPARATOR . 'OX');
    }
    if (!(isset($GLOBALS['_MAX']['CONF']))) {
        $GLOBALS['_MAX']['CONF'] = parseDeliveryIniFile();
    }
    setupConfigVariables();
}

function OA_setTimeZone($timezone) {

    date_default_timezone_set($timezone);
    $GLOBALS['_DATE_TIMEZONE_DEFAULT'] = $timezone;
}

function OA_setTimeZoneUTC() {

    OA_setTimeZone('UTC');
}

function OA_setTimeZoneLocal() {

    $tz = !empty($GLOBALS['_MAX']['PREF']['timezone']) ? $GLOBALS['_MAX']['PREF']['timezone'] : 'GMT';
    OA_setTimeZone($tz);
}

function OX_getHostName() {

    if (!empty($_SERVER['HTTP_HOST'])) {
        $host = explode(':', $_SERVER['HTTP_HOST']);
        $host = $host[0];
    } else if (!empty($_SERVER['SERVER_NAME'])) {
        $host = explode(':', $_SERVER['SERVER_NAME']);
        $host = $host[0];
    }
    return $host;
}

function OX_getHostNameWithPort() {

    if (!empty($_SERVER['HTTP_HOST'])) {
        $host = $_SERVER['HTTP_HOST'];
    } else if (!empty($_SERVER['SERVER_NAME'])) {
        $host = $_SERVER['SERVER_NAME'];
    }
    return $host;
}

function setupIncludePath() {

    static $checkIfAlreadySet;
    if (isset($checkIfAlreadySet)) {
        return;
    }
    $checkIfAlreadySet = true;
    $oxPearPath = MAX_PATH . DIRECTORY_SEPARATOR . 'lib' . DIRECTORY_SEPARATOR . 'pear';
    $oxZendPath = MAX_PATH . DIRECTORY_SEPARATOR . 'lib';
    set_include_path($oxPearPath . PATH_SEPARATOR . $oxZendPath . PATH_SEPARATOR . get_include_path());
}

OX_increaseMemoryLimit(OX_getMinimumRequiredMemory());
if (!defined('E_DEPRECATED')) {
    define('E_DEPRECATED', 0);
}
setupServerVariables();
setupDeliveryConfigVariables();
$conf = $GLOBALS['_MAX']['CONF'];
$GLOBALS['_OA']['invocationType'] = array_search(basename($_SERVER['SCRIPT_FILENAME']), $conf['file']);
if (!empty($conf['debug']['production'])) {
    error_reporting(E_ALL & ~(E_NOTICE | E_WARNING | E_DEPRECATED | E_STRICT));
} else {
    error_reporting(E_ALL & ~(E_DEPRECATED | E_STRICT));
}

/* BASE OPENX SETTING --END */

/* AUCTION CURL */

function Data_Connect_Curl($url="",$postdata){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
    curl_setopt($ch, CURLOPT_POSTFIELDS,$postdata);
    curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: application/json",'Content-Length: ' . strlen($postdata)));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
    curl_setopt($ch, CURLOPT_TIMEOUT, 1);
    $string = curl_exec($ch);
    curl_close($ch);
    $string = json_decode($string, true);

    return $string;
}

function make_atom_json_data()
{
    $_data_value['id'] = (int)rand();
    $_data_value['site']['id'] = (int)$_GET['zoneid'];
    
    if($_GET['loc'])
        $_data_value['site']['loc'] = $_GET['loc'];
    else
        $_data_value['site']['loc'] = $_SERVER['HTTP_REFERER'];

    $_data_value['site']['realip'] = RealIP();
    $_data_value['site']['agent'] = $_SERVER['HTTP_USER_AGENT'];

    return json_encode($_data_value);
}

function RealIP()
{
    if (isset($_SERVER)){
        if(isset($_SERVER["HTTP_X_FORWARDED_FOR"])){
            $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
            if(strpos($ip,",")){
                $exp_ip = explode(",",$ip);
                $ip = $exp_ip[0];
            }
        }else if(isset($_SERVER["HTTP_CLIENT_IP"])){
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        }else{
            $ip = $_SERVER["REMOTE_ADDR"];
        }
    }else{
        if(getenv('HTTP_X_FORWARDED_FOR')){
            $ip = getenv('HTTP_X_FORWARDED_FOR');
            if(strpos($ip,",")){
                $exp_ip=explode(",",$ip);
                $ip = $exp_ip[0];
            }
        }else if(getenv('HTTP_CLIENT_IP')){
            $ip = getenv('HTTP_CLIENT_IP');
        }else {
            $ip = getenv('REMOTE_ADDR');
        }
    }
    return $ip;
}

function MAX_javascriptToHTML($string, $varName, $output = true, $localScope = true, $width, $height) {

    if(!empty($string))
    {
        $jsLines = array ();
        $search = array("\\","\r",'"',"'",'<');
        $replace = array("\\\\",'','\"',"\\'",'<"+"');
        
        $string = str_replace($search, $replace, $string);
        $lines = explode("\n", $string);
        foreach ($lines as $line) {
            if (trim($line) != '') {
                $jsLines[] = $varName . ' += "' . trim($line) . '\n";';
            }
        }
        $buffer = (($localScope) ? 'var ' : '') . $varName . " = '';\n";
        $buffer .= implode("\n", $jsLines);
    
        if ($output == true) {
            $buffer .="\n
            if({$varName} != '')
            {
            var iframe = document.write('<iframe id=\'{$varName}\' width=\'".$width."px\' height=\'".$height."px\' border=0 scrolling=no frameborder=0 ></iframe>');";
            $buffer .= "var ifrm = document.getElementById('{$varName}');
    ifrm = (ifrm.contentWindow) ? ifrm.contentWindow : (ifrm.contentDocument.document) ? ifrm.contentDocument.document : ifrm.contentDocument;
    ifrm.document.open();
    ifrm.document.write({$varName});
    ifrm.document.close();}\n";
    
        }
    
        return $buffer;
    }
}



/* AUCTION CURL */

$_JSON_DATA = make_atom_json_data();

//$output = Data_Connect_Curl('http://internal-Atom-PN-LB-736622659.ap-northeast-1.elb.amazonaws.com/service/bidding',$_JSON_DATA);
$output = Data_Connect_Curl('http://atom.adop.cc/service/bidding',$_JSON_DATA);
$varName ='OX_' . substr(md5(uniqid('', 1)), 0, 8);

$_out_put_html = $output['seatbid'][0]['bid'][0]['adm'];

echo MAX_javascriptToHTML($_out_put_html, $varName,true,true, $output['seatbid'][0]['bid'][0]['width'],$output['seatbid'][0]['bid'][0]['height'] );


function origin_js_func($zoneid) {

    echo "var m3_u = (location.protocol=='https:'?'https://" . $GLOBALS['_MAX']['CONF']['webpath']['delivery'] . "/ajs.php':'http://" . $GLOBALS['_MAX']['CONF']['webpath']['delivery'] . "/ajs.php');\n";
    echo "var m3_r = Math.floor(Math.random()*99999999999);\n";
    echo "if (!document.MAX_used) document.MAX_used = ',';\n";
    echo "document.write (\"<scr\"+\"ipt type='text/javascript' src='\"+m3_u);\n";
    echo "document.write (\"?zoneid=" . $zoneid . "\");\n";
    echo "document.write ('&amp;cb=' + m3_r);\n";
    echo "if (document.MAX_used != ',') document.write (\"&amp;exclude=\" + document.MAX_used);\n";
    echo "document.write (document.charset ? '&amp;charset='+document.charset : (document.characterSet ? '&amp;charset='+document.characterSet : ''));\n";
    echo "document.write (\"&amp;loc=\" + escape(window.location));\n";
    echo "if (document.referrer) document.write (\"&amp;referer=\" + escape(document.referrer));\n";
    echo "if (document.mmm_fo) document.write (\"&amp;mmm_fo=1\");\n";
    echo "document.write (\"'><\/scr\"+\"ipt>\");\n";
}


if($_out_put_html == ""){

    origin_js_func($_GET['zoneid']);
    exit;
}

?>



