<?php

/*	Track  each unique viewers in djax (single users from single system not based on user agent )	*/

function djax_tracking_viewer()
{
    $aConf = $GLOBALS['_MAX']['CONF'];

    $unique_parameters=$GLOBALS['_MAX']['device'];

    $type=($unique_parameters['is_wireless_device']=='true')?'mobile':'web';

    $app_id=$_REQUEST['device_appid'];

    $udid=$_REQUEST['udid'];

    $user_agent=(!empty($_REQUEST['useragent']))?$_REQUEST['useragent']:$GLOBALS['_MAX']['CLIENT']['ua'];


    if($type=='web' && empty($app_id))
    {
        $os=$GLOBALS['_MAX']['CLIENT']['os'];

        $app_id='';

        $os_version=$GLOBALS['_MAX']['CLIENT']['version'];

        $device_model='';

        $device_type='Display';

        $screen_resolution=$_GET['screen_resoultion'];

        if($aConf['plugins']['Deliveryoptions']==1)
        {
            $is_carrier=djax_carrier();
        }
        else
        {
            $is_carrier='';
        }

        $viewer_id=djax_uniqueViewerID($screen_resolution,$os,$os_version,$is_carrier,true);



    }
    else if($type=='mobile' || !empty($app_id))
    {

        $os=(!empty($_REQUEST['deviceos']))?$_REQUEST['deviceos']:$unique_parameters['device_os'];

        $os_version=(!empty($_REQUEST['deviceosversion']))?$_REQUEST['deviceosversion']:$unique_parameters['deviceos_version'];

        $device_model=(!empty($_REQUEST['devicemodel']))?$_REQUEST['devicemodel']:$unique_parameters['device_model'];

        if($unique_parameters['is_tablet']=='true')
        {
            $device_type='Tablet';
        }
        else
        {
            $device_type='Mobile';
        }

        if(!empty($_REQUEST['screenwidth']) && !empty($_REQUEST['screenheight']))
        {
            $screen_resolution=$_REQUEST['screenwidth']."X".$_REQUEST['screenheight'];

        }
        else
        {
            $screen_resolution=$unique_parameters['device_width']."X".$unique_parameters['device_height'];
        }
        $brand_name=(!empty($_REQUEST['devicebrand']))?$_REQUEST['devicebrand']:$unique_parameters['device_brand'];

        //$is_carrier=(!empty($_REQUEST['carrier'])?$_REQUEST['carrier']:djax_carrier());



        $viewer_id=djax_uniqueViewerID($screen_resolution,$os,$os_version,$device_model,$brand_name,$app_id,$udid,$useragnet);

    }

    $_SESSION['viewer_id']=$viewer_id;

    setcookie('viewer_id',$viewer_id,9999999999);

    $geo_records=$GLOBALS['_MAX']['CLIENT_GEO'];

    $country_code=(!empty($_REQUEST['country']))?$_REQUEST['country']:$GLOBALS['_MAX']['CLIENT_GEO']['country_code'];

    $continent=$GLOBALS['_MAX']['CLIENT_GEO']['country_code'];

    $region=(!empty($_REQUEST['region']))?$_REQUEST['region']:$GLOBALS['_MAX']['CLIENT_GEO']['region'];

    $city=(!empty($_REQUEST['city']))?$_REQUEST['city']:$GLOBALS['_MAX']['CLIENT_GEO']['city'];

    $lattitude=(!empty($_REQUEST['lattitude']))?$_REQUEST['lattitude']:$GLOBALS['_MAX']['CLIENT_GEO']['latitude'];

    $longitude=(!empty($_REQUEST['longitude']))?$_REQUEST['longitude']:$GLOBALS['_MAX']['CLIENT_GEO']['longitude'];

    $isp=(!empty($_REQUEST['carrier']))?$_REQUEST['carrier']:$GLOBALS['_MAX']['CLIENT_GEO']['isp'];

    $area_code=(!empty($_REQUEST['postal_code']))?'':$GLOBALS['_MAX']['CLIENT_GEO']['area_code'];

    $postal_code=(!empty($_REQUEST['postal_code']))?$_REQUEST['postal_code']:$GLOBALS['_MAX']['CLIENT_GEO']['postal_code'];

    $country_query = OA_Dal_Delivery_query("SELECT name FROM {$aConf['table']['prefix']}dj_country WHERE country_code='$country_code'")or die(mysql_error());

    $fetch_country = OA_Dal_Delivery_fetchAssoc($country_query)or die(mysql_error());

    $country_name = ucfirst($fetch_country['name']);

    $displaywidth=$_REQUEST['displaywidth'];

    $displayheight=$_REQUEST['displayheight'];

    if($_REQUEST['language'])
    {
        $language =$_REQUEST['language'];
    }
    else
    {
        $temp_var=explode(',',$_SERVER['HTTP_ACCEPT_LANGUAGE']);

        $language =$temp_var[0];
    }

    $deviceappmd5=(!empty($_REQUEST['device_app_md5']))?$_REQUEST['device_app_md5']:'';

    $deviceappsha1=(!empty($_REQUEST['device_app_sha1']))?$_REQUEST['device_app_sha1']:'';

    $select_query="select * from {$aConf['table']['prefix']}dj_track_viewers where viewer_id='$viewer_id'";

    $query=OA_Dal_Delivery_query($select_query) or die(mysql_error());


    if(OA_Dal_Delivery_numRows($query)==0)
    {

        $insert_query="INSERT INTO {$aConf['table']['prefix']}dj_track_viewers(`viewer_id`, `device_type`,`language`, `os`, `os_version`, `screen_resolution`, `device_model_name`,device_brand,`app_device_id`,udid,display_width,display_height,didmd5,didsha1,user_name,useremail,userphone,ua) VALUES ('$viewer_id','$device_type','".$language."','$os','$os_version', '$screen_resolution', '$device_model','$brand_name','$app_id','$udid','$displaywidth','$displayheight','".$deviceappmd5."','".$deviceappsha1."','".$_REQUEST['username']."','".$_REQUEST['useremail']."','".$_REQUEST['userphone']."','".$user_agent."')";

        OA_Dal_Delivery_query($insert_query)or die(mysql_error());

    }

    return $viewer_id;
}

function djax_carrier()
{

    $aConf = $GLOBALS['_MAX']['CONF'];

    if($aConf['plugins']['DeliveryOptions']==1)
    {
        $ip=$_SERVER['REMOTE_ADDR'];

        $query="select id from {$aConf['table']['prefix']}dj_carrier_detail where start_ip>='$ip' and end_ip<='$ip'";

        $exe_query=OA_Dal_Delivery_query($query);

        if(OA_Dal_Delivery_numRows($exe_query)>=0)
        {
            $djax_carrier=OA_Dal_Delivery_fetchAssoc($exe_query);
        }

        $carrier_id=(!empty($djax_carrier['id']))?$djax_carrier['id']:0;

        return $carrier_id;
    }

}

function djax_uniqueViewerID($screen_resolution,$os,$os_version,$device_model,$is_carrier,$brand_name,$appid,$udid)
{
    $create_unique_user= $screen_resolution.$os.$os_version.$device_model.$brand_name.$is_carrier.$appid.$udid;

    $cookiePrefix = $GLOBALS['_MAX']['MAX_COOKIELESS_PREFIX'];

    return $cookiePrefix . substr(md5($create_unique_user), 0, 32);
}

function djax_tracking_ad_request($viewerid)
{
    $conf = $GLOBALS['_MAX']['CONF'];

    $user_agent=(!empty($_REQUEST['useragent']))?$_REQUEST['useragent']:$GLOBALS['_MAX']['CLIENT']['ua'];

    $browser_name=$GLOBALS['_MAX']['CLIENT']['long_name'];

    $browser_version=$GLOBALS['_MAX']['CLIENT']['version'];

    if($conf['plugins']['Mobilesite']['RTB']==1)
    {
        $zone_info=djax_zoneinfo($_REQUEST['zoneid']);
    }

    $djax_zoneid=$_REQUEST['zoneid'];

    $date_time=date('Y-m-d H:00:00',mktime(date('H'),0,0,date('m'),date('d'),date('Y')));

    $url=$_REQUEST['loc'];

    $loc=str_replace('//','?',$url);

    $is_homepage=explode('/',$loc);

    $ishomepage=(isset($is_homepage[1]))?0:1;

    $geo_records=$GLOBALS['_MAX']['CLIENT_GEO'];

    $country_code=(!empty($_REQUEST['country']))?$_REQUEST['country']:$GLOBALS['_MAX']['CLIENT_GEO']['country_code'];

    $country_query = OA_Dal_Delivery_query("SELECT name FROM {$conf['table']['prefix']}dj_country WHERE country_code='$country_code'")or die(mysql_error());

    $countryname=(!empty($_REQUEST['countryname']))?$_REQUEST['countryname']:$country_query['name'];

    $continent=$GLOBALS['_MAX']['CLIENT_GEO']['country_code'];

    $region=(!empty($_REQUEST['region']))?$_REQUEST['region']:$GLOBALS['_MAX']['CLIENT_GEO']['region'];

    $city=(!empty($_REQUEST['city']))?$_REQUEST['city']:$GLOBALS['_MAX']['CLIENT_GEO']['city'];

    $lattitude=(!empty($_REQUEST['lattitude']))?$_REQUEST['lattitude']:$GLOBALS['_MAX']['CLIENT_GEO']['latitude'];

    $longitude=(!empty($_REQUEST['longitude']))?$_REQUEST['longitude']:$GLOBALS['_MAX']['CLIENT_GEO']['longitude'];

    $isp=(!empty($_REQUEST['carrier']))?$_REQUEST['carrier']:$GLOBALS['_MAX']['CLIENT_GEO']['isp'];

    $area_code=(!empty($_REQUEST['postal_code']))?'':$GLOBALS['_MAX']['CLIENT_GEO']['area_code'];

    $postal_code=(!empty($_REQUEST['postal_code']))?$_REQUEST['postal_code']:$GLOBALS['_MAX']['CLIENT_GEO']['postal_code'];

    if($conf['plugins']['DeliveryOptions']==1)
    {
        $is_carrier=(!empty($_REQUEST['carrier'])?$_REQUEST['carrier']:djax_carrier());
    }
    else
    {
        $is_carrier=='';
    }

    $timezone=$_REQUEST['timezone'];

    if($unique_parameters['is_tablet']=='true')
    {
        $device_type='Tablet';
    }
    else if($unique_parameters['is_wireless_device']=='true')
    {
        $device_type='Mobile';
    }
    else
    {
        $device_type='Display';
    }

    $djax_devicetype=(!empty($_REQUEST['connectiontype']))?$_REQUEST['connectiontype']:$device_type;

    $ip=(!empty($_REQUEST['ip']))?$_REQUEST['ip']:$_SERVER['REMOTE_ADDR'];

    $jsversion=(!empty($GLOBALS['_MAX']['CLIENT']['javascript']))?true:false;

    $jsenabled=(!empty($_REQUEST['is_js_enabled']))?$_REQUEST['is_js_enabled']:$jsversion;

    $insert_query="INSERT INTO {$conf['table']['prefix']}dj_track_ad_requests(`viewer_id`, `date_time`, `zone_id`, `bidding_price`, `user_agent`, `browser_version`, `domain`, `site_url`, `site_referrer_url`, `is_home_page`, `is_jsenable`,`carrier_name`, `app_id`, `user_longitude`, `user_latitude`, `connectiontype`, `data_speed`, `country_code`, `country_name`, `region`, `city`, `zipcode`,external_ip,timezone) VALUES
('$viewerid','$date_time','".$djax_zoneid."', '".$zone_info['djax_floor_price']."','$user_agent','$browser_version','".$_SERVER['HTTP_HOST']."','".$_GET['loc']."', '".$_GET['referrer']."',$ishomepage,'".$jsenabled."','".$is_carrier."','".$_REQUEST['device_appid']."','".$longitude."','".$lattitude."','".$djax_devicetype."','".$_REQUEST['dataspeed']."','".$country_code."','$countryname','$region','$city','$postal_code','$ip','$timezone')";



    OA_Dal_Delivery_query($insert_query);

    return mysql_insert_id();
}

function djax_zoneinfo($zoneid)
{

    $conf = $GLOBALS['_MAX']['CONF'];

    $query="select djax_floor_price from  ".OX_escapeIdentifier($conf['table']['prefix'].$conf['table']['zones'])."   where zoneid='$zoneid'";

    $exe_query=OA_Dal_Delivery_query($query);

    $record= OA_Dal_Delivery_fetchAssoc($exe_query);

    return $record['djax_floor_price'];
}

function djax_track_ad_responses($ad_details,$adid,$type)
{

    $conf = $GLOBALS['_MAX']['CONF'];

    /*Need Make changes based on ECPM */

    $viewerid=(!isset($_COOKIE['viewer_id']))?$_COOKIE['viewer_id']:'';

    $request_id = $_SESSION['request_id'];

    $datetime = date('Y-m-d',mktime(0,0,0,date('m'),date('d'),date('Y')));


    $select_campaign = OA_Dal_Delivery_query("SELECT *
		FROM ".OX_escapeIdentifier($conf['table']['prefix'].$conf['table']['campaigns'])."  AS c,
		     ".OX_escapeIdentifier($conf['table']['prefix'].$conf['table']['banners'])."  AS b WHERE b.bannerid='".$adid."' AND 			      c.campaignid=b.campaignid");

    $fetch_campaign = OA_Dal_Delivery_fetchAssoc($select_campaign);

    $campaign_id = $fetch_campaign['campaignid'];


    foreach($ad_details as $key => $value)
    {


        if($conf['plugins']['Mobilesite']['RTB']==1)
        {

            if($fetch_campaign['is_rtb_campaign']==1)
            {

                $djax_auction=OA_Dal_Delivery_fetchAssoc(OA_Dal_Delivery_query("SELECT auction_type FROM {$conf['table']['prefix']}dj_ad_exchange  where exchange_id='".$fetch_campaign['adx_id']."'"));

                if($djax_data['auction_type']==1)
                {
                    $revenue=$GLOBALS['_MAX']['highest_bids'][0];
                }
                else
                {
                    if(!empty($GLOBALS['_MAX']['highest_bids'][1]))
                    {
                        $revenue=$GLOBALS['_MAX']['highest_bids'][1]+0.01;
                    }
                    else
                    {
                        $revenue=$GLOBALS['_MAX']['highest_bids'][0];
                    }
                }
            }
            else
            {

                $revenue=($value['revenue_type']==1)?($value['revenue']/1000):$value['revenue'];

            }
        }
        else
        {

            $revenue=($value['revenue_type']==1)?($value['revenue']/1000):$value['revenue'];

        }
        if($value['ad_id']==$adid)
        {

            $insert_query="INSERT INTO {$conf['table']['prefix']}dj_track_ad_responses(`request_id`, `win_bid_amount`, `ad_id`,`win_status`) VALUES ('$request_id', '$revenue', '".$value['ad_id']."', '1');";


            OA_Dal_Delivery_query($insert_query);


        }
        else
        {

            if($campaign_id!=$value['placement_id'])
            {

                $insert_query="INSERT INTO {$conf['table']['prefix']}dj_track_ad_responses(`request_id`,`ad_id`, `lose_bid_amount`, `reason_for_lose`, `win_status`) VALUES ('$request_id','".$value['ad_id']."', '".$revenue."', 'Lose due to bid', '0');";

                OA_Dal_Delivery_query($insert_query);
            }


        }

    }


}



?>
