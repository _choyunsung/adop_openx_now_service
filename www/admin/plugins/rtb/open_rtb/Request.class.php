<?php
namespace Adrequest;


use Helper\utils;					use Helper\validate;				

use Rtb_objects\impression_object;	use Rtb_objects\banner_object;		use Rtb_objects\site_object;		use Rtb_objects\segment_object;
use Rtb_objects\publisher_object;	use Rtb_objects\content_object;		use Rtb_objects\producer_object;	use Rtb_objects\user_object;
use Rtb_objects\device_object;		use Rtb_objects\geo_object;			use Rtb_objects\data_object;	

use Rtb_objects\Request_handler;	use Rtb_objects\traffic;

#Include Rtb OBjects
require_once('etc/_Objects/App_object.class.php');			require_once('etc/_Objects/Banner_object.class.php');
require_once('etc/_Objects/Content_object.class.php');		require_once('etc/_Objects/Data_object.class.php');
require_once('etc/_Objects/Device_object.class.php');		require_once('etc/_Objects/Geo_object.class.php');
require_once('etc/_Objects/Impression_object.class.php');	require_once('etc/_Objects/Producer_object.class.php');
require_once('etc/_Objects/Publisher_object.class.php');	require_once('etc/_Objects/Segment_object.class.php');
require_once('etc/_Objects/Site_object.class.php');			require_once('etc/_Objects/Traffic.class.php');
require_once('etc/_Objects/User_object.class.php');			require_once('etc/_Objects/Request_handler.class.php');
#Include helpers
require_once('etc/_Helpers/Utils.class.php');				require_once('etc/_Helpers/Validate.class.php');
require_once('etc/_Helpers/Curl.class.php');

#Include exchange
//require_once('etc/_Exchange/C8.class.php'); 				//require_once('etc/_Exchange/Exchange_warpper.class.php');	
require_once('etc/_Exchange/Open_rtb.class.php');
/**
 * Request warpper class
 *
 * This class handles all the paramenters and send it to SSP component to get a response,
 * It inherits 2 other class params values are defined and validated
 *
 */

class Request extends Request_handler
{
	 public function __construct() 
	 {
		#Define class __construct
	 }
	 
	 private function warpper($obj=false) 
	 {
		return true;
	 }
}

/* End of file request.php */
