	function request(request_id)
	{
		if(request_id !== "No response")
		{
			$.ajax({
				type: "POST",
				url: "src/s_script/ajax.php",
				//dataType: "json",
				data: { request_id: request_id,type:"request"}
				
				})
				.done(function( msg ) {
					$("#data").html(prettyPrintOne(JSON.stringify($.parseJSON(msg), null, 4)));
					
					$("#myModal").modal("show");
					
			});

		}
	}
	function response(response_id)
	{
		if(response_id !== "No response")
		{
			$.ajax({
				type: "POST",
				url: "src/s_script/ajax.php",
				//dataType: "json",
				data: { request_id: response_id,type:"response"}
				
				})
				.done(function( msg ) {
					
					$("#data").html(msg);
					$("#myModal").modal("show");
					
			});

		}
	}

	$(document).ready(function() {
		 $('#reservation').daterangepicker(
		 {
			ranges: {
				 'Today': [moment(), moment()],
				 'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
				 'Last 7 Days': [moment().subtract('days', 6), moment()],
				 'Last 30 Days': [moment().subtract('days', 29), moment()],
				 'This Month': [moment().startOf('month'), moment().endOf('month')],
				 'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
			 },
		},
		function(start, end) {
			$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
		});
	});
	$('#reservation').on('apply', function(ev, picker) {
		var sdate=picker.startDate.format('YYYY-MM-DD');
		var edate=picker.endDate.format('YYYY-MM-DD');
		var myurl=REPURL;
		
		document.location = myurl + "?sdate="+sdate+"&edate="+edate;
	});

