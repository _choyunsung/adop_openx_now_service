<?php
namespace Rtb_objects;
use Helper\utils;
use Helperalidate;
class Geo_object {
    public function save($request_id, $object_id, $flag = '1', $request, $time_stamp) {
        $aConf = $GLOBALS['_MAX']['CONF'];
        if ($flag == '1') {
            $insert_array = array('request_id' => "$request_id", 'user_pk_id' => "$object_id");
        }
        if ($flag == '2') {
            $insert_array = array('request_id' => "$request_id", 'device_pk_id' => "$object_id");
        }
        if ($request_id AND $object_id AND $request AND $time_stamp) {
            $object = $this->geo_object();
            $geo_object = $this->format_geo_object($object);
            $tmp_prepared_request = array_intersect_key($request, $geo_object);
            foreach ($tmp_prepared_request as $key => $data) {
                $prepared_request[$geo_object[$key]['name']] = $data;
            }
            if (isset($prepared_request)) {
                foreach ($prepared_request as $key => $data) {
                    if (is_array($data)) {
                        $formated = implode(",", array_values($data));
                        $json_converted = json_encode($formated);
                    } else {
                        $json_converted = json_encode($data);
                    }
                    $insert_array[$key] = $json_converted;
                }
            }
            $columns = implode(",", array_keys($insert_array));
            $escaped_values = array_values($insert_array);
            $values = implode(",", $escaped_values);
            $sql = "INSERT INTO `{$aConf['table']['prefix']}dj_geo_object`($columns) VALUES ($values)";
//            $result = OA_Dal_Delivery_query($sql) or die(mysql_error());
            $geo_pk_id = mysql_insert_id();
            if ($geo_pk_id) {
                $log_msg = "    [Initandlisten]  Geo object intialized";
                debug_rtb($log_msg, PEAR_LOG_INFO);
                return $geo_pk_id;
            } else return false;
        }
    }
    public function geo_object() {
        $geo_object = array('lattitude' => array('name' => 'lat', 'type' => '_float', 'scope' => 'optional', 'object' => 'Geo_Object'), 'longtude' => array('name' => 'lon', 'type' => '_float', 'scope' => 'optional', 'object' => 'Geo_Object'), 'country' => array('name' => 'country', 'type' => '_string', 'scope' => 'optional', 'object' => 'Geo_Object'), 'region' => array('name' => 'region', 'type' => '_string', 'scope' => 'optional', 'object' => 'Geo_Object'), 'regionfips104' => array('name' => 'regionfips104', 'type' => '_string', 'scope' => 'optional', 'object' => 'Geo_Object'), 'metro' => array('name' => 'metro', 'type' => '_string', 'scope' => 'optional', 'object' => 'Geo_Object'), 'city' => array('name' => 'city', 'type' => '_string', 'scope' => 'optional', 'object' => 'Geo_Object'), 'zip' => array('name' => 'zip', 'type' => '_string', 'scope' => 'optional', 'object' => 'Geo_Object'), 'type' => array('name' => 'type', 'type' => '_integer', 'scope' => 'recommended', 'object' => 'Geo_Object'));
        return $geo_object;
    }
    public function format_geo_object($object) {
        if ($object) {
            $site_object = array();
            foreach ($object as $key => $data) $site_object[$key] = $data;
            return $site_object;
        }
    }
    public function fetch($object) {
        $ob = array();
        if ($object) {
            $geo_object = $this->geo_object();
            $tmp = array_intersect_key($object, $geo_object);
            foreach ($tmp as $key => $data) $ob['geo'][$geo_object[$key]['name']] = $data;
            if ($ob) return json_encode($ob);
            else return false;
        }
    }
};