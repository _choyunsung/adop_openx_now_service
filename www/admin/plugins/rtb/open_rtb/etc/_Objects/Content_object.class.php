<?php
namespace Rtb_objects;
use Helper\utils;
use Helperalidate;
class Content_object {
    public function save($request_id, $object_id, $flag, $request, $time_stamp) {
        $aConf = $GLOBALS['_MAX']['CONF'];
        if ($flag == '1') $insert_array = array('request_id' => "$request_id", 'site_pk_id' => "$object_id");
        if ($flag == '2') $insert_array = array('request_id' => "$request_id", 'app_pk_id' => "$object_id");
        if ($request_id AND $request AND $time_stamp) {
            $object = $this->content_object();
            $content_object = $this->format_content_object($object);
            $tmp_prepared_request = array_intersect_key($request, $content_object);
            foreach ($tmp_prepared_request as $key => $data) $prepared_request[$content_object[$key]['name']] = $data;
            if (isset($prepared_request)) {
                foreach ($prepared_request as $key => $data) {
                    if (is_array($data)) {
                        $formated = implode(",", array_values($data));
                        $json_converted = json_encode($formated);
                    } else {
                        $json_converted = json_encode($data);
                    }
                    $insert_array[$key] = $json_converted;
                }
            }
            $columns = implode(",", array_keys($insert_array));
            $escaped_values = array_values($insert_array);
            $values = implode(",", $escaped_values);
            $sql = "INSERT INTO `{$aConf['table']['prefix']}dj_content_object`($columns) VALUES ($values)";
//            $result = OA_Dal_Delivery_query($sql) or die(mysql_error());
            $content_pk_id = mysql_insert_id();
            if ($content_pk_id) {
                $log_msg = "    [Initandlisten]  Content object intialized";
                debug_rtb($log_msg, PEAR_LOG_INFO);
                return $content_pk_id;
            } else return false;
        }
    }
    public function content_object() {
        $content_object = array('content_id' => array('name' => 'id', 'type' => '_string', 'scope' => 'optional', 'object' => 'Content_Object'), 'content_title' => array('name' => 'title', 'type' => '_string', 'scope' => 'optional', 'object' => 'Content_Object'), 'content_series' => array('name' => 'series', 'type' => '_string', 'scope' => 'optional', 'object' => 'Content_Object'), 'content_season' => array('name' => 'season', 'type' => '_string', 'scope' => 'optional', 'object' => 'Content_Object'), 'content_original_url' => array('name' => 'url', 'type' => '_string', 'scope' => 'optional', 'object' => 'Content_Object'), 'content_category' => array('name' => 'cat', 'type' => '_array_of_string', 'scope' => 'optional', 'object' => 'Content_Object'), 'keywords' => array('name' => 'keywords', 'type' => '_string', 'scope' => 'optional', 'object' => 'Content_Object'), 'content_rating' => array('name' => 'contentrating', 'type' => '_string', 'scope' => 'optional', 'object' => 'Content_Object'), 'user_rating' => array('name' => 'userrating', 'type' => '_string', 'scope' => 'optional', 'object' => 'Content_Object'), 'context' => array('name' => 'context', 'type' => '_string', 'scope' => 'optional', 'object' => 'Content_Object'), 'is_live_stream' => array('name' => 'livestream', 'type' => '_integer', 'scope' => 'optional', 'object' => 'Content_Object'), 'source_relationship' => array('name' => 'sourcerelationship', 'type' => '_integer', 'scope' => 'optional', 'object' => 'Content_Object'), 'length_of_content' => array('name' => 'len', 'type' => '_integer', 'scope' => 'optional', 'object' => 'Content_Object'), 'qagmediarating' => array('name' => 'qagmediarating', 'type' => '_integer', 'scope' => 'optional', 'object' => 'Content_Object'), 'is_embeddable' => array('name' => 'embeddable', 'type' => '_integer', 'scope' => 'optional', 'object' => 'Content_Object'), 'language' => array('name' => 'language', 'type' => '_string', 'scope' => 'optional', 'object' => 'Content_Object'));
        return $content_object;
    }
    public function format_content_object($object) {
        if ($object) {
            $site_object = array();
            foreach ($object as $key => $data) $site_object[$key] = $data;
            return $site_object;
        }
    }
    public function fetch($object) {
        $ob = array();
        if ($object) {
            $content_object = $this->content_object();
            $tmp = array_intersect_key($object, $content_object);
            foreach ($tmp as $key => $data) $ob['content'][$content_object[$key]['name']] = $data;
            if ($ob) return json_encode($ob);
            else return false;
        }
    }
};