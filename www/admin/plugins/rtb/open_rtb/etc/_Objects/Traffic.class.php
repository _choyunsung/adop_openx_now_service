<?php
namespace Rtb_objects;
use Helper\utils;
use Helperalidate;
class Traffic {
    public function save_rtb_objects($request_id, $request, $time_stamp, $for_exchange, $campaign_id) {
        $aConf = $GLOBALS['_MAX']['CONF'];
        if ($request_id AND $request AND $time_stamp AND $for_exchange AND $campaign_id) {
            $Impression_object = new Impression_object();
            $imp_pk_id = $Impression_object->save($request_id, $request, $time_stamp);
            $imp_object = $Impression_object->fetch($request);
            $Banner_object = new Banner_object();
            $banner_object_id = $Banner_object->save($request_id, $imp_pk_id, $request, $time_stamp);
            $banner_object = $Banner_object->fetch($request);
            if ($request['is_for'] == '1') {
                $Site_object = new Site_object();
                $site_object_id = $Site_object->save($request_id, $request, $time_stamp);
                $site_object = $Site_object->fetch($request);
                $Publisher_object = new Publisher_object();
                $publisher_object_id = $Publisher_object->save($request_id, $site_object_id, $flag = '1', $request, $time_stamp);
                $publisher_object = $Publisher_object->fetch($request);
                $Content_object = new Content_object();
                $content_object_id = $Content_object->save($request_id, $site_object_id, $flag = '1', $request, $time_stamp);
                $content_object = $Content_object->fetch($request);
                $Producer_object = new Producer_object();
                $producer_object_id = $Producer_object->save($request_id, $content_object_id, $request, $time_stamp);
                $producer_object = $Producer_object->fetch($request);
            }
            if ($request['is_for'] == '2') {
                $App_object = new App_object();
                $app_object_id = $App_object->save($request_id, $request, $time_stamp);
                $app_object = $App_object->fetch($request);
                $Publisher_object = new Publisher_object();
                $publisher_object_id = $Publisher_object->save($request_id, $app_object_id, $flag = '2', $request, $time_stamp);
                $publisher_object = $Publisher_object->fetch($request);
                $Content_object = new Content_object();
                $content_object_id = $Content_object->save($request_id, $app_object_id, $flag = '2', $request, $time_stamp);
                $content_object = $Content_object->fetch($request);
                $Producer_object = new Producer_object();
                $producer_object_id = $Producer_object->save($request_id, $content_object_id, $request, $time_stamp);
                $producer_object = $Producer_object->fetch($request);
            }
            $User_object = new User_object();
            $user_object_id = $User_object->save($request_id, $request, $time_stamp);
            $user_object = $User_object->fetch($request);
            $Device_object = new Device_object();
            $device_object_id = $Device_object->save($request_id, $request, $time_stamp);
            $device_object = $Device_object->fetch($request);
            $Geo_object = new Geo_object();
            $geo_object_id = $Geo_object->save($request_id, $user_object_id, $flag = '1', $request, $time_stamp);
            $geo_object_id = $Geo_object->save($request_id, $device_object_id, $flag = '2', $request, $time_stamp);
            $geo_object = $Geo_object->fetch($request);
            $Data_object = new Data_object();
            $data_object_id = $Data_object->save($request_id, $user_object_id, $request, $time_stamp);
            $data_object = $Data_object->fetch($request);
            $Segment_object = new Segment_object();
            $segment_object_id = $Segment_object->save($request_id, $data_object_id, $request, $time_stamp);
            $segment_object = $Segment_object->fetch($request);
            if ($request['is_for'] == '1') {
                $insert_array_objects = array('imp_object' => "$imp_object", 'banner_object' => "$banner_object", 'site_object' => "$site_object", 'content_object' => "$content_object", 'device_object' => "$device_object", 'user_object' => "$user_object", 'publisher_object' => "$publisher_object", 'producer_object' => "$producer_object", 'geo_object' => "$geo_object", 'data_object' => "$data_object", 'segment_object' => "$segment_object");
            } else if ($request['is_for'] == '2') {
                $insert_array_objects = array('imp_object' => "$imp_object", 'banner_object' => "$banner_object", 'app_object' => "$app_object", 'content_object' => "$content_object", 'device_object' => "$device_object", 'user_object' => "$user_object", 'publisher_object' => "$publisher_object", 'producer_object' => "$producer_object", 'geo_object' => "$geo_object", 'data_object' => "$data_object", 'segment_object' => "$segment_object",);
            }
            $insert_array = array();
            if ($request_id AND $request AND $time_stamp AND $insert_array_objects) {
                $object = $this->request_object();
                $request_object = $this->format_request_object($object);
                $utils = new Utils;
                $defined_constants = $utils->defined_constants();
                foreach ($request_object as $key => $data) $prepared_request[$defined_constants[$key]['name']] = $data;
                if (isset($request_object)) {
                    foreach ($request_object as $key => $data) {
                        if (array_key_exists($key, $request)) {
                            if (is_array($request[$key])) {
                                $formated = implode(",", array_values($request[$key]));
                                $json_converted = ($formated);
                            } else {
                                $json_converted = ($request[$key]);
                            }
                            $insert_array[$data] = $json_converted;
                        }
                    }
                }
                $insert_array_objects = array_merge($insert_array, $insert_array_objects);
                $tmp_columns = implode(",", array_keys($insert_array_objects));
                $escaped_values = array_values($insert_array_objects);
                $a = array_map(array($this, "encrypt"), $insert_array_objects);
                $tmp_values = implode(",", $a);
                if (!empty($tmp_columns)) $columns = 'request_id,request_for_network,campaign_id,' . $tmp_columns;
                else $columns = 'request_id,request_for_network,campaign_id';
                if (!empty($tmp_values)) $values = $request_id . ',' . $for_exchange . ',' . $campaign_id . ',' . $tmp_values;
                else $values = $request_id . ',' . $for_exchange . ',' . $campaign_id;
                $sql = "INSERT INTO `{$aConf['table']['prefix']}dj_request`($columns) VALUES ($values)";
//                $result = OA_Dal_Delivery_query($sql) or die(mysql_error());
                $pk_id = mysql_insert_id();
                return $insert_array_objects;
            }
        }
    }
    public function encrypt($r) {
        return ('"' . base64_encode($r) . '"');
    }
    public function request_object() {
        $request_object = array('auction_type' => array('name' => 'at', 'type' => '_integer', 'scope' => 'optional', 'object' => 'Request_Object'), 'request_max_time' => array('name' => 'tmax', 'type' => '_integer', 'scope' => 'optional', 'object' => 'Request_Object'), 'buyer_seats' => array('name' => 'wseat', 'type' => '_array_of_string', 'scope' => 'optional', 'object' => 'Request_Object'), 'allimps' => array('name' => 'allimps', 'type' => '_integer', 'scope' => 'optional', 'object' => 'Request_Object'), 'currency' => array('name' => 'cur', 'type' => '_array_of_string', 'scope' => 'optional', 'object' => 'Request_Object'), 'blocked_advertiser_category' => array('name' => 'bcat', 'type' => '_array_of_string', 'scope' => 'optional', 'object' => 'Request_Object'), 'blocked_advertiser' => array('name' => 'badv', 'type' => '_array_of_string', 'scope' => 'optional', 'object' => 'Request_Object'));
        return $request_object;
    }
    public function format_request_object($object) {
        if ($object) {
            $request_object = array();
            foreach ($object as $key => $data) $request_object[$key] = $data['name'];
            return $request_object;
        }
    }
};