<?php
namespace Rtb_objects;
use Helper\utils;
use Helperalidate;
class Device_object {
    public function save($request_id, $request, $time_stamp) {
        $aConf = $GLOBALS['_MAX']['CONF'];
        $insert_array = array('request_id' => "$request_id");
        if ($request_id AND $request AND $time_stamp) {
            $object = $this->device_object();
            $device_object = $this->format_device_object($object);
            $tmp_prepared_request = array_intersect_key($request, $device_object);
            foreach ($tmp_prepared_request as $key => $data) $prepared_request[$device_object[$key]['name']] = $data;
            if (isset($prepared_request)) {
                foreach ($prepared_request as $key => $data) {
                    if (is_array($data)) {
                        $formated = implode(",", array_values($data));
                        $json_converted = json_encode($formated);
                    } else {
                        $json_converted = json_encode($data);
                    }
                    $insert_array[$key] = $json_converted;
                }
            }
            $columns = implode(",", array_keys($insert_array));
            $escaped_values = array_values($insert_array);
            $values = implode(",", $escaped_values);
            $sql = "INSERT INTO `{$aConf['table']['prefix']}dj_device_object`($columns) VALUES ($values)";
//            $result = OA_Dal_Delivery_query($sql) or die(mysql_error());
            $device_pk_id = mysql_insert_id();
            if ($device_pk_id) {
                $log_msg = "    [Initandlisten]  Device object intialized";
                debug_rtb($log_msg, PEAR_LOG_INFO);
                return $device_pk_id;
            } else return false;
        }
    }
    public function device_object() {
        $device_object = array('track' => array('name' => 'dnt', 'type' => '_integer', 'scope' => 'recommended', 'object' => 'Device_Object'), 'user_agent' => array('name' => 'ua', 'type' => '_string', 'scope' => 'recommended', 'object' => 'Device_Object'), 'ip_address' => array('name' => 'ip', 'type' => '_string', 'scope' => 'recommended', 'object' => 'Device_Object'), 'SHA1_hashed_device_id' => array('name' => 'didsha1', 'type' => '_string', 'scope' => 'optional', 'object' => 'Device_Object'), 'MD5_hashed_device_id' => array('name' => 'didmd5', 'type' => '_string', 'scope' => 'optional', 'object' => 'Device_Object'), 'SHA1_hashed_paltform_specific_id' => array('name' => 'dpidsha1', 'type' => '_string', 'scope' => 'optional', 'object' => 'Device_Object'), 'MD5_hashed_paltform_specific_id' => array('name' => 'dpidmd5', 'type' => '_string', 'scope' => 'optional', 'object' => 'Device_Object'), 'ipv6' => array('name' => 'ipv6', 'type' => '_string', 'scope' => 'optional', 'object' => 'Device_Object'), 'carrier' => array('name' => 'carrier', 'type' => '_string', 'scope' => 'optional', 'object' => 'Device_Object'), 'language' => array('name' => 'language', 'type' => '_string', 'scope' => 'optional', 'object' => 'Device_Object'), 'make' => array('name' => 'make', 'type' => '_string', 'scope' => 'optional', 'object' => 'Device_Object'), 'model' => array('name' => 'model', 'type' => '_string', 'scope' => 'optional', 'object' => 'Device_Object'), 'os' => array('name' => 'Os', 'type' => '_string', 'scope' => 'optional', 'object' => 'Device_Object'), 'os_version' => array('name' => 'Osv', 'type' => '_string', 'scope' => 'optional', 'object' => 'Device_Object'), 'js' => array('name' => 'Js', 'type' => '_integer', 'scope' => 'optional', 'object' => 'Device_Object'), 'connection_type' => array('name' => 'connectiontype', 'type' => '_integer', 'scope' => 'optional', 'object' => 'Device_Object'), 'device_type' => array('name' => 'devicetype', 'type' => '_integer', 'scope' => 'optional', 'object' => 'Device_Object'), 'flash_version' => array('name' => 'flashver', 'type' => '_string', 'scope' => 'optional', 'object' => 'Device_Object'),);
        return $device_object;
    }
    public function format_device_object($object) {
        if ($object) {
            $site_object = array();
            foreach ($object as $key => $data) $site_object[$key] = $data;
            return $site_object;
        }
    }
    public function fetch($object) {
        $ob = array();
        if ($object) {
            $device_object = $this->device_object();
            $tmp = array_intersect_key($object, $device_object);
            foreach ($tmp as $key => $data) $ob['device'][$device_object[$key]['name']] = $data;
            if (!empty($ob)) return json_encode($ob);
            else return false;
        }
    }
};