<?php 
namespace Rtb_objects;
use Helper\utils;
use Helperalidate;
class Data_object {
    public function save($request_id, $object_id, $request, $time_stamp) {
        $aConf = $GLOBALS['_MAX']['CONF'];
        $insert_array = array('request_id' => "$request_id", 'user_pk_id' => "$object_id");
        if ($request_id AND $request AND $time_stamp) {
            $object = $this->data_object();
            $data_object = $this->format_data_object($object);
            $tmp_prepared_request = array_intersect_key($request, $data_object);
            foreach ($tmp_prepared_request as $key => $data) $prepared_request[$data_object[$key]['name']] = $data;
            if (isset($prepared_request)) {
                foreach ($prepared_request as $key => $data) {
                    if (is_array($data)) {
                        $formated = implode(",", array_values($data));
                        $json_converted = json_encode($formated);
                    } else {
                        $json_converted = json_encode($data);
                    }
                    $insert_array[$key] = $json_converted;
                }
            }
            $columns = implode(",", array_keys($insert_array));
            $escaped_values = array_values($insert_array);
            $values = implode(",", $escaped_values);
            $sql = "INSERT INTO `{$aConf['table']['prefix']}dj_data_object`($columns) VALUES ($values)";
//            $result = OA_Dal_Delivery_query($sql);
            $data_pk_id = mysql_insert_id();
            if ($data_pk_id) {
                $log_msg = "    [Initandlisten]  Data object intialized";
                debug_rtb($log_msg, PEAR_LOG_INFO);
                return $data_pk_id;
            } else return false;
        }
    }
    public function data_object() {
        $data_object = array('data_id' => array('name' => 'id', 'type' => '_string', 'scope' => 'optional', 'object' => 'Data_Object'), 'data_name' => array('name' => 'name', 'type' => '_string', 'scope' => 'optional', 'object' => 'Data_Object'));
        return $data_object;
    }
    public function format_data_object($object) {
        if ($object) {
            $site_object = array();
            foreach ($object as $key => $data) $site_object[$key] = $data;
            return $site_object;
        }
    }
    public function fetch($object) {
        $ob = array();
        if ($object) {
            $data_object = $this->data_object();
            $tmp = array_intersect_key($object, $data_object);
            foreach ($tmp as $key => $data) $ob['data'][$data_object[$key]['name']] = $data;
            if ($ob) return json_encode($ob);
            else return false;
        }
    }
};