<?php

namespace Rtb_objects;

use Helper\utils;
use Helper\validate;

/**
 * Request class
 *
 * This class handles all the paramenters and send it to SSP component to get a response,
 * It inherits 2 other class params values are defined and validated
 *
 */
class Request_handler {

    var $error_log = array();

    public function __construct() {
        //constructor method
    }

    /*
     * 
     * name: request
     * @param
     * @return
     * 
     */

    public function request($request_details) {


        $aConf = $GLOBALS['_MAX']['CONF'];
        $required_params = $this->is_required_params_provided();
        $utils = new Utils;
        if (empty($required_params)) {

            $is_valid = $this->validate_params();

            //check for required params
            $all_params_error = array_map(array($utils, 'walk_error_array'), $is_valid);
            $consolidated_errors = $utils->array_remove_empty($all_params_error);

            if (!empty($consolidated_errors)) {
                $error_message = $utils->formated_error_message($consolidated_errors);
                $this->error_log = $error_message;
                return (json_encode($error_message));
            } else {
                $data = $utils->isolate_data_array($is_valid);
                //ALL params verified, send to create request method
                $json_response = self::create_request($data, $request_details);
                if ($json_response)
                #return the response back.
                    return ($json_response);
                else
                    return false;
            }
        }
        else {
            $this->error_log = $required_params;
            return (json_encode($required_params));
        }
    }

    /*
     * 
     * name: unknown
     * @param
     * @return
     * 
     */

    public function create_request($request_data, $request_details) {

        $aConf = $GLOBALS['_MAX']['CONF'];
        $time_stamp = date('Y:m:d H:i:s');
        $impression_id = md5(uniqid($time_stamp, true));

        $request = array();
        //Replace with original params
        $utils = new Utils;
        $defined_constants = $utils->defined_constants();
        //Error handling method written in Utils class
        $old_error_handler = set_error_handler(array($utils, 'myErrorHandler'));

        foreach ($request_data as $key => $data)
            $request[$defined_constants[$key]['name']] = $data;
        foreach ($request_details as $cam_id => $ex_id)
            $req_details.=$cam_id . ':' . $ex_id . '||';

        //INSERT TRAFFIC RECORD
        $record_traffic = "INSERT INTO {$aConf['table']['prefix']}dj_traffic_raw_data (impression_id, raw_data,request_details) VALUES ('" . $impression_id . "','" . json_encode($request) . "','" . $req_details . "');";
        $result = OA_Dal_Delivery_query($record_traffic);


        $request_id = mysql_insert_id();
        $request_id_array['request_id'] = $request_id;

        $log_msg = "[Initandlisten] Request initiated for REQUESTID-" . $request_id;
        debug_rtb($log_msg, PEAR_LOG_INFO);

        //Merge with the request data
        $impression_id_array['impression_id'] = $impression_id;
        $this->set_value('impression_id', $impression_id);
        $request_data = array_merge($impression_id_array, $request_data);
        $request_data = array_merge($request_id_array, $request_data);

        $traffic = new Traffic();
        //DEFUALT :: For the exchange that is folowing open RTB standards strictly
        if ($request_details) {
            $log_msg = "[Initandlisten]  *****Listening objects ";
            debug_rtb($log_msg, PEAR_LOG_INFO);
            $log_msg.="[Initandlisten]   *****Initilizing...";
            debug_rtb($log_msg, PEAR_LOG_INFO);
            
            foreach ($request_details as $campaign_id => $exchange_id) {
              $openrtb = "SELECT exchange_id,exchange_name,auction_type,is_stirct_open_rtb_standard as is_rtb FROM {$aConf['table']['prefix']}dj_ad_exchange where exchange_id='" . $exchange_id . "'";

                $result = OA_Dal_Delivery_query($openrtb) or die(mysql_error());

                while ($row = OA_Dal_Delivery_fetchAssoc($result, MYSQLI_ASSOC)) {
    		    	if($row['is_rtb'] == 1 || $row['is_rtb'] == "1") {
                        $class_name = 'Open_rtb';
                    }
        			else {
    			       $class_name = ucfirst($row['exchange_name']);
    			    }
                    $log_msg="   [Initandlisten]  *****Requesting for CAMPAIGNID-".$campaign_id;
                    debug_rtb($log_msg, PEAR_LOG_INFO);
                    $auction_type = array("auction_type" => $row['auction_type']);

                    $request_data = array_merge($request_data, $auction_type);

                    if (class_exists($class_name)) {
                        $save_objects = $traffic->save_rtb_objects($request_id, $request_data, $time_stamp, $row['exchange_id'], $campaign_id);
                   
                        if ($save_objects) {
                      
                            $data = $this->prepare_request_hirearchy($request_id, $impression_id, $save_objects);
                            #update formated request
                 
                          $update_data = "UPDATE `{$aConf['table']['prefix']}dj_request` SET `complete_request` ='" . json_encode($data) . "' WHERE `request_id` ='" . $request_id . "'";
//                            $result = OA_Dal_Delivery_query($update_data) or die(mysql_error());
                            $log_msg = "    [Requestgenerator]  Request formed with provided objects sucessfully";
                            debug_rtb($log_msg, PEAR_LOG_INFO);   
                        
                            $obj = new $class_name();
                            $obj->execute($data, $request_id, $row['exchange_id'], $impression_id, $campaign_id);
                  
                        } else {
                            $this->error_code = '501';
                            $this->error_log = "Network error.Try again after some time.";
                        }
                    }
                }
            }
        }
        //IF EVERY THING GOES WELL RETURN THE RESPONSE

        $json_response = $this->collect_response($impression_id);

        if ($json_response)
        #return the response back to request method.
            return ($json_response);
        else
            return false;
    }

    /*
     * 
     * name: unknown
     * @param
     * @return
     * 
     */

    public function collect_response($impression_id) {

        $aConf = $GLOBALS['_MAX']['CONF'];
        if ($impression_id) {
            $winbidresponse = "SELECT response_pk_id,actual_response,campaign_id FROM  {$aConf['table']['prefix']}dj_response where impression_id='$impression_id'";
            $result = OA_Dal_Delivery_query($winbidresponse);
            	while ($row = OA_Dal_Delivery_fetchAssoc($result))
		{
                $json_response = base64_decode($row['actual_response']);
                $response[$row['campaign_id']][$row['response_pk_id']] = json_decode($json_response, true);
            	}
            if ($response) {
                $log_msg = "    [Responsegenerator]  Request successful, returning response";
                debug_rtb($log_msg, PEAR_LOG_INFO);
                $log_msg = "    [General]  *****************COMPLETED***************";
                debug_rtb($log_msg, PEAR_LOG_INFO);
               return json_encode($response);
            } else{
                $log_msg = "    [Responsegenerator]  Request successful, returning empty response";
                debug_rtb($log_msg, PEAR_LOG_INFO);
                $log_msg = "    [General]  *****************COMPLETED***************";
                debug_rtb($log_msg, PEAR_LOG_INFO);
                return false;
            }
        } else
            return false;
    }

    /*
     * 
     * name: unknown
     * @param
     * @return
     * 
     */

    public function win_notice($response_id, $bid_amt) {

        $aConf = $GLOBALS['_MAX']['CONF'];
        if ($response_id AND $bid_amt) {
            #Fetch the win bid response
            $win_notice = "UPDATE  {$aConf['table']['prefix']}dj_response SET  win_notice =  '1' , win_bid_amt = '" . $bid_amt . "' WHERE  response_pk_id ='" . $response_id . "'";
//            $result = OA_Dal_Delivery_query($win_notice);

            //Fetch the win exchange id
            $exchange = "SELECT a.exchange_id as exchange_id,a.win_notice_url as notice_url,b.exchange_name as exchange_name,b.is_stirct_open_rtb_standard FROM  {$aConf['table']['prefix']}dj_response as a JOIN {$aConf['table']['prefix']}dj_ad_exchange as b ON a.exchange_id=b.exchange_id WHERE a.response_pk_id='" . $response_id . "'";
            $result = OA_Dal_Delivery_query($exchange);

            while ($row = OA_Dal_Delivery_fetchAssoc($result)) {
                $class_name = ucfirst($row['exchange_name']);
                $notice_url = ucfirst($row['notice_url']);
                $standard = ucfirst($row['is_stirct_open_rtb_standard']);
            }
            if ($standard == '1')
                $class_name = new Open_rtb();
            else
                $class_name = new $class_name;
            //PING THE EXCHANGE NOTICE URL
            $notice = $class_name->win_notice($notice_url);

            if ($notice)
                return true;
            else
                return false;
        } else
            return false;
    }

    /*
     * 
     * name: unknown
     * @param
     * @return
     * 
     */

    public function validate_params() {
        //Load the predefined constants
        $utils = new Utils;
        $validate = new Validate;
        $defined_constants = $utils->defined_constants();

        if (is_array($defined_constants) AND !empty($defined_constants)) {
            foreach ($defined_constants as $key => $value) {
                if (array_key_exists($key, $defined_constants) AND !empty($this->$key)) {
                    switch ($value['type']) {
                        case '_integer':
                            $prep[] = $validate->validate_is_integer($key, $this->$key);
                            break;
                        case '_float':
                            $prep[] = $validate->validate_is_float($key, $this->$key);
                            break;
                        case '_string':
                            $prep[] = $validate->validate_is_string($key, $this->$key);
                            break;
                        case '_array_of_string':
                            $prep[] = $validate->validate_is_array_string($key, $this->$key);
                            break;
                        case '_array_of_integer':
                            $prep[] = $validate->validate_is_array_integer($key, $this->$key);
                            break;
                    }
                }
            }
            return $prep;
        }
    }

    /*
     * 
     * name: unknown
     * @param
     * @return
     * 
     */

    public function is_required_params_provided() {
        $utils = new Utils;
        $required_params = $utils->create_required_params_array();
        if (is_array($required_params) AND $required_params) {
            foreach ($required_params as $key => $value) {
                if (empty($this->$key))
                    $error[$key] = $key . " Is required";
            }
            if (!empty($error))
                return $error;
        } else
            return false;
    }

    /*
     * 
     * name: unknown
     * @param
     * @return
     * 
     */

    public function set_value($feild_name, $feild_value) {
        $utils = new Utils;
        $defined_constants = $utils->defined_constants();

        if (!empty($feild_name) AND !empty($feild_value)) {
            if (array_key_exists($feild_name, $defined_constants)) {
                $log_msg = "    [ParamsInitilizer]  " . $feild_name . "  value received";
                debug_rtb($log_msg, PEAR_LOG_INFO);
                $this->$feild_name = $feild_value;
                return true;
            } else {
                $log_msg = "    [ParamsInitilizerError]  " . $feild_name . "  value not set";
                debug_rtb($log_msg, PEAR_LOG_INFO);
                $this->error_log = $feild_name . ' is not a valid parameter';
                return $feild_name . ' is not a valid parameter';
            }
        }
    }

    /*
     * 
     * name: unknown
     * @param
     * @return
     * 
     */

    public function set_value_by_array($value_array) {
        $utils = new Utils;
        $defined_constants = $utils->defined_constants();
        if (is_array($value_array) AND !empty($value_array)) {
            foreach ($value_array as $key => $data) {

                if (array_key_exists($key, $defined_constants)) {
                    $log_msg = "    [ParamsInitilizer]  " . $key . "  value received";
                    debug_rtb($log_msg, PEAR_LOG_INFO);
                    $this->$key = $data;
                } else {
                    $log_msg = "    [ParamsInitilizerError]  " . $key . "  value not set";
                    debug_rtb($log_msg, PEAR_LOG_INFO);
                    $error_log = $key . ' is not a valid parameter';
                    return $key . ' is not a valid parameter';
                }
            }
        }
    }

    /*
     * 
     * name: unknown
     * @param
     * @return
     * 
     */

    public function prepare_request_hirearchy($request_id, $impression_id, $tmp_objects) {
        if ($request_id AND $impression_id AND $tmp_objects) {
            $object = array('id' => $request_id); //Add request id dynamically for each request.
            $request_object = new Traffic();
            $req_objects = $request_object->request_object();

            foreach ($req_objects as $key => $data)
                if (!empty($tmp_objects[$data['name']]))
                    $object[$data['name']] = $tmp_objects[stripslashes($data['name'])];

            #impression object array
            if (isset($tmp_objects['imp_object']))
                $imp_object = (json_decode($tmp_objects['imp_object'], TRUE));

            #banner object array
            if (isset($tmp_objects['banner_object']))
                $banner_object = (array) json_decode($tmp_objects['banner_object']);
            #site object array
            if (isset($tmp_objects['site_object']))
                $site_object = (array) json_decode($tmp_objects['site_object']);
            #app object array
            if (isset($tmp_objects['app_object']))
                $app_object = (array) json_decode($tmp_objects['app_object']);
            #publisher object array
            if (isset($tmp_objects['publisher_object']))
                $publisher_object = (array) json_decode($tmp_objects['publisher_object']);
            #content object array
            if (isset($tmp_objects['content_object']))
                $content_object = (array) json_decode($tmp_objects['content_object']);
            #producer object array
            if (isset($tmp_objects['producer_object']))
                $producer_object = (array) json_decode($tmp_objects['producer_object']);
            #user object array
            if (isset($tmp_objects['user_object']))
                $user_object = (array) json_decode($tmp_objects['user_object']);
            #device object array
            if (isset($tmp_objects['device_object']))
                $device_object = (array) json_decode($tmp_objects['device_object']);
            #geo object array
            if (isset($tmp_objects['geo_object']))
                $geo_object = (array) json_decode($tmp_objects['geo_object']);
            #data object array
            if (isset($tmp_objects['data_object']))
                $data_object = (array) json_decode($tmp_objects['data_object']);
            #segment object array
            if (isset($tmp_objects['segment_object']))
                $segment_object = (array) json_decode($tmp_objects['segment_object']);

            if (isset($imp_object['imp']))
                $a = (array) $imp_object['imp'];


            if (isset($banner_object['banner']))
                $b = (array) $banner_object['banner'];

            if (!empty($site_object)) {
                if (isset($site_object['site'])) {
                    $c = (array) $site_object['site'];
                    $site_or_app = 'site';
                }
            } else {
                if (isset($app_object['app'])) {
                    $c = (array) $app_object['app'];
                    $site_or_app = 'app';
                }
            }
            if (isset($publisher_object['app']))
                $d = (array) $publisher_object['publisher'];
            if (isset($content_object['content']))
                $e = (array) $content_object['content'];
            if (isset($producer_object['producer']))
                $f = (array) $producer_object['producer'];
            if (isset($user_object['user']))
                $g = (array) $user_object['user'];
            if (isset($device_object['device']))
                $h = (array) $device_object['device'];
            if (isset($geo_object['geo']))
                $i = (array) $geo_object['geo'];
            if (isset($data_object['data']))
                $j = (array) $data_object['data'];
            if (isset($segment_object['segment']))
                $k = (array) $segment_object['segment'];


            if (!empty($a))
                $object['imp'][0] = $a;
            if (!empty($b))
                $object['imp'][0]['banner'] = $b;
            if (!empty($c))
                $object[$site_or_app] = $c;
            if (!empty($d))
                $object[$site_or_app]['publisher'] = $d;
            if (!empty($e))
                $object[$site_or_app]['content'] = $e;
            if (!empty($f))
                $object[$site_or_app]['content']['producer'] = $f;

            if (!empty($g))
                $object['user'] = $g;

            if (!empty($h))
                $object['device'] = $h;
            if (!empty($i)) {
                $object['user']['geo'] = $i;
                $object['device']['geo'] = $i;
            }
            if (!empty($j))
                $object['user']['data'] = $j;
            if (!empty($k))
                $object['user']['data']['segment'] = $k;

            return ($object);
        }
    }

    /*
     * 
     * name: unknown
     * @param
     * @return
     * 
     */

    public function debug($error_log = false) {
        echo "<br/>=========================================================================<br/>";
        echo "<h3>RTB request - Debugger</h3>";
        echo "==============================================================================<br/>";

        if (!empty($this->error_log)) {
            echo "<h4>Response</h4>";
            echo "<code>" . 'Request error' . "</code><br/>\n\n";
            echo "<h4>Errors</h4>";
            echo "<strong>Code:</strong> 500 <br/>\n";
            echo "<br/>";
            echo "<strong>Message:</strong> <br/>";
            $i = 1;
            if (is_array($this->error_log)) {
                foreach ($this->error_log as $data) {
                    echo $i . ')' . $data;
                    echo "<br/>";
                    $i++;
                }
            } else
                echo $i . ')' . $this->error_log;

            echo "<br/>\n";
        } else
            echo "<h4>Success</h4>";

        exit;
    }

}

/* End of file request.php */
