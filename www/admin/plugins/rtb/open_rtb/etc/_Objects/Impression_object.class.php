<?php
namespace Rtb_objects;
use Helper\utils;
use Helperalidate;
class Impression_object {
    public function save($request_id, $request, $time_stamp) {
        $aConf = $GLOBALS['_MAX']['CONF'];
        $insert_array = array('request_id' => "$request_id");
        if ($request_id AND $request AND $time_stamp) {
            $object = $this->impression_object();
            $impression_object = $this->format_impression_object($object);
            $tmp_prepared_request = array_intersect_key($request, $impression_object);
            foreach ($tmp_prepared_request as $key => $data) $prepared_request[$impression_object[$key]['name']] = $data;
            if (isset($prepared_request)) {
                foreach ($prepared_request as $key => $data) {
                    if (is_array($data)) {
                        $formated = implode(",", array_values($data));
                        $json_converted = json_encode($formated);
                    } else {
                        $json_converted = json_encode($data);
                    }
                    $insert_array[$key] = $json_converted;
                }
            }
            $columns = implode(",", array_keys($insert_array));
            $escaped_values = array_values($insert_array);
            $values = implode(",", $escaped_values);
            $sql = "INSERT INTO `{$aConf['table']['prefix']}dj_impression_object`($columns) VALUES ($values)";
//            $result = OA_Dal_Delivery_query($sql) or die(mysql_error());
            $imp_pk_id = mysql_insert_id();
            if ($imp_pk_id) {
                $log_msg = "    [Initandlisten]  Impression object intialized";
                debug_rtb($log_msg, PEAR_LOG_INFO);
                return $imp_pk_id;
            } else return false;
        }
    }
    public function impression_object() {
        $impression_object = array('impression_id' => array('name' => 'id', 'type' => '_string', 'scope' => 'optional', 'object' => 'Impression_Object'), 'is_instl' => array('name' => 'instl', 'type' => '_integer', 'scope' => 'optional', 'object' => 'Impression_Object'), 'tagid' => array('name' => 'tagid', 'type' => '_string', 'scope' => 'optional', 'object' => 'Impression_Object'), 'bid_floor' => array('name' => 'bidfloor', 'type' => '_float', 'scope' => 'optional', 'object' => 'Impression_Object'), 'bid_floor_currency' => array('name' => 'bidfloorcur', 'type' => '_string', 'scope' => 'optional', 'object' => 'Impression_Object'), 'iFrame_buster' => array('name' => 'iframebuster', 'type' => '_array_of_string', 'scope' => 'optional', 'object' => 'Impression_Object'));
        return $impression_object;
    }
    public function format_impression_object($object) {
        if ($object) {
            $impression_object = array();
            foreach ($object as $key => $data) $impression_object[$key] = $data;
            return $impression_object;
        }
    }
    public function fetch($object) {
        $ob = array();
        if ($object) {
            $impression_object = $this->impression_object();
            $tmp = array_intersect_key($object, $impression_object);
            foreach ($tmp as $key => $data) $ob['imp'][$impression_object[$key]['name']] = $data;
            if ($ob) return json_encode($ob);
            else return false;
        }
    }
};