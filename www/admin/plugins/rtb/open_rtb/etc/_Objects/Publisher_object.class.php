<?php
namespace Rtb_objects;
use Helper\utils;
use Helperalidate;
class Publisher_object {
    public function save($request_id, $object_id, $flag, $request, $time_stamp) {
        $aConf = $GLOBALS['_MAX']['CONF'];
        if ($flag == '1') $insert_array = array('request_id' => "$request_id", 'site_pk_id' => "$object_id");
        if ($flag == '2') $insert_array = array('request_id' => "$request_id", 'app_pk_id' => "$object_id");
        if ($request_id AND $request AND $time_stamp) {
            $object = $this->publisher_object();
            $publisher_object = $this->format_publisher_object($object);
            $tmp_prepared_request = array_intersect_key($request, $publisher_object);
            foreach ($tmp_prepared_request as $key => $data) $prepared_request[$publisher_object[$key]['name']] = $data;
            if (isset($prepared_request)) {
                foreach ($prepared_request as $key => $data) {
                    if (is_array($data)) {
                        $formated = implode(",", array_values($data));
                        $json_converted = json_encode($formated);
                    } else {
                        $json_converted = json_encode($data);
                    }
                    $insert_array[$key] = $json_converted;
                }
            }
            $columns = implode(",", array_keys($insert_array));
            $escaped_values = array_values($insert_array);
            $values = implode(",", $escaped_values);
            $sql = "INSERT INTO `{$aConf['table']['prefix']}dj_publisher_object`($columns) VALUES ($values)";
//            $result = OA_Dal_Delivery_query($sql);
            $publisher_pk_id = mysql_insert_id();
            if ($publisher_pk_id) {
                $log_msg = "    [Initandlisten]  Publisher object intialized";
                debug_rtb($log_msg, PEAR_LOG_INFO);
                return $publisher_pk_id;
            } else return false;
        }
    }
    public function publisher_object() {
        $publisher_object = array('publisher_id' => array('name' => 'id', 'type' => 'string', 'scope' => 'recommended', 'object' => 'Publisher_Object'), 'publisher_name' => array('name' => 'name', 'type' => 'string', 'scope' => 'optional', 'object' => 'Publisher_Object'), 'publisher_category' => array('name' => 'cat', 'type' => 'array_of_string', 'scope' => 'optional', 'object' => 'Publisher_Object'), 'publisher_domain_name' => array('name' => 'domain', 'type' => '_string', 'scope' => 'optional', 'object' => 'Publisher_Object'));
        return $publisher_object;
    }
    public function format_publisher_object($object) {
        if ($object) {
            $site_object = array();
            foreach ($object as $key => $data) $site_object[$key] = $data;
            return $site_object;
        }
    }
    public function fetch($object) {
        $ob = array();
        if ($object) {
            $publisher_object = $this->publisher_object();
            $tmp = array_intersect_key($object, $publisher_object);
            foreach ($tmp as $key => $data) $ob['publisher'][$publisher_object[$key]['name']] = $data;
            if ($ob) return json_encode($ob);
            else return false;
        }
    }
};