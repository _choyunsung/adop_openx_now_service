<?php
namespace Rtb_objects;
use Helper\utils;
use Helperalidate;
class Site_object {
    public function save($request_id, $request, $time_stamp) {
        $aConf = $GLOBALS['_MAX']['CONF'];
        $insert_array = array('request_id' => "$request_id");
        if ($request_id AND $request AND $time_stamp) {
            $object = $this->site_object();
            $site_object = $this->format_site_object($object);
            $tmp_prepared_request = array_intersect_key($request, $site_object);
            foreach ($tmp_prepared_request as $key => $data) $prepared_request[$site_object[$key]['name']] = $data;
            if (isset($prepared_request)) {
                foreach ($prepared_request as $key => $data) {
                    if (is_array($data)) {
                        $formated = implode(",", array_values($data));
                        $json_converted = json_encode($formated);
                    } else {
                        $json_converted = json_encode($data);
                    }
                    $insert_array[$key] = $json_converted;
                }
            }
            $columns = implode(",", array_keys($insert_array));
            $escaped_values = array_values($insert_array);
            $values = implode(",", $escaped_values);
            $sql = "INSERT INTO `{$aConf['table']['prefix']}dj_site_object`($columns) VALUES ($values)";
//            $result = OA_Dal_Delivery_query($sql) or die(mysql_error());
            $site_pk_id = mysql_insert_id();
            if ($site_pk_id) {
                $log_msg = "    [Initandlisten]  Site object intialized";
                debug_rtb($log_msg, PEAR_LOG_INFO);
                return $site_pk_id;
            } else return false;
        }
    }
    public function site_object() {
        $site_object = array('site_id' => array('name' => 'id', 'type' => '_string', 'scope' => 'recommended', 'object' => 'Site_Object'), 'site_name' => array('name' => 'name', 'type' => '_string', 'scope' => 'optional', 'object' => 'Site_Object'), 'site_domain_name' => array('name' => 'domain', 'type' => '_string', 'scope' => 'optional', 'object' => 'Site_Object'), 'site_category' => array('name' => 'cat', 'type' => '_array_of_string', 'scope' => 'optional', 'object' => 'Site_Object'), 'site_section_category' => array('name' => 'sectioncat', 'type' => '_array_of_string', 'scope' => 'optional', 'object' => 'Site_Object'), 'site_page_category' => array('name' => 'pagecat', 'type' => '_array_of_string', 'scope' => 'optional', 'object' => 'Site_Object'), 'impression_page_url' => array('name' => 'page', 'type' => '_string', 'scope' => 'recommended', 'object' => 'Site_Object'), 'has_privacy_policy' => array('name' => 'privacypolicy', 'type' => '_integer', 'scope' => 'optional', 'object' => 'Site_Object'), 'refferer_url' => array('name' => 'ref', 'type' => '_string', 'scope' => 'optional', 'object' => 'Site_Object'), 'search_string' => array('name' => 'search', 'type' => 'string', 'scope' => 'optional', 'object' => 'Site_Object'),);
        return $site_object;
    }
    public function format_site_object($object) {
        if ($object) {
            $site_object = array();
            foreach ($object as $key => $data) $site_object[$key] = $data;
            return $site_object;
        }
    }
    public function fetch($object) {
        $ob = array();
        if ($object) {
            $site_object = $this->site_object();
            $tmp = array_intersect_key($object, $site_object);
            foreach ($tmp as $key => $data) $ob['site'][$site_object[$key]['name']] = $data;
            if ($ob) return json_encode($ob);
            else return false;
        }
    }
};