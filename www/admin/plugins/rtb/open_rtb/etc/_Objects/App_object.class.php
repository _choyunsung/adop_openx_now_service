<?php
namespace Rtb_objects;
use Helper\utils;
use Helperalidate;
class App_object {
    public function save($request_id, $request, $time_stamp) {
        $aConf = $GLOBALS['_MAX']['CONF'];
        $insert_array = array('request_id' => "$request_id");
        if ($request_id AND $request AND $time_stamp) {
            $object = $this->app_object();
            $app_object = $this->format_app_object($object);
            $tmp_prepared_request = array_intersect_key($request, $app_object);
            foreach ($tmp_prepared_request as $key => $data) $prepared_request[$app_object[$key]['name']] = $data;
            if (isset($prepared_request)) {
                foreach ($prepared_request as $key => $data) {
                    if (is_array($data)) {
                        $formated = implode(",", array_values($data));
                        $json_converted = json_encode($formated);
                    } else {
                        $json_converted = json_encode($data);
                    }
                    $insert_array[$key] = $json_converted;
                }
            }
            $columns = implode(",", array_keys($insert_array));
            $escaped_values = array_values($insert_array);
            $values = implode(",", $escaped_values);
            $sql = "INSERT INTO `{$aConf['table']['prefix']}dj_app_object`($columns) VALUES ($values)";
//            $result = OA_Dal_Delivery_query($sql) or die($conn->error);
            $app_pk_id = mysql_insert_id();
            if ($site_pk_id) {
                $log_msg = "    [Initandlisten]  App object intialized";
                debug_rtb($log_msg, PEAR_LOG_INFO);
                return $app_pk_id;
            } else return false;
        }
    }
    public function app_object() {
        $app_object = array('app_id' => array('name' => 'id', 'type' => '_string', 'scope' => 'recommended', 'object' => 'App_Object'), 'app_name' => array('name' => 'name', 'type' => '_string', 'scope' => 'optional', 'object' => 'App_Object'), 'app_domain_name' => array('name' => 'domain', 'type' => '_string', 'scope' => 'optional', 'object' => 'App_Object'), 'app_category' => array('name' => 'cat', 'type' => '_array_of_string', 'scope' => 'optional', 'object' => 'App_Object'), 'app_section_category' => array('name' => 'sectioncat', 'type' => '_array_of_string', 'scope' => 'optional', 'object' => 'App_Object'), 'app_page_category' => array('name' => 'pagecat', 'type' => '_array_of_string', 'scope' => 'optional', 'object' => 'App_Object'), 'app_version' => array('name' => 'ver', 'type' => '_string', 'scope' => 'optional', 'object' => 'App_Object'), 'app_bundle' => array('name' => 'bundle', 'type' => '_string', 'scope' => 'recommended', 'object' => 'App_Object'), 'has_privacy_policy' => array('name' => 'privacypolicy', 'type' => '_integer', 'scope' => 'optional', 'object' => 'App_Object'), 'paid' => array('name' => 'paid', 'type' => '_integer', 'scope' => 'optional', 'object' => 'App_Object'), 'storeurl' => array('name' => 'storeurl', 'type' => '_string', 'scope' => 'optional', 'object' => 'App_Object'),);
        return $app_object;
    }
    public function format_app_object($object) {
        if ($object) {
            $site_object = array();
            foreach ($object as $key => $data) $site_object[$key] = $data;
            return $site_object;
        }
    }
    public function fetch($object) {
        $ob = array();
        if ($object) {
            $app_object = $this->app_object();
            $tmp = array_intersect_key($object, $app_object);
            foreach ($tmp as $key => $data) $ob['app'][$app_object[$key]['name']] = $data;
            if ($ob) return json_encode($ob);
            else return false;
        }
    }
};