<?php
namespace Rtb_objects;
use Helper\utils;
use Helperalidate;
class User_object {
    public function save($request_id, $request, $time_stamp) {
        $aConf = $GLOBALS['_MAX']['CONF'];
        $insert_array = array('request_id' => "$request_id");
        if ($request_id AND $request AND $time_stamp) {
            $object = $this->user_object();
            $user_object = $this->format_user_object($object);
            $tmp_prepared_request = array_intersect_key($request, $user_object);
            foreach ($tmp_prepared_request as $key => $data) $prepared_request[$user_object[$key]['name']] = $data;
            if (isset($prepared_request)) {
                foreach ($prepared_request as $key => $data) {
                    if (is_array($data)) {
                        $formated = implode(",", array_values($data));
                        $json_converted = json_encode($formated);
                    } else {
                        $json_converted = json_encode($data);
                    }
                    $insert_array[$key] = $json_converted;
                }
            }
            $columns = implode(",", array_keys($insert_array));
            $escaped_values = array_values($insert_array);
            $values = implode(",", $escaped_values);
            $sql = "INSERT INTO `{$aConf['table']['prefix']}dj_user_object`($columns) VALUES ($values)";
//            $result = OA_Dal_Delivery_query($sql) or die(mysql_error());
            $user_pk_id = mysql_insert_id();
            if ($user_pk_id) {
                $log_msg = "    [Initandlisten]  User object intialized";
                debug_rtb($log_msg, PEAR_LOG_INFO);
                return $user_pk_id;
            } else return false;
        }
    }
    public function user_object() {
        $user_object = array('user_id' => array('name' => 'id', 'type' => '_string', 'scope' => 'recommended', 'object' => 'User_Object'), 'buyer_uid' => array('name' => 'buyeruid', 'type' => '_string', 'scope' => 'recommended', 'object' => 'User_Object'), 'year_of_birth' => array('name' => 'yob', 'type' => '_integer', 'scope' => 'optional', 'object' => 'User_Object'), 'gender' => array('name' => 'gender', 'type' => '_string', 'scope' => 'optional', 'object' => 'User_Object'), 'keywords' => array('name' => 'keywords', 'type' => '_array_of_string', 'scope' => 'optional', 'object' => 'User_Object'), 'customdata' => array('name' => 'customdata', 'type' => '_string', 'scope' => 'optional', 'object' => 'User_Object'));
        return $user_object;
    }
    public function format_user_object($object) {
        if ($object) {
            $site_object = array();
            foreach ($object as $key => $data) $site_object[$key] = $data;
            return $site_object;
        }
    }
    public function fetch($object) {
        $ob = array();
        if ($object) {
            $user_object = $this->user_object();
            $tmp = array_intersect_key($object, $user_object);
            foreach ($tmp as $key => $data) $ob['user'][$user_object[$key]['name']] = $data;
            if ($ob) return json_encode($ob);
            else return false;
        }
    }
};