<?php
namespace Rtb_objects;
use Helper\utils;
use Helperalidate;
class Segment_object {
    public function save($request_id, $object_id, $request, $time_stamp) {
        $aConf = $GLOBALS['_MAX']['CONF'];
        $insert_array = array('request_id' => "$request_id", 'data_pk_id' => "$object_id");
        if ($request_id AND $request AND $time_stamp) {
            $object = $this->segment_object();
            $segment_object = $this->format_segment_object($object);
            $tmp_prepared_request = array_intersect_key($request, $segment_object);
            foreach ($tmp_prepared_request as $key => $data) $prepared_request[$segment_object[$key]['name']] = $data;
            if (isset($prepared_request)) {
                foreach ($prepared_request as $key => $data) {
                    if (is_array($data)) {
                        $formated = implode(",", array_values($data));
                        $json_converted = json_encode($formated);
                    } else {
                        $json_converted = json_encode($data);
                    }
                    $insert_array[$key] = $json_converted;
                }
            }
            $columns = implode(",", array_keys($insert_array));
            $escaped_values = array_values($insert_array);
            $values = implode(",", $escaped_values);
            $sql = "INSERT INTO `{$aConf['table']['prefix']}dj_segment_object`($columns) VALUES ($values)";
//            $result = OA_Dal_Delivery_query($sql) or die(mysql_error());
            $segment_pk_id = mysql_insert_id();
            if ($segment_pk_id) {
                $log_msg = "    [Initandlisten]  Segment object intialized";
                debug_rtb($log_msg, PEAR_LOG_INFO);
                return $segment_pk_id;
            } else return false;
        }
    }
    public function segment_object() {
        $segment_object = array('segment_id' => array('name' => 'id', 'type' => '_string', 'scope' => 'optional', 'object' => 'Segment_Object'), 'segment_name' => array('name' => 'name', 'type' => '_string', 'scope' => 'optional', 'object' => 'Segment_Object'), 'segment_value' => array('name' => 'value', 'type' => '_string', 'scope' => 'optional', 'object' => 'Segment_Object'));
        return $segment_object;
    }
    public function format_segment_object($object) {
        if ($object) {
            $site_object = array();
            foreach ($object as $key => $data) $site_object[$key] = $data;
            return $site_object;
        }
    }
    public function fetch($object) {
        $ob = array();
        if ($object) {
            $segment_object = $this->segment_object();
            $tmp = array_intersect_key($object, $segment_object);
            foreach ($tmp as $key => $data) $ob['segment'][$segment_object[$key]['name']] = $data;
            if ($ob) return json_encode($ob);
            else return false;
        }
    }
};