<?php
namespace Rtb_objects;
use Helper\utils;
use Helperalidate;
class Banner_object {
    public function save($request_id, $imp_pk_id, $request, $time_stamp) {
        $aConf = $GLOBALS['_MAX']['CONF'];
        $insert_array = array('request_id' => "$request_id", 'imp_pk_id' => "$imp_pk_id");
        if ($request_id AND $imp_pk_id AND $request AND $time_stamp) {
            $object = $this->banner_object();
            $banner_object = $this->format_banner_object($object);
            $tmp_prepared_request = array_intersect_key($request, $banner_object);
            foreach ($tmp_prepared_request as $key => $data) $prepared_request[$banner_object[$key]['name']] = $data;
            if (isset($prepared_request)) {
                foreach ($prepared_request as $key => $data) {
                    if (is_array($data)) {
                        $formated = implode(",", array_values($data));
                        $json_converted = json_encode($formated);
                    } else {
                        $json_converted = json_encode($data);
                    }
                    $insert_array[$key] = $json_converted;
                }
            }
            $columns = implode(",", array_keys($insert_array));
            $escaped_values = array_values($insert_array);
            $values = implode(",", $escaped_values);
            $sql = "INSERT INTO `{$aConf['table']['prefix']}dj_banner_object`($columns) VALUES ($values)";
            $result = OA_Dal_Delivery_query($sql) or die($conn->error);
            $banner_pk_id = mysql_insert_id();
            if ($banner_pk_id) {
                $log_msg = "    [Initandlisten]  Banner object intialized";
                debug_rtb($log_msg, PEAR_LOG_INFO);
                return $banner_pk_id;
            } else return false;
        }
    }
    public function banner_object() {
        $banner_object = array('banner_width' => array('name' => 'w', 'type' => '_integer', 'scope' => 'recommended', 'object' => 'Banner_Object'), 'banner_height' => array('name' => 'h', 'type' => '_integer', 'scope' => 'recommended', 'object' => 'Banner_Object'), 'banner_id' => array('name' => 'id', 'type' => '_string', 'scope' => 'recommended', 'object' => 'Banner_Object'), 'position' => array('name' => 'pos', 'type' => '_integer', 'scope' => 'optional', 'object' => 'Banner_Object'), 'blocked_type' => array('name' => 'btype', 'type' => '_array_of_integers', 'scope' => 'optional', 'object' => 'Banner_Object'), 'blocked_creative_attr' => array('name' => 'battr', 'type' => '_array_of_integers', 'scope' => 'optional', 'object' => 'Banner_Object'), 'prefered_mimes' => array('name' => 'mimes', 'type' => '_array_of_string', 'scope' => 'optional', 'object' => 'Banner_Object'), 'is_top_frame' => array('name' => 'topframe', 'type' => '_integer', 'scope' => 'optional', 'object' => 'Banner_Object'), 'expandable_direction' => array('name' => 'expdir', 'type' => '_array_of_integers', 'scope' => 'optional', 'object' => 'Banner_Object'), 'supported_api' => array('name' => 'api', 'type' => '_array_of_integers', 'scope' => 'optional', 'object' => 'Banner_Object'));
        return $banner_object;
    }
    public function format_banner_object($object) {
        if ($object) {
            $banner_object = array();
            foreach ($object as $key => $data) $banner_object[$key] = $data;
            return $banner_object;
        }
    }
    public function fetch($object) {
        $ob = array();
        if ($object) {
            $banner_object = $this->banner_object();
            $tmp = array_intersect_key($object, $banner_object);
            foreach ($tmp as $key => $data) $ob['banner'][$banner_object[$key]['name']] = $data;
            if ($ob) return json_encode($ob);
            else return false;
        }
    }
};