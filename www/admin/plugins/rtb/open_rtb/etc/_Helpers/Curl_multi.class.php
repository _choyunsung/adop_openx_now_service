<?php class Curl {
    private $to = 0;
    private $sq = 0;
    private $sm = 0;
    private $mx = 0;
    private $mh;
    private $rc = 0;
    private $ms;
    private $cl = array();
    private $rs = array();
    private $hs = array();
    private $rh = NULL;
    private $sh = NULL;
    private $op = array();
    private $go = array();
    function __construct($n, $s = 1) {
        $this->mx = $n;
        $this->ms = $s;
        $this->mh = curl_multi_init();
    }
    private function unCurl($u, $p, $t, $o, $s) {
        $hi = curl_init();
        curl_setopt($hi, CURLOPT_FOLLOWLOCATION, true);
        foreach ($this->go as $i => $v) curl_setopt($hi, $i, $v);
        foreach ($o as $i => $v) curl_setopt($hi, $i, $v);
        if ($t) curl_setopt($hi, CURLOPT_TIMEOUT, $t);
        if ($p) {
            curl_setopt($hi, CURLOPT_POST, 1);
            curl_setopt($hi, CURLOPT_POSTFIELDS, $p);
        }
        curl_setopt($hi, CURLOPT_URL, $u);
        curl_setopt($hi, CURLOPT_HEADER, $this->headers);
        curl_setopt($hi, CURLOPT_RETURNTRANSFER, 1);
        curl_multi_add_handle($this->mh, $hi);
        $this->to++;
        $if = array('req' => $s, 'url' => $u, 'start' => time(), 'seq' => $this->to);
        $this->hs[$hi] = $if;
        if ($x = & $this->sh) {
            $this->rc++;
            $if['handle'] = $hi;
            $x($if);
            $this->rc--;
        }
        $this->sm++;
    }
    private function useq($u, $l) {
        $pu = parse_url($u);
        $pu = $pu['scheme'] . '://' . $pu['host'];
        $lpu = strlen($pu);
        $ok = true;
        foreach ($this->hs as $hs) if (substr($hs['url'], 0, $lpu) == $pu) {
            $ok = false;
            break;
        }
        if ($ok) for ($i = 0;$i < $l;$i++) if (substr($this->cl[$j][0], 0, $lpu) == $pu) {
            $ok = false;
            break;
        }
        return $ok;
    }
    private function coge($in) {
        $hi = & $in['handle'];
        $rsi = & $this->hs[$hi];
        $hz = curl_getinfo($hi, CURLINFO_HEADER_SIZE);
        $r = curl_multi_getcontent($hi);
        $h = substr($r, 0, $hz);
        $r = substr($r, $hz);
        $this->rs[$i = $rsi['req']] = array('result' => $r, 'error' => $in['result'], 'header' => $h, 'url' => $rsi['url'], 'start' => $rsi['start'], 'done' => time(), 'seq' => $rsi['seq']);
        unset($this->hs[$hi]);
        if ($x = & $this->rh) {
            $this->rc++;
            $x($this->rs[$i], $i);
            $this->rc--;
        }
        curl_multi_remove_handle($this->mh, $hi);
        $this->sm--;
        $l = count($this->cl);
        for ($i = 0;$i < $l;$i++) {
            list($u, $p, $t, $o, $q, $s) = $this->cl[$i];
            if ($s == - 2) {
                if (!$ok = $this->useq($u, $i)) continue;
                break;
            }
            if (!$s or $this->rs[$s]) break;
        }
        if ($i < $l) {
            array_splice($this->cl, $i, 1);
            $this->unCurl($u, $p, $t, $o, $q);
        }
        while (curl_multi_exec($this->mh, $ac) == CURLM_CALL_MULTI_PERFORM);
    }
    private function sched() {
        if ($this->rc < 20) while ($in = curl_multi_info_read($this->mh)) $this->coge($in);
    }
    public function setOpt($o, $v, $g = false) {
        if ($g) $this->go[$o] = $v;
        else $this->op[$o] = $v;
    }
    public function addURL($u, $p = '', $t = 0, $s = 0) {
        $q = $this->sq++;
        if (!$this->cl and !$this->hs) $s = 0;
        else if ($s == - 1) $s = $q;
        if ($this->sm < $this->mx) {
            if (!$s) $ok = true;
            else if ($s > 0) $ok = $this->rs[$s];
            else if ($s == - 2) {
                $ok = $this->useq($u, count($this->cl));
            }
        }
        if ($ok) $this->unCurl($u, $p, $t, $this->op, $q + 1);
        else $this->cl[] = array($u, $p, $t, $this->op, $q + 1, $s);
        $f = microtime(true) + floatval($this->ms) / 1000;
        while (microtime(true) < $f) {
            while (curl_multi_exec($this->mh, $ac) == CURLM_CALL_MULTI_PERFORM);
            if (!curl_multi_select($this->mh, 0.001)) break;
        }
        $this->sched();
        $this->op = array();
        return $this->sq;
    }
    public function getResults() {
        return $this->rs;
    }
    public function startHandler($f) {
        $this->sh = & $f;
        $this->sched();
    }
    public function resultHandler($f) {
        $this->rh = & $f;
        $this->sched();
    }
    public function wait($ms) {
        $f = microtime(true) + floatval($ms) / 1000;
        while (microtime(true) < $f) {
            while (curl_multi_exec($this->mh, $ac) == CURLM_CALL_MULTI_PERFORM);
            curl_multi_select($this->mh, 0.001);
        }
        $this->sched();
    }
    public function endWait() {
        if ($this->to) while (true) {
            while (curl_multi_exec($this->mh, $ac) == CURLM_CALL_MULTI_PERFORM);
            curl_multi_select($this->mh, 0.1);
            while ($in = curl_multi_info_read($this->mh)) $this->coge($in);
            if (!$ac) {
                foreach ($this->hs as $i) if (!$this->rs[$i['req']]) continue 2;
                break;
            }
        }
        curl_multi_close($this->mh);
    }
};