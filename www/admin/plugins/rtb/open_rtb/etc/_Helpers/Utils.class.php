<?php
namespace Helper;
class Utils {
    public function formated_error_message($arr) {
        $msg = array();
        while (list($key, $val) = each($arr)) {
            if (is_array($val)) {
                foreach ($val as $k => $v) if (trim($v) != "") $msg[] = $v;
            } else {
                if (trim($val) != "") $msg[] = $val;
            }
        }
        unset($arr);
        return $msg;
    }
    public function array_remove_empty($arr) {
        $narr = array();
        while (list($key, $val) = each($arr)) {
            if (is_array($val)) {
                $val = self::array_remove_empty($val);
                if (count($val) != 0) $narr[$key] = $val;
            } else {
                if (trim($val) != "") $narr[$key] = $val;
            }
        }
        unset($arr);
        return $narr;
    }
    public function isolate_data_array($array) {
        $data = array();
        if (is_array($array)) {
            foreach ($array as $key => $arrayElement) {
                if (is_array($arrayElement['data'])) {
                    foreach ($arrayElement['data'] as $k => $d) {
                        $data[$k] = $d;
                    }
                }
            }
        }
        return $data;
    }
    public function defined_constants() {
        $alias = array('is_for' => array('name' => 'is_for', 'type' => '_integer', 'scope' => 'required'), 'auction_type' => array('name' => 'at', 'type' => '_integer', 'scope' => 'optional'), 'request_max_time' => array('name' => 'tmax', 'type' => '_integer', 'scope' => 'optional'), 'buyer_seats' => array('name' => 'wseat', 'type' => '_array_of_string', 'scope' => 'optional'), 'allimps' => array('name' => 'allimps', 'type' => '_integer', 'scope' => 'optional'), 'currency' => array('name' => 'cur', 'type' => '_array_of_string', 'scope' => 'optional'), 'blocked_advertiser_category' => array('name' => 'bcat', 'type' => '_array_of_string', 'scope' => 'optional'), 'blocked_advertiser' => array('name' => 'badv', 'type' => '_array_of_string', 'scope' => 'optional'), 'impression_id' => array('name' => 'id', 'type' => '_string', 'scope' => 'optional'), 'banner_width' => array('name' => 'w', 'type' => '_integer', 'scope' => 'recommended'), 'banner_height' => array('name' => 'h', 'type' => '_integer', 'scope' => 'recommended'), 'is_instl' => array('name' => 'instl', 'type' => '_integer', 'scope' => 'optional'), 'tag_id' => array('name' => 'tagid', 'type' => '_string', 'scope' => 'optional'), 'bid_floor' => array('name' => 'bidfloor', 'type' => '_float', 'scope' => 'optional'), 'bid_floor_currency' => array('name' => 'bidfloorcur', 'type' => '_string', 'scope' => 'optional'), 'iFrame_buster' => array('name' => 'iframebuster', 'type' => '_array_of_string', 'scope' => 'optional'), 'banner_id' => array('name' => 'id', 'type' => '_string', 'scope' => 'recommended'), 'position' => array('name' => 'pos', 'type' => '_integer', 'scope' => 'optional'), 'blocked_type' => array('name' => 'btype', 'type' => '_array_of_integer', 'scope' => 'optional'), 'blocked_creative_attr' => array('name' => 'battr', 'type' => '_array_of_integer', 'scope' => 'optional'), 'prefered_mimes' => array('name' => 'mimes', 'type' => '_array_of_string', 'scope' => 'optional'), 'is_top_frame' => array('name' => 'topframe', 'type' => '_integer', 'scope' => 'optional'), 'expandable_direction' => array('name' => 'expdir', 'type' => '_array_of_integer', 'scope' => 'optional'), 'supported_api' => array('name' => 'api', 'type' => '_array_of_integer', 'scope' => 'optional'), 'site_id' => array('name' => 'id', 'type' => '_string', 'scope' => 'recommended'), 'site_name' => array('name' => 'name', 'type' => '_string', 'scope' => 'optional'), 'site_domain_name' => array('name' => 'domain', 'type' => '_string', 'scope' => 'optional'), 'site_category' => array('name' => 'cat', 'type' => '_array_of_string', 'scope' => 'optional'), 'site_section_category' => array('name' => 'sectioncat', 'type' => '_array_of_string', 'scope' => 'optional'), 'site_page_category' => array('name' => 'pagecat', 'type' => '_array_of_string', 'scope' => 'optional'), 'impression_page_url' => array('name' => 'page', 'type' => '_string', 'scope' => 'recommended'), 'has_privacy_policy' => array('name' => 'privacypolicy', 'type' => '_integer', 'scope' => 'optional'), 'refferer_url' => array('name' => 'ref', 'type' => '_string', 'scope' => 'optional'), 'search_string' => array('name' => 'search', 'type' => '_string', 'scope' => 'optional'), 'publisher_id' => array('name' => 'id', 'type' => '_string', 'scope' => 'recommended'), 'publisher_name' => array('name' => 'name', 'type' => '_string', 'scope' => 'optional'), 'publisher_category' => array('name' => 'cat', 'type' => '_array_of_string', 'scope' => 'optional'), 'publisher_domain_name' => array('name' => 'domain', 'type' => '_string', 'scope' => 'optional'), 'content_id' => array('name' => 'id', 'type' => '_string', 'scope' => 'optional'), 'content_title' => array('name' => 'title', 'type' => '_string', 'scope' => 'optional'), 'content_series' => array('name' => 'series', 'type' => '_string', 'scope' => 'optional'), 'content_season' => array('name' => 'season', 'type' => '_string', 'scope' => 'optional'), 'content_original_url' => array('name' => 'url', 'type' => '_string', 'scope' => 'optional'), 'content_category' => array('name' => 'cat', 'type' => '_array_of_string', 'scope' => 'optional'), 'keywords' => array('name' => 'keywords', 'type' => '_string', 'scope' => 'optional'), 'content_rating' => array('name' => 'contentrating', 'type' => '_string', 'scope' => 'optional'), 'user_rating' => array('name' => 'userrating', 'type' => '_string', 'scope' => 'optional'), 'context' => array('name' => 'context', 'type' => '_string', 'scope' => 'optional'), 'is_live_stream' => array('name' => 'livestream', 'type' => '_integer', 'scope' => 'optional'), 'source_relationship' => array('name' => 'sourcerelationship', 'type' => '_integer', 'scope' => 'optional'), 'content_producer_id' => array('name' => 'id', 'type' => '_string', 'scope' => 'optional'), 'content_producer_name' => array('name' => 'name', 'type' => '_string', 'scope' => 'optional'), 'content_producer_category' => array('name' => 'cat', 'type' => '_array_of_string', 'scope' => 'optional'), 'content_producer_domain' => array('name' => 'domain', 'type' => '_string', 'scope' => 'optional'), 'length_of_content' => array('name' => 'len', 'type' => '_integer', 'scope' => 'optional'), 'qagmediarating' => array('name' => 'qagmediarating', 'type' => '_integer', 'scope' => 'optional'), 'is_embeddable' => array('name' => 'embeddable', 'type' => '_integer', 'scope' => 'optional'), 'language' => array('name' => 'language', 'type' => '_string', 'scope' => 'optional'), 'app_id' => array('name' => 'id', 'type' => '_string', 'scope' => 'recommended'), 'app_name' => array('name' => 'name', 'type' => '_string', 'scope' => 'optional'), 'app_domain_name' => array('name' => 'domain', 'type' => '_string', 'scope' => 'optional'), 'app_category' => array('name' => 'cat', 'type' => '_array_of_string', 'scope' => 'optional'), 'app_section_category' => array('name' => 'sectioncat', 'type' => '_array_of_string', 'scope' => 'optional'), 'app_page_category' => array('name' => 'pagecat', 'type' => '_array_of_string', 'scope' => 'optional'), 'app_version' => array('name' => 'ver', 'type' => '_string', 'scope' => 'optional'), 'app_bundle' => array('name' => 'bundle', 'type' => '_string', 'scope' => 'recommended'), 'has_privacy_policy' => array('name' => 'privacypolicy', 'type' => '_integer', 'scope' => 'optional'), 'paid' => array('name' => 'paid', 'type' => '_integer', 'scope' => 'optional'), 'storeurl' => array('name' => 'storeurl', 'type' => '_string', 'scope' => 'optional'), 'track' => array('name' => 'dnt', 'type' => '_integer', 'scope' => 'recommended'), 'user_agent' => array('name' => 'ua', 'type' => '_string', 'scope' => 'recommended'), 'ip_address' => array('name' => 'ip', 'type' => '_string', 'scope' => 'recommended'), 'lattitude' => array('name' => 'lat', 'type' => '_float', 'scope' => 'optional'), 'longtude' => array('name' => 'lon', 'type' => '_float', 'scope' => 'optional'), 'country' => array('name' => 'country', 'type' => '_string', 'scope' => 'optional'), 'region' => array('name' => 'region', 'type' => '_string', 'scope' => 'optional'), 'regionfips104' => array('name' => 'regionfips104', 'type' => '_string', 'scope' => 'optional'), 'metro' => array('name' => 'metro', 'type' => '_string', 'scope' => 'optional'), 'city' => array('name' => 'city', 'type' => '_string', 'scope' => 'optional'), 'zip' => array('name' => 'zip', 'type' => '_string', 'scope' => 'optional'), 'type' => array('name' => 'type', 'type' => '_integer', 'scope' => 'recommended'), 'SHA1_hashed_device_id' => array('name' => 'didsha1', 'type' => '_string', 'scope' => 'optional'), 'MD5_hashed_device_id' => array('name' => 'didmd5', 'type' => '_string', 'scope' => 'optional'), 'SHA1_hashed_paltform_specific_id' => array('name' => 'dpidsha1', 'type' => '_string', 'scope' => 'optional'), 'MD5_hashed_paltform_specific_id' => array('name' => 'dpidmd5', 'type' => '_string', 'scope' => 'optional'), 'ipv6' => array('name' => 'ipv6', 'type' => '_string', 'scope' => 'optional'), 'carrier' => array('name' => 'carrier', 'type' => '_string', 'scope' => 'optional'), 'language' => array('name' => 'language', 'type' => '_string', 'scope' => 'optional'), 'make' => array('name' => 'make', 'type' => '_string', 'scope' => 'optional'), 'model' => array('name' => 'model', 'type' => '_string', 'scope' => 'optional'), 'os' => array('name' => 'Os', 'type' => '_string', 'scope' => 'optional'), 'os_version' => array('name' => 'Osv', 'type' => '_string', 'scope' => 'optional'), 'js' => array('name' => 'Js', 'type' => '_integer', 'scope' => 'optional'), 'connection_type' => array('name' => 'connectiontype', 'type' => '_integer', 'scope' => 'optional'), 'device_type' => array('name' => 'devicetype', 'type' => '_integer', 'scope' => 'optional'), 'flash_version' => array('name' => 'flashver', 'type' => '_string', 'scope' => 'optional'), 'user_id' => array('name' => 'id', 'type' => '_string', 'scope' => 'recommended'), 'buyer_uid' => array('name' => 'buyeruid', 'type' => '_string', 'scope' => 'recommended'), 'year_of_birth' => array('name' => 'yob', 'type' => '_integer', 'scope' => 'optional'), 'gender' => array('name' => 'gender', 'type' => '_string', 'scope' => 'optional'), 'keywords' => array('name' => 'keywords', 'type' => '_array_of_string', 'scope' => 'optional'), 'customdata' => array('name' => 'customdata', 'type' => '_string', 'scope' => 'optional'), 'data_id' => array('name' => 'id', 'type' => '_string', 'scope' => 'optional'), 'data_name' => array('name' => 'name', 'type' => '_string', 'scope' => 'optional'), 'segment_id' => array('name' => 'id', 'type' => '_string', 'scope' => 'optional'), 'segment_name' => array('name' => 'name', 'type' => '_string', 'scope' => 'optional'), 'segment_value' => array('name' => 'value', 'type' => '_string', 'scope' => 'optional'));
        return $alias;
    }
    public function create_required_params_array() {
        $defined_constants = self::defined_constants();
        if (is_array($defined_constants) AND !empty($defined_constants)) {
            foreach ($defined_constants as $key => $value) {
                if ($value['scope'] == 'required') $prep[$key] = $value['scope'];
            }
            if (!empty($prep)) return $prep;
            else return true;
        } else return false;
    }
    public function myErrorHandler($errno, $errstr, $errfile, $errline) {
        switch ($errno) {
            case E_USER_NOTICE:
                $file = "./RTB_error_log.txt";
                file_put_contents($file, $errstr . "</n><br/>", FILE_APPEND | LOCK_EX);
            break;
        }
        return true;
    }
    public function walk_error_array($request_data) {
        if (!empty($request_data['error'])) {
            if (!empty($request_data['error'])) {
                return $request_data['error'];
            } else return false;
        } else return false;
    }
};