<?php

/*
+---------------------------------------------------------------------------+
| Revive Adserver                                                           |
| http://www.revive-adserver.com                                            |
|                                                                           |
| Copyright: See the COPYRIGHT.txt file.                                    |
| License: GPLv2 or later, see the LICENSE.txt file.                        |
+---------------------------------------------------------------------------+
*/

// Require the initialisation file
require_once '../../init.php';

// Required files
require_once MAX_PATH . '/lib/OA/Dal.php';
require_once MAX_PATH . '/lib/max/Admin/Languages.php';
require_once MAX_PATH . '/www/admin/config.php';
require_once MAX_PATH . '/www/admin/lib-statistics.inc.php';
require_once MAX_PATH . '/lib/OA/Session.php';
require_once MAX_PATH . '/lib/OA/Admin/Menu.php';
require_once MAX_PATH . '/lib/max/other/html.php';
require_once MAX_PATH . '/lib/OA/Auth.php';
require_once MAX_PATH . '/lib/OA/Admin/UI/UserAccess.php';

$publishers = $GLOBALS[session][user]->aUser[publishers];                 // 등록된 매체사 코드
$account_id = $GLOBALS[session][user]->aUser[default_account_id];         // 관리자 등급
$maccount_id = $GLOBALS[session][user]->aAccount[account_id];            // 작업자 관리자 등급
$agency_id = $GLOBALS[session][user]->aAccount[agency_id];                // 작업자 아이디 등급

if($maccount_id > 4) {
    $doAgency = OA_Dal::staticGetDO('agency', $agency_id);
    $publishers = $doAgency->publishers;
}

if($account_id > "2" or $publishers) {
	header("Location: ./adulturl_sub.php?comIdx=".$publishers);
	exit;
}

/*-------------------------------------------------------*/
/* HTML framework                                        */
/*-------------------------------------------------------*/
if ($agencyid != '') {
    //addPageTools($agencyid);
    if (OA_Permission::isAccount(OA_ACCOUNT_ADMIN)) {
//     	OA_Admin_Menu::setAgencyPageContext($agencyid, 'agency-edit.php');
//     	$doAgency = OA_Dal::staticGetDO('agency', $agencyid);
//         MAX_displayInventoryBreadcrumbs(array(array("name" => $doAgency->name)), "agency");
    	phpAds_PageHeader("4.1.3");
    	phpAds_ShowSections(array("4.1.2", "4.1.3"));
    } else {
        phpAds_PageHeader(null);
        phpAds_ShowSections(array("4.1", "4.2", "4.3", "4.4"));
    }
} else {
	MAX_displayInventoryBreadcrumbs(array(array("name" => phpAds_getClientName($agencyid))), "agency");
	phpAds_PageHeader("4.1.1");
	phpAds_ShowSections(array("4.1.1"));
}
$tabindex = 1;

//phpAds_PageHeader(null, 3);
/*-------------------------------------------------------*/
/* Main code                                             */
/*-------------------------------------------------------*/

require_once MAX_PATH . '/lib/OA/Admin/Template.php';

$oTpl = new OA_Admin_Template('adulturl.html');

// Ensure that any template variables for the authentication plugin are set
$oPlugin = OA_Auth::staticGetAuthPlugin();
$oPlugin->setTemplateVariables($oTpl);

$oTpl->assign('infomessage', OA_Session::getMessage());

$oTpl->assign('entityIdName', 'agencyid');
$oTpl->assign('no', 1);
$oTpl->assign('subPage', 'adulturl_sub.php');

$networkData["startNo"] = 0;
$networkData["listCount"] = 1000;

$adultList = adultList($networkData);

$oTpl->assign('adult_list', $adultList["result"]);
$oTpl->display();

/*-------------------------------------------------------*/
/* HTML framework                                        */
/*-------------------------------------------------------*/

phpAds_PageFooter();

function adultList($networkData) {
	$conf = $GLOBALS ['_MAX'] ['CONF'];
	$url = "http://" . $conf['webpath']['api_url'] . ":8091/m/adult/list/" . $conf['webpath']['api_key'];
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
	curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: application/json"));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($networkData));
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
	$string = curl_exec($ch);
	curl_close($ch);

	$string = json_decode($string, true);

	return $string;
}

function addPageTools($agencyId)
{
    addPageLinkTool($GLOBALS["strLinkUser_Key"], "agency-user-start.php?agencyid={$agencyId}", "iconAdvertiserAdd", $GLOBALS["strAddNew"] );
}


?>
