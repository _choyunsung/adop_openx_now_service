<?php
// Require the initialisation file
require_once '/var/www/OptimaA/init.php';

// Required files
require_once MAX_PATH . '/lib/OA/Dal.php';
require_once MAX_PATH . "/lib/OA/Dal/Delivery/mysql.php";

	function mergeConfigFiles($realConfig, $fakeConfig) {
		foreach ( $fakeConfig as $key => $value ) {
			if (is_array ( $value )) {
				if (! isset ( $realConfig [$key] )) {
					$realConfig [$key] = array ();
				}
				$realConfig [$key] = mergeConfigFiles ( $realConfig [$key], $value );
			} else {
				if (isset ( $realConfig [$key] ) && is_array ( $realConfig [$key] )) {
					$realConfig [$key] [0] = $value;
				} else {
					if (isset ( $realConfig ) && ! is_array ( $realConfig )) {
						$temp = $realConfig;
						$realConfig = array ();
						$realConfig [0] = $temp;
					}
					$realConfig [$key] = $value;
				}
			}
		}
		unset ( $realConfig ['realConfig'] );
		return $realConfig;
	}
	
	function parseDeliveryIniFile($configPath = null, $configFile = null, $sections = true) {
		if (! $configPath) {
			$configPath = MAX_PATH . '/var';
		}
		if ($configFile) {
			$configFile = '.' . $configFile;
		}
		$host = OX_getHostName ();
		$configFileName = $configPath . '/' . $host . $configFile . '.conf.php';
		$conf = @parse_ini_file ( $configFileName, $sections );
		if (isset ( $conf ['realConfig'] )) {
			$realconf = @parse_ini_file ( MAX_PATH . '/var/' . $conf ['realConfig'] . '.conf.php', $sections );
			$conf = mergeConfigFiles ( $realconf, $conf );
		}
		if (! empty ( $conf )) {
			return $conf;
		} elseif ($configFile === '.plugin') {
			$pluginType = basename ( $configPath );
			$defaultConfig = MAX_PATH . '/plugins/' . $pluginType . '/default.plugin.conf.php';
			$conf = @parse_ini_file ( $defaultConfig, $sections );
			if ($conf !== false) {
				return $conf;
			}
			echo "Revive Adserver could not read the default configuration file for the {$pluginType} plugin";
			exit ( 1 );
		}
		$configFileName = $configPath . '/default' . $configFile . '.conf.php';
			$conf = @parse_ini_file ( $configFileName, $sections );
				if (isset ( $conf ['realConfig'] )) {
						$conf = @parse_ini_file ( MAX_PATH . '/var/' . $conf ['realConfig'] . '.conf.php', $sections );
		}
		if (! empty ( $conf )) {
		return $conf;
		}
		if (file_exists ( MAX_PATH . '/var/INSTALLED' )) {
				echo "Revive Adserver has been installed, but no configuration file was found.\n";
				exit ( 1 );
		}
		echo "Revive Adserver has not been installed yet -- please read the INSTALL.txt file.\n";
		exit ( 1 );
	}
	
	function _convertOffset($offset)
	{
		$offset = ((($offset / 1000 ) / 60 ) / 60);
	
		//  calculate offset hours
		$offsetHours = str_pad((int) abs($offset), 2, 0, STR_PAD_LEFT);
	
		// retrieve percentage and if only one character add ending 0
		$offsetMinutes = strstr($offset, '.');
		$offsetMinutes = (strlen($offsetMinutes) >= 2) ? $offsetMinutes : str_pad($offsetMinutes, 2, 0);
	
		//  caculate minutes
		$offsetMinutes = round((60 * $offsetMinutes), 0);
	
		//  if minutes < 2 characters add leading zero
		$offsetMinutes = str_pad(((int) $offsetMinutes), 2, 0, STR_PAD_LEFT);
	
		//  build offset
		$offset = $offsetHours;// . $offsetMinutes;
	
		return $offset;
	}
	

	Class Mediation_Class {
		function setCompareData($date, $zoneid = -1, $bannerid = -1, $api_clicks = 0, $api_revenue = 0, $api_impressions = 0, $api_request = 0) {
			global $_DATE_TIMEZONE_DATA;
			$table = $GLOBALS["_MAX"]["CONF"]["table"]["prefix"] . $GLOBALS["_MAX"]["CONF"]["table"]["data_summary_ad_hourly"];
			$query = "SELECT ifnull(sum(impressions), 0) AS t_impressions, ifnull(sum(clicks), 0) AS t_clicks, ifnull(sum(total_revenue), 0) AS t_total_revenue, ifnull(sum(requests), 0) AS t_requests FROM " . $table . " 
 						WHERE substr(date_time, 1, 10) = '" . $date . "' AND zone_id = " . $zoneid . " AND ad_id = " . $bannerid;
			$sum_res = OA_Dal_Delivery_query ( $query );
			$sum_res_info = OA_Dal_Delivery_fetchAssoc ( $sum_res );
			
			$debugMsg = "yesterdaySumQuery : " . $query . "\n";

// 			echo "globals - " . $GLOBALS['_MAX']['PREF']['timezone'] . "<BR>";
// 			echo "timezone = " . $_DATE_TIMEZONE_DATA[$GLOBALS['_MAX']['PREF']['timezone']]['offset'] . "<BR>";
// 			echo "offset = " . _convertOffset($_DATE_TIMEZONE_DATA[$GLOBALS['_MAX']['PREF']['timezone']]['offset']) . "<BR>";
			
			$intTime = 23;
			if( $_DATE_TIMEZONE_DATA[$GLOBALS['_MAX']['PREF']['timezone']]['offset'] >= 0 ) {
				$intTime = 23 - (int)_convertOffset($_DATE_TIMEZONE_DATA[$GLOBALS['_MAX']['PREF']['timezone']]['offset']);
			} else {
				$intTime = 23 + (int)_convertOffset($_DATE_TIMEZONE_DATA[$GLOBALS['_MAX']['PREF']['timezone']]['offset']);
				if( $intTime >= 24 ) {
					$intTime = $intTime - 24;
				}
			}
			
			$where_time = $intTime . ":00:00";
			$query = "SELECT * FROM " . $table . " WHERE date_time = '" . $date . " " . $where_time . "' AND zone_id = " . $zoneid . " AND ad_id = " . $bannerid;
 			$res = OA_Dal_Delivery_query ( $query );
 			$res_info = OA_Dal_Delivery_fetchAssoc ( $res );
 			
 			$debugMsg .= "yesterdayQuery : " . $query . "\n";
 			
 			if( $res_info ) {
 			    $p_api_clicks = ( ($sum_res_info["t_clicks"] - $res_info["clicks"]) != 0 ) ? $api_clicks : 0;
 				$add_clicks = ( $api_clicks > $sum_res_info["t_clicks"] ) ? $api_clicks - ( $p_api_clicks - ( $sum_res_info["t_clicks"] - $res_info["clicks"] )) : 0;
 				$p_api_revenue = ( ($sum_res_info["t_total_revenue"] > 0 ) && ($sum_res_info["t_total_revenue"] - $res_info["total_revenue"]) != 0 ) ? $api_revenue : 0;
 				$add_revenue = ( $api_revenue > $sum_res_info["t_total_revenue"] ) ? $api_revenue - ( $p_api_revenue - ( $sum_res_info["t_total_revenue"] - $res_info["total_revenue"] )) : 0;
 				$p_api_request = ( ($sum_res_info["t_requests"] - $res_info["requests"]) != 0 ) ? $api_request : 0;
 				$add_request = ( $api_request > $sum_res_info["t_requests"] ) ? $api_request - ( $p_api_request - ( $sum_res_info["t_requests"] - $res_info["requests"] )) : 0;
 				
 				$u_query = "UPDATE " . $table . " SET clicks = clicks + " . $add_clicks . ", total_revenue = ifnull(total_revenue, 0) + " . $add_revenue . ", requests = requests + " . $add_request . " WHERE date_time = '" . $date . " " . $where_time . "' AND zone_id = " . $zoneid . " AND ad_id = " . $bannerid;
 				$u_res = OA_Dal_Delivery_query ( $u_query );
 				$debugMsg .= "u_Query1 : " . $u_query . "\n";
 				$u_query = "UPDATE " . $GLOBALS["_MAX"]["CONF"]["table"]["prefix"] . $GLOBALS["_MAX"]["CONF"]["table"]["data_intermediate_ad"] . " SET clicks = clicks + " . $add_clicks . ", requests = requests + " . $add_request . " WHERE date_time = '" . $date . " " . $where_time . "' AND zone_id = " . $zoneid . " AND ad_id = " . $bannerid;
 				$u_res = OA_Dal_Delivery_query ( $u_query );
 				$debugMsg .= "u_Query2 : " . $u_query . "\n";
 			} else {
 				$add_clicks = $api_clicks - $sum_res_info["t_clicks"];
 				$add_revenue = $api_revenue - $sum_res_info["t_total_revenue"];
 				$add_impressions = $api_impressions - $sum_res_info["t_impressions"];
 				$add_request = $api_request - $sum_res_info["t_requests"];
 				
 				$i_query = "INSERT INTO " . $table . " SET date_time = '" . $date . " " . $where_time . "', ad_id = " . $bannerid . ", requests = " . $add_request . ", zone_id = " . $zoneid . ", impressions = " . $add_impressions . ", clicks = " . $add_clicks . ", conversions = 0, total_num_items = 0, total_basket_value = 0, total_revenue = " . $add_revenue . ", updated = UTC_Timestamp()";
 				$i_res = OA_Dal_Delivery_query ( $i_query );
 				$debugMsg .= "i_Query1 : " . $i_query . "\n";
 				$i_query = "INSERT INTO " . $GLOBALS["_MAX"]["CONF"]["table"]["prefix"] . $GLOBALS["_MAX"]["CONF"]["table"]["data_intermediate_ad"] . " SET date_time = '" . $date . " " . $where_time . "', operation_interval = 60, operation_interval_id = 60, interval_start = '" . $date . " " . $where_time . "', interval_end = '" . $date . " " . $intTime . ":59:59', ad_id = " . $bannerid . ", creative_id = 0, zone_id = " . $zoneid . ", requests = " . $add_request . ", impressions = " . $add_impressions . ", clicks = " . $add_clicks . ", conversions = 0, total_basket_value = 0, total_num_items = 0, updated = UTC_Timestamp()";
 				$i_res = OA_Dal_Delivery_query ( $i_query );
 				$debugMsg .= "i_Query2 : " . $i_query . "\n";
 			}

 			OA::debug($debugMsg, PEAR_LOG_INFO); 
		}
		
		function data_optima_m($eCPM_Row) {
			$conf = $GLOBALS ['_MAX'] ['CONF'];
			$date = date("Y-m-d", strtotime("- 1day"));
			$url = "http://" . $conf['webpath']['api_url'] . ":8091/m/adunit/" . $conf['webpath']['api_key'] . "/" . $date;

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$adunitList = curl_exec($ch);
			curl_close($ch);
			
			$apiData = json_decode($adunitList, true);
// 			print_r($apiData);
			
			OA::debug($apiData, PEAR_LOG_INFO);
			
			if( strtolower($apiData['status']) == 'ok' && count($apiData['result']) > 0 ) {
				foreach ($apiData['result'] As $Val) {
					if( $date == $Val['dateDt'] ) {
// 						echo "zone_id = " . $Val["zoneNo"] . ", banner_id = " . $Val['bannerNo'] . ", clicks = " . $Val['clickNo'] . ", impressions = " . $Val['imprNo'] . ", request = " . $Val['reqNo'] . ", revenue = " . $Val['revNo'] . "<BR>";
					   foreach ($eCPM_Row As $zoneArray) {
                            if( $zoneArray["zoneid"] == $Val["zoneNo"] ) {
                                self::setCompareData($date, $Val['zoneNo'], $Val['bannerNo'], $Val['clickNo'], $Val['revNo'], $Val['imprNo'], $Val['reqNo']);
                            }
                       }
					}
				}
				self::run_mpe();
			} else {
				OA::debug("API Error - " . $url, PEAR_LOG_ERR);
			}
			
			self::setAllDataApi();
		}
		
		function setAllDataApi() {
			$conf = $GLOBALS ['_MAX'] ['CONF'];
			$url = "http://" . $conf['webpath']['api_url'] . ":8091/m/adunit/all/" . $conf['webpath']['api_key'];
			$getJsonData = self::getCodeData();
			
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: application/json"));
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($getJsonData));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
			$adunitList = curl_exec($ch);
			curl_close($ch);
		}
		
		function getCodeData() {
			$query = "SELECT zA.ad_zone_assoc_id, zA.zone_id, zA.ad_id, b.ad_unit_nm, b.net_com_no, b.com_no FROM " . $GLOBALS["_MAX"]["CONF"]["table"]["prefix"] . $GLOBALS["_MAX"]["CONF"]["table"]["ad_zone_assoc"] . " zA LEFT JOIN " . $GLOBALS["_MAX"]["CONF"]["table"]["prefix"] . $GLOBALS["_MAX"]["CONF"]["table"]["banners"] . " b ON zA.ad_id = b.bannerid ORDER BY zA.zone_id DESC";
			$res = OA_Dal_Delivery_query ( $query );
			$tempArray = array();
			$arrayCount = 0;
			while ( $row = OA_Dal_Delivery_fetchAssoc ( $res ) ) {
				$tempArray[$arrayCount]['zoneNo'] 			= $row['zone_id'];
				$tempArray[$arrayCount]['bannerNo'] 		= $row['ad_id'];
				
				$tempArray[$arrayCount]['comNo']			= $row['com_no'];
				$tempArray[$arrayCount]['netComNo'] 		= $row['net_com_no'];
				$tempArray[$arrayCount]['adUnitNm'] 		= $row['ad_unit_nm'];
				
				$arrayCount++;
			}
			
			return $tempArray;
		}
		
		function run_mpe() {
			include MAX_PATH . "/www/admin/run-mpe.php";
		}
	}
	
	if( $_GET['uLoc'] == 'cl' ) {
    	$_POST['username'] = 'adop';
    	$_POST['password'] = 'adop123$';
    	$_COOKIE['mediation'] = md5(uniqid('adop', 1));
	}
	
	require_once MAX_PATH . '/lib/OA/Preferences.php';
	
	require_once MAX_PATH . '/www/admin/config.php';
	setupDeliveryConfigVariables();	
	
    $query = "SELECT 
        prefer.account_id, agency.agencyid, affiliate.affiliateid, zone.zoneid
    FROM
        " . $GLOBALS["_MAX"]["CONF"]["table"]["prefix"] . $GLOBALS["_MAX"]["CONF"]["table"]["account_preference_assoc"] . " AS prefer,
        " . $GLOBALS["_MAX"]["CONF"]["table"]["prefix"] . $GLOBALS["_MAX"]["CONF"]["table"]["agency"] . " AS agency,
        " . $GLOBALS["_MAX"]["CONF"]["table"]["prefix"] . $GLOBALS["_MAX"]["CONF"]["table"]["affiliates"] . " AS affiliate,
        " . $GLOBALS["_MAX"]["CONF"]["table"]["prefix"] . $GLOBALS["_MAX"]["CONF"]["table"]["zones"] . " AS zone
    WHERE
        prefer.preference_id = 8 AND prefer.value = 1 AND prefer.account_id = agency.account_id AND affiliate.agencyid = agency.agencyid AND affiliate.affiliateid = zone.affiliateid";
    $res = OA_Dal_Delivery_query ( $query );
    while ( $eCPM_Row = OA_Dal_Delivery_fetchAssoc ( $res ) ) {
        $eCPM_Array[] = $eCPM_Row;
    }
        
    if( $eCPM_Array ) {
        Mediation_Class::data_optima_m($eCPM_Array);
    } else 
        OA::debug("Mediation Empty !!!", PEAR_LOG_ERR);
