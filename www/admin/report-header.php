<?php
/*
+---------------------------------------------------------------------------+
| Revive Adserver                                                           |
| http://www.revive-adserver.com                                            |
|                                                                           |
| Copyright: See the COPYRIGHT.txt file.                                    |
| License: GPLv2 or later, see the LICENSE.txt file.                        |
+---------------------------------------------------------------------------+
*/

// Require the initialisation file
//require_once '/var/www/OptimaA/init.php';
require_once '../../init.php';

// Include required files
require_once MAX_PATH . '/lib/OA/Admin/Reports/Index.php';
require_once MAX_PATH . '/www/admin/config.php';
require_once MAX_PATH . '/www/admin/lib-maintenance-priority.inc.php';
require_once MAX_PATH . '/lib/OA/Dal.php';
require_once MAX_PATH . '/lib/OA/Dll.php';
require_once MAX_PATH . '/www/admin/config.php';
require_once MAX_PATH . '/www/admin/lib-statistics.inc.php';
require_once MAX_PATH . '/lib/max/Admin/Languages.php';
require_once MAX_PATH . '/lib/OX/Admin/Redirect.php';

// Register input variables
phpAds_registerGlobal ('selection');

$con = mysql_connect($GLOBALS['_MAX']['CONF']['database']['host'], $GLOBALS['_MAX']['CONF']['database']['username'], $GLOBALS['_MAX']['CONF']['database']['password']);
mysql_select_db($GLOBALS['_MAX']['CONF']['database']['name'], $con) or die("culnot select:" . mysql_error());
mysql_query("SET NAMES 'utf8'");

$table_prefix = $GLOBALS['_MAX']['CONF']['table']['prefix'];

/*테이블*/
$tableCampaigns = $table_prefix.'campaigns';
$tableBanners   = $table_prefix.'banners';
$tableSummary   = $table_prefix.'data_summary_ad_hourly';
$tableClient   = $table_prefix.'clients';

$_campaignid = $_REQUEST['campaignid'];
$_agencyid = $_REQUEST['agencyid'];
$_clientid = $_REQUEST['clientid'];
$_kind = $_REQUEST['kind'];
$_preid = $_REQUEST['preid'];

//$_agencyid = 1;
//$_clientid = 82;

//$_campaignid = 30;
//$_agencyid = 1;
//$_kind = 'client';
if($_kind == "campaign"){
?>
    <option value='-1' <?php if ($_campaignid == '' || $_campaignid == "-1") echo 'selected="selected"';$campaignname = $GLOBALS['strAll'];?>><?php echo $GLOBALS['strCampaigns']?></option>
<?php
    $query = "SELECT m.campaignid, m.campaignname FROM ".$tableCampaigns." AS m,";
    $query .= $tableClient." AS c,";
    $query .= $tableBanners." AS b";
    $query .= " WHERE m.campaignid = b.campaignid AND m.clientid = c.clientid";

    if($_agencyid > 0){
        $query .= " AND c.agencyid = '".$_agencyid."'";
    }
    if($_clientid > 0){
        $query .= " AND m.clientid = '".$_clientid."'";
    }
    $sql = mysql_query($query);
    while($row = mysql_fetch_array($sql)){
?>
    <option value="<?php echo $row['campaignid']?>" <?php if($_preid > 0){if($_preid == $row['campaignid']){echo 'selected="selected"';$campaignname = $row['campaignname'];}}else{if($_campaignid == $row['campaignid']){echo 'selected="selected"';$campaignname = $row['campaignname'];}}?>><?php echo $row['campaignname']?></option>
<?php
    }
}
?>
<?php
if($_kind == "client"){
?>
    <option value='-1' <?php if ($_clientid == '' || $_clientid == "-1") echo 'selected="selected"';$clientname = $GLOBALS['strAll'];?>><?php echo $GLOBALS['strClient']?></option>
<?php

    if($_campaignid < 0){
        $query = "SELECT * FROM ".$tableClient;
        if($_agencyid > 0){
            $query .= " WHERE agencyid = '".$_agencyid."'";
        }

    }else{
        $query = "SELECT c.clientid, c.clientname FROM ".$tableCampaigns." AS m, ";
        $query .= $tableClient." AS c ";
        $query .= " WHERE m.clientid = c.clientid";

        if($_agencyid > 0){
            $query .= " AND c.agencyid = '".$_agencyid."'";
        }
        if($_campaignid > 0){
            $query .= " AND m.campaignid = '".$_campaignid."'";
        }
    }

    $sql = mysql_query($query);
    while($row = mysql_fetch_array($sql)){
?>
    <option value="<?php echo $row['clientid']?>" <?php if($_preid > 0){if($_preid == $row['clientid']){echo 'selected="selected"';$clientname = $row['clientname'];}}else{if($_clientid == $row['clientid']){echo 'selected="selected"';$clientname = $row['clientname'];}}?>><?php echo $row['clientname']?></option>
<?php
    }
}
?>