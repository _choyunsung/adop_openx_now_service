<?php
// Require the initialisation file
require_once '/var/www/OptimaA/init.php';

// Required files
require_once MAX_PATH . '/lib/pear/Date/TimeZone.php';
require_once MAX_PATH . '/lib/OA/Dal.php';
require_once MAX_PATH . "/lib/OA/Dal/Delivery/mysql.php";

function _convertOffset($offset)
{
    $offset = ((($offset / 1000 ) / 60 ) / 60);

    //  calculate offset hours
    $offsetHours = str_pad((int) abs($offset), 2, 0, STR_PAD_LEFT);

    // retrieve percentage and if only one character add ending 0
    $offsetMinutes = strstr($offset, '.');
    $offsetMinutes = (strlen($offsetMinutes) >= 2) ? $offsetMinutes : str_pad($offsetMinutes, 2, 0);

    //  caculate minutes
    $offsetMinutes = round((60 * $offsetMinutes), 0);

    //  if minutes < 2 characters add leading zero
    $offsetMinutes = str_pad(((int) $offsetMinutes), 2, 0, STR_PAD_LEFT);

    //  build offset
    $offset = $offsetHours;// . $offsetMinutes;

    return $offset;
}
/*
$query = "SELECT 
            campaignid, clientid, campaignname, activate_time 
          FROM 
            " . $GLOBALS["_MAX"]["CONF"]["table"]["prefix"] . $GLOBALS["_MAX"]["CONF"]["table"]["campaigns"] . " 
          WHERE status = 2";
$res = OA_Dal_Delivery_query ( $query );
$campaignArray = array();
$arrayCount = 0;
while ( $row = OA_Dal_Delivery_fetchAssoc ( $res ) ) {
    $campaignArray[$arrayCount]['campaignid']       = $row['campaignid'];
    $campaignArray[$arrayCount]['clientid'] 	    = $row['clientid'];
    $campaignArray[$arrayCount]['campaignname'] 	= $row['campaignname'];
    $campaignArray[$arrayCount]['activate_time'] 	= $row['activate_time'];
    $vQuery = "
            SELECT prefer.value FROM 
                " . $GLOBALS["_MAX"]["CONF"]["table"]["prefix"] . $GLOBALS["_MAX"]["CONF"]["table"]["clients"] . " AS clients,
                " . $GLOBALS["_MAX"]["CONF"]["table"]["prefix"] . $GLOBALS["_MAX"]["CONF"]["table"]["agency"] . " AS agency,
                " . $GLOBALS["_MAX"]["CONF"]["table"]["prefix"] . $GLOBALS["_MAX"]["CONF"]["table"]["account_preference_assoc"] . " AS prefer
            WHERE 
                clients.clientid = '" . $campaignArray[$arrayCount]['clientid'] . "' AND clients.agencyid = agency.agencyid AND prefer.preference_id = 16 AND agency.account_id = prefer.account_id";
    $vRes = OA_Dal_Delivery_query ( $vQuery );
    $vTimeZone = OA_Dal_Delivery_fetchAssoc ( $vRes );
    if( $vTimeZone ) {
        $campaignArray[$arrayCount]['timezone']    = $vTimeZone['value'];
        $campaignArray[$arrayCount]['offset'] 	   = $GLOBALS['_DATE_TIMEZONE_DATA'][$campaignArray[$arrayCount]['timezone']]['offset'];
        $campaignArray[$arrayCount]['time'] 	   = _convertOffset($GLOBALS['_DATE_TIMEZONE_DATA'][$campaignArray[$arrayCount]['timezone']]['offset']);
        $campaignArray[$arrayCount]['operaTime']   = date("Y-m-d H:i:s", strtotime($campaignArray[$arrayCount]['activate_time']) + ( $GLOBALS['_DATE_TIMEZONE_DATA'][$campaignArray[$arrayCount]['timezone']]['offset'] / 1000));
    }
    $arrayCount++;
}

echo "<pre>";
print_r($campaignArray);
*/
// 서버 시간이 UTC  일 경우 DB 값 그데로 비교 하여 업데이트 처리 
if( $_GET['campaigns'] ) {
    $uQuery = "UPDATE " . $GLOBALS["_MAX"]["CONF"]["table"]["prefix"] . $GLOBALS["_MAX"]["CONF"]["table"]["campaigns"] . " SET 
                 status = 0 
               WHERE status = 2 AND activate_time <= now()";
    $res = OA_Dal_Delivery_query ( $uQuery );
    echo "success";
} else
    echo "fail";
?>