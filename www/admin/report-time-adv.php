<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?php
    // Require the initialisation file
    require_once '../../init.php';

    // Include required files
    require_once MAX_PATH . '/lib/OA/Admin/Reports/Index.php';
    require_once MAX_PATH . '/www/admin/config.php';
    require_once MAX_PATH . '/www/admin/lib-maintenance-priority.inc.php';
    require_once MAX_PATH . '/lib/OA/Dal.php';
    require_once MAX_PATH . '/lib/OA/Dll.php';
    require_once MAX_PATH . '/www/admin/config.php';
    require_once MAX_PATH . '/www/admin/lib-statistics.inc.php';
    require_once MAX_PATH . '/lib/max/Admin/Languages.php';
    require_once MAX_PATH . '/lib/OX/Admin/Redirect.php';

    // Register input variables
    phpAds_registerGlobal ('selection');

    // Security check
    OA_Permission::enforceAccount(OA_ACCOUNT_ADMIN, OA_ACCOUNT_MANAGER, OA_ACCOUNT_ADVERTISER, OA_ACCOUNT_TRAFFICKER);

    // Load the required language files
    Language_Loader::load('report');

    /*-------------------------------------------------------*/
    /* HTML framework                                        */
    /*-------------------------------------------------------*/

    /*데이터베이스연결*/
    $con = mysql_connect($GLOBALS['_MAX']['CONF']['database']['host'], $GLOBALS['_MAX']['CONF']['database']['username'], $GLOBALS['_MAX']['CONF']['database']['password']);
    mysql_select_db($GLOBALS['_MAX']['CONF']['database']['name'], $con) or die("culnot select:" . mysql_error());
    mysql_query("SET NAMES 'utf8'");
    /*데이터베이스연결*/
    $table_prefix = $GLOBALS['_MAX']['CONF']['table']['prefix'];

    /*테이블*/
    $tableCampaigns = $table_prefix.'campaigns';
    $tableBanners   = $table_prefix.'banners';
    $tableSummary   = $table_prefix.'data_summary_ad_hourly';
    $tableClient   = $table_prefix.'clients';
    $tableAffiliates   = $table_prefix.'affiliates';
    $tableZones   = $table_prefix.'zones';

    //$_agencyid = OA_Permission::getEntityId();
    $_clientid = OA_Permission::getEntityId();

    $query = "SELECT * FROM ".$tableClient;
    $query .= " WHERE clientid = ".$_clientid;
    $sql = mysql_query($query);
    while($row = mysql_fetch_array($sql)){
        $_agencyid = $row['agencyid'];
    }

    /*상태값*/
    $_status = !empty($_REQUEST['status']) ? $_REQUEST['status'] : '-1';

    /*클라이언트값*/
    //$_clientid = !empty($_REQUEST['clientid']) ? $_REQUEST['clientid'] : '-1';

    /*캠페인값*/
    $_campaignid = !empty($_REQUEST['campaignid']) ? $_REQUEST['campaignid'] : '-1';

    /*배너값*/
    $_bannerid = !empty($_REQUEST['bannerid']) ? $_REQUEST['bannerid'] : '-1';

    /*사이트값*/
    $_affiliateid = !empty($_REQUEST['affiliateid']) ? $_REQUEST['affiliateid'] : '-1';

    /*사이트값*/
    $_zoneid = !empty($_REQUEST['zoneid']) ? $_REQUEST['zoneid'] : '-1';

    $date =  $_REQUEST['date'];

    /*타입*/
    $type =  $_REQUEST['type'];

function time_zone_hour($date) {
    $con1 = mysql_connect($GLOBALS['_MAX']['CONF']['database']['host'], $GLOBALS['_MAX']['CONF']['database']['username'], $GLOBALS['_MAX']['CONF']['database']['password']);
    mysql_select_db($GLOBALS['_MAX']['CONF']['database']['name'], $con1) or die("culnot select:" . mysql_error());
    $table_prefix = $GLOBALS['_MAX']['CONF']['table']['prefix'];
    $timezone_query = mysql_query("select preference_id  from " . $table_prefix . "preferences where preference_name='timezone'");
    $timezone_row = mysql_fetch_array($timezone_query);
    $timezone_id = $timezone_row['preference_id'];
    $sess = $GLOBALS['session']['user'];
    foreach ($sess as $key => $value) {
        $ke[] = $key;
        $val[] = $value;
    }
    $acc = $val['1']['account_id'];
    $query = mysql_query("select value from " . $table_prefix . "account_preference_assoc where preference_id=" . $timezone_id . " AND account_id=" . $acc) or die(mysql_error());
    $row = mysql_fetch_array($query);
    $timezone = $row['value'];
    $value_query = mysql_query("select value from " . $table_prefix . "dj_timezone where timezone='" . $timezone . "' ") or die(mysql_error());
    $value_row = mysql_fetch_array($value_query);
    $offset = $value_row['value'];

    $offsetarr = explode('.', $offset);
    $hour = $offsetarr['0'];
    $minute = $offsetarr['1'];
    $start = strtotime($date);
    if ($hour < 0) {
        $start = $start + ($hour * 60 - $minute) * 60;
    } else {
        $start = $start + ($hour * 60 + $minute) * 60;
    }
    $con_date = date("Y-m-d H:i:s", $start);
    return $con_date;
}

function time_zone($start, $end) {
    $con1 = mysql_connect($GLOBALS['_MAX']['CONF']['database']['host'], $GLOBALS['_MAX']['CONF']['database']['username'], $GLOBALS['_MAX']['CONF']['database']['password']);
    mysql_select_db($GLOBALS['_MAX']['CONF']['database']['name'], $con1) or die("culnot select:" . mysql_error());
    $table_prefix = $GLOBALS['_MAX']['CONF']['table']['prefix'];
    $timezone_query = mysql_query("select preference_id  from " . $table_prefix . "preferences where preference_name='timezone'");
    $timezone_row = mysql_fetch_array($timezone_query);
    $timezone_id = $timezone_row['preference_id'];
    $sess = $GLOBALS['session']['user'];
    foreach ($sess as $key => $value) {
        $ke[] = $key;
        $val[] = $value;
    }
    $acc = $val['1']['account_id'];
    $query = mysql_query("select value from " . $table_prefix . "account_preference_assoc where preference_id='" . $timezone_id . "' AND account_id='" . $acc . "'") or die(mysql_error());
    $row = mysql_fetch_array($query);
    $timezone = $row['value'];
    $value_query = mysql_query("select value from " . $table_prefix . "dj_timezone where timezone='" . $timezone . "' ") or die(mysql_error());
    $value_row = mysql_fetch_array($value_query);
    $offset = $value_row['value'];
    $offsetarr = explode('.', $offset);
    $hour = $offsetarr['0'];
    $minute = $offsetarr['1'];
    $start = strtotime($start);
    if ($hour < 0) {
        $start = $start - ($hour * 60 - $minute) * 60;
    } else {
        $start = $start - ($hour * 60 + $minute) * 60;
    }
    $start_date = date("Y-m-d H:i:s", $start);
    $end = strtotime($end);
    if ($hour < 0) {
        $end = $end - ($hour * 60 - $minute) * 60;
    } else {
        $end = $end - ($hour * 60 + $minute) * 60;
    }
    $end_date = date("Y-m-d H:i:s", $end);
    $date[] = $start_date;
    $date[] = $end_date;
    return $date;
}

$start = date($date." 00:00:00");
$end = date($date." 23:59:59");
//$datetemp = time_zone($start, $end);
//$start = $datetemp[0];
//$end = $datetemp[1];
?>
<style>
    .aright{text-align:right;}
    .aleft{text-align:left;}
</style>
<script type="text/javascript">

    function time_data_search(){
        var frm = document.timedata_search;
        var url = "report-time-adv.php?type=<?php echo $type?>&bannerid="+frm.bannerid.value+"&affiliateid="+frm.affiliateid.value+"&campaignid="+frm.campaignid.value+"&clientid="+frm.clientid.value+"&status="+frm.status.value+"&date="+frm.date.value+"&zoneid="+frm.zoneid.value;
        $.ajax({
            type : "GET",
            url: url,
            success: function(data) {
                parent.$('#layer').html(data);
            }
        });
    }

    var frm = document.redirect_form;
    function time_data_close(){
        parent.$('#mask').fadeOut(1000);
        parent.$('#layer').hide();
    }

</script>
<div style="padding:10px;">
    <div style="text-align:right;padding-bottom:10px;">
        <a href="javascript:time_data_close();"><input class="soflow" type="button" style="cursor:hand;width:30px;" value="X"></a>
    </div>
    <!--차트-->
    <!--<div style="height:500px;">
        <div id="impressions_time_chart" style="width:800px;height:250px;"></div>
        <div id="clicks_time_chart" style="width:800px;height:250px;"></div>
        <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
    </div>-->
    <!--차트-->

    <!--기간조회 POST값-->
    <div>
        <div style="border:1px solid silver;margin:10px;padding:10px;width:740px;">
            <form name='timedata_search' method="get">
                <?php
                    if($type == "campaign"){
                ?>
                <input type="hidden" name="affiliateid" value="-1">
                <input type="hidden" name="bannerid" value="-1">
                <input type="hidden" name="zoneid" value="-1">
                <select class="soflow" name='status' id='status' tabindex='1'>
                    <option value='-1' <?php if ($_status == '' || $_status == "-1") echo 'selected="selected"'?>><?php echo $GLOBALS['strCampaignStatus']?></option>
                    <option value='0' <?php if ($_status == "0") echo 'selected="selected"'?>><?php echo $GLOBALS['strCampaignStatusRunning'];?></option>
                    <option value='1' <?php if ($_status == "1") echo 'selected="selected"'?>><?php echo $GLOBALS['strCampaignStatusPaused'];?></option>
                    <option value='2' <?php if ($_status == "2") echo 'selected="selected"';?>><?php echo $GLOBALS['strCampaignStatusAwaiting'];?></option>
                    <option value='3' <?php if ($_status == "3") echo 'selected="selected"';?>><?php echo $GLOBALS['strCampaignStatusExpired'];?></option>
                    <option value='4' <?php if ($_status == "4") echo 'selected="selected"'?>><?php echo $GLOBALS['strCampaignStatusInactive'];?></option>
                </select>
                <select class="soflow" name='clientid' id='clientid' tabindex='1'>
                    <option value="<?php echo $row['clientid']?>" <?php if ($_clientid == '' || $_clientid == "-1") echo 'selected="selected"'?>><?php echo $GLOBALS['strClient'];?></option>
                    <?php
                    $query = "SELECT * FROM ".$tableClient;
                    if($_agencyid > 0){
                        $query .= " WHERE agencyid = '".$_agencyid."' AND clientid = ".$_clientid;
                    }

                    $sql = mysql_query($query);
                    while($row = mysql_fetch_array($sql)){
                    ?>
                        <option value="<?php echo $row['clientid']?>" <?php if($_clientid == $row['clientid']){echo 'selected="selected"';}?>><?php echo $row['clientname']?></option>
                    <?php
                    }
                    ?>
                </select>
                <select class="soflow" name='campaignid' id='campaignid' tabindex='1'>
                    <option value='-1' <?php if ($_campaignid == '' || $_campaignid == "-1") echo 'selected="selected"'?>><?php echo $GLOBALS['strCampaigns']?></option>
                    <?php
                    $query = "SELECT m.campaignid, m.campaignname FROM ".$tableCampaigns." AS m,";
                    $query .= $tableClient." AS c,";
                    $query .= $tableBanners." AS b";
                    $query .= " WHERE m.campaignid = b.campaignid AND m.clientid = c.clientid";
                    if($_agencyid > 0){
                        $query .= " AND c.agencyid = '".$_agencyid."'";
                    }
                    $sql = mysql_query($query);
                    while($row = mysql_fetch_array($sql)){
                    ?>
                        <option value="<?php echo $row['campaignid']?>" <?php if($_campaignid == $row['campaignid']){echo 'selected="selected"';}?>><?php echo $row['campaignname']?></option>
                    <?php
                    }
                    ?>
                </select>
                <?php
                    }elseif($type == "banner"){
                ?>
                <input type="hidden" name="status" value="-1">
                <input type="hidden" name="campaignid" value="-1">
                <input type="hidden" name="affiliateid" value="-1">
                <input type="hidden" name="clientid" value="-1">
                <input type="hidden" name="zoneid" value="-1">
                <select class="soflow" name='bannerid' id='bannerid' tabindex='1'>
                    <option value='-1' <?php if ($_bannerid == '' || $_bannerid == "-1") echo 'selected="selected"'?>><?php echo $GLOBALS['strBanners']?></option>
                <?php
                    $query = "SELECT b.bannerid, b.description FROM ".$tableBanners." AS b,";
                    $query .= $tableClient." AS c,";
                    $query .= $tableCampaigns." AS m ";
                    $query .= " WHERE m.campaignid = b.campaignid AND m.clientid = c.clientid";
                    if($_agencyid > 0){
                        $query .= " AND c.agencyid = '".$_agencyid."'";
                    }
                    $sql = mysql_query($query);
                    while($row = mysql_fetch_array($sql)){
                ?>
                        <option value="<?php echo $row['bannerid']?>" <?php if($_bannerid == $row['bannerid']){echo 'selected="selected"';}?>><?php echo $row['description']?></option>
                <?php
                    }
                ?>
                </select>
                <?php
                    }elseif($type == "site"){
                ?>
                <input type="hidden" name="status" value="-1">
                <input type="hidden" name="campaignid" value="-1">
                <input type="hidden" name="bannerid" value="-1">
                <input type="hidden" name="clientid" value="-1">
                <input type="hidden" name="zoneid" value="-1">
                <select class="soflow" name='affiliateid' id='affiliateid' tabindex='1'>
                    <option value='-1' <?php if ($_affiliateid == '' || $_affiliateid == "-1") echo 'selected="selected"'?>><?php echo $GLOBALS['strAffiliates']?></option>
                    <?php
                    $query = "SELECT * FROM ".$tableAffiliates;
                    if($_agencyid > 0){
                        $query .= " WHERE agencyid = '".$_agencyid."'";
                    }
                    $sql = mysql_query($query);
                    while($row = mysql_fetch_array($sql)){
                    ?>
                        <option value="<?php echo $row['affiliateid']?>" <?php if($_affiliateid == $row['affiliateid']){echo 'selected="selected"';}?>><?php echo $row['name']?></option>
                    <?php
                    }
                    ?>
                </select>
                <?php
                    }elseif($type == "zone"){
                ?>
                <input type="hidden" name="status" value="-1">
                <input type="hidden" name="campaignid" value="-1">
                <input type="hidden" name="bannerid" value="-1">
                <input type="hidden" name="clientid" value="-1">
                <input type="hidden" name="affiliateid" value="-1">
                <select class="soflow" name='zoneid' id='zoneid' tabindex='1'>
                    <option value='-1' <?php if ($_zoneid == '' || $_zoneid == "-1") echo 'selected="selected"'?>><?php echo $GLOBALS['strZones']?></option>
                    <?php
                    $query = "SELECT z.zoneid, z.zonename FROM ".$tableZones." AS z,";
                    $query .= $tableAffiliates." AS a";
                    $query .= " WHERE z.affiliateid = a.affiliateid";
                    if($_agencyid > 0){
                        $query .= " AND a.agencyid = '".$_agencyid."'";
                    }
                    $sql = mysql_query($query);
                    while($row = mysql_fetch_array($sql)){
                    ?>
                        <option value="<?php echo $row['zoneid']?>" <?php if($_zoneid == $row['zoneid']){echo 'selected="selected"';}?>><?php echo $row['zonename']?></option>
                    <?php
                    }
                    ?>
                </select>
                <?php
                    }
                ?>
                <label for='date'></label>
                <input class="soflow_date" name="date" id="date" type="text" value="<?php if(!empty($date)){echo $date;}else{echo date('Y-m-d');};?>" tabindex="2"/>
                <input class="soflow" type="button" style="cursor:hand;" onclick="return time_data_search();" value="<?php echo $GLOBALS['strSearch']?>"/>
            </form>
        </div>
    </div>
    <br/>
    <div style="border:1px solid silver;margin:10px;padding:10px;width:740px;">
        <div>
            <div style="text-align:left;float:left;margin-left:0px;margin-top:5px;font-size:14px;font-weight:bold;">
                <?php echo $GLOBALS['strTimeReport'];?>
            </div>
            <div style="text-align:right;">
                <a download="time_report.xls" href="#" onclick="return ExcellentExport.excel(this, 'time_data', 'Report');">
                    <input type="button" class="soflow" value="Excel" style="cursor:hand;margin-right:0px;margin-top:0px;"/>
                </a>
            </div>
        </div>
        <table width="100%" id="time_data">
            <tr>
                <th scope="col"><?php echo $GLOBALS['strTime']?></th>
                <th scope="col"><?php echo $GLOBALS['strImpressions']?></th>
                <th scope="col"><?php echo $GLOBALS['strClicks']?></th>
                <th scope="col"><?php echo $GLOBALS['strCTRShort']?> (%)</th>
                <th scope="col"><?php echo $GLOBALS['strRevenue']?> (₩)</th>
                <th scope="col"><?php echo $GLOBALS['strECPM']?></th>
            </tr>
            <?php
            $query = "SELECT
                    s.date_time as date_time,
                    HOUR(s.date_time) as hour,
                    s.total_revenue as revenue,
                    m.campaignname as campaignname,
                    a.name as name,
                    m.status as status,
                    c.clientname as clientname,
                    z.zonename as zonename,
                    z.zoneid as zoneid,
                    m.activate_time as activate_time,
                    m.expire_time as expire_time,
                    SUM(s.requests) AS requests,
                    SUM(s.impressions) AS impressions,
                    SUM(s.clicks) AS clicks,
                    SUM(s.total_revenue) AS revenue,
                    DATE_FORMAT(date_add(s.date_time, interval +9 hour), '%Y-%m-%d') AS day
                    FROM ".$tableCampaigns." AS m,
                    ".$tableBanners." AS b,
                    ".$tableSummary." AS s,
                    ".$tableClient." AS c,
                    ".$tableAffiliates." AS a,
                    ".$tableZones." AS z
                    WHERE m.campaignid = b.campaignid
                    AND m.clientid = c.clientid
                    AND b.bannerid = s.ad_id
                    AND a.affiliateid = z.affiliateid
                    AND z.zoneid = s.zone_id";

            if($_status != '' && $_status >= 0){
                $query .= " AND m.status = '".$_status."'";
            }
            if($_clientid != '' && $_clientid >= 0){
                $query .= " AND c.clientid = '".$_clientid."'";
            }
            if($_campaignid != '' && $_campaignid >= 0){
                $query .= " AND m.campaignid = '".$_campaignid."'";
            }
            if($_bannerid != '' && $_bannerid >= 0){
                $query .= " AND b.bannerid = '".$_bannerid."'";
            }
            if($_affiliateid != '' && $_affiliateid >= 0){
                $query .= " AND a.affiliateid = '".$_affiliateid."'";
            }
            if($_zoneid != '' && $_zoneid >= 0){
                $query .= " AND z.zoneid = '".$_zoneid."'";
            }
            if($_agencyid > 0){
                $query .= " AND c.agencyid = '".$_agencyid."'";
            }

            //$query .= " AND date_time >= '" . $start . "' AND date_time < '" . $end . "'";
            $query .= " AND DATE_ADD(s.date_time, interval 9 hour) BETWEEN '" . $start . "' AND '" . $end . "'";

            $query .= " GROUP BY hour";
            $sql = mysql_query($query);

            $total_impressions = 0;
            $total_clicks = 0;
            $total_revenue = 0;
            $ctr = 0;
            $ecpm = 0;

            while ($row = mysql_fetch_assoc($sql)) {
                $date_time = time_zone_hour($row['date_time']);
                $date_hours = explode(" ", $date_time);
                $hour_split = $date_hours[1];
                $date_hour = explode(":", $hour_split);
                $hour[] = $date_hour[0];

                $impressions[] = $row['impressions'];
                $clicks[] = $row['clicks'];
                $revenue[] = $row['revenue'];

                $view = time_zone_hour($row['date_time']);
                $view_date_temp = explode(" ", $view);
                $view_date_det = $view_date_temp[0];
                $view_date[] = $view_date_det;

                $total_impressions += $row['impressions'];
                $total_clicks += $row['clicks'];
                $total_revenue += $row['revenue'];

                $count = mysql_num_rows($sql);
            }
        ?>
        <?php
            $time = array('0' => '00:00-00:59', '1' => '01:00-01:59', '2' => '02:00-02:59', '3' => '03:00-03:59', '4' => '04:00-04:59', '5' => '05:00-05:59', '6' => '06:00-06:59', '7' => '07:00-07:59', '8' => '08:00-08:59', '9' => '09:00-09:59', '10' => '10:00-10:59', '11' => '11:00-11:59', '12' => '12:00-12:59', '13' => '13:00-13:59', '14' => '14:00-14:59', '15' => '15:00-15:59', '16' => '16:00-16:59', '17' => '17:00-17:59', '18' => '18:00-18:59', '19' => '19:00-19:59', '20' => '20:00-20:59', '21' => '21:00-21:59', '22' => '22:00-22:59', '23' => '23:00-23:59');
            $j = 0;
            $time_chart_data = array();
            $total_ctr = 0;
            $total_ecpm = 0;
            for ($i = 0;$i < 24;$i++) {
                if (in_array($i, $hour)) {
                    $new_hour = $hour[$j];
                    $next_hour = $hour[$j + 1];
                    if ($new_hour == $next_hour) {
                        $impressions[$j] = $impressions[$j] + $impressions[$j + 1];
                        $clicks[$j] = $clicks[$j] + $clicks[$j + 1];
                        $revenue[$j] = $revenue[$j] + $revenue[$j + 1];
                        $ctr = round(($clicks[$j] / $impressions[$j]) * 100, 1);
                        $ecpm = round(($revenue[$j] / $impressions[$j]) * 1000, 3);
                    }
                    $ctr = round(($clicks[$j] / $impressions[$j]) * 100, 1);
                    $ecpm = round(($revenue[$j] / $impressions[$j]) * 1000, 3);

                    $time_chart_data[$i]['time'] = $time[$i];
                    $time_chart_data[$i]['impressions'] = $impressions[$j];
                    $time_chart_data[$i]['clicks'] = $clicks[$j];

                    $total_ctr += ($clicks[$j] / $impressions[$j]) * 100;
                    $total_ecpm += ($revenue[$j] / $impressions[$j]) * 1000;

        ?>
            <tr>
                <td class="dark" style="color:#0767A8;">
                    <img src="assets/images/icon-time.gif" width="16" height="16" align="absmiddle" />
                    &nbsp;<?php echo $time[$i]; ?>
                </td>
                <td class="aright dark">
                    <?php echo number_format($impressions[$j]); ?>
                </td>
                <td class="aright dark">
                    <?php echo number_format($clicks[$j]); ?>
                </td>
                <td class="aright dark">
                    <?php if($ctr != ''){ echo number_format($ctr, 1); }else{ echo "0.0"; }?>
                </td>
                <td class="aright dark">
                    <?php if($revenue[$j] != ''){ echo number_format(round($revenue[$j])); }else{ echo "0"; }?>
                </td>
                <td class="aright dark">
                    <?php if($ecpm != ''){echo number_format($ecpm, 3); }else{ echo "0.000"; }?>
                </td>
            </tr>
        <?php
                if($new_hour == $next_hour){
                    $j = $j + 2;
                }else{
                    $j++;
                }
            }else{
        ?>
            <tr>
                <td class="dark "style="color: #0767A8;">
                    <img src="assets/images/icon-time.gif" width="16" height="16" align="absmiddle" />
                    &nbsp;<?php echo $time[$i];?>
                </td>
                <td class="aright dark">
                    <?php echo "-";?>
                </td>
                <td class="aright dark">
                    <?php echo "-";?>
                </td>
                <td class="aright dark">
                    <?php echo "-";?>
                </td>
                <td class="aright dark">
                    <?php echo "-";?>
                </td>
                <td class="aright dark">
                    <?php echo "-";?>
                </td>
            </tr>
        <?php
            }
        }
        ?>
            <tr>
                <td class="dark" style="color: #0767A8;"><?php echo $GLOBALS['strTotal']?></td>
                <td class="aright dark"><?php echo number_format($total_impressions);?></td>
                <td class="aright dark"><?php echo number_format($total_clicks);?></td>
                <td class="aright dark"> - </td>
                <td class="aright dark"><?php echo number_format(round($total_revenue));?></td>
                <td class="aright dark"> - </td>
            </tr>
            <tr>
                <td class="dark" style="color: #0767A8;"><?php echo $GLOBALS['strAverage']?></td>
                <td class="aright dark"><?php echo number_format($total_impressions/24);?></td>
                <td class="aright dark"><?php echo number_format($total_clicks/24);?></td>
                <td class="aright dark"><?php echo number_format(round($total_ctr/24, 1), 1);?></td>
                <td class="aright dark"><?php echo number_format(round($total_revenue/24));?></td>
                <td class="aright dark"><?php echo number_format(round($total_ecpm/24, 3), 3);?></td>
            </tr>
        </table>
    </div>
</div>

<!-- 시간별차트 -->
<script type="text/javascript">

    /*
    google.load("visualization", "1", {packages:["corechart"]});
    google.setOnLoadCallback(drawVisualization_impressions);
    google.setOnLoadCallback(drawVisualization_clicks);

    function drawVisualization_impressions() {
        // Some raw data (not necessarily accurate)
        var data = google.visualization.arrayToDataTable([
            ['Time', 'Impr.'],
            <?php
                if(count($time_chart_data) > 0){
                    foreach($time_chart_data as $row){
                        echo "['".$row['time']."', ".$row['impressions']."],";
                   }
                }else{
                    echo "['0', 0]";
                }
            ?>
        ]);

        var options = {
            title : 'Time Impressions',
            vAxis: {title: "Count"},
            hAxis: {title: "Time"},
            seriesType: "bars",
            series: {1: {type: "line"}}
        };

        var chart = new google.visualization.ComboChart(document.getElementById('impressions_time_chart'));
        chart.draw(data, options);
    }

    function drawVisualization_clicks() {
        // Some raw data (not necessarily accurate)
        var data = google.visualization.arrayToDataTable([
            ['Time', 'Clicks'],
            <?php
                if(count($time_chart_data) > 0){
                    foreach($time_chart_data as $row){
                        echo "['".$row['time']."', ".$row['clicks']."],";
                   }
                }else{
                    echo "['0', 0]";
                }
            ?>
        ]);

        var options = {
            title : 'Time Clicks',
            vAxis: {title: "Count"},
            hAxis: {title: "Time"},
            seriesType: "bars",
            series: {1: {type: "line"}}
        };

        var chart = new google.visualization.ComboChart(document.getElementById('clicks_time_chart'));
        chart.draw(data, options);
    }
*/
    /*
    $(function () {
        $('#container').highcharts({
            title: {
                text: 'Monthly Average Temperature',
                x: -20 //center
            },
            subtitle: {
                text: 'Source: WorldClimate.com',
                x: -20
            },
            xAxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
            },
            yAxis: {
                title: {
                    text: 'Temperature (°C)'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                valueSuffix: '°C'
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: [{
                name: 'Tokyo',
                data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
            }, {
                name: 'New York',
                data: [-0.2, 0.8, 5.7, 11.3, 17.0, 22.0, 24.8, 24.1, 20.1, 14.1, 8.6, 2.5]
            }, {
                name: 'Berlin',
                data: [-0.9, 0.6, 3.5, 8.4, 13.5, 17.0, 18.6, 17.9, 14.3, 9.0, 3.9, 1.0]
            }, {
                name: 'London',
                data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
            }]
        });
    });
    */
</script>
<!--//시간별차트 -->

