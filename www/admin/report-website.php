<?php
/*
+---------------------------------------------------------------------------+
| Revive Adserver                                                           |
| http://www.revive-adserver.com                                            |
|                                                                           |
| Copyright: See the COPYRIGHT.txt file.                                    |
| License: GPLv2 or later, see the LICENSE.txt file.                        |
+---------------------------------------------------------------------------+
*/

// Require the initialisation file
require_once '../../init.php';

// Include required files
require_once MAX_PATH . '/lib/OA/Admin/Reports/Index.php';
require_once MAX_PATH . '/www/admin/config.php';
require_once MAX_PATH . '/www/admin/lib-maintenance-priority.inc.php';
require_once MAX_PATH . '/lib/OA/Dal.php';
require_once MAX_PATH . '/lib/OA/Dll.php';
require_once MAX_PATH . '/www/admin/config.php';
require_once MAX_PATH . '/www/admin/lib-statistics.inc.php';
require_once MAX_PATH . '/lib/max/Admin/Languages.php';
require_once MAX_PATH . '/lib/OX/Admin/Redirect.php';

// Register input variables
phpAds_registerGlobal ('selection');

// Security check
OA_Permission::enforceAccount(OA_ACCOUNT_ADMIN, OA_ACCOUNT_MANAGER, OA_ACCOUNT_ADVERTISER, OA_ACCOUNT_TRAFFICKER);

// Load the required language files
Language_Loader::load('report');

/*-------------------------------------------------------*/
/* HTML framework                                        */
/*-------------------------------------------------------*/

phpAds_PageHeader("3");
?>
<?php
/*-------------------------------------------------------*/
/* Main code                                             */
/*-------------------------------------------------------*/
?>
<script type="text/javascript" src="https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1.1','packages':['line']}]}"></script>
<script type="text/javascript">
    /**
     * ExcellentExport.
     * A client side Javascript export to Excel.
     *
     * @author: Jordi Burgos (jordiburgos@gmail.com)
     *
     * Based on:
     * https://gist.github.com/insin/1031969
     * http://jsfiddle.net/insin/cmewv/
     *
     * CSV: http://en.wikipedia.org/wiki/Comma-separated_values
     */

    /*
     * Base64 encoder/decoder from: http://jsperf.com/base64-optimized
     */


    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
    var fromCharCode = String.fromCharCode;
    var INVALID_CHARACTER_ERR = ( function() {
        // fabricate a suitable error object
        try {
            document.createElement('$');
        } catch (error) {
            return error;
        }
    }());

    // encoder
    window.btoa || (window.btoa = function(string) {
        var a, b, b1, b2, b3, b4, c, i = 0, len = string.length, max = Math.max, result = '';

        while (i < len) {
            a = string.charCodeAt(i++) || 0;
            b = string.charCodeAt(i++) || 0;
            c = string.charCodeAt(i++) || 0;

            if (max(a, b, c) > 0xFF) {
                throw INVALID_CHARACTER_ERR;
            }

            b1 = (a >> 2) & 0x3F;
            b2 = ((a & 0x3) << 4) | ((b >> 4) & 0xF);
            b3 = ((b & 0xF) << 2) | ((c >> 6) & 0x3);
            b4 = c & 0x3F;

            if (!b) {
                b3 = b4 = 64;
            } else if (!c) {
                b4 = 64;
            }
            result += characters.charAt(b1) + characters.charAt(b2) + characters.charAt(b3) + characters.charAt(b4);
        }
        return result;
    });

    // decoder
    window.atob || (window.atob = function(string) {
        string = string.replace(/=+$/, '');
        var a, b, b1, b2, b3, b4, c, i = 0, len = string.length, chars = [];

        if (len % 4 === 1)
            throw INVALID_CHARACTER_ERR;

        while (i < len) {
            b1 = characters.indexOf(string.charAt(i++));
            b2 = characters.indexOf(string.charAt(i++));
            b3 = characters.indexOf(string.charAt(i++));
            b4 = characters.indexOf(string.charAt(i++));

            a = ((b1 & 0x3F) << 2) | ((b2 >> 4) & 0x3);
            b = ((b2 & 0xF) << 4) | ((b3 >> 2) & 0xF);
            c = ((b3 & 0x3) << 6) | (b4 & 0x3F);

            chars.push(fromCharCode(a));
            b && chars.push(fromCharCode(b));
            c && chars.push(fromCharCode(c));
        }
        return chars.join('');
    });


    ExcellentExport = (function() {
        var version = "1.3";
        var uri = {excel: 'data:application/vnd.ms-excel;base64,', csv: 'data:application/csv;base64,'};
        var template = {excel: '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'};
        var base64 = function(s) {
            return window.btoa(unescape(encodeURIComponent(s)));
        };
        var format = function(s, c) {
            return s.replace(/{(\w+)}/g, function(m, p) {
                return c[p];
            });
        };

        var get = function(element) {
            if (!element.nodeType) {
                return document.getElementById(element);
            }
            return element;
        };

        var fixCSVField = function(value) {
            var fixedValue = value;
            var addQuotes = (value.indexOf(',') !== -1) || (value.indexOf('\r') !== -1) || (value.indexOf('\n') !== -1);
            var replaceDoubleQuotes = (value.indexOf('"') !== -1);

            if (replaceDoubleQuotes) {
                fixedValue = fixedValue.replace(/"/g, '""');
            }
            if (addQuotes || replaceDoubleQuotes) {
                fixedValue = '"' + fixedValue + '"';
            }
            return fixedValue;
        };

        var tableToCSV = function(table) {
            var data = "";
            for (var i = 0, row; row = table.rows[i]; i++) {
                for (var j = 0, col; col = row.cells[j]; j++) {
                    data = data + (j ? ',' : '') + fixCSVField(col.innerHTML);
                }
                data = data + "\r\n";
            }
            return data;
        };

        var ee = {
            /** @expose */
            excel: function(anchor, table, name) {
                table = get(table);
                var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML};
                var hrefvalue = uri.excel + base64(format(template.excel, ctx));
                anchor.href = hrefvalue;
                // Return true to allow the link to work
                return true;
            },
            /** @expose */
            csv: function(anchor, table) {
                table = get(table);
                var csvData = tableToCSV(table);
                var hrefvalue = uri.csv + base64(csvData);
                anchor.href = hrefvalue;
                return true;
            }
        };

        return ee;
    }());
</script>
<style type="text/css">
    .aright{text-align:right;}
    .aleft{text-align:left;}

    table{border-collapse:collapse}
    th{border:1px solid #999999;padding:10px;background-color:#EDEDED;color:#555555;font-weight:bold;}
    td{border:1px solid silver;padding:10px}

    select.soflow {
        border: 1px solid #AAA;
        color: #555;
        font-size: inherit;
        margin:5px;
        overflow: hidden;
        height:30px;
        padding: 5px 10px;
        text-overflow: ellipsis;
        white-space: nowrap;
        width: 100px;
    }

    input.soflow {
        border: 1px solid #AAA;
        color: #555;
        font-size: inherit;
        margin:5px;
        height:30px;
        overflow: hidden;
        padding: 5px 10px;
        text-overflow: ellipsis;
        white-space: nowrap;
        background-color:#EDEDED;
        width: 100px;
    }

    input.soflow_date {
        border: 1px solid #ccc;
        color: #555;
        font-size: inherit;
        margin:5px;
        height:30px;
        overflow: hidden;
        padding-left: 5px;
        text-overflow: ellipsis;
        white-space: nowrap;
        width: 100px;
    }
</style>
<?php

/*데이터베이스연결*/
$con = mysql_connect($GLOBALS['_MAX']['CONF']['database']['host'], $GLOBALS['_MAX']['CONF']['database']['username'], $GLOBALS['_MAX']['CONF']['database']['password']);
mysql_select_db($GLOBALS['_MAX']['CONF']['database']['name'], $con) or die("culnot select:" . mysql_error());
mysql_query("SET NAMES 'utf8'");
/*데이터베이스연결*/
$table_prefix = $GLOBALS['_MAX']['CONF']['table']['prefix'];

/*테이블*/
$tableCampaigns = $table_prefix.'campaigns';
$tableBanners   = $table_prefix.'banners';
$tableSummary   = $table_prefix.'data_summary_ad_hourly';
$tableClients   = $table_prefix.'clients';
$tableAffiliates   = $table_prefix.'affiliates';
$tableZones = $table_prefix.'zones';

$_agencyid = OA_Permission::getEntityId();

/*상태값*/
$_status = !empty($_REQUEST['status']) ? $_REQUEST['status'] : '-1';

/*사이트값*/
$_affiliateid = !empty($_REQUEST['affiliateid']) ? $_REQUEST['affiliateid'] : '-1';

/*캠페인값*/
$_campaignid = !empty($_REQUEST['campaignid']) ? $_REQUEST['campaignid'] : '-1';

$_period_term = !empty($_REQUEST['period_term']) ? $_REQUEST['period_term'] : 'day';

/*기간조희*/
$_period = !empty($_REQUEST['period_preset']) ? $_REQUEST['period_preset'] : '';

if ($_period == '') {
    $start = date('Y-m-d 00:00:00', strtotime('Today - 31 Day'));
    $end = date('Y-m-d 23:59:59', strtotime('Today - 1 Day'));
    $period = 'last_30_days';
//     $result = time_zone($start, $end);
//     $startdate = $result[0];
//     $endate = $result[1];
    $startdate = $start;
    $endate = $end;
}else{
    $start = $_REQUEST['period_start'];
    $end = $_REQUEST['period_end'];
    $startdate = date("$start 00:00:00");
    $endate = date("$end 23:59:59");
}

function time_zone_hour($date) {
    $con1 = mysql_connect($GLOBALS['_MAX']['CONF']['database']['host'], $GLOBALS['_MAX']['CONF']['database']['username'], $GLOBALS['_MAX']['CONF']['database']['password']);
    mysql_select_db($GLOBALS['_MAX']['CONF']['database']['name'], $con1) or die("culnot select:" . mysql_error());
    $table_prefix = $GLOBALS['_MAX']['CONF']['table']['prefix'];
    $timezone_query = mysql_query("select preference_id  from " . $table_prefix . "preferences where preference_name='timezone'");
    $timezone_row = mysql_fetch_array($timezone_query);
    $timezone_id = $timezone_row['preference_id'];
    $sess = $GLOBALS['session']['user'];
    foreach ($sess as $key => $value) {
        $ke[] = $key;
        $val[] = $value;
    }
    $acc = $val['1']['account_id'];
    $query = mysql_query("select value from " . $table_prefix . "account_preference_assoc where preference_id=" . $timezone_id . " AND account_id=" . $acc) or die(mysql_error());
    $row = mysql_fetch_array($query);
    $timezone = $row['value'];
    $value_query = mysql_query("select value from " . $table_prefix . "dj_timezone where timezone='" . $timezone . "' ") or die(mysql_error());
    $value_row = mysql_fetch_array($value_query);
    $offset = $value_row['value'];

    $offsetarr = explode('.', $offset);
    $hour = $offsetarr['0'];
    $minute = $offsetarr['1'];
    $start = strtotime($date);
    if ($hour < 0) {
        $start = $start + ($hour * 60 - $minute) * 60;
    } else {
        $start = $start + ($hour * 60 + $minute) * 60;
    }
    $con_date = date("Y-m-d H:i:s", $start);
    return $con_date;
}

function time_zone($start, $end) {
    $con1 = mysql_connect($GLOBALS['_MAX']['CONF']['database']['host'], $GLOBALS['_MAX']['CONF']['database']['username'], $GLOBALS['_MAX']['CONF']['database']['password']);
    mysql_select_db($GLOBALS['_MAX']['CONF']['database']['name'], $con1) or die("culnot select:" . mysql_error());
    $table_prefix = $GLOBALS['_MAX']['CONF']['table']['prefix'];
    $timezone_query = mysql_query("select preference_id  from " . $table_prefix . "preferences where preference_name='timezone'");
    $timezone_row = mysql_fetch_array($timezone_query);
    $timezone_id = $timezone_row['preference_id'];
    $sess = $GLOBALS['session']['user'];
    foreach ($sess as $key => $value) {
        $ke[] = $key;
        $val[] = $value;
    }
    $acc = $val['1']['account_id'];
    $query = mysql_query("select value from " . $table_prefix . "account_preference_assoc where preference_id='" . $timezone_id . "' AND account_id='" . $acc . "'") or die(mysql_error());
    $row = mysql_fetch_array($query);
    $timezone = $row['value'];
    $value_query = mysql_query("select value from " . $table_prefix . "dj_timezone where timezone='" . $timezone . "' ") or die(mysql_error());
    $value_row = mysql_fetch_array($value_query);
    $offset = $value_row['value'];
    $offsetarr = explode('.', $offset);
    $hour = $offsetarr['0'];
    $minute = $offsetarr['1'];
    $start = strtotime($start);
    if ($hour < 0) {
        $start = $start - ($hour * 60 - $minute) * 60;
    } else {
        $start = $start - ($hour * 60 + $minute) * 60;
    }
    $start_date = date("Y-m-d H:i:s", $start);
    $end = strtotime($end);
    if ($hour < 0) {
        $end = $end - ($hour * 60 - $minute) * 60;
    } else {
        $end = $end - ($hour * 60 + $minute) * 60;
    }
    $end_date = date("Y-m-d H:i:s", $end);
    $date[] = $start_date;
    $date[] = $end_date;
    return $date;
}
/*기간조희*/
?>
<!--기간조회 POST값-->
<div>
    <script type="text/javascript">
        function click_affiliate(affiliateid){
            var form = document.getElementById('period_preset').form;
            if (checkDates(form)) {
                form.affiliateid.value = affiliateid;
                form.submit();
            }
            return false;
        }

        function click_campaign(campaignid){
            var form = document.getElementById('period_preset').form;
            if (checkDates(form)) {
                form.campaignid.value = campaignid;
                form.submit();
            }
            return false;
        }
    </script>
    <div style="border:1px solid silver;margin:10px;padding:10px;width:95%;">
        <form name='search' method='get'>
            <div>
                <select class="soflow" style="width:165px;" name='affiliateid' id='affiliateid' tabindex='1'>
                    <option value='-1' <?php if ($_affiliateid == '' || $_affiliateid == "-1") echo 'selected="selected"';$websitename = $GLOBALS['strAll'];?>><?php echo $GLOBALS['strAffiliates']?></option>
                    <?php
                        $query = "SELECT * FROM ".$tableAffiliates;
                        if($_agencyid > 0){
                            $query .= " WHERE agencyid = '".$_agencyid."'";
                        }

                    $sql = mysql_query($query);
                        while($row = mysql_fetch_array($sql)){
                    ?>
                        <option value="<?php echo $row['affiliateid']?>" <?php if($_affiliateid == $row['affiliateid']){echo 'selected="selected"';$websitename = $row['name'];}?>><?php echo $row['name']?></option>
                    <?php
                        }
                    ?>
                </select>
                <?php
                    if($_affiliateid > 0){
                ?>
                <select class="soflow" style="width:210px;"  name='campaignid' id='campaignid' tabindex='1'>
                    <option value='-1' <?php if ($_campaignid == '' || $_campaignid == "-1") echo 'selected="selected"';$campaignname = $GLOBALS['strAll'];?>><?php echo $GLOBALS['strCampaigns']?></option>
                    <?php
                    $query = "SELECT
                                    m.campaignid as campaignid,
                                    m.campaignname as campaignname
                                    FROM ".$tableCampaigns." AS m,
                                    ".$tableClients." AS c,
                                    ".$tableZones." AS z,
                                    ".$tableBanners." AS b,
                                    ".$tableSummary." AS s,
                                    ".$tableAffiliates." AS a
                                    WHERE m.campaignid = b.campaignid
                                    AND m.clientid = c.clientid
                                    AND b.bannerid = s.ad_id
                                    AND a.affiliateid = z.affiliateid
                                    AND z.zoneid = s.zone_id";

                    if($_affiliateid != "" && $_affiliateid >= 0){
                        $query .= " AND a.affiliateid = '".$_affiliateid."'";
                    }

                    $query .= " GROUP BY m.campaignid, m.campaignname";

                    $sql = mysql_query($query);
                    while($row = mysql_fetch_array($sql)){
                    ?>
                        <option value="<?php echo $row['campaignid']?>" <?php if($_campaignid == $row['campaignid']){echo 'selected="selected"';$campaignname = $row['campaignname'];}?>><?php echo $row['campaignname']?></option>
                    <?php
                    }
                    ?>
                </select>
                <?php
                    }
                ?>
                <select class="soflow" name='period_term'>
                    <option value="day" <?php if ($_period_term == "day") echo 'selected="selected"';?>><?php echo $GLOBALS['strBreakdownByDay'];?></option>
                    <option value="week" <?php if ($_period_term == "week") echo 'selected="selected"';?>><?php echo $GLOBALS['strBreakdownByWeek'];?></option>
                    <option value="month" <?php if ($_period_term == "month") echo 'selected="selected"';?>><?php echo $GLOBALS['strBreakdownByMonth'];?></option>
                </select>
                <select class="soflow" name='period_preset' id='period_preset' onchange='periodFormChange(1)' tabindex='1'>
                    <option value='today' <?php if ($_period == "today") echo 'selected="selected"'?>><?php echo $GLOBALS['strCollectedToday'];?></option>
                    <option value='yesterday' <?php if ($_period == "yesterday") echo 'selected="selected"'?>><?php echo $GLOBALS['strCollectedYesterday'];?></option>
                    <option value='last_7_days' <?php if ($_period == "last_7_days") echo 'selected="selected"';?>><?php echo $GLOBALS['strLast7Days'];?></option>
                    <option value='last_30_days' <?php if ($_period == "last_30_days" || $_period == "") echo 'selected="selected"';?>><?php echo $GLOBALS['strLast30Days'];?></option>
                    <!--
                <option value='this_month' <?php if ($_period == "this_month") echo 'selected="selected"';?>><?php echo $GLOBALS['strCollectedThisMonth'];?></option>
                <option value='last_month' <?php if ($_period == "last_month") echo 'selected="selected"'?>><?php echo $GLOBALS['strCollectedLastMonth'];?></option>
                <option value='all_stats' <?php if ($_period == "all_stats") echo 'selected="selected"';?>><?php echo $GLOBALS['strCollectedAllStats'];?></option>
                -->
                </select>
                <label for='period_start'></label>
                <input class="soflow_date"  name="period_start" id="period_start" type="text" value="<?php if(!empty($start)){echo substr($start, 0, 10);}else{echo date('Y-m-d');};?>" tabindex="2"/>
                <input type='image' src='assets/images/icon-calendar-d.gif' id='period_start_button' align='absmiddle' border='0' tabindex='3' />
                <label for='period_end'></label>
                <input class="soflow_date" name="period_end" id="period_end" type="text" value="<?php if(!empty($end)){echo substr($end, 0, 10);}else{echo date('Y-m-d');};?>" tabindex="4" />
                <input type='image' src='assets/images/icon-calendar-d.gif' id='period_end_button' align='absmiddle' border='0' tabindex='3' />
                <input class="soflow" type="button" onclick="return periodFormSubmit();" style="cursor:pointer;" value="<?php echo $GLOBALS['strSearch']?>"/>
        </form>
    </div>
</div>
<!--기간조회 POST값-->
<!--차트-->
<div style="width:95%;height:460px;overflow-x:scroll;border:1px solid silver;margin:10px;padding:10px;">
    <div style="font-size:14px;font-weight:bold;margin-bottom:5px;">
        [ <?php echo $GLOBALS['strAffiliates']?> : <?php echo $websitename;?> ]
        <?php
            if($campaignname != ''){
        ?>
        [ <?php echo $GLOBALS['strCampaigns']?> : <?php echo $campaignname;?> ]
        <?php
            }
        ?>
    </div>
    <div id="daily_chart" style="border:1px solid silver;margin-right:10px;padding:10px;"></div>
</div>
</div>
<div style="clear:both;"></div>
<!--차트-->
<div style="border:1px solid silver;margin:10px;padding:10px;width:95%;">
    <div>
        <div style="text-align:left;float:left;margin-left:0px;margin-top:5px;font-size:14px;font-weight:bold;">
            <?php echo $GLOBALS['strWebsiteReport'];?>
        </div>
        <div style="text-align:right;">
            <a download="website_report.xls" href="#" onclick="return ExcellentExport.excel(this, 'website_data', 'Website Report');">
                <input type="button" class="soflow" value="Excel" style="cursor:hand;margin-right:0px;margin-top:0px;"/>
            </a>
        </div>
    </div>
    <table width="100%" id="website_data">
        <tr>
            <th><?php echo $GLOBALS['strAffiliates']?></th>
            <th><?php echo $GLOBALS['strImpressions']?></th>
            <th><?php echo $GLOBALS['strClicks']?></th>
            <th><?php echo $GLOBALS['strCTRShort']?></th>
        </tr>
    <?php
        $query = "SELECT
                        a.affiliateid as affiliateid,
                        a.name as name,
                        SUM(s.requests) AS requests,
                        SUM(s.impressions) AS impressions,
                        SUM(s.clicks) AS clicks,
                        SUM(s.total_revenue) AS revenue
                        FROM ".$tableCampaigns." AS m,
                        ".$tableClients." AS c,
                        ".$tableZones." AS z,
                        ".$tableBanners." AS b,
                        ".$tableSummary." AS s,
                        ".$tableAffiliates." AS a
                        WHERE m.campaignid = b.campaignid
                        AND m.clientid = c.clientid
                        AND b.bannerid = s.ad_id
                        AND a.affiliateid = z.affiliateid
                        AND z.zoneid = s.zone_id";

        if($_affiliateid >= 0){
            $query .= " AND a.affiliateid = '".$_affiliateid."'";
        }
        if($_agencyid > 0){
            $query .= " AND c.agencyid = '".$_agencyid."'";
        }
//         $query .= " AND DATE_FORMAT(s.date_time, '%Y-%m-%d') >= '".$startdate."'
//                         AND DATE_FORMAT(s.date_time, '%Y-%m-%d') <= '".$endate."'
//                         GROUP BY a.affiliateid, a.name";
        $query .= " AND DATE_ADD(s.date_time, interval 9 hour) BETWEEN '" . $startdate . "' AND '" . $endate . "'
                        GROUP BY a.affiliateid, a.name";

        $sql = mysql_query($query);
        $chart_data = array();
        $i = 0;
        while($row = mysql_fetch_array($sql)){

            $chart_data[$i]['name'] = $row['name'];
            $chart_data[$i]['impressions'] = $row['impressions'];
            $chart_data[$i]['clicks'] = $row['clicks'];
    ?>
        <tr class="web">
            <td class="dark"><a href="javascript:click_affiliate(<?php echo $row['affiliateid']?>);"><?php echo $row['name']?></a></td>
            <td class="aright dark"><?php echo  number_format($row['impressions'])?></td>
            <td class="aright dark"><?php echo  number_format($row['clicks'])?></td>
            <td class="aright dark"><?php if($row['impressions'] > 0){ echo round($row['clicks']/$row['impressions']* 100, 3);}?></td>
        </tr>
    <?php
            $i++;
        }
    ?>
    </table>
</div>
<?php
if($_affiliateid > 0){
    $display = "";
}else{
    $display = "display:none;";
}
?>
<div style="<?php echo $display?>">
    <div style="border:1px solid silver;margin:10px;padding:10px;width:95%;">
        <table width="100%" id="website_campaign_data">
            <tr>
                <th scope="col"><?php echo $GLOBALS['strCampaigns']?></th>
                <th scope="col"><?php echo $GLOBALS['strImpressions']?></th>
                <th scope="col"><?php echo $GLOBALS['strClicks']?></th>
                <th scope="col"><?php echo $GLOBALS['strCTRShort']?> (%)</th>
            </tr>
            <?php
            $query = "SELECT
                            m.campaignid as campaignid,
                            m.campaignname as campaignname,
                            SUM(s.requests) AS requests,
                            SUM(s.impressions) AS impressions,
                            SUM(s.clicks) AS clicks,
                            SUM(s.total_revenue) AS revenue
                            FROM ".$tableCampaigns." AS m,
                            ".$tableClients." AS c,
                            ".$tableZones." AS z,
                            ".$tableBanners." AS b,
                            ".$tableSummary." AS s,
                            ".$tableAffiliates." AS a
                            WHERE m.campaignid = b.campaignid
                            AND m.clientid = c.clientid
                            AND b.bannerid = s.ad_id
                            AND a.affiliateid = z.affiliateid
                            AND z.zoneid = s.zone_id";

            if($_affiliateid != "" && $_affiliateid >= 0){
                $query .= " AND a.affiliateid = '".$_affiliateid."'";
            }
            if($_agencyid > 0){
                $query .= " AND c.agencyid = '".$_agencyid."'";
            }
            if($_campaignid != "" && $_campaignid >= 0){
                $query .= " AND m.campaignid = '".$_campaignid."'";
            }
//             $startdate = substr($startdate, 0, 10);
//             $endate = substr($endate, 0, 10);
//             $query .= " AND DATE_FORMAT(s.date_time, '%Y-%m-%d') >= '".$startdate."'
//                             AND DATE_FORMAT(s.date_time, '%Y-%m-%d') <= '".$endate."'
//                             GROUP BY m.campaignid, m.campaignname";
            $query .= " AND DATE_ADD(s.date_time, interval 9 hour) BETWEEN '" . $startdate . "' AND '" . $endate . "'
                        GROUP BY m.campaignid, m.campaignname";

            $sql = mysql_query($query);
            if($_campaignid > 0){
                $chart_data = array();
            }
            $i = 0;
            while($row = mysql_fetch_array($sql)){
                if($_campaignid > 0){
                    $chart_data[$i]['name'] = $row['campaignname'];
                    $chart_data[$i]['impressions'] = $row['impressions'];
                    $chart_data[$i]['clicks'] = $row['clicks'];
                }
                ?>
                <tr class="web">
                    <td class="dark"><a href="javascript:click_campaign(<?php echo $row['campaignid']?>);"><?php echo $row['campaignname']?></a></td>
                    <td class="aright dark"><?php echo  number_format($row['impressions'])?></td>
                    <td class="aright dark"><?php echo  number_format($row['clicks'])?></td>
                    <td class="aright dark"><?php if($row['impressions'] > 0){ echo round($row['clicks']/$row['impressions']* 100, 3);}?></td>
                </tr>
                <?php
                $i++;
            }
            ?>
        </table>
    </div>
</div>
<?php
   // if($_affiliateid != ''){
?>

<?php
if($_affiliateid > 0){
    $display = "";
}else{
    $display = "display:none;";
}
?>
<div style="<?php echo $display?>">
<!--캠페인 하나당 기간대별 조회-->
<div style="border:1px solid silver;margin:10px;padding:10px;width:95%;">
    <div>
        <div style="text-align:left;float:left;margin-left:0px;margin-top:5px;font-size:14px;font-weight:bold;">
            <?php echo $GLOBALS['strDayReport'];?>
        </div>
        <div style="text-align:right;">
            <a download="daily_website_report.xls" href="#" onclick="return ExcellentExport.excel(this, 'daily_website_data', 'Website Report');">
                <input type="button" class="soflow" value="Excel" style="cursor:hand;margin-right:0px;margin-top:0px;"/>
            </a>
        </div>
    </div>
    <table width="100%" id="daily_website_report">
        <tr>
            <th scope="col"><?php echo $GLOBALS['strDate']?></th>
            <th scope="col"><?php echo $GLOBALS['strImpressions']?></th>
            <th scope="col"><?php echo $GLOBALS['strClicks']?></th>
            <th scope="col"><?php echo $GLOBALS['strCTRShort']?> (%)</th>
            <th scope="col"><?php echo $GLOBALS['strRevenue']?> (₩)</th>
            <th scope="col"><?php echo $GLOBALS['strECPM']?></th>
        </tr>
        <?php
        $query = "SELECT
                DATE_ADD(s.date_time, interval +9 hour) as date_time,
                a.name as name,
                m.status as status,
                c.clientname as clientname,
                m.activate_time as activate_time,
                m.expire_time as expire_time,
                SUM(s.requests) AS requests,
                SUM(s.impressions) AS impressions,
                SUM(s.clicks) AS clicks,
                SUM(s.total_revenue) AS revenue,
                DATE_FORMAT(date_add(s.date_time, interval +9 hour), '%Y-%m-%d') AS day,
                DATE_FORMAT(date_add(s.date_time, interval +9 hour), '%Y-%m') AS month,
                DATE_FORMAT(date_add(s.date_time, interval +9 hour), '%Y-%u') AS week
                FROM ".$tableCampaigns." AS m,
                ".$tableBanners." AS b,
                ".$tableSummary." AS s,
                ".$tableClients." AS c,
                ".$tableZones." AS z,
                ".$tableAffiliates." AS a
                WHERE m.campaignid = b.campaignid
                AND m.clientid = c.clientid
                AND b.bannerid = s.ad_id
                AND a.affiliateid = z.affiliateid
                AND z.zoneid = s.zone_id";

        if($_affiliateid > 0){
            $query .= " AND a.affiliateid = '".$_affiliateid."'";
        }
        if($_agencyid > 0){
            $query .= " AND c.agencyid = '".$_agencyid."'";
        }
        if($_campaignid != "" && $_campaignid >= 0){
            $query .= " AND m.campaignid = '".$_campaignid."'";
        }
//         $start = date($startdate." 00:00:00");
//         $end = date($endate." 23:59:59");
        $start = $startdate;
        $end = $endate;
//         $datetemp = time_zone($start, $end);
//         $start = $datetemp[0];
//         $end = $datetemp[1];

//         $query .= " AND s.date_time >= '" . $start . "' AND s.date_time < '" . $end . "'";
        $query .= " AND DATE_ADD(s.date_time, interval 9 hour) BETWEEN '" . $start . "' AND '" . $end . "'";
        if($_period_term == 'day'){
            $query .= " GROUP BY day";
        }elseif($_period_term == 'month'){
            $query .= " GROUP BY month";
        }elseif($_period_term == 'week'){
            $query .= " GROUP BY week";
        }

        $sql = mysql_query($query);

        $total_impressions = 0;
        $total_clicks = 0;
        $total_revenue = 0;
        $ctr = 0;
        $ecpm = 0;

        while ($row = mysql_fetch_assoc($sql)) {
//             $date_time = time_zone_hour($row['date_time']);
            $date_time = $row['date_time'];
            $date_hours = explode(" ", $date_time);
            $hour_split = $date_hours[1];
            $date_hour = explode(":", $hour_split);
            $hour[] = $date_hour[0];

            $impressions[] = $row['impressions'];
            $clicks[] = $row['clicks'];
            $revenue[] = $row['revenue'];

//             $view = time_zone_hour($row['date_time']);
            $view = $row['date_time'];
            $view_date_temp = explode(" ", $view);
            $view_date_det = $view_date_temp[0];
            $start_date_det = $view_date_temp[0];
            $start_date[] = $start_date_det;

            if($_period_term == "month"){
                $data_month = explode("-", $view_date_det);
                $view_date_det = $data_month[0]."-".$data_month[1];
            }

            if($_period_term == "week"){
                $view_date_det = date("Y-W", strtotime($view_date_det));
            }

            $view_date[] = $view_date_det;

            $total_impressions += $row['impressions'];
            $total_clicks += $row['clicks'];
            $total_revenue += $row['revenue'];

            $count = mysql_num_rows($sql);
        }

        $start_date = array_unique($start_date);
        $view_date = array_unique($view_date);

        $j = 0;

        if ($period != 'all_stats') {
            $end_da = explode(" ", $end);
            $end = $end_da[0];
            $start_da = explode(" ", $start);
            $start = $start_da[0];
            $date1 = strtotime($start);
            $date2 = strtotime($end);
            $dateDiff = $date2 - $date1;
            if($_period_term == "day"){
                $count = floor($dateDiff / (60 * 60 * 24));
            }elseif($_period_term == "month"){
                $count = floor($dateDiff / (60 * 60 * 24 * 28));
            }elseif($_period_term == "week"){
                $count = floor($dateDiff / (60 * 60 * 24 * 7));
            }
            $count = $count + 1;
        } else {
            $start = $start_date[0];
            $end_da = explode(" ", $end);
            $end = $end_da[0];
            $date1 = strtotime($start);
            $date2 = strtotime($end);
            $dateDiff = $date2 - $date1;
            if($_period_term == "day"){
                $count = floor($dateDiff / (60 * 60 * 24));
            }elseif($_period_term == "month"){
                $count = floor($dateDiff / (60 * 60 * 24 * 28));
            }elseif($_period_term == "week"){
                $count = floor($dateDiff / (60 * 60 * 24 * 7));
            }
            $count = $count + 1;
        }

        $daily_chart_data = array();
        $total_ctr = 0;
        $total_ecpm = 0;
        for ($i = 0; $i < $count; $i++) {
            $ctr = ($clicks[$j] / $impressions[$j]) * 100;
            $ctr_percent = round($ctr, 1);
            $ecpm = ($revenue[$j] / $impressions[$j]) * 1000;
            $ecpm_final = round($ecpm, 3);
            if($_period_term == 'day'){
                $new_date = date('Y-m-d', strtotime($start . $i . 'day'));
            }elseif($_period_term == 'month'){
                $new_date = date('Y-m', strtotime($start . $i . 'month'));
            }elseif($_period_term == 'week'){
                $new_date = date('Y-W', strtotime($start . $i . 'week'));
            }

            if (in_array($new_date, $view_date)) {
                $cur_date = $view_date[$j];
                $next_date = $view_date[$j + 1];
                if ($cur_date == $next_date) {
                    $impressions[$j] = $impressions[$j] + $impressions[$j + 1];
                    $clicks[$j] = $clicks[$j] + $clicks[$j + 1];
                    $ctr = ($clicks[$j] / $impressions[$j]) * 100;
                    $ctr_percent = round($ctr, 1);
                    $ecpm = ($revenue[$j] / $impressions[$j]) * 1000;
                    $ecpm_final = round($ecpm, 3);
                }

                if($_period_term == 'day'){
                    $daily_chart_data[$i]['day'] = date('Y-m-d ', strtotime($start . $i . 'day'));
                }elseif($_period_term == 'month'){
                    $daily_chart_data[$i]['day'] = date('Y-m ', strtotime($start . $i . 'month'));
                }elseif($_period_term == 'week'){
                    $daily_chart_data[$i]['day'] = date('Y-W ', strtotime($start . $i . 'week'));
                }

                $daily_chart_data[$i]['impressions'] = $impressions[$j];
                $daily_chart_data[$i]['clicks'] = $clicks[$j];
                $day = date('Y-m-d', strtotime($start . $i . 'day'));
                $total_ctr += $ctr;
                $total_ecpm += $ecpm;
        ?>
		<tr>
		    <td class="dark" style="color: #0767A8;">
                <img src="assets/images/icon-date.gif" width="16" height="16" align="absmiddle" />
                &nbsp;<a href="javascript:time_data('<?php echo $day?>');"><?php echo $day; ?></a>
            </td>
            <td class="aright dark" >
                <?php echo number_format($impressions[$j]);?>
            </td>
            <td class="aright dark" >
                <?php echo number_format($clicks[$j]);?>
            </td>
            <td class="aright dark" >
                <?php if($ctr_percent != ''){echo number_format($ctr_percent, 1);}else{echo "0.0";}?>
            </td>
            <td class="aright dark" >
                <?php if($revenue[$j] != ''){echo number_format(round($revenue[$j]));}else{echo "0";}?>
            </td>
            <td class="aright dark" >
                <?php if($ecpm_final != ''){echo number_format($ecpm_final, 3);}else{echo "0.000";}?>
            </td>
        </tr>
		<?php
                if ($cur_date == $next_date) {
                    $j = $j + 2;
                } else {
                    $j++;
                }
            }else{
		?>
        <tr>
		    <td class="dark" style="color: #0767A8;">
                <img src="assets/images/icon-date.gif" width="16" height="16" align="absmiddle" />
                &nbsp;<?php echo date('Y-m-d ', strtotime($start . $i . 'day'));?>
            </td>
            <td class="aright dark" >
                -
            </td>
            <td class="aright dark" >
                -
            </td>
            <td class="aright dark" >
                -
            </td>
            <td class="aright dark" >
                -
            </td>
            <td class="aright dark" >
                -
            </td>
       </tr>
    <?php
            }
        }
    ?>
        <tr>
            <td class="dark" style="color: #0767A8;"><?php echo $GLOBALS['strTotal']?></td>
            <td class="aright dark"><?php echo number_format($total_impressions);?></td>
            <td class="aright dark"><?php echo number_format($total_clicks);?></td>
            <td class="aright dark"> - </td>
            <td class="aright dark"><?php echo number_format(round($total_revenue));?></td>
            <td class="aright dark"> - </td>
        </tr>
        <tr>
            <td class="dark" style="color: #0767A8;"><?php echo $GLOBALS['strAverage']?></td>
            <td class="aright dark"><?php echo number_format($total_impressions/$count);?></td>
            <td class="aright dark"><?php echo number_format($total_clicks/$count);?></td>
            <td class="aright dark"><?php echo number_format(round($total_ctr/$count, 1), 1);?></td>
            <td class="aright dark"><?php echo number_format(round($total_revenue/$count));?></td>
            <td class="aright dark"><?php echo number_format(round($total_ecpm/$count, 3), 3);?></td>
        </tr>
    </table>
</div>
</div>
<?php
  // }
?>
<script src="assets/js/jquery-1.11.2.min.js"></script>
<script src="http://code.highcharts.com/highcharts.js"></script>
<script>
    /**
     * Grid-light theme for Highcharts JS
     * @author Torstein Honsi
     */

// Load the fonts
    Highcharts.createElement('link', {
        href: '//fonts.googleapis.com/css?family=Dosis:400,600',
        rel: 'stylesheet',
        type: 'text/css'
    }, null, document.getElementsByTagName('head')[0]);

    Highcharts.theme = {
        colors: ["#7cb5ec", "#f7a35c", "#90ee7e", "#7798BF", "#aaeeee", "#ff0066", "#eeaaee",
            "#55BF3B", "#DF5353", "#7798BF", "#aaeeee"],
        chart: {
            backgroundColor: null,
            style: {
                fontFamily: "Dosis, sans-serif"
            }
        },
        title: {
            style: {
                fontSize: '16px',
                fontWeight: 'bold',
                textTransform: 'uppercase'
            }
        },
        tooltip: {
            borderWidth: 0,
            backgroundColor: 'rgba(219,219,216,0.8)',
            shadow: false
        },
        legend: {
            itemStyle: {
                fontWeight: 'bold',
                fontSize: '13px'
            }
        },
        xAxis: {
            gridLineWidth: 1,
            labels: {
                style: {
                    fontSize: '12px'
                }
            }
        },
        yAxis: {
            minorTickInterval: 'auto',
            title: {
                style: {
                    textTransform: 'uppercase'
                }
            },
            labels: {
                style: {
                    fontSize: '12px'
                }
            }
        },
        plotOptions: {
            candlestick: {
                lineColor: '#404048'
            }
        },


        // General
        background2: '#F0F0EA'

    };

    // Apply the theme
    Highcharts.setOptions(Highcharts.theme);
</script>

<script>
    $(function () {
        $('#daily_chart').highcharts({
            title: {
                text: '',
                x: 20 //center
            },
            subtitle: {
                text: '',
                x: 20
            },
            xAxis: {
                categories: [
                    <?php
                        foreach($daily_chart_data as $row){
                    ?>
                    '<?php echo $row["day"]?>',
                    <?php
                        }
                    ?>
                ],
                title: {
                    text: "<?php echo $GLOBALS['strDate']?>"
                },
                labels: {
                    rotation: 90,
                    style: {
                        fontSize: '10px',
                        fontFamily: 'Verdana, sans-serif'
                    },
                    overflow: 'justify'
                }
            },
            yAxis: [{
                min: 0,
                labels: {
                    overflow: 'justify',
                    style: {
                        color: Highcharts.getOptions().colors[3],
                        fontSize: '12px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                },
                title: {
                    text: "<?php echo $GLOBALS['strImpressions']?>"
                },
                style: {
                    color: Highcharts.getOptions().colors[3]
                }
            },{
                min: 0,
                gridLineWidth: 0,
                labels: {
                    style: {
                        color: Highcharts.getOptions().colors[4],
                        fontSize: '12px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                },
                title: {
                    text: "<?php echo $GLOBALS['strClicks']?>"
                },opposite: true
            }],
            tooltip: {
                valueSuffix: '',
                shared: true

            },
            legend: {
                floating: true,
                layout: 'vertical',
                align: 'left',
                verticalAlign: 'top',
                x:100,
                y:0,
                borderWidth: 1,
                backgroundColor:'#FCFFC5'
            },
            series: [{
                name: "<?php echo $GLOBALS['strImpressions']?>",
                yAxis: 0,
                zIndex : 1,
                color: Highcharts.getOptions().colors[3],
                data: [
                    <?php
                        foreach($daily_chart_data as $row){
                    ?>
                    <?php echo $row['impressions']?>,
                    <?php
                        }
                    ?>
                ]},{
                name: "<?php echo $GLOBALS['strClicks']?>",
                type:'column',
                yAxis: 1,
                zIndex : 0,
                color: Highcharts.getOptions().colors[4],
                data: [
                    <?php
                        foreach($daily_chart_data as $row){
                    ?>
                    <?php echo $row['clicks']?>,
                    <?php
                        }
                    ?>
                ]
            }
            ]
        });
    });


    $(function () {
        $('#impressions_chart').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            title: {
                text: "<?php echo $GLOBALS['strImpressions']?>"
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        },
                        showInLegend: false
                    }
                }
            },
            series: [{
                type: 'pie',
                name: "<?php echo $GLOBALS['strImpressions']?>",
                data: [
                    <?php
                    foreach($chart_data as $row){
                        echo "['".$row['name']."', ".$row['impressions']."],";
                    }
                    ?>
                ]
            }]
        });
    });

    $(function () {
        $('#clicks_chart').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            title: {
                text: "<?php echo $GLOBALS['strClicks']?>"
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        },
                        showInLegend: false
                    }
                }
            },
            series: [{
                type: 'pie',
                name: "<?php echo $GLOBALS['strClicks']?>",
                data: [
                    <?php
                    foreach($chart_data as $row){
                        echo "['".$row['name']."', ".$row['clicks']."],";
                    }
                    ?>
                ]
            }]
        });
    });
</script>

<?php
/*-------------------------------------------------------*/
/* HTML framework                                        */
/*-------------------------------------------------------*/
phpAds_PageFooter();
?>

<!--일별차트 -->
<script type="text/javascript">
    //google.load("visualization", "1", {packages:["corechart"]});
    //google.setOnLoadCallback(drawVisualization);

    function drawVisualization() {
        // Some raw data (not necessarily accurate)
        var data = google.visualization.arrayToDataTable([
            ["<?php echo $GLOBALS['strDate']?>", "<?php echo $GLOBALS['strImpressions']?>", "<?php echo $GLOBALS['strClicks']?>"],
            <?php
                if(count($daily_chart_data) > 0){
                    foreach($daily_chart_data as $row){
                        echo "['".$row['day']."', ".$row['impressions'].", ".$row['clicks']."],";
                   }
                }else{
                    echo "['0', 0, 0]";
                }
            ?>
        ]);

        var options = {
            width: 800,
            chart: {
                title: '',
                subtitle: ''
            },
            series: {
                0: { axis: 'impressions' }, // Bind series 0 to an axis named 'distance'.
                1: { axis: 'clicks' } // Bind series 1 to an axis named 'brightness'.
            },
            axes: {
                y: {
                    impressions: {label: "<?php echo $GLOBALS['strImpressions']?>"}, // Left y-axis.
                    clicks: {side: 'right', label: "<?php echo $GLOBALS['strClicks']?>"} // Right y-axis.
                }
            }
        };

        var chart = new google.charts.Line(document.getElementById('daily_chart_'));
        chart.draw(data, options);
    }
</script>
<!--//일별차트 -->
<!--영역차트 -->
<script type="text/javascript">
    google.load("visualization", "1", {packages:["corechart"]});
    //google.setOnLoadCallback(drawChart_impressions);
    //google.setOnLoadCallback(drawChart_clicks);
    function drawChart_impressions() {

        var data = google.visualization.arrayToDataTable([
            ["<?php echo $GLOBALS['strAffiliates']?>", "<?php echo $GLOBALS['strImpressions']?>"],
        <?php
            if(count($chart_data) > 0){
                foreach($chart_data as $row){
                    echo "['".$row['name']."', ".$row['impressions']."],";
                }
            }else{
                echo "['0',0]";
            }
        ?>

        ]);

        var options = {
            title: "<?php echo $GLOBALS['strImpressions']?>"
        };

        var chart = new google.visualization.PieChart(document.getElementById('impressions_chart_'));

        chart.draw(data, options);
    }

    function drawChart_clicks() {

        var data = google.visualization.arrayToDataTable([
            ["<?php echo $GLOBALS['strAffiliates']?>", "<?php echo $GLOBALS['strClick']?>"],
            <?php
                if(count($chart_data) > 0){
                    foreach($chart_data as $row){
                        echo "['".$row['name']."', ".$row['clicks']."],";
                    }
                }else{
                    echo "['0',0]";
                }
            ?>

        ]);

        var options = {
            title: "<?php echo $GLOBALS['strClicks']?>"
        };

        var chart = new google.visualization.PieChart(document.getElementById('clicks_chart_'));

        chart.draw(data, options);
    }
</script>
<!--//영역차트 -->
<script type='text/javascript'>

    $( document ).ready(function() {
        $(".web").click(function(){
            aff = this.id;
            var str1 = 'zonesel';
            var zonesel = str1+aff;

            if($(".zonesel").is(":visible")){
                $(".zonesel").hide();
            }else{
                $(".zonesel").show();
            }
        })
    });

    Calendar.setup({
        inputField : 'period_start',
        ifFormat   : '%Y-%m-%d',
        button     : 'period_start_button',
        align      : 'Bl',
        weekNumbers: false,
        firstDay   : 1,
        electric   : false
    });

    Calendar.setup({
        inputField : 'period_end',
        ifFormat   : '%Y-%m-%d',
        button     : 'period_end_button',
        align      : 'Bl',
        weekNumbers: false,
        firstDay   : 1,
        electric   : false
    });

    Calendar.setup({
        inputField : 'period_start',
        ifFormat   : '%Y-%m-%d',
        button     : 'period_start',
        align      : 'Bl',
        weekNumbers: false,
        firstDay   : 1,
        electric   : false
    });

    Calendar.setup({
        inputField : 'period_end',
        ifFormat   : '%Y-%m-%d',
        button     : 'period_end',
        align      : 'Bl',
        weekNumbers: false,
        firstDay   : 1,
        electric   : false
    });

    //오늘
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();
    if(dd < 10) {
        dd='0'+dd
    }
    if(mm < 10) {
        mm='0'+mm
    }
    today_result = yyyy+'-'+mm+'-'+dd;
    //어제
    var yesterday = new Date(today);
    yesterday.setDate(today.getDate() - 1);
    var dd = yesterday.getDate();
    var mm = yesterday.getMonth()+1; //January is 0!
    var yyyy = yesterday.getFullYear();
    if(dd < 10) {
        dd='0'+dd
    }
    if(mm < 10) {
        mm='0'+mm
    }
    yesterday_result = yyyy+'-'+mm+'-'+dd;
    //7일전
    var last_7 = new Date(today);
    last_7.setDate(today.getDate() - 8);
    var dd = last_7.getDate();
    var mm = last_7.getMonth()+1; //January is 0!
    var yyyy = last_7.getFullYear();
    if(dd < 10) {
        dd='0'+dd
    }
    if(mm < 10) {
        mm='0'+mm
    }
    last_7_result = yyyy+'-'+mm+'-'+dd;
    //30일전
    var last_30 = new Date(today);
    last_30.setDate(today.getDate() - 31);
    var dd = last_30.getDate();
    var mm = last_30.getMonth()+1; //January is 0!
    var yyyy = last_30.getFullYear();
    if(dd < 10) {
        dd='0'+dd
    }
    if(mm < 10) {
        mm='0'+mm
    }
    last_30_result = yyyy+'-'+mm+'-'+dd;


    $("#period_preset").change(function(){
        if($(this).val() == "today"){
            $("#period_start").val(today_result);
            $("#period_end").val(today_result);
        }else if($(this).val() == "yesterday"){
            $("#period_start").val(yesterday_result);
            $("#period_end").val(yesterday_result);
        }else if($(this).val() == "last_7_days"){
            $("#period_start").val(last_7_result);
            $("#period_end").val(yesterday_result);
        }else if($(this).val() == "last_30_days"){
            $("#period_start").val(last_30_result);
            $("#period_end").val(yesterday_result);
        }else if($(this).val() == "this_month"){

        }else if($(this).val() == "last_month"){

        }else if($(this).val() == "all_stats"){
            $("#period_start").val("0000-00-00");
            $("#period_end").val(yesterday_result);
        }
    });
    function periodFormChange(bAutoSubmit){
        var o = document.getElementById('period_preset');
        var periodSelectName = o.options[o.selectedIndex].value;
        var specific = periodSelectName == 'specific';
        var advertiser = periodSelectName == 'advertiser';
        var periodTabIndex = 2;
        //document.getElementById('period_start').readOnly = !specific;
        //document.getElementById('period_start_button').disabled = !specific;
        //document.getElementById('period_end').readOnly = !specific;
        //document.getElementById('period_end_button').disabled = !specific;

        document.getElementById('period_start').style.backgroundColor = '#FFFFFF';
        document.getElementById('period_end').style.backgroundColor = '#FFFFFF';
        document.getElementById('period_start').tabIndex = periodTabIndex;
        document.getElementById('period_start_button').tabIndex = periodTabIndex + 1;
        document.getElementById('period_end').tabIndex = periodTabIndex + 2;
        document.getElementById('period_end_button').tabIndex = periodTabIndex + 3;
        document.getElementById('period_start_button').src = 'assets/images/icon-calendar.gif';
        document.getElementById('period_end_button').src = 'assets/images/icon-calendar.gif' ;

        //document.getElementById('period_start_button').readOnly = !specific;
        //document.getElementById('period_end_button').readOnly = !specific;
        //document.getElementById('period_start_button').style.cursor = specific ? 'auto' : 'default';
        //document.getElementById('period_end_button').style.cursor = specific ? 'auto' : 'default';
    }

    periodFormChange(0);

    function periodFormSubmit(){
        var form = document.getElementById('period_preset').form;
        if (checkDates(form)) {
          form.submit();
        }
        return false;
    }

    function checkDates(form){
        var startField = form.period_start;
        var endField = form.period_end;

        if (!startField.disabled && startField.value != '') {
            var start = Date.parseDate(startField.value, '%Y-%m-%d');
        }

        if (!startField.disabled && endField.value != '') {
            var end = Date.parseDate(endField.value, '%Y-%m-%d');
        }
        return true;
    }

</script>

<div id="mask" onclick="javascript:time_data_close();" style="position:absolute;z-index:9;background-color:#000;display:none;left:0;top:0;"></div>
<div id="layer" style="display:none;width:800px;height:1240px;;z-index:10;background-color:#fff"></div>
<script type="text/javascript">

    function time_data_close(){
        $('#mask').fadeOut(1000);
        $('#layer').hide();
    }

    function time_data(date) {
        var maskHeight = $(document).height();
        var maskWidth = $(window).width();

        $('#mask').css({'width':maskWidth, 'height':maskHeight});

        $('#mask').fadeIn(1000);
        $('#mask').fadeTo("slow", 0.8);

        var $layerPopupObj = $('#layer');
        var left = ( $(window).scrollLeft() + ($(window).width() - $layerPopupObj.width()) / 2 );
        var top = ( $(window).scrollTop() + ($(window).height() - $layerPopupObj.height()) / 2 );
        $layerPopupObj.css({'left':left,'top':top, 'position':'absolute'});

        $('#layer').show();
        var params = "affiliateid=<?php echo $_REQUEST['affiliateid']?>&clientid=<?php echo $_REQUEST['clientid']?>&status=<?php echo $_REQUEST['status']?>";
        $.ajax({
            type : "GET",
            data : params,
            url: 'report-time.php?type=site&date='+date,
            success: function(data) {
                $('#layer').html(data);
            }
        });
    }
</script>
<?php
    phpAds_PageFooter();
?>