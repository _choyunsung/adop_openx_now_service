var AWS = require('aws-sdk')
  , fs = require('fs')
  , async = require('async')
  , http = require('http')

AWS.config.loadFromPath('/var/www/manually_key.json');

var ec2 = new AWS.EC2({ region: 'ap-northeast-1' })
  , autoscaling = new AWS.AutoScaling({ region: 'ap-northeast-1' });

var privateKey = fs.readFileSync('/var/www/key/adop_Optima.pem');

function getInstanceIds(callback) {
  autoscaling.describeAutoScalingGroups({
    AutoScalingGroupNames: ['optima_instance_autoscaling_group'],
  }, function (err, data) {
    instanceIds = [];
    if (!err) {
      data.AutoScalingGroups[0].Instances.forEach(function (e) {
        instanceIds.push(e.InstanceId);
      });
    }
    callback(instanceIds);
  });
}

function getInstanceIpAddress(instanceId, callback) {
  ec2.describeInstances({
    InstanceIds: [ instanceId ]
  }, function (err, data) {
    if (err)
      console.log(err, err.stack);
    callback(data.Reservations[0].Instances[0].PrivateIpAddress);
  });
}

function deploy(hosts){
    var options = {
        host: hosts,
        path: '/manually_deploy.php'
    };

    callback = function(response) {
        var str = '';

        //another chunk of data has been recieved, so append it to `str`
        response.on('data', function (chunk) {
            str += chunk;
        });

        //the whole response has been recieved, so we just print it out here
        response.on('end', function () {
            console.log(str);
        });
    }

    http.request(options, callback).end();
}

getInstanceIds(function (instanceIds) {
  instanceIds.forEach(function (e) {
    getInstanceIpAddress(e, function (ipAddress) {
      console.log(ipAddress);
      deploy(ipAddress);
    });
  });
});
