<?php

/*
+---------------------------------------------------------------------------+
| Revive Adserver                                                           |
| http://www.revive-adserver.com                                            |
|                                                                           |
| Copyright: See the COPYRIGHT.txt file.                                    |
| License: GPLv2 or later, see the LICENSE.txt file.                        |
+---------------------------------------------------------------------------+
*/

/**
 * @package    Plugin
 * @subpackage openxDeliveryLog
 */

MAX_Dal_Delivery_Include();

function Plugin_deliveryLog_oxLogClick_logClick_Delivery_logClick($adId = 0, $zoneId = 0, $okToLog = true)
{


	$tableprefix=$GLOBALS['_MAX']['CONF']['table']['prefix'];

	$aConf = $GLOBALS['_MAX']['CONF'];

	if (!$okToLog) { return false; }

    	$aData = $GLOBALS['_MAX']['deliveryData'];
	
	if($GLOBALS['_MAX']['CONF']['plugins']['RTB']==1 || $GLOBALS['_MAX']['CONF']['plugins']['Mobilesite']==1 || $GLOBALS['_MAX']['CONF']['plugins']['MobileAPPAndroid']==1 || $GLOBALS['_MAX']['CONF']['plugins']['MobileAPPiOS']==1)
	{
		$publishershare=djax_publishershare($aData['zone_id']);

		$select_revenue_type = OA_Dal_Delivery_query("SELECT *
			FROM ".OX_escapeIdentifier($aConf['table']['prefix'].$aConf['table']['campaigns'])."  AS c,
			     ".OX_escapeIdentifier($aConf['table']['prefix'].$aConf['table']['banners'])."  AS b WHERE b.bannerid='$adId' AND 			      c.campaignid=b.campaignid");
			     
		$fetch_revenue_type = OA_Dal_Delivery_fetchAssoc($select_revenue_type); 		     

		$temp_variable=explode('__',$_GET['oadest']); 

		$temp=explode('=',$temp_variable[1]);

		if($temp[0]=='request_id')
		{
		$request_id=$temp[1];
		}
		else
		{
		$request_id =$_REQUEST['request_id'];
		}

		$is_duplicatclick_request=OA_Dal_Delivery_fetchAssoc(OA_Dal_Delivery_query("select * from {$tableprefix}data_bkt_c where dj_request_id='$request_id' and interval_start='".$aData['interval_start']."'"));	

		if($GLOBALS['_MAX']['CONF']['plugins']['RTB']==1)
		{
			if($fetch_revenue_type['is_rtb_campaign']==1)
			{

				 $win_bid=0;
			}
			else
			{
				if($fetch_revenue_type['revenue_type']=='2')	
				{	
						if(!empty($is_duplicatclick_request['dj_win_bid']))
						{
								
							$costf=$fetch_revenue_type['revenue']*($publishershare/100);		
								 
							$win_bid=$is_duplicatclick_request['dj_win_bid']+$fetch_revenue_type['revenue'];

							$cost = $is_duplicatclick_request['cost'] + $costf;

							OA_Dal_Delivery_query("update {$tableprefix}data_bkt_c set dj_win_bid='$win_bid',cost='$cost' where dj_request_id='$request_id' and  interval_start='".$aData['interval_start']."'");
							}
						else
						{
					
							$win_bid=$fetch_revenue_type['revenue'];
				
							$cost=$win_bid*($publishershare/100);		

						}
		
				}	
				else
				{
						$cost=0;
						$win_bid =0;
				}
			}
		}
		else
		{
				if($fetch_revenue_type['revenue_type']=='2')	
				{	
						if(!empty($is_duplicatclick_request['dj_win_bid']))
						{
							
							$costf=$fetch_revenue_type['revenue']*($publishershare/100);		
								 
							$win_bid=$is_duplicatclick_request['dj_win_bid']+$fetch_revenue_type['revenue'];

							$cost = $is_duplicatclick_request['cost'] + $costf;

							OA_Dal_Delivery_query("update {$tableprefix}data_bkt_c set dj_win_bid='$win_bid',cost='$cost' where dj_request_id='$request_id' and  interval_start='".$aData['interval_start']."'");
							}
						else
						{
							$win_bid=$fetch_revenue_type['revenue'];
				
							$cost=$win_bid*($publishershare/100);		

						}
		
				}	
				else
				{
						$cost=0;
						$win_bid =0;
				}
			}


		}
	
	if($GLOBALS['_MAX']['CONF']['plugins']['RTB']==1 || $GLOBALS['_MAX']['CONF']['plugins']['Mobilesite']==1 || $GLOBALS['_MAX']['CONF']['plugins']['MobileAPPAndroid']==1 || $GLOBALS['_MAX']['CONF']['plugins']['MobileAPPiOS']==1)
	{
	    	$aQuery = array(
		'interval_start' => $aData['interval_start'],
		'creative_id'    => $aData['creative_id'],
		'zone_id'        => $aData['zone_id'],
		'dj_request_id'  => $request_id,
		'dj_win_bid'	 => $win_bid,
		'cost'	 => $cost
	   	 );
	}
	else
	{
	$aQuery = array(
        'interval_start' => $aData['interval_start'],
        'creative_id'    => $aData['creative_id'],
        'zone_id'        => $aData['zone_id']
    	);
	}

    return OX_bucket_updateTable('data_bkt_c', $aQuery);
}


function djax_publishershare($zoneid)
{

	$tableprefix=$GLOBALS['_MAX']['CONF']['table']['prefix'];
	$publisher_share=0;

	$aData = $GLOBALS['_MAX']['deliveryData'];

	$select = OA_Dal_Delivery_query("select a.publishershare,a.affiliateid from 
		{$tableprefix}affiliates as a ,{$tableprefix}zones as z where z.zoneid='".$zoneid."' AND   
		a.affiliateid=z.affiliateid ");

	$row = OA_Dal_Delivery_fetchAssoc($select);

	$publishershare = $row['publishershare'];

	if(empty($row['publishershare']))
	{
					
			

				$defaultShare = OA_Dal_Delivery_query("select * from {$tableprefix}dj_admin_configuration") or die(mysql_error());

				if(OA_Dal_Delivery_numRows($defaultShare) > 0) 
				{

						$rowShare = OA_Dal_Delivery_fetchAssoc($defaultShare);

						$publishershare = $rowShare['pubshare'];	
						
						
				}
		
	}	

	return $publishershare;
}

?>
