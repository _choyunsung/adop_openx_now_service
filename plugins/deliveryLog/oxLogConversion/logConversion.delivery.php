<?php

/*
+---------------------------------------------------------------------------+
| OpenX v${RELEASE_MAJOR_MINOR}                                                                |
| =======${RELEASE_MAJOR_MINOR_DOUBLE_UNDERLINE}                                                                |
|                                                                           |
| Copyright (c) 2003-2009 OpenX Limited                                     |
| For contact details, see: http://www.openx.org/                           |
|                                                                           |
| This program is free software; you can redistribute it and/or modify      |
| it under the terms of the GNU General Public License as published by      |
| the Free Software Foundation; either version 2 of the License, or         |
| (at your option) any later version.                                       |
|                                                                           |
| This program is distributed in the hope that it will be useful,           |
| but WITHOUT ANY WARRANTY; without even the implied warranty of            |
| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
| GNU General Public License for more details.                              |
|                                                                           |
| You should have received a copy of the GNU General Public License         |
| along with this program; if not, write to the Free Software               |
| Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA |
+---------------------------------------------------------------------------+
$Id: logConversion.delivery.php 39415 2009-07-07 13:19:13Z matteo.beccati $
*/

/**
 * @package    Plugin
 * @subpackage openxDeliveryLog
 */

MAX_Dal_Delivery_Include();

/**
 * A function to log conversions.
 *
 * @param integer $trackerId The ID of the tracker for which the conversion is to be logged.
 * @param array $serverRawIp The "raw IP address" value to use for the conversion.
 * @param array $aConversion An array of the conversion details, as returned from the
 *                           MAX_trackerCheckForValidAction() function.
 * @return array An array...
 */
function Plugin_deliveryLog_oxLogConversion_logConversion_Delivery_logConversion($trackerId, $serverRawIp, $aConversion, $okToLog = true)
{

	$tableprefix=$GLOBALS['_MAX']['CONF']['table']['prefix'];

    	if (!$okToLog) { return false; }
    	// Initiate the connection to the database (before using mysql_real_escape_string)
 	OA_Dal_Delivery_connect('rawDatabase');

    	$table = $GLOBALS['_MAX']['CONF']['table']['prefix'] . 'data_bkt_a';

	    if (empty($GLOBALS['_MAX']['NOW'])) {
		$GLOBALS['_MAX']['NOW'] = time();
	    }
	    $time = $GLOBALS['_MAX']['NOW'];

	$aData = $GLOBALS['_MAX']['deliveryData'];

	if($GLOBALS['_MAX']['CONF']['plugins']['RTB']==1 || $GLOBALS['_MAX']['CONF']['plugins']['Mobilesite']==1 || $GLOBALS['_MAX']['CONF']['plugins']['MobileAPPAndroid']==1 || $GLOBALS['_MAX']['CONF']['plugins']['MobileAPPiOS']==1)
	{
	    	$tracker_parameter=$aConversion['cid'].'_'. $aConversion['zid'];

	    	$aConf = $GLOBALS['_MAX']['CONF'];

		if(!empty($_REQUEST['transaction_id']))
		{
			$query_tranaction="select * from {$aConf['table']['prefix']}dj_s2s_track where transaction_id='".$_REQUEST['transaction_id']."'";

			$res = OA_Dal_Delivery_query($query_tranaction);

			$results =OA_Dal_Delivery_fetchAssoc($res);
	
			$request_id=$results['request_id'];

		}
		else
		{
	    		$request_id=(!empty($_REQUEST['request_id']))?$_REQUEST['request_id']:$_COOKIE['OXLCR'][$tracker_parameter];
		}

		if($GLOBALS['_MAX']['CONF']['plugins']['RTB']==1)
		{
		    	$select_revenue_type = OA_Dal_Delivery_query("SELECT *
				FROM ".OX_escapeIdentifier($aConf['table']['prefix'].$aConf['table']['campaigns'])."  AS c,
				     ".OX_escapeIdentifier($aConf['table']['prefix'].$aConf['table']['banners'])."  AS b WHERE b.bannerid='".$aConversion['cid']."' AND 			      c.campaignid=b.campaignid");
				     
		   	$fetch_revenue_type = OA_Dal_Delivery_fetchAssoc($select_revenue_type); 

		    	if($fetch_revenue_type['is_rtb_campaign']==1)
			{	
				    $win_bid=0;
			}
			else
			{
				    if($fetch_revenue_type['revenue_type']==3)
				    {

					$win_bid= $fetch_revenue_type['revenue'];

				    }
			}

		}
		else if($fetch_revenue_type['revenue_type']==3)
		{

				$win_bid= $fetch_revenue_type['revenue'];

		}

		$select = OA_Dal_Delivery_query("select a.publishershare,a.affiliateid from 
			{$aConf['table']['prefix']}affiliates as a ,{$aConf['table']['prefix']}zones as z where z.zoneid='".$aConversion['zid']."' AND   
			a.affiliateid=z.affiliateid ");

		$row = OA_Dal_Delivery_fetchAssoc($select);

		$publishershare = $row['publishershare'];

		if(empty($row['publishershare']))
		{						
					
						$defaultShare = OA_Dal_Delivery_query("select * from {$aConf['table']['prefix']}dj_admin_configuration");

						if(OA_Dal_Delivery_numRows($defaultShare) > 0)
						{

							$rowShare = OA_Dal_Delivery_fetchAssoc($defaultShare);

							$publishershare = $rowShare['pubshare'];	
						
						
						}
		}				
				
		$cost=$win_bid*($publishershare/100);	
	

		if(!empty($tracker_parameter))
		{

			unset($_COOKIE['OXLCR'][$tracker_parameter]);

		}

		$imp_track_cookie=$_COOKIE['OXLIA'][$aConversion['cid']];

		if(!empty($imp_track_cookie))
		{
			$temp_impdata=explode('-',$imp_track_cookie);

			if($temp_impdata[1]==$aConversion['zid'])
			{
				unset($_COOKIE['OXLIA'][$aConversion['cid']]);
			}

		}

		$click_track_cookie=$_COOKIE['OXLCA'][$aConversion['cid']];

		if(!empty($click_track_cookie))
		{
			$temp_clickdata=explode('-',$click_track_cookie);

			if($temp_clickdata[1]==$aConversion['zid'])
			{
				unset($_COOKIE['OXLCA'][$aConversion['cid']]);
			}

		}


		$is_duplicatclick_request=OA_Dal_Delivery_fetchAssoc(OA_Dal_Delivery_query("select * from {$tableprefix}data_bkt_a where dj_request_id='$request_id'"));
	


		if(empty($is_duplicatclick_request['dj_request_id']))
		{

		    $aValues = array(
			'server_ip'        => $serverRawIp,
			'tracker_id'       => $trackerId,
			'date_time'        => gmdate('Y-m-d H:i:s', $time),
			'action_date_time' => gmdate('Y-m-d H:i:s', $aConversion['dt']),
			'creative_id'      => $aConversion['cid'],
			'zone_id'          => $aConversion['zid'],
			'dj_request_id'	   => $request_id,
			'dj_win_bid'	   => $win_bid,
			'ip_address'       => $_SERVER['REMOTE_ADDR'],
			'action'           => $aConversion['action_type'],
			'window'           => $aConversion['window'],
			'status'           => $aConversion['status'],
			'cost'	 => $cost
		    );

		    // Need to also escape identifier as "window" is reserved since PgSQL 8.4
		    $aFields = array_map('OX_escapeIdentifier', array_keys($aValues));
		    $aValues = array_map('OX_escapeString', $aValues);

		    $query = "
			INSERT INTO
			    {$table}
			    (" . implode(', ', $aFields) . ")
			VALUES
			    ('" . implode("', '", $aValues) . "')
		    ";
		    $result = OA_Dal_Delivery_query($query, 'rawDatabase');
		    if (!$result) {
			return false;
		    }
		    $aResult = array(
			'server_conv_id' => OA_Dal_Delivery_insertId('rawDatabase', $table, 'server_conv_id'),
			'server_raw_ip' => $serverRawIp
		    );
		    return $aResult;

		}
	}
	else
	{
		    $aValues = array(
			'server_ip'        => $serverRawIp,
			'tracker_id'       => $trackerId,
			'date_time'        => gmdate('Y-m-d H:i:s', $time),
			'action_date_time' => gmdate('Y-m-d H:i:s', $aConversion['dt']),
			'creative_id'      => $aConversion['cid'],
			'zone_id'          => $aConversion['zid'],
			'ip_address'       => $_SERVER['REMOTE_ADDR'],
			'action'           => $aConversion['action_type'],
			'window'           => $aConversion['window'],
			'status'           => $aConversion['status']
		    );

		    // Need to also escape identifier as "window" is reserved since PgSQL 8.4
		    $aFields = array_map('OX_escapeIdentifier', array_keys($aValues));
		    $aValues = array_map('OX_escapeString', $aValues);

		    $query = "
			INSERT INTO
			    {$table}
			    (" . implode(', ', $aFields) . ")
			VALUES
			    ('" . implode("', '", $aValues) . "')
		    ";
		    $result = OA_Dal_Delivery_query($query, 'rawDatabase');
		    if (!$result) {
			return false;
		    }
		    $aResult = array(
			'server_conv_id' => OA_Dal_Delivery_insertId('rawDatabase', $table, 'server_conv_id'),
			'server_raw_ip' => $serverRawIp
		    );
		    return $aResult;


	}

}

?>

