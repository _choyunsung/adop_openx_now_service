<?php

/*
+---------------------------------------------------------------------------+
| Revive Adserver                                                           |
| http://www.revive-adserver.com                                            |
|                                                                           |
| Copyright: See the COPYRIGHT.txt file.                                    |
| License: GPLv2 or later, see the LICENSE.txt file.                        |
+---------------------------------------------------------------------------+
*/

/**
 * @package    Plugin
 * @subpackage openxDeliveryLog
 */

//DAC 042

	function Plugin_deliveryLog_OxLogImpression_LogImpression_Delivery_logImpression($adId = 0, $zoneId = 0, $okToLog = true)
	{

		$aConf = $GLOBALS['_MAX']['CONF'];

		$aData = $GLOBALS['_MAX']['deliveryData'];

		if($GLOBALS['_MAX']['CONF']['plugins']['RTB']==1 || $GLOBALS['_MAX']['CONF']['plugins']['Mobilesite']==1 || $GLOBALS['_MAX']['CONF']['plugins']['MobileAPPAndroid']==1 || $GLOBALS['_MAX']['CONF']['plugins']['MobileAPPiOS']==1)
		{

		
		 $select_revenue_type = OA_Dal_Delivery_query("SELECT *
			FROM ".OX_escapeIdentifier($aConf['table']['prefix'].$aConf['table']['campaigns'])."  AS c,
			     ".OX_escapeIdentifier($aConf['table']['prefix'].$aConf['table']['banners'])."  AS b WHERE b.bannerid='$adId' AND 			      c.campaignid=b.campaignid");
			     
		$fetch_revenue_type = OA_Dal_Delivery_fetchAssoc($select_revenue_type); 
		
		if($GLOBALS['_MAX']['CONF']['plugins']['RTB']==1)
		{
			if($fetch_revenue_type['is_rtb_campaign']==1)
			{

			$djax_auction=OA_Dal_Delivery_fetchAssoc(OA_Dal_Delivery_query("SELECT auction_type FROM {$aConf['table']['prefix']}dj_ad_exchange  where exchange_id='".$fetch_revenue_type['adx_id']."'"));

					$djax_data=explode('|',$_REQUEST['highest_bids']);

					if($djax_auction['auction_type']==1)
					{
					
						$win_bid =$djax_data[0];
					}
					else
					{
						if(!empty($djax_data[1])){ $temp_data=$djax_data[1]+0.01;}else{$temp_data=$djax_data[0];}
						$win_bid =$temp_data;
					}

					$win_bid=$win_bid/1000;

			}
			else
			{
					if($fetch_revenue_type['revenue_type']=='1')	
					{	
						$win_bid = $fetch_revenue_type['revenue']/1000;
					}
			}
		}
		else
		{
				if($fetch_revenue_type['revenue_type']=='1')	
				{	
					$win_bid = $fetch_revenue_type['revenue']/1000;
				}
		}
				

		if(!empty($_REQUEST['request_id']))
		{

			$request_id = $_REQUEST['request_id'];
		}
		else if(!empty($_SESSION))
		{

			$request_id = $_SESSION['request_id'];
		}


		$select = OA_Dal_Delivery_query("select a.publishershare,a.affiliateid from 
		{$aConf['table']['prefix']}affiliates as a ,{$aConf['table']['prefix']}zones as z where z.zoneid='".$aData['zone_id']."' AND   
		a.affiliateid=z.affiliateid ") or die(mysql_error());

		$row = OA_Dal_Delivery_fetchAssoc($select);

		$publishershare = $row['publishershare'];

			if(empty($row['publishershare'])) 
			{
	
												
				
						$defaultShare = OA_Dal_Delivery_query("select * from {$aConf['table']['prefix']}dj_admin_configuration");

								if(OA_Dal_Delivery_numRows($defaultShare) > 0)
								{

									$rowShare =OA_Dal_Delivery_fetchAssoc($defaultShare);

									$publishershare = $rowShare['pubshare'];	
						
								}


						


			}
				
			$cost=$win_bid*($publishershare/100);	


	    	if (!$okToLog) { return false; }
	    	$aData = $GLOBALS['_MAX']['deliveryData'];
	    	$aQuery = array(
		'interval_start' => $aData['interval_start'],
		'creative_id'    => $aData['creative_id'],
		'zone_id'        => $aData['zone_id'],
		'dj_request_id'  => $request_id,
		'dj_win_bid'	 => $win_bid,
		'cost'	 => $cost
		
	    	);
    	}
	else
	{

		    $aQuery = array(
			'interval_start' => $aData['interval_start'],
			'creative_id'    => $aData['creative_id'],
			'zone_id'        => $aData['zone_id']
		    );

	}

    		return OX_bucket_updateTable('data_bkt_m', $aQuery);
	}

