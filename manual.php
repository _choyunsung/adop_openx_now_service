<?php

function getUserIP()
{
    $client  = @$_SERVER['HTTP_CLIENT_IP'];
    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote  = $_SERVER['REMOTE_ADDR'];

    if(filter_var($client, FILTER_VALIDATE_IP))
    {
        $ip = $client;
    }
    elseif(filter_var($forward, FILTER_VALIDATE_IP))
    {
        $ip = $forward;
    }
    else
    {
        $ip = $remote;
    }

    return $ip;
}
//2016-01-13 리사 요청으로 아이피 체크 해제
//$user_ip = getUserIP();
//
//if($user_ip!="112.221.85.219"){
//    echo "{\"remoteip\":\"ERROR\"}";
//    die;
//}

$output = shell_exec('/var/www/manually_deploy.sh');

echo '<script type="text/javascript">alert("수동 라이브 완료!!");history.go(-1);</script>';