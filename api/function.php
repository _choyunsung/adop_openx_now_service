<?php 
    function MsgPrint($command="", $status="", $msg="", $data="") {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
    
        if($data)
            echo '{"command":"'.$command.'","status":'.$status.',"ads":'.$data.'}';
        else
            echo '{"command":"'.$command.'","status":'.$status.',"msg":"'.$msg.'"}';
    
        exit;
    }
    
    function deviceCheck() {
        @$user_agent = @$_SERVER["HTTP_USER_AGENT"];
        $deviceType		= "";
    
        if (stristr($user_agent, "Android"))
            $deviceType = "android";
        else if (stristr($user_agent, "iPhone"))
            $deviceType = "iphone";
        else if(stristr($user_agent, "iPad"))
            $deviceType = "ipad";
        else if(stristr($user_agent, "iPod"))
            $deviceType = "ipod";
    
        return $deviceType;
    }
    
    function zonid_ad($zone_type, $clientname, $adult, $affiliateid, $campaignid) {
        ////////////////////////////////[zoneid Insert start]
        $doZones = OA_Dal::factoryDO('zones');
        $doZones->affiliateid = $affiliateid;
        $doZones->zonename = $clientname."_".$adult;
        $doZones->zonetype = phpAds_ZoneCampaign;
        $doZones->description = "";
        $doZones->comments = "";
        $doZones->delivery = "";
        $doZones->oac_category_id  = "";

        if($zone_type == "320"){
            $doZones->width = "320";
            $doZones->height = "50";
        } else {
            $doZones->width = "768";
            $doZones->height = "1024";
        }
        
        // The following fields are NOT NULL but do not get values set in the form.
        // Should these fields be changed to NULL in the schema or should they have a default value?
        $doZones->category = '';
        $doZones->ad_selection = '';
        $doZones->chain = '';
        $doZones->prepend = '';
        $doZones->append = '';
        
        $doZones->show_capped_no_cookie = 0;
     
        $zoneId= $doZones->insert();
        ////////////////////////////////[zoneid Insert end]
    
        
        ////////////////////////////////[zoneid Banner Insert start]
        $aLinkedPlacements = Admin_DA::getPlacementZones(array('zone_id' => $zoneId), false, 'placement_id');
        
        if (!isset($aLinkedPlacements[$campaignid])) {
            Admin_DA::addPlacementZone(array('zone_id' => $zoneId, 'placement_id' => $campaignid));
        }
        
        MAX_addLinkedAdsToZone($zoneId, $campaignid);
        ////////////////////////////////[zoneid Banner Insert end]
    
        return $zoneId;
    }