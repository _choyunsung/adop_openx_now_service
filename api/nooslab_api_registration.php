<?php 
    ini_set('display_errors', 'on');
    
    require_once 'function.php';
    require_once '../init.php';

    // 광고주 ID
    $agencyid = @$_REQUEST['clientid'];
    if(!$agencyid) MsgPrint('error','-9999','clientid null');
    
    // ID
    $campaignid = @$_REQUEST['campaignid'];
    if(!$campaignid) MsgPrint('error','-9999','campaignid null');
    
    // Name
    $clientname = @$_REQUEST['clientname'];
    if(!$clientname) MsgPrint('error','-9999','clientname null');

    // Website
    $website = @$_REQUEST['website'];
    if(!$website) MsgPrint('error','-9999','website null');

    // Email
    $email = @$_REQUEST['email'];
    if(!$email) MsgPrint('error','-9999','email null');

    // contact
    $contact = @$_REQUEST['contact'];
    if(!$contact) MsgPrint('error','-9999','contact null');
    
    // Adult
    $adult = (@$_REQUEST['adult']?@$_REQUEST['adult']:"n");
    
//     if( (int)$agencyid != 74 ) {
//         MsgPrint('error','-9999','Not Nooslab');
//     }

    // Required files
    require_once MAX_PATH . '/lib/OA/Dal.php';
    require_once MAX_PATH . '/www/admin/lib-zones.inc.php';
    require_once MAX_PATH . '/lib/OA/Dll/Publisher.php';

   
    // MySQL 콘넥트 엔진 인클루드
    $conf = $GLOBALS ['_MAX'] ['CONF'];
    if (isset ( $conf ['origin'] ['type'] ) && is_readable ( MAX_PATH . '/lib/OA/Dal/Delivery/' . strtolower ( $conf ['origin'] ['type'] ) . '.php' )) {
        require (MAX_PATH . '/lib/OA/Dal/Delivery/' . strtolower ( $conf ['origin'] ['type'] ) . '.php');
    } else {
        require (MAX_PATH . '/lib/OA/Dal/Delivery/' . strtolower ( $conf ['database'] ['type'] ) . '.php');
    }

    ////////////////////////////////[website Insert end]
    $query = "SELECT *  FROM " . OX_escapeIdentifier ( $conf ['table'] ['prefix'] . $conf ['table'] ['affiliates'] ) . " where website = '" . $website . "'";
    
    $res = OA_Dal_Delivery_query ( $query );
    $websiteInfo = OA_Dal_Delivery_fetchAssoc ( $res );
    if (!empty ( $websiteInfo )) {
        MsgPrint('error','-9999','website duplication');
    }
    
    $publisherData["agencyid"]      = $agencyid;
    $publisherData["contact"]       = $contact;
    $publisherData["email"]         = $email;
    $publisherData["name"]          = $clientname;
    $publisherData["website"]       = $website;
    
    $doPublisher = OA_Dal::factoryDO('affiliates');
    $doPublisher->setFrom($publisherData);
    $affiliateid = $doPublisher->insert();
    ////////////////////////////////[website Insert end]
    
    // 띠배너
    $zoneId_320 = zonid_ad("320", $clientname, $adult, $affiliateid, $campaignid);

    // 전면배너
    $zoneId_768 = zonid_ad("768", $clientname, $adult, $affiliateid, $campaignid);

    $data[0]["zoneid"] = $zoneId_320;
    $data[0]["width"] = "320";
    $data[0]["height"] = "50";

    $data[1]["zoneid"] = $zoneId_768;
    $data[1]["width"] = "768";
    $data[1]["height"] = "1024";
    
    MsgPrint('ok','200','',json_encode($data));